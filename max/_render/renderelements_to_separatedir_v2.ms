 --This script will place your Render Element files in named subfolders according to where the main pass is stored and what the name of the Render Element is.
-- To run this script go to, customize -> Toolbars -> Category -> Super_oko -> drag REtoFolders on toolbar.
--Code by Blazej Kowalski 2012


macroScript REtoFolders
	category:"Super_oko"
	toolTip:""
(



if renderSceneDialog.isopen() == true then renderSceneDialog.close()
Rout = rendOutputFilename 
Rout_path_array = undefined
Rout_path = undefined
Rout_fileName = undefined
Rout_fileExt = undefined

if Rout != "" then
(
	Rout_path = getFilenamePath Rout
	Rout_fileName = getFilenameFile Rout
) else messagebox "Please set the render path first"
	
re = maxOps.GetCurRenderElementMgr()

NmElements = re.NumRenderElements()

if ((NmElements > 0) and (Rout != "")) then
(
for x = 0 to (NmElements - 1) do
(
	DirfileNameType = getFilenameType (re.GetRenderElementFilename x)
		
	path3 = re.GetRenderElement x
	ElementfileName2 = path3.elementname
	ElementfileName3 = Rout_fileName + "_" + ElementfileName2 + DirfileNameType
	
	makedir (Rout_path+ElementfileName2)
	re.SetRenderElementFilename x (Rout_path+ElementfileName2+"\\"+ElementfileName3)
	
)
messageBox "done!"
renderSceneDialog.open()
)
else 
	(
		
		if NmElements == 0 then (messageBox "Please add some render elemens first")
	)
)