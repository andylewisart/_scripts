selectedMaterials = #()
selectedObjs = #()
itr = 1
for i in (selection as array) do
(
    if (superclassof i == GeometryClass) and (i.material !=undefined)then append selectedObjs i 
    itr+=1
)

for i in selectedObjs do
(
    if (finditem selectedMaterials i.material) == 0 do append selectedMaterials i.material
)


for i in selectedMaterials do
(
    thisArray = for o in selectedObjs where o.material == i collect o
    select thisArray
    MaterialToShader = Material_to_Shader ()
    MaterialToShader.material = i
    Enviroment = Environment_Background_Switcher__mi ()
    Enviroment.environment_shader = MaterialToShader
    CutOutMat = Matte_Shadow_Reflection__mi  ao_on:false catch_indirect:false catch_shadows:false background_shader:Enviroment
    $.material = CutOutMat
)