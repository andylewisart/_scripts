frameRate

units.systemType

skipRenderedFrames

viewport.setType #view_camera

renderWidth
renderHeight

rendTimeType

rendNthFrame

renderers.current

backgroundColor

re = maxOps.GetCurRenderElementMgr()
re.GetElementsActive()
--use the below commands to verify that the render path is going to the right computer
rendOutputFilename
sysInfo.username
sysInfo.computername
--use below to check textures for network paths
for i = 1 to AssetManager.GetNumAssets() do
(
local AUIO = AssetManager.GetAssetByIndex i--get the AssetUser IObject
local AFile = AUIO.getfilename()--get the Asset's File Name
format"File:%\n"AFile--format the info
)
