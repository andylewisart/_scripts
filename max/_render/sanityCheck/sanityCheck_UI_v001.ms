--Sanity Checker written with maxscript and dotnet. 
--Written by Andy Lewis



Try(DestroyDialog sanityCheck)catch()




rollout sanityCheck "LoomFire Render Sanity Check" width:600 height:400--360
(
        
    dotNetControl lv "system.windows.forms.listView" pos:[0,0] width:600 height:400    
    local normalfont, boldfont
    local lvColumnsArr=#("Render Setting", "Current Value", "Suggested Fix")
    local arr, olditems=#()
	struct renderSettingCheck(settingValue, fixMessage, fixFunction)
    
    fn initLv theLv = (
        theLv.view=(dotNetClass "system.windows.forms.view").details
        theLv.FullRowSelect=true
        theLv.GridLines=true
        theLv.MultiSelect=true --multiple selections 
        theLv.CheckBoxes=false
        theLv.hideSelection=false
        theLv.IsAccessible=true
        theLv.LabelEdit=false --true
        )
        
    fn addColumns theLv columnsAr = (
        theLv.columns.add columnsAr[1] 150
        theLv.columns.add columnsAr[2] 150
		theLv.columns.add columnsAr[3] 295

        )
			
	fn getFrameRate = 
	(
		struct renderProperties(
			thisName = "Frame Rate",
			settingValue = (frameRate as string + " FPS"),
			fixMessage =  "Change FPS to 24",
			correct,
			fixedText = "24 FPS",
			fn fixThis = 
			(
				frameRate = 24
			)
		)
				
		if frameRate == 24 then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)

	)
	
	fn getSystemUnits = 
	(
		struct renderProperties(
			thisName = "System Units",
			settingValue = (units.systemType as string),
			fixMessage =  "Change to cm",
			correct,
			fixedText = "centimeters",
			fn fixThis = 
			(
				units.systemType = #centimeters
			)
		)
				
		if units.systemType == #centimeters then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
        
	fn getSkipRenderedFrames = 
	(
		
		struct renderProperties(
			thisName = "Skip Rendered Frames",
			settingValue = (skipRenderedFrames as string),
			fixMessage =  "Change to False",
			correct,
			fixedText = "False",
			fn fixThis = 
			(
				skipRenderedFrames = False
			)
		)
				
		if skipRenderedFrames == false then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
        
	fn getActiveViewport = 
	(
		struct renderProperties(
			thisName = "Active viewport",
			settingValue,
			fixMessage =  "Assign active viewport to camera",
			correct,
			fixedText,
			fn fixThis = 
			(
				
				viewport.setType #view_camera
				bob = getActiveCamera()
				fixedText = bob.name
			)
		)
		
		if viewport.getType()== #view_camera then 
		(
			bob = getActiveCamera()
			thisSetting = renderProperties correct: True settingValue: bob.name
		) 
		else
		(
			thisSetting = renderProperties correct: False settingValue:"Viewport not camera"
		)
	)
	
	fn getRenderDimensions = 
	(
		
		struct renderProperties(
			thisName = "Resolution",
			settingValue = ((renderWidth as string)+"x"+(renderHeight as string)),
			fixMessage =  "Change to 1080p",
			correct,
			fixedText = "1920x1080",
			fn fixThis = 
			(
				renderWidth = 1920
				renderHeight = 1080
			)
		)
				
		if renderWidth == 1920 and  renderHeight == 1080 then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
	
	fn getTimeOutputType = 
	(
		
		struct renderProperties(
			thisName = "Time Output Type",
			settingValue,
			fixMessage =  "Change to Active Time Segment",
			correct,
			fixedText = "Active Time Segment",
			fn fixThis = 
			(
				rendTimeType = 2
			)
		)
				
		if rendTimeType ==2 then 
		(
			thisSetting = renderProperties correct: True settingValue: "Active Time Segment"

		) 
		else
		(
			bob = ""
			if rendTimeType == 1 then bob = "Single" else if rendTimeType == 3 then bob = "Range" else if rendTimeType == 1 then bob = "Frames"
			thisSetting = renderProperties correct: False settingValue: bob
		)
	)
	
	fn getCurrentRenderer = 
	(

		struct renderProperties(
			thisName = "Current Renderer",
			settingValue,
			fixMessage =  "Change to Mental Ray",
			correct,
			fixedText = "Mental Ray",
			fn fixThis = 
			(
				renderers.current = mental_ray_renderer ()
			)
		)
				
		if renderers.current == "mental_ray_renderer:mental_ray_renderer" then 
		(
			thisSetting = renderProperties correct: True settingValue: "Mental Ray"

		) 
		else
		(
			thisSetting = renderProperties correct: False settingValue: (renderers.current as string)
		)
	)
	
	fn getBackgroundColor = 
	(
		backgroundColor
		struct renderProperties(
			thisName = "Background Color",
			settingValue = (backgroundColor as string),
			fixMessage =  "Change to black",
			correct,
			fixedText = "(color 0 0 0)",
			fn fixThis = 
			(
				backgroundColor = (color 0 0 0)
			)
		)
				
		if backgroundColor == (color 0 0 0) then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
	
	fn getRenderElementsActive = 
	(

		struct renderProperties(
			thisName = "Render Elements Active",
			settingValue = (	
				(re = maxOps.GetCurRenderElementMgr()
				re.GetElementsActive()) as string),
			fixMessage =  "Activate Render Elements",
			correct,
			fixedText = "Render Elements Active",
			fn fixThis = 
			(
				re.setElementsActive True
			)
		)
				
		if re.GetElementsActive() ==  true then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
	
	fn getOutputDestinations = 
	(

		
		struct renderProperties(
			thisName = "Render output path",
			settingValue = (rendOutputFilename as string),
			fixMessage =  "WARNING ONLY. Change manually.",
			correct,
			fixedText = rendOutputFilename,
			fn fixThis = 
			(
				messagebox "Please fix this manually. Feel free to give ideas on how to solve this in a way that satisfies both a state set and a non-state set solution."
			)
		)
		
		compNameRegex = sysInfo.computername
		outputPath = rendOutputFilename
		rgx = dotnetObject "System.Text.RegularExpressions.Regex" ""
		regexOpt = dotNetClass "System.Text.RegularExpressions.RegexOptions"
		found = rgx.IsMatch outputPath compNameRegex regexOpt.IgnoreCase
		if found then 
		(
			thisSetting = renderProperties correct: True

		) 
		else
		(
			thisSetting = renderProperties correct: False
		)
	)
	
	fn getTexturePaths = 
	(
		for i = 1 to AssetManager.GetNumAssets() do
		(
		local AUIO = AssetManager.GetAssetByIndex i--get the AssetUser IObject
		local AFile = AUIO.getfilename()--get the Asset's File Name
		format"File:%\n"AFile--format the info
		)
	)

		
    fn populateList theLv arr = 
	(
		rows=#()


		for t=1 to arr.count do 
		(
			print arr[t].thisNAme
-- 			print "88888888888"
			li=dotNetObject "System.Windows.Forms.ListViewItem" (arr[t].thisName)
			li.UseItemStyleForSubItems=true
			colAdd=240+(if (mod t 2)==0 then 10 else -10)
			
-- 			
			li.subitems.add (arr[t].settingValue)
			if arr[t].correct == False then
			(
				li.BackColor=li.backcolor.fromARGB colAdd (colAdd-100) (colAdd-100)
				li.font = boldFont
				li.subitems.add (arr[t].fixMessage)
			)
			else (li.BackColor=li.backcolor.fromARGB colAdd colAdd colAdd)
			
--             li.subitems.add arr[t][2]
			append rows li
		)

        theLv.items.addRange rows
-- 		
-- 		
-- 		
        theLv.Update()
	)
		

	
    on sanityCheck open do (
		renderSceneDialog.close()
		
        normalfont = dotNetObject "System.Drawing.Font" "Microsoft Sans Serif" 11 \
        (dotNetClass "System.Drawing.FontStyle").Regular (dotNetClass "System.Drawing.GraphicsUnit").Pixel
        boldfont = dotNetObject "System.Drawing.Font" "Microsoft Sans Serif" 11 \
        (dotNetClass "System.Drawing.FontStyle").Bold (dotNetClass "System.Drawing.GraphicsUnit").Pixel
        
        s=getFilenamePath (getSourceFileName()) --path to this script
        --print (maxFilePath+maxFileName) -- "" if no scene
            
        f=s+"Icons\\Add.gif"
        if doesFileExist f then btnAddValue.images=#(f, f, 1, 1, 1, 1, 1)
        
        f=s+"Icons\\Remove.gif"
        if doesFileExist f then btnRemoveValue.images=#(f, f, 1, 1, 1, 1, 1)
        
        arr=#()
		arr[1] = getFrameRate()
        arr[2]= getSystemUnits()
        arr[3]=getSkipRenderedFrames() 
        arr[4]=getActiveViewport()
        arr[5]= getRenderDimensions()
        arr[6]= getTimeOutputType()
        arr[7]=getCurrentRenderer() 
		arr[8]=getBackgroundColor()
        arr[9]= getRenderElementsActive()
        arr[10]=getOutputDestinations()
--         arr[11]=#("Texture paths", getTexturePaths() as string)

        
        initLv lv
        addColumns lv lvColumnsArr
        populateList lv arr
        )
        
    on sanityCheck resized arg do 
	(
		lv.height=arg.y
		lv.width=arg.x
	)
	
	on lv MouseClick arg do (
	
		theItem = lv.GetItemAt arg.x arg.y

		theSubItem = (theItem.GetSubItemAt arg.x arg.y)
-- 		print theSubItem.text
		
		
-- 		row = theItem.Index
-- 		col = theSubItem.name as integer
-- 		
-- 		if arg.button==arg.Button.Left then (
-- 			floaterSnippets.title="r: "+(row as string)+" c: "+(col as string)+" v: "+lv.items.item[row].subitems.item[col].text
-- 			)
-- 		if arg.button==arg.Button.Right then pushPrompt ("ArrItem: "+arr[row+1] as string)
	)

    on lv MouseDoubleClick arg do (
		theItem = lv.GetItemAt arg.x arg.y
		if arr[theItem.index+1].correct == False do
		(
			arr[theItem.index+1].fixThis()
			lv.items.item[theItem.index].subitems.item[1].text=arr[theItem.index+1].fixedText
			lv.items.item[theItem.index].subitems.item[2].text=""
			
			hit=(lv.HitTest (dotNetObject "System.Drawing.Point" arg.x arg.y))
			row=hit.item.Index
			colAdd=(if (mod row 2)==0 then 20 else -20)
			lv.items.item[row].BackColor=lv.items.item[row].BackColor.fromARGB (100+colAdd) (220+colAdd) (100+colAdd)
		)

        )
        
)
createDialog sanityCheck style: #(#style_titlebar, #style_border, #style_sysmenu, #style_resizing, #style_minimizebox) lockHeight: false \
    lockWidth: false 

