-- Built using PolyBoost tools and such
-- Written angrily by Andy Lewis
createIt = False
Try(DestroyDialog surfaceMapGenerator)catch()
if queryBox "This script will hold your max file and fetch it when it finishes. Don't do any additional work until you're done using this script. \n\nIn the unlikely event that the script malfunctions, close the dialog to fetch the file.\n\nContinue?" title: " Map Making Time!" then
(
    holdMaxFile()
    createIt = True
)

rollout surfaceMapGenerator  "Loomfire Surface Map Generator"
(

    local shellObjList = #()
    local tileList = #()
    local tileArray = #()
    local objArray = #()
    local dialogOpsMapName = ""
    local udim = 0
    local emptyObj 
    local debugPrint = False
    local Selobj
    local mapTypes = #()
    local successfulGeneration = False

    group "Bitmap options:"
    (   
        
        spinner siz1 "W:" range:[1,16000,512] type:#integer fieldwidth:36 across:2 align:#left
        spinner mch2 "Channel" range:[1,99,1] type:#integer fieldwidth:24 align:#right 
        spinner siz2 " H:" range:[1,16000,512] type:#integer fieldwidth:36 across:2 align:#left
        spinner seamSpin "Bleed" range:[0,100,2] type:#integer fieldwidth:24 align:#right 
        
    )
    group "Bitmap Type:" 
    (  
        checkbox cavityChx "Cavity"  across:2 
        button cavityTest "Preview"height: 15 align:#right
        spinner contspin "Contrast" range:[0,100,1] type:#integer fieldwidth:30 align:#center
        checkbox densityChx "Density" across:2 
        button densityTest "Preview"height: 15 align:#right
        checkbox dustChx "Dust" across:2 
        button dustTest "Preview"height: 15 align:#right
        checkbox sssChx "Subsurface" across:2 
        button sssTest "Preview"height: 15 align:#right
        spinner blurAmtSpin "Blur:" range:[0,10000,1] type:#integer fieldwidth:34 align:#center
        checkbox occChx "Occlusion" across:2
        button occTest "Preview"height: 15 align:#right
        checkbox select2BitmapChx "Select2Bitmap" across:2
        button select2BitmapTest "Preview"height: 15 align:#right
    )
    group""
    (   
        dropdownlist dropFileType items:#(".jpg", ".exr")
        edittext  fileLoc "Choose directory:" across:2 fieldWidth:100 align:#left labelontop: true text:@"S:\outfit\artists\Andy\_max\_testFiles\dumpingGrounds"
        button getFile "Dir..." height: 33 align:#right offset:[0,4]
        edittext mapName "Specify map name:" fieldWidth:145 labelontop: true text:"myAsset"
        checkbox overwriteFiles "Overwrite Existing Files"
        button generateMaps "Generate" width:155 
    )

    fn saveOpUIAccess = (

        local WindowHandle = DialogMonitorOPS.GetWindowHandle()
        local title =  (UIAccessor.GetWindowText WindowHandle)
        


        try
        (

            if findstring title dialogOpsMapName  != undefined do
            (
                mapSaveBtn =  (windows.getChildrenHWND WindowHandle)[3]
        
                UIAccessor.PressButton mapSaveBtn[1]
                UIAccessor.closedialog WindowHandle
            )
        )
        catch() 

        if title == "JPEG Image Control" do
        (
            UIAccessor.PressButtonByName WindowHandle "OK"
        )
        
        if title == "OpenEXR Configuration" do
        (
            UIAccessor.PressButtonByName WindowHandle "OK"
        )
        
        if title == " Save Image " do
        (
            lineEdit =  (windows.getChildrenHWND WindowHandle)[11]

            theFile = ((fileLoc.text as string)+@"\" + mapName.text + "_"+ dialogOpsMapName +"." + (udim as string) + dropFileType.selected)
            print theFile
            UIAccessor.SetWindowText lineEdit[1] theFile
            
            overrideRadio = (windows.getChildrenHWND WindowHandle)[28]
            automaticRadio = (windows.getChildrenHWND WindowHandle)[26]
            saveBtn = (windows.getChildrenHWND WindowHandle)[17]

            BM_SETCHECK = 0x00F1
            
            windows.sendMessage automaticRadio[1] BM_SETCHECK  0 0      
            windows.sendMessage overrideRadio[1] BM_SETCHECK  1 0

            UIAccessor.PressButton saveBtn[1]

            
        )

        if title == "File Exists" do
        (

            for i in (windows.getChildrenHWND WindowHandle) do
            (
                format "%\n" i
            )
            if overwriteFiles.checked == True do(

                UIAccessor.PressButtonByName WindowHandle "&Yes"

            )
        )
        if title == "Pizza" do
        (

            UIAccessor.PressButtonByName WindowHandle "OK"
            DialogMonitorOPS.unRegisterNotification id:#saveOpUIAccess
            DialogMonitorOPS.enabled = false
        )

        true
    )
        
    fn renderMaps = 
    (



        if dialogOpsMapName == "Cavity" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBCavityMap $ siz1.value siz2.value mch2.value contspin.value seamSpin.value
            messagebox "BLEHHHHHHHHH" title:"Pizza"

        )

        if dialogOpsMapName == "Density" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBDensityMap $ siz1.value siz2.value mch2.value seamSpin.value
            messagebox "BLEHHHHHHHHH" title:"Pizza"
        )

        if dialogOpsMapName == "Dust" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBDustMap $ siz1.value siz2.value mch2.value seamSpin.value
            messagebox "BLEHHHHHHHHH" title:"Pizza"
        )        
        if dialogOpsMapName == "SubSurface" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBSubSurfaceMap $ siz1.value siz2.value mch2.value blurAmtSpin.value seamSpin.value
            messagebox "BLEHHHHHHHHH" title:"Pizza"
        ) 
        if dialogOpsMapName == "Occlusion" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBOcclusionMap $ siz1.value siz2.value mch2.value blurAmtSpin.value seamSpin.value
            messagebox "BLEHHHHHHHHH" title:"Pizza"
        ) 
        if dialogOpsMapName == "Selection" do
        (   

            DialogMonitorOPS.enabled = true
            DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
            PolyBSelToBitmap $ siz1.value siz2.value mch2.value 0
            messagebox "BLEHHHHHHHHH" title:"Pizza"
        ) 
    )

    fn detachUVShells = 
    (

        tileArray = #()

        unwrapper = unwrap_uvw()
        modPanel.addModToSelection (unwrapper) ui:on

        local moduvw = selection[1].modifiers[#unwrap_uvw]
        local vcnt=moduvw.unwrap.numbervertices()
        moduvw.unwrap.selectVertices #{1..vcnt}
 
        -- count uv elements
        local vertelemarray=#()
        for i=1 to vcnt do vertelemarray[i]=0
        local elem=0
        modPanel.setCurrentObject moduvw
        subobjectlevel = 1
        ---------------------------------------------------
        for v=1 to vcnt do
        (

            if(vertelemarray[v]==0)then
            (   -- vertex v is not in array yet
                moduvw.unwrap.selectVertices #{v}
                moduvw.unwrap2.SelectElement()
                elem+=1
                elemverts=moduvw.unwrap.getselectedvertices()

                for i in elemverts do
                (   -- which vertices go to which element, vert[1]=vert[2]= ... =1 , vert[7]=vert[8]= ... =2 , ...
                    vertelemarray[i]=elem
                )
            )
            
        )
        
        subobjectlevel = 0
        --amount of elements

        -----------------------------------------------------
        local elemarray=#()
        for e=1 to elem do
        (

            local notFound = true
            for v=1 to vertelemarray.count while notFound do
            (
                if vertelemarray[v]==e then
                (   -- found vertex index 
                    elemarray[e]=v
                    notFound = false
                )
            )
        )
        --with this we get element from vertex

        ---------------------------------------------------
        modPanel.setCurrentObject moduvw
        subobjectlevel = 1


        

        for i=1 to elemarray.count do
        (   -- go through all elements

            moduvw.unwrap.selectVertices #{elemarray[i]}
            moduvw.unwrap2.SelectElement()

            moduvw.unwrap2.vertToFaceSelect() 
            posArray = moduvw.unwrap2.getSelCenter() 

            tilex = int(ceil posArray[1])
            tiley = int(ceil posArray[2])


            for x=1 to tilex do
            (
                if tileArray[x] == undefined do
                (
                    tileArray[x] = #(#{})
                )
                for y=1 to tiley do
                (
                    if tileArray[x][y] == undefined do
                    (
                        tileArray[x][y] = #{}
                    )
                )
            )
        
            append tileArray[tilex][tiley] elemarray[i]

            
        ) 
          
        for j=1 to tileArray.count do
        (
            for k=1 to tileArray[j].count do
            (   
                if tileArray[j][k].count != 0 do
                (   
                    select emptyObj

                    moduvw.unwrap.selectVertices tileArray[j][k]
                    moduvw.unwrap2.SelectElement()

                    moduvw.unwrap2.vertToFaceSelect() 

                    editPoly = Edit_Poly()
                    modPanel.addModToSelection (editPoly) ui:on
                    subobjectlevel = 4
                    $.modifiers[#Edit_Poly].ButtonOp #DetachFace 

                    deleteModifier $ 1
                    actionMan.executeAction 0 "40021" -- select all
                    numObj = selection as array

                    actionMan.executeAction 0 "40044"
                    select emptyObj

                    if numObj.count > 1 do
                    (
                        actionMan.executeAction 0 "40044"
                    )

                    udim = 1000 + ((j-1)+1+(k-1)*10)

                    for i in mapTypes do
                    (
                        dialogOpsMapName = i
                        setWaitCursor()
                        renderMaps()
                        setArrowCursor()
                    )


                    if numObj.count >1 do
                    (
                        max delete
                    )
                )
            )
        )

        select emptyObj
        deleteModifier $ 1
        
        IsolateSelection.ExitIsolateSelectionMode()

    )
        

    on getFile pressed do
    (
        try
        (
            thePath = getSavePath initialDir:(GetDir #maxroot)
            fileLoc.text = thePath
            generateMaps.enabled = true
        )
        catch()

    )

    on generateMaps pressed do
    (

        if ((dotnetclass "system.IO.directory").exists fileLoc.text) then
        (

            if cavityChx.checked == True do
            (   
                append mapTypes "Cavity"
            )
            if densityChx.checked == True do
            (    
                append mapTypes "Density"
            )
            if dustChx.checked == True do
            (   
                append mapTypes "Dust"
            )        
            if sssChx.checked == True do
            (                
                append mapTypes "SubSurface"
            ) 
            if occChx.checked == True do
            (   
                append mapTypes "Occlusion"
            ) 
            if select2BitmapChx.checked == True do
            (   
                append mapTypes "Selection"
            ) 


            if mapTypes.count == 0 then(
                messagebox "No maps selected"
            )
            else
            (

                Selobj = selection as array

                if Selobj.count == 0 then
                (
                    messagebox "No objects selected"
                )
                else
                (
                    carryOn = True



                    for x=1 to Selobj.count while carryOn do 
                    (
                        if classof Selobj[x].baseobject != Editable_Poly then(
                            messagebox "Objects base must be Editable Poly for this tool to work."
                            carryOn = False
                        )
                        else
                        ( 
                            if(polyOp.getMapSupport Selobj[x].baseobject mch2.value) == False do
                            (   
                                messagebox ("Map Channel doesn't exist at the base level on " + Selobj[x].name +"\n\nUnfortunately this tool will only read UVs at the base level.")beep:false
                                carryOn = False         
                            )
                        )


                    )

                    if carryOn do 
                    (
                        st = timestamp()

                        with redraw off
                        undo off
--                         suspendEditing()

                        macros.run "PolyTools" "EmptyObject"
                        emptyObj = $
                        for x=1 to Selobj.count do 
                        (
                            polyop.attach emptyObj Selobj[x]
                        )

                        select emptyObj
                        IsolateSelection.EnterIsolateSelectionMode()
                        detachUVShells() 
                        successfulGeneration = True
                        et = timestamp()
                        print ("time to compute: " )
                        print (et-st)
                        with redraw on
                        undo on

--                         resumeEditing()
                        messagebox "Map generation completed. Fetching..." title:"Rad"
                        fetchMaxFile quiet:true
                        DestroyDialog surfaceMapGenerator
                    )
                )
            )

        )
        else
        (
            messagebox "Please enter a valid DIRECTORY name."
        )
    )
    on cavityTest pressed do
    (
        if (selection.count == 1) and (classof $.baseobject == Editable_Poly) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                setWaitCursor()
                PolyBCavityMap $ siz1.value siz2.value mch2.value contspin.value seamSpin.value
                setArrowCursor()
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )
    on densityTest pressed do
    (
        if (selection.count == 1) and (classof $.baseobject == Editable_Poly) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                setWaitCursor()
                PolyBDensityMap $ siz1.value siz2.value mch2.value seamSpin.value
                setArrowCursor()
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )
    on dustTest pressed do
    (
        if (selection.count == 1) and (classof $.baseobject == Editable_Poly) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                setWaitCursor()
                PolyBDustMap $ siz1.value siz2.value mch2.value seamSpin.value
                setArrowCursor()
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )
    on sssTest pressed do
    (
        if (selection.count == 1) and (classof $.baseobject == Editable_Poly) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                setWaitCursor()
                PolyBSubSurfaceMap $ siz1.value siz2.value mch2.value blurAmtSpin.value seamSpin.value
                setArrowCursor()
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )
    on occTest pressed do
    (
        if (selection.count == 1) and (classof $.baseobject == Editable_Poly) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                setWaitCursor()
                PolyBOcclusionMap $ siz1.value siz2.value mch2.value blurAmtSpin.value seamSpin.value
                setArrowCursor()
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )
    on dlist selected state do (siz1.value = dlist.selected as integer ; siz2.value = dlist.selected as integer)
    on select2BitmapTest pressed do
    (
        if PolyBoost.validobjfunc() and (subobjectLevel != 0) do
        (
            if (polyOp.getMapSupport $.baseobject mch2.value) then
            (
                PolyBSelToBitmap $ siz1.value siz2.value mch2.value 0
            )
            else (messagebox "Map Channel doesn't exist" beep:false )
        )
    )

    on surfaceMapGenerator close do
    (   
        if successfulGeneration == False do messagebox "Fetching file..."
        try(fetchMaxFile quiet:true) catch()
    )
)

if createIt == True do 
(
    createDialog surfaceMapGenerator width: 175
)


