-------------------------------------------------------------------------------
-- AdjustShutter.ms
-- By Yane Markulev
-- v 1.2
-- Created On: 21/11/2011
-- Modified On: 26/05/2011
-- tested using Max 2011, VRay 2.00.02
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Description:
-- Adjusts the VRayPhysicalCamera Shutter when you change its F-number so that the overall render exposure and light stay unchanged. In this way you can strengthen Depth of Field and Motion Blur effects artificially.
-------------------------------------------------------------------------------

fn camera_filter o = 
	(
		ClassOf o == VRayPhysicalCamera
	)
renderSceneDialog.close()
rollout AdjustWidth "AdjustRenderWidth" width:150 
	
	(
		label space " "
		label title1 "ARTRAY Auto Adjust" 
		label title2 "Render Width"
		label space2 " "
		pickbutton choosecam "Pick Camera" align:#center width:140 height:20 filter:camera_filter
		spinner oldfspin "Old Focal:" type:#float range:[0.01,5000,0.01] enabled:false 
		spinner oldsspin "Old Width:" type:#float range:[0,900000000,0] enabled:false 
		spinner newfspin "New Width:" type:#integer range:[0,500000,0] enabled:false 
		spinner newsspin "New Focal:" type:#float range:[0,900000000,0] enabled:false 
		button btn2 "Apply" align:#center width:140 height:20 enabled:false
		
			
		on AdjustWidth open do
			(
				if classof $ == VRayPhysicalCamera then
					(
						choosecam.text = $.name
						oldfspin.value = $.focal_length
						oldsspin.value = renderWidth
						newfspin.value = renderWidth
						newfspin.enabled = true
					)
				else
					(
						camerapick = selectByName title:"Please select a single VRayPhysicalCamera :)" buttonText:"This one!" filter:camera_filter showHidden:true single:true
						if classof camerapick == VRayPhysicalCamera then
							(
								select camerapick
								choosecam.text = $.name
								oldfspin.value = $.focal_length
								oldsspin.value = renderWidth
								newfspin.value = renderWidth
								newfspin.enabled = true
							)
						else
							( 
								messagebox "Please pick a single VRayPhysicalCamera :)"
							)
					)
			)
			
		on choosecam picked obj do
			(
				select obj
				choosecam.text = obj.name
				oldfspin.value = $.focal_length
				oldsspin.value = renderWidth
				newfspin.value = renderWidth
				newfspin.enabled = true
			)

		on newfspin changed spinvalue do 
			(
				spinvalue = newfspin.value
				newsspin.value = (oldsspin.value/newfspin.value)*oldfspin.value
				btn2.enabled = true
			)
			
		on btn2 pressed do
			(
				if classof $ == VRayPhysicalCamera then
					(
						$.focal_length = newsspin.value
						renderWidth = newfspin.value
						destroydialog AdjustWidth
					)
				else
					(
						choosecam.text = "Pick"
						messagebox "Please pick again your VRayPhysicalCamera :)"
					)
			)
	)

createdialog AdjustWidth bgcolor:[50,50,50] fgcolor:[220,150,0] style:#(#style_toolwindow, #style_sysmenu) pos:[10,100]

---------------------------------------------------------------------