﻿
try(DestroyDialog infuseDraftRender)catch()
rollout infuseDraftRender "Infuse Draft Render"
(
	local vr = renderers.current
	local execDraft = #()
	local execRestore = #()
	local origRenderWidth = RenderWidth
	local origRenderHeight = RenderHeight
	local origOverrideMtl = vr.options_overrideMtl_on
	
	
	label lbl_Resolution "Resolution"
	radiobuttons resolution_radio labels:#("Full", "Half", "Quarter", "Custom %")
	spinner custResolution_spin "" enabled:false align:#center range:[1,100,33.3]
	group ""(
	checkbutton overrideMtl_chxBtn "Override Mtl"
	)
	button render_btn "Render" width:infuseDraftRender.width height: 35
	




	fn ChangeRes =
	(

		if resolution_radio.state == 2 then 
		(
			RenderHeight = origRenderHeight/2
			RenderWidth = origRenderWidth/2
		)
		else if resolution_radio.state == 3 then 
		(
			RenderHeight = origRenderHeight/4
			RenderWidth = origRenderWidth/4
		)
		else if resolution_radio.state == 4 then 
		(
			RenderHeight = origRenderHeight*(custResolution_spin.value/100)
			RenderWidth = origRenderWidth*(custResolution_spin.value/100)
		)
	)
	
	fn undoChangeRes = 
	(
		RenderHeight = origRenderHeight
		RenderWidth = origRenderWidth
	)
	
	fn enableOverride =
	(
		vr.options_overrideMtl_on =true
	)
	fn undoEnableOverride =
	(
		vr.options_overrideMtl_on =origOverrideMtl
	)
	on resolution_radio changed state do
	(
		if resolution_radio.state == 4 then custResolution_spin.enabled = true else custResolution_spin.enabled = false
	)
	on render_btn pressed do
	(
		clearlistener()
		if renderSceneDialog.isOpen() then renderSceneDialog.close()
	print "@@@1"
		if resolution_radio.state != 1 do append execDraft "ChangeRes()"
			print "@@@2"
		if overrideMtl_chxBtn.checked == true do append execDraft "enableOverride()"
			print "@@@3"
		for i in execDraft do
		(
			format "method: %\n" i
			execute ("infuseDraftRender."+i)
		)	
		max render last
		print "@@@5"
		for i in execDraft do
		(
			format "method: % \n" i
			execute ("infuseDraftRender.undo"+i)
		)
		execDraft = # ()
		
	)
)
createdialog infuseDraftRender


