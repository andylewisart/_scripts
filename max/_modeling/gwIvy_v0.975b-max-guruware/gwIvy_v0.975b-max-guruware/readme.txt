where to put what...

this archive contains gw::Ivy plugins v0.975b for:

 version          | folder
------------------+------------------
 3ds max 2010 x32 | 3dsmax2010\x32\
 3ds max 2010 x64 | 3dsmax2010\x64\
 3ds max 2011 x32 | 3dsmax2011\x32\
 3ds max 2011 x64 | 3dsmax2011\x64\
 3ds max 2012 x32 | 3dsmax2012\x32\
 3ds max 2012 x64 | 3dsmax2012\x64\
 3ds max 2013 x32 | 3dsmax2013\x32\
 3ds max 2013 x64 | 3dsmax2013\x64\
 3ds max 2014     | 3dsmax2014
 3ds max 2015     | 3dsmax2015
 3ds max 2016     | 3dsmax2016

select your 3ds max version
copy gw_Ivy.dlo to your 3ds max-plugins folder


19.08.2015
-= guruware =-
