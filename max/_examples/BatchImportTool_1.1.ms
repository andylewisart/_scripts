--import files from a folder
--save the files as a max file
--merge the max files in scene and export as obj, fbx, max
--1.01 adds .igs support
--the list is not importing the files by slection but by the list so starting at 1
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~
-- <<< Script Begins Here >>>
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~

try (destroyDialog rlt_BatchImportTool) catch ()
rollout rlt_BatchImportTool "Batch Import Tool v1.0"
(
-- general locals
local files, sourceDir
local filesListBox = #()
local processTheScene, collectFileNames, existFile, existDir, makeDirPath, getTheFile, stripBasePath
local debug=false
--local filesToProcess="*.max"
local filesToProcess="*.stp"
-- script process specific locals

-- this function is called to import the file
-- this function is called to load the file
-- fn getTheFile theFile=loadMaxFile theFile
fn getTheFile theFile=
(
--	format "Importing %...\n" theFile	-- This would be nice, but the process closes the Listener.
	importFile theFile #noPrompt quiet:true -- To allow manually setting of import parameters between files,
									-- remove '#noPrompt'
	
)

-- this function is called after the file is loaded. 
-- If the scene is processed ok, return true. Otherwise return false.
-- The name of the file and the filestream for logging are the parameters 
fn processTheScene theFile logfile =
(	
	redrawViews()
	theOutputFile=(getFilenamePath theFile)+(getFilenamefile theFile)+".max"
--	format "Saving % as %...\n" theFile theOutputFile	-- This would be nice, but the process closes the Listener.
	saveMaxFile theOutputFile
--add exportfuncinality in here
--	delete objects
	resetMaxFile #noPrompt
	true
	
)
--function to save and export files
fn mergeExport TheSelection GroupBox =
(
		local toMerge = #()

		for i = 1 to TheSelection.count do append toMerge files[TheSelection[i]]

		for i=1 to toMerge.count do (
			theFileName = getFilenameFile toMerge[i]
			theFileNameExt = (sourceDir + theFileName as string + ".max")
			local mergedObjects = #()

			-- Try to merge the file, if it can't use the importfile interface
			if (mergeMAXFile theFileNameExt #noRedraw #select #mergeDups #renameMtlDups #neverReparent quiet:true) == false then (
				importFile theFileNameExt #noPrompt
			)
			
			mergedObjects = getCurrentSelection()
			-- If group merged checkbox is checked, group the new objects.
			if GroupBox.checked then 
				group mergedObjects name:(theFileName as string)
			--move the groups apart
			if i != 1 then 
			(
			ObjName = mergedObjects[1]
			ObjPos = ObjName.position
			PosOffset = [0,10.*i,0]
			NewObjPos = (ObjPos + PosOffset)
			ObjName.position = NewObjPos
			)

		)
		saveMaxFile (sourceDir as string + "BatchedFiles.max")
		theClasses =exporterPlugin.classes
		exportFile (sourceDir as string + "BatchedFiles") #noPrompt using:theClasses[8]
		resetMaxFile #noPrompt		
	
)

-- this function is called to collect all the files in a base directory with a given extension.
-- if subdirs:true is specified, files in subdirectories are also collected
fn collectFileNames baseDir ext subdirs:false fileList: =
(	--format "Processing: %\n" baseDir

	if fileList == unsupplied do (fileList=#(); if baseDir[baseDir.count] != "\\" do baseDir += "\\")
--get the files with the specified extension in this directory
	for fname in (getFiles (baseDir+ext)) do append fileList fname
--recursively process each subdirectory
	if subdirs do 
		for aDir in (getDirectories (baseDir+"*.*")) do 
			collectFileNames aDir ext subdirs:true fileList:fileList
	fileList
   
)

-- a function to determine if a file exists
fn existFile fname = (getfiles fname).count != 0 

-- a function to determine if a directory exists
fn existDir dname = 
(	if dname[dname.count] == "\\" or dname[dname.count] == "/" do dname=substring dname 1 (dname.count-1)
	(getDirectories dname).count != 0 
	
)

-- a function to create a directory path structure. Returns true if success
-- have to make each directory in the path structure separately
fn makeDirPath theDir =
(	local parsedDir = filterstring theDir "\\"
	local dirString = parsedDir[1] -- the drive
	for i=2 to parsedDir.count do 
	(	dirString += "//"+parsedDir[i]
		if not (existDir dirString) do 
			if not (makeDir dirString) do return false
	)
	true
	
)

-- a function to strip the base directory from the file name in the array of file names
fn stripBasePath baseDir files=
(	local baseDirLen=baseDir.count+1
	for i=1 to files.count do files[i]=substring files[i] baseDirLen -1

)

-- define the user interface
dropdownlist FileFormat "Import File Format" items:#(".stp",".step",".STEP",".stl",".iges",".igs") selection:1--maybe add 3ds obj and any other file types
Label srclbl "Source Directory:" offset:[0,5]
Button SourcePath "--none--" width:150 enabled:true
checkbox doSubDirs "Process subdirectories" enabled:true checked:true 
checkbox chk_groupMerged "Group merged objects" enabled:true checked:true  height:15
multilistbox lst_files "Files: " height:15 offset:[-2,0] enabled:false
button btn_none "None" align:#left width:35 height:16 enabled:false
button btn_all "All" offset:[0,-21] align:#right width:35 height:16 enabled:false
label num2process_t "# Files to process:" across:2 align:#left offset:[0,5]
label numFilesToProcess "0" offset:[0,5]
Button Process "Start File Processing" enabled:false offset:[0,5]


--deselct all button
on btn_none pressed do ( lst_files.selection = #{} )
--select all button
on btn_all pressed do (
		lst_files.selection = #{1..(lst_files.items.count)}
)

-- following is executed when the utility is started
on LoadAndRenderAll open do 
(	sourceDir=undefined
	files=#()
)

-- define the user interface handlers
  on FileFormat selected i do
  (
	filesToProcess= "*" + FileFormat.items[i] as string

  )
--     on ExportFileFormat selected i do
--   (
-- 	filesToExport = ExportFileFormat.items[i] as string

--   )

on SourcePath pressed do 
(	local tmp=getSavePath caption:"Specify Source Directory"
	if tmp != undefined do 
	(	sourceDir=tmp
		if sourceDir[sourceDir.count] != "\\" do sourceDir += "\\"
		SourcePath.text=sourceDir
		if debug do (format "Source directory: %\n" sourceDir)
		Process.enabled=true
		files=collectFileNames sourceDir filesToProcess subdirs:doSubDirs.checked
		stripBasePath sourceDir files
		numFilesToProcess.text=files.count as string
		--populate the files list box
		lst_files.items = for f in files collect (filenameFromPath f)
		lst_files.enabled = true
		btn_all.enabled = true
		btn_none.enabled = true
	)
	
)--End on SourcePath

on doSubDirs changed state do
(	if sourceDir != undefined do 
	(	files=collectFileNames sourceDir filesToProcess subdirs:doSubDirs.checked
		stripBasePath sourceDir files
		numFilesToProcess.text=files.count as string
	)
)

on Process pressed do
(	local theFile, status_ok
	local num_failed=0
	local num_converted=0
	deletefile (sourceDir+"convert_file.log")
	logfile=createfile (sourceDir+"convert_file1.log")
	if logfile == undefined do throw "Error creating log file in " sourceDir
	progressStart "Processing progress"
	progressUpdate 0
	resetMaxFile #noPrompt
	local toProcess = #()
	local sel = (lst_files.selection as array)
	for i = 1 to sel.count do append toProcess files[sel[i]]
	for fin in toProcess do 
	(				
		-- rebuild the file name
		theFile=sourceDir + fin as string
		format "Processing file: %\n" theFile to:logfile
		flush logfile
		format "Processing file: %\n" theFile
		--run getTheFile Function
		try (status_ok=getTheFile theFile)
		catch (status_ok=false)
		if (getProgressCancel()) do exit
		progressEnd ()
		progressStart "Processing progress"
		--run the proceesTheScene function
		if status_ok do status_ok=processTheScene theFile logfile
		if status_ok == #cancel do exit
		if status_ok then
			num_converted += 1
		else
		(	format "Error processing file: %\n" theFile to:logfile
			num_failed += 1
		)

		for c in (windows.getchildrenhwnd 0) where c[5] == "Progress : STEP Importer" do UIAccessor.CloseDialog c[1]
		for c in (windows.getchildrenhwnd 0) where c[5] == "Progress : IGES Importer" do UIAccessor.CloseDialog c[1] --for iges import
	)

	selectedItems = (lst_files.selection as array)
	mergeExport selectedItems chk_groupMerged
	progressEnd ()
	resetMaxFile #noPrompt
	close logfile
	gc()
	messagebox ("Number of failures processing files: "+(num_failed as string)+ 
			    "\nNumber of successes processing files: "+(num_converted as string)+
				"\nSee file "+(logfile as string)+" for processing log") title:"Processing Complete"

)--End on Process

)--End LoadAndRenderAll
createDialog rlt_BatchImportTool 210 450
