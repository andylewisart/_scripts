global getRelativeMasterScriptPath
fn getRelativeMasterScriptPath scriptFileName = 
(
	pattern =@".*MASTER"
	rgx = dotnetObject "System.Text.RegularExpressions.Regex" pattern
	result = rgx.match scriptFileName
	result.value
)

fn get_current_version_number =
(
	
)