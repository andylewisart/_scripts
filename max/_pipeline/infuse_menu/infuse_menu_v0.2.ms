-- macroScript localCopy category:"Infuse" --some macro script
-- (
-- 		print "local copy"
-- 		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
-- 		thecopypastefile = "\pastefile.max" --this is the filename to save as...
-- 		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete string
-- 		if $ != undefined do 
-- 			saveNodes $ thecopypastestring --this saves ("copies") the objects as a file
-- )
-- macroScript localPaste category:"Infuse" --some macro script
-- (
-- 		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
-- 		thecopypastefile = "\pastefile.max" --this is the filename to save as...
-- 		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete stringq

-- 		mergemaxfile (thecopypastedir + thecopypastefile) #select --this merges ("pastes") and selects objects from file
-- )
macroScript Copy_Objects_To_File Category:"Infuse" toolTip:"Copy Objects to File"
(
	if keyboard.shiftPressed then
	(
		print "network copy"
		userName = ((dotNetClass "System.Environment").GetEnvironmentVariable "UserName")

		thecopypastedir = @"\\thanos\infuse\People\AndyL\_networkCopy\" -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = username+"_pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete string
		print thecopypastestring
		if $ != undefined do 
			saveNodes $ thecopypastestring --this saves ("copies") the objects as a file
	)
	else
	(
		print "local copy"
		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = "\pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete string
		if $ != undefined do 
			saveNodes $ thecopypastestring --this saves ("copies") the objects as a file
	)
)

macroScript Paste_Objects_From_File Category:"Infuse" toolTip:"Paste Objects from File"
(
	if keyboard.shiftPressed then
	(

		fileIn "\\\\thanos\3D\_LIBRARY\_scripts\max\_pipeline\copy_paste_buffer\infuse_copyPaste_UI_v0.1.ms"
	)
	else
	(
		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = "\pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete stringq

		mergemaxfile (thecopypastedir + thecopypastefile) #select --this merges ("pastes") and selects objects from file
	)
)


-- theMainMenu = menuMan.getMainMenuBar() --get the main menu bar
-- if (menuMan.findMenu "Infuse") == undefined then 
-- (
-- 	theMenu = menuMan.createMenu "Infuse"
-- )
-- else
-- (
-- 	theMenu = menuMan.findMenu "Infuse"
-- )
-- theMenu = menuMan.createMenu "Infuse" --create a menu called Forum Help
-- theSubMenu = menuMan.createSubMenuItem "Copy_Objects_To_File" theMenu --create a SubMenuItem
-- theMainMenu.addItem theSubMenu (theMainMenu.numItems()+1) --add the SubMenu to the Main Menu
-- theSubMenu = menuMan.createSubMenuItem "Paste_Objects_From_File" theMenu --create a SubMenuItem
-- theMainMenu.addItem theSubMenu (theMainMenu.numItems()+1) --add the SubMenu to the Main Menu
-- theAction = menuMan.createActionItem "Copy_Objects_To_File"  theMenu--create an ActionItem from the MacroScript
-- theMenu.addItem theAction (theMenu.numItems()+1) --add the ActionItem to the menu
-- theAction = menuMan.createActionItem "Paste_Objects_From_File" theMenu --create an ActionItem from the MacroScript
-- theMenu.addItem theAction (theMenu.numItems()+1) --add the ActionItem to the menu
-- menuMan.updateMenuBar() --update the menu bar

-- fn addToInfuseMenu menuName subMenuName actionItemName= 
-- (
-- 	theMainMenu = menuMan.getMainMenuBar()
-- 	theMenu = menuMan.createMenu menuName 
-- 	theSubMenu = menuMan.createSubMenuItem subMenuName theMenu
-- 	theMainMenu.addItem theSubMenu (theMainMenu.numItems()+1) 
-- 	theAction = menuMan.createActionItem actionItemName menuName
-- 	theMenu.addItem theAction (theMenu.numItems()+1)
-- 	menuMan.updateMenuBar() 
-- )

-- addToInfuseMenu "Infuse" "localCopy" "localCopy"
-- addToInfuseMenu "Infuse" "Paste_Objects_From_File" "Paste_Objects_From_File"

-- theMainMenu = menuMan.getMainMenuBar() --get the main menu bar
-- theSubMenu = menuMan.createSubMenuItem "Paste_Objects_From_File" (menuMan.findMenu "Infuse") --create a SubMenuItem
-- theMainMenu.addItem theSubMenu (theMainMenu.numItems()+1) --add the SubMenu to the Main Menu
-- menuMan.updateMenuBar() --update the menu bar
-- try (menuMan.unRegisterMenu (menuMan.findMenu "Custom Tools") ) catch()
 
 
try (menuMan.unRegisterMenu (menuMan.findMenu "Infuse") ) catch()
theMainMenu = menuMan.getMainMenuBar()

menu = menuMan.createMenu "Infuse"
sub = menuMan.createMenu "Copy/Paste"

a = menuMan.createActionItem "Copy_Objects_To_File" "Infuse"
sub.addItem a -1
b = menuMan.createActionItem "Paste_Objects_From_File" "Infuse"
sub.addItem b -1

item = menuMan.createSubMenuItem "Copy/Paste" sub

menu.addItem item -1
mitem = menuMan.createSubMenuItem "Infuse" menu
index = theMainMenu.numItems() -- add before last (usualy it's "Help")
theMainMenu.addItem mitem index

menuMan.updateMenuBar()
