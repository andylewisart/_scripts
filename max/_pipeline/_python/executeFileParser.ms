fn execute_file_parser = (
python.execute ("
import fileParser
reload(fileParser)
from fileParser import FileParser
print '*****'	
thisFilePath = MaxPlus.Core.EvalMAXScript(\"maxfilepath\")
thisFileName = MaxPlus.Core.EvalMAXScript(\"maxfilename\")
print '????'	
derp = FileParser(thisFilePath.Get(), thisFileName.Get())
print '#####'	
stringMethod = (MaxPlus.Core.EvalMAXScript(\"python_file_parser_cmd\").Get()).split(' ')
print '&&&&&', stringMethod	
methodToCall= getattr(derp, stringMethod[0])
#check if method called has args
#TODO: make this more flexible so that it doesn't assume two args of specific types.
if len(stringMethod) == 2:
	print '(((((('	
	MaxPlus.Core.EvalMAXScript('file_parser_result = @\"%s\"'%methodToCall(stringMethod[1]))
elif len(stringMethod) == 3:
	print '))))))'	
	thisPath = str(getattr(derp, stringMethod[1])())
	MaxPlus.Core.EvalMAXScript('file_parser_result = @\"%s\"'%methodToCall(thisPath, stringMethod[2]))
else:
	print '+++++'	
	MaxPlus.Core.EvalMAXScript('file_parser_result = @\"%s\"'%methodToCall())


		
")
)

-- python_file_parser_cmd = "get_explicit_folder get_render_preview_path stateName"
-- python_file_parser_cmd = "set_version_folder get_render_preview_path"
-- python_file_parser_cmd = "get_render_preview_path"
-- execute_file_parser()
-- file_parser_result


--REGEX FOR PATH WITH SPACES? WORKS ON REGEXR BUT NOT HERE:
--Matches file path (either leter drive or network drive), then uses OR to match the rest of the non-whitespace strings. Maybe it works?
--((\w:|\\\\).+\s.*\\[^\s]+)|[^\s]+