fn execute_file_parser python_command result_var = (
python.execute ("
import fileParser
reload(fileParser)
from fileParser import FileParser

thisFilePath = MaxPlus.Core.EvalMAXScript(\"maxfilepath\")
thisFileName = MaxPlus.Core.EvalMAXScript(\"maxfilename\")

derp = FileParser(thisFilePath.Get(), thisFileName.Get())
stringMethod = MaxPlus.Core.EvalMAXScript(\"python_file_parser_cmd\").Get()
methodToCall = getattr(derp, stringMethod)
MaxPlus.Core.EvalMAXScript('thisFileVersion = @\"%s\"'%methodToCall())


		
")
)
python_file_parser_cmd = "get_version_from_filename"
thisFileVersion = undefined
execute_file_parser "get_version_from_filename" thisFileVersion