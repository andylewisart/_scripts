maxmodify mode--switch to Modify tab of Command panel

sel = $.modifiers[1]
vertCount = sel.GetNumVertices()

vertCountArray = #{}
for s=1 to vertCount do append vertCountArray s

elemArray = #()
itr = 1
subobjectLevel = 1
while vertCountArray.isEmpty == False and itr < 200 do
(
    nextIndex = (vertCountArray as array)[1]

    sel.select #vertex #{nextIndex}
    sel.convertSelection #vertex #element
    sel.convertSelection #element #vertex
    append elemArray  (sel.getSelection #vertex)
    sel.SetSelection #Vertex #{}

    vertCountArray = vertCountArray - elemArray[itr]

    itr+=1
)


for x=1 to elemArray.count do
(
    index = random x elemArray.count 
    swap elemArray[x] elemArray[index]
)

percent = (elemArray.count * 75)/100
for i=1 to elemArray.count-percent do
(   
    sel.select #vertex elemArray[i]
    sel.ButtonOp #DeleteVertex
)