macroScript RplcInst category:"CTools" tooltip:"Replace with Instance"
(
obj = Pickobject ()
	for i in selection do
	(
		objInst = instance obj
		objInst.transform = i.transform
		objInst.parent = i.parent
	)--End For
	delete selection
) 