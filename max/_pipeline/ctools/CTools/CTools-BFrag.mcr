macroScript BFrag category:"CTools" toolTip:"pShape Boolean Fragmentation"
(
	obj = selection[1]
	pShapes = $PShape* as array
	for i in pShapes do
	(
		objCpy = copy obj
		psBool = copy i
		obj - psBool
		psBool * objCpy
		delete objCpy
		update psBool
		update obj
		psBool.name = uniqueName (obj.name + "_Frag_")
	)--End For
	delete pShapes
) 