macroScript BoolUnion Category:"CTools" toolTip:"Boolean Union"
(
	fn geoFilt o = (superClassOf o == GeometryClass)
	subOp = PickObject prompt:"Pick Geometry" filter:geoFilt
	obj = selection[1]
	obj + subOp
	delete subOp
	update obj
	select obj
) 