macroScript IsoFume category:"CTools" toolTip:"Isolate FumeContainers"
(
	ffx = for i in objects where classOf i == FumeFX collect i
	if selection.count > 0 then
	(
		ffx.renderable = false
		selection.renderable = true
		for i in ffx where i.isSelected != true do (hide i)
	)
	else
	(
		ffx.renderable = true
		unhide ffx
	)
) 