macroScript pflow category:"CTools" toolTip:"PF Preset"
(
PF_Source X_Coord:20 Y_Coord:0 isSelected:on Logo_Size:8 Emitter_Length:10 Emitter_Width:10 Emitter_Height:0.01 pos:[0,0,0]
particleFlow.BeginEdit()
op1 = Birth()
op2 = Position_Icon()
op3 = Speed()
op4 = Rotation()
op5 = ShapeStandard()
op6 = RenderParticles()
op7 = DisplayParticles()
op7.color = $.wireColor
ev1 = Event()
ev1.SetPViewLocation ($.X_Coord) ($.Y_Coord+100)
particleFlow.EndEdit()
ev1.AppendAction op1
ev1.AppendAction op2
ev1.AppendAction op3
ev1.AppendAction op4
ev1.AppendAction op5
ev1.AppendAction op7
$.AppendAction op6
$.AppendInitialActionList ev1
particleFlow.openParticleView()
$'Particle View 01'.Show_Action_Depot = off
$'Particle View 01'.Show_Action_Description = off
$.Quantity_Viewport = 100
) 