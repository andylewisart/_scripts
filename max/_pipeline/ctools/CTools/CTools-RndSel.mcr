macroScript RndSel category:"CTools" toolTip:"Random Select"
(
objSel = selection as array
rollout selTool "Selection Tools" width:160 height:80
(
	spinner spn_Amount "1 /  " pos:[40,32] width:104 height:16 scale:0.1 range:[1,1000,1] type:#integer
	groupBox grp1 "Random Select" pos:[8,8] width:144 height:56
	on spn_Amount changed val do
	(
		newSel = for i in objSel where (random 1 val == 1) collect i
		select newSel
	)
)
createDialog selTool
) 