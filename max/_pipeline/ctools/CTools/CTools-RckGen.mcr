macroScript RckGen category:"CTools" toolTip:"Rock Generator"
(
ts = timeStamp()
seed ts
rndSize = random 6 12
rndType = random 0 2
rndNoise1 = random 5 10
nRock = Geosphere()
nRock.name = uniqueName "Boulder"
nRock.radius = rndSize
nRock.baseType = rndType
if rndType == 2 then
(
	nRock.segs = 1
)--End If
Else
(
	nRock.segs = 2
)--End Else
addModifier nRock (Noisemodifier ())
nRock.modifiers[#Noise].scale = rndNoise1
nRock.modifiers[#Noise].strength = [rndNoise1,rndNoise1,rndNoise1]
nRock.modifiers[#Noise].fractal = on
nRock.modifiers[#Noise].seed = random 0 9999
convertToPoly nRock
numFaces = nRock.faces.count
for i in 0 to (random 0 3) do
(
	rndFace = random 1 numFaces
	nRock.selectedFaces = #(rndFace)
	edgeArray = polyOp.getFaceEdges nRock rndFace
	nRock.Hinge = edgeArray[rndType + 1]
	nRock.hingeAngle = (random 5 45)
	nRock.EditablePoly.HingefromEdge()
)--End For
addModifier nRock (TurboSmooth ())
convertToPoly nRock
addModifier nRock (subdivide ())
nRock.modifiers[#Subdivide].size = .75
addModifier nRock (Noisemodifier ())
nRock.modifiers[#Noise].scale = random 1 10
nStr = random .05 .2
nRock.modifiers[#Noise].strength = [nStr,nStr,nStr]
nRock.modifiers[#Noise].fractal = on
nRock.modifiers[#Noise].roughness = 1
addModifier nRock (TurboSmooth ())
convertToPoly nRock
) 