macroScript UnHdDst category:"CTools" toolTip:"Unhide by Distance"
(
	dst = 24
	if units.SystemType == #feet then untSclr = 12  else untSclr = 1
	for i in $* where (i.ishidden == true) collect i
	for i in hdnObjs do
	(
		if distance i.center selection[1].pos < (dst / untSclr) then (unhide i)
	)
) 