macroScript SelByName category:"CTools" tooltip:"SelByName"
(
bName = trimRight selection[1].name "0123456789"
allObj = for i in (execute ("$'" + bName + "'*")) where i.ishidden != true collect i
select allObj
)--End SelByName
