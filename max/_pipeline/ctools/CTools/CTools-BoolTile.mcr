macroScript BoolTile category:"CTools" toolTip:"Roof_Boolean Tile"
(
	tile = $'Tile_RoofP*' as array
ops = $'Operand_RoofP*' as array
if tile.count == ops.count then
(
	cnt = 0
	endLoop = tile.count
	progressStart ("Booleaning Roof Planes . . .")
	for i in 1 to endLoop do
	(
		prgCount = (cnt / endLoop * 100) as integer
		progressUpdate prgCount
		cnt += 1 as float
		tile[i] * ops[i]
		update tile[i]
	)--End For
	progressEnd ()
	delete ops
)--End
Else
(
	messagebox "Check Counts"
)--End Elseh
)
