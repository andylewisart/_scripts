macroScript Algn2Pln category:"CTools" toolTip:"Roof_Align-to-Plane"
(
rp = selection
rt = pickObject ()
convertToPoly rp
for i in 1 to rp.count do
(
	nt = copy rt
	nt.name = "Tile_" + rp[i].name
	nt.dir = polyOp.getFaceNormal rp[i] 1
	lowVerts = #()
	for ii in 1 to rp[i].numVerts do
	(
		if (polyOp.getVert rp[i] ii).z == rp[i].min.z then
		(
			lowVerts += #(polyOp.getVert rp[i] ii)
		)--End If
	)--End For Verts
	if lowVerts.count == 2 then
	(
		nt.pos = (lowVerts[1] + lowVerts[2]) / 2
	)--End If
	if lowVerts.count == 1 then
	(
		nt.pos = lowVerts[1]
	)--End If
	if lowVerts.count > 2 then
	(
		nt.pos = polyOp.getFaceCenter rp[i] 1
	)--End If
)--End For
)
