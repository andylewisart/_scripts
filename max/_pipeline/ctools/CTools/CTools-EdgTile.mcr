macroScript EdgTile category:"CTools" toolTip:"Roof_Edge Tile"
(
	shps = selection as array
tType = PickObject()
et = copy tType
pC = Path_Constraint()
pC.follow = on
pC.axis = 1
et.pos.controller = pC
addModifier shps (Renderable_Spline())
shps[1].modifiers[1].SymmetricalOrRectangular = 0
shps[1].modifiers[1].sides = 4
shps[1].modifiers[1].thickness = 30
for i in 1 to shps.count do
(
	pC.appendTarget shps[i] 100
	nTile = snapshot et
	nOp = shps[i]
	convertToMesh nOp
	in coordSys Local move nTile [0,-3,0]
	extrudeFace nOp #(9,10) 12 100
	nTile * nOp
	update nTile
	delete nOp
	pC.deleteTarget 1
)--End For
delete et
) 