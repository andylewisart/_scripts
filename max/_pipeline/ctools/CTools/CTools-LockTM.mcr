macroScript LockTM category:"CTools" toolTip:"LockTM"
(
	if (getTransformLockFlags selection[1] as array)[1] == undefined then
	(
		setTransformLockFlags $ #all
		print "Transforms Locked"
	)
	else
	(
		setTransformLockFlags $ #none
		print "Transforms Unlocked"
	)
) 