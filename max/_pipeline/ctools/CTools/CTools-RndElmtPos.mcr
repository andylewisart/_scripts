macroScript RndElmtPos category:"CTools" toolTip:"Randomize Element Position"
(
	fSel = $.modifiers[#unwrap_uvw].getSelectedFaces()
	for i in ($.modifiers[#unwrap_uvw].getSelectedFaces() as array) do
	(
		fSel2 = (deleteItem fSel i)
		$.modifiers[#unwrap_uvw].selectFaces fSel2
		$.modifiers[#unwrap_uvw].selectElement()
		$.modifiers[#unwrap_uvw].moveSelected (random [.2, .2, 0] [-.2, -.2, 0])
	)
) 