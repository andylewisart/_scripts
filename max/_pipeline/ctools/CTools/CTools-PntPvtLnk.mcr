macroScript PntPvtLnk category:"CTools" tooltip:"Linked Point at Pivot"
(
	p = Point()
	p.wirecolor = (color 0 255 0)
	for i in 1 to selection.count do
	(
		p.size = (distance selection[i].max selection[i].min)/4
		if i > 1 then
		(
			pInst = instance p
			pInst.wirecolor = (color 0 255 0)
			pInst.name = ("CP_" + selection[i].name)
			pInst.transform = selection[i].transform
			selection[i].parent = pInst
		)
		Else
		(
			p.name = ("CP_" + selection[i].name)
			p.transform = selection[i].transform
			selection[i].parent = p
		)
	)
) 