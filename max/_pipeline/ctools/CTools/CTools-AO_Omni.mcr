macroScript AO_Omni category:"CTools" toolTip:"AO Omni Light"
(
	nLght = omniLight()
	nLght.affectDiffuse = false
	nLght.affectSpecular = false
	nLght.ambientOnly = true
	nLght.projector = true
	aoMap = Ambient_Reflective_Occlusion__3dsmax()
	aoMap.max_distance = 10
	nLght.projectorMap = aoMap
) 