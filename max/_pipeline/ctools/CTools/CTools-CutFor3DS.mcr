macroScript CutFor3DS category:"CTools" toolTip:"Cut for 3DS Export"
(
	if classOf $ != Editable_mesh then (convertToMesh $)
	vAry = for i in 1 to 21333 collect (i)
	stp = 0
	while $.numFaces > 21333 do
	(
		stp += 1
		dMesh = meshOp.detachFaces $ vAry delete:true asMesh:true
		update $
		nMesh = Editable_mesh()
		nMesh.mesh = dMesh
		update nMesh
		nMesh.name = $.name + "_Mesh-" + stp as string
		nMesh.transform = $.transform
		nMesh.material = $.material
	)
) 