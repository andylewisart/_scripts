macroScript MshAttch category:"CTools" toolTip:"Mesh Attach"
(
if selection.count > 1 then
(
	undo off
	(
		convertToMesh selection[1]
		for i in 2 to selection.count do (attach selection[1] selection[2])--End For
	)--End Undo Off
)--End If
) 