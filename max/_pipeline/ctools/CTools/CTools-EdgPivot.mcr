macroScript EdgPivot category:"CTools" toolTip:"Edge Pivot"
(
	if selection[1] != undefined then
	(
		obj = selection[1]
		vrts = polyOp.getEdgeVerts obj (polyOp.getEdgeSelection obj as array)[1]
		vPos1 = polyOp.getVert obj vrts[1]
		vPos2 = polyOp.getVert obj vrts[2]
		mrk = 0
		if vPos1.z > vPos2.z then (eVec = normalize (vPos1 - vPos2))
		else
		(
			eVec = normalize (vPos2 - vPos1)
			mrk = 1
		)
		upVec = [0,0,1]
		rtVec = normalize (cross upVec eVec)
		upVec = normalize (cross rtVec eVec)
		if mrk == 0 then (eMtrx = matrix3 rtVec upVec eVec vPos2)
		else (eMtrx = matrix3 rtVec upVec eVec vPos1)
		pt1 = point()
		pt1.name = ("Edge-Pivot_" + obj.name)
		pt1.transform = eMtrx
		pt1.size = (length (vPos1 - vPos2)) + 1
		pt1.axisTripod = on
		pt1.cross = off
		pt1.box = on
		pt1.wirecolor = (color 0 225 0)
		obj.parent = pt1
	)
) 