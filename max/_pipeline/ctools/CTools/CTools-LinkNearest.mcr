macroScript LinkNearest category:"CTools" toolTip:"Link to Nearest Pick"
(
	pObj = pickObject ()
	bName = trimRight pObj.name "0123456789"
	allObj = for i in (execute ("$" + bName + "*")) where i.ishidden != true collect i
	for i in selection do
	(
		pDist = for i2 in allObj collect (distance i.pos i2.pos)
		i.parent = allObj[(findItem pDist (amin pDist))]	
	)
) 