macroScript GetInstnce category:"CTools" toolTip:"Get Instances"
(
InstanceMgr.GetInstances $ &rptInstances
trueInstances = for n in rptInstances where (areNodesInstances $ n) collect n
select trueInstances
) 