macroScript PFSplnr category:"CTools" toolTip:"PF Spliner"
(
fltr_PFSplnr = newRolloutFloater "PF Spliner" 170 300
rollout rollout_PFSplnr "PF Spliner" width:160 height:280
(
	button btn_CrtSpln "Create Splines" pos:[30,200] width:100 height:32
	button btn_PkSrc  "Pick PF Source" pos:[30,16] width:96 height:32
	label lbl1 "Frame Range" pos:[40,56] width:68 height:16
	spinner spn_FStrt "" pos:[80,72] width:60 height:16 range:[-9999,9999,0] type:#integer 
	spinner spn_FEnd "" pos:[80,96] width:60 height:16 range:[-9999,9999,30] type:#integer
	spinner spn_Incr "" pos:[80,128] width:60 height:16 range:[1,9999,5] type:#integer
	label lbl5 "Increment" pos:[24,128] width:48 height:16
	editText edt_Name "" pos:[24,168] width:112 height:16 text:"PF-Shape_"
	label lbl7 "Spline Name Base" pos:[40,152] width:96 height:16
	label lbl8 "From:" pos:[24,72] width:32 height:16
	label lbl9 "To:" pos:[24,96] width:32 height:16
	label lbl10 "Written by Charley Carlat" pos:[24,240] width:120 height:16
	label lbl11 "www.charleycarlat.com" pos:[27,256] width:112 height:16
	global shNm = "PF-Shape_"
	global fStrt = 0
	global fEnd = 30
	global incr = 5
	on btn_PkSrc pressed do
	(
		fn pfFilt flt = (classOf flt == PF_Source)
		global pf = pickObject message:"Pick PF Source...." filter:pfFilt
		btn_PkSrc.caption = pf.name
	)--End btn_PkSrc
	on spn_FStrt changed stValue do (global fStrt = stValue)
	on spn_FEnd changed eValue do (global fEnd = eValue)
	on spn_Incr changed sIState do	(global incr = sIState)
	on edt_Name changed eNmState do (global shNm = eNmState)
	on btn_CrtSpln pressed do
	(
		allShps = #()
		i2 = 1
		pTest = false
		
		-----Checking for all valid particle by sliderTime specified above--------
		sliderTime = (fEnd +1)
		while not pTest do
		(
		pf.particleIndex = i2
			if pf.particlePosition == [0,0,0] then
			(
				pTest = true
				global numParts = i2 - 1
			)--End If
			i2 += 1
		)--End Do
		--------------------------------Creating shapes-----------------------------
		for i in 1 to numParts do
		(
			newShp = splineShape()
			addNewSpline newShp
			newShp.name = (shNm + i as string)
			allShps += #(newShp)
		)--end For
		-------------------Adding Knots to Splines at specified intervals-------------
		for t in fStrt to (fEnd/incr) do-- the "to" number here defines the multiplier or total number of knots
		(
			sliderTime = (t * incr)--sets the sliderTime multiplied by above number
			for i in 1 to numParts do
			(
				pf.particleIndex = i
				pId = pf.particleID
				if pId != 0 then
				(
					pfShp = execute ("$'" + shNm + pId as string + "'")
					addKnot pfShp 1 #smooth #curve pf.particlePosition
				)--End If
			)
		)--End For
		select allShps
		updateShape $
	)--End Btn
	on rollout_PFSplnr open do
	(
	)	
)--End rollout
addRollout rollout_PFSplnr fltr_PFSplnr
) 