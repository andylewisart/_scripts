macroScript RndClr category:"CTools" toolTip:"Random WireColor"
(
	meshObjs = $
	allMaxColors = #([166,229,229],[134,6,6],[134,59,8],[134,110,8],[113,134,6],[61,134,6],[6,134,58],[6,134,113],[8,110,134],[8,61,138],[8,8,136],[57,8,136],[108,8,136],[138,8,110],[214,229,166],[176,26,26],[177,88,26],[177,148,26],[148,177,26],[88,177,26],[26,177,88],[26,177,148],[28,149,177],[28,89,177],[28,28,177],[85,28,177],[145,28,177],[177,28,149],[229,166,215],[224,86,86],[224,143,87],[224,198,87],[198,224,87],[143,224,87],[87,224,143],[87,224,198],[88,199,225],[88,143,225],[88,88,225],[140,88,225],[196,88,225],[225,88,199])
	if selection.count != 0 then
	(
		
		if selection.count > 1 then
		(
			for i in 1 to selection.count do
			(
				rndColor = random 1 42
				meshObjs[i].wirecolor = allMaxColors[rndColor]
			)--End For
		)--End If Multiple
		Else
		(
			rndColor = random 1 42
			meshObjs.wirecolor = allMaxColors[rndColor]
		)--End If Single
	)
	Else
	(
		messageBox "Nothing Selected"
	)--End If Nothing
) 