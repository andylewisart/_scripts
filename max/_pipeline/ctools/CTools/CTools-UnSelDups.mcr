macroScript UnSelDups category:"CTools"  toolTip:"Unselect Duplicates"
(
	global dups = #()
	global checked = #()
	for i in selection do
	(
		if superClassOf i == GeometryClass then
		(
			for i2 in $* do
			(
				if (meshOp.getFaceCenter i.mesh 1) == (meshOp.getFaceCenter i2.mesh 1) and i != i2 and findItem dups i2 == 0 and findItem checked i2 == 0 then
				(
					dups += #(i2)
				)
			)
			checked +=(i)
		)
	)
	select dups
) 