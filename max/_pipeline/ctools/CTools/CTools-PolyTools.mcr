macroScript PolyTools Category:"CTools" toolTip:"Poly Tools"
(
	fltr_PlyTools = newRolloutFloater "Poly Tools:"  170 420
	rollout Rollout_PlyTools "PolyTools:" width:162 height:390
	(
		groupBox grp1 "Select by Normals" pos:[8,8] width:144 height:180
		button btn_UpNode "Pick Up Node" pos:[16,104] width:128 height:24 enabled:false
		dropDownList ddl_Axis "" pos:[16,72] width:80 height:21 items:#("X", "Y", "Z", "Pick") selection:3 enabled:false
		checkbox chk_Invt "Invert" pos:[24,136] width:56 height:16 enabled:false
		label lbl_UpNode "Up Node" pos:[24,56] width:56 height:16
		spinner spn_NormThresh "" pos:[88,160] width:56 height:16 enabled:false range:[-1,1,.5] scale:0.01
		label lbl_Threshold "Threshold" pos:[24,160] width:56 height:16
		groupBox grpFcArea "Select By Face Area" pos:[8,200] width:144 height:80
		spinner spn_FcArea "" pos:[88,240] width:56 height:16 range:[0,100,.10] scale:0.01
		label lbl_FcAreaThresh "Threshold" pos:[24,240] width:56 height:16
		button btn_SelFaces "Process Selection" pos:[24,288] width:112 height:32
		checkbox chk_FcNorm "Active" pos:[24,32] width:64 height:16 enabled:true
		checkbox chk_FcArea "Active" pos:[24,216] width:64 height:16 checked:true
		button btn3 "3" pos:[16,350] width:24 height:24
		button btn4 "4" pos:[48,350] width:24 height:24
		button btn5 "5" pos:[80,350] width:24 height:24
		button btn6 "6" pos:[112,350] width:24 height:24
		groupBox grp3 "Edge Number Filter" pos:[8,330] width:144 height:56
		on ddl_Axis selected idx do
			(
				if idx < 4 then
				(
					btn_UpNode.enabled = false
				)--End If
				if idx == 4 then
				(
					btn_UpNode.enabled = true
				)--End If
			)--End ddl_Axis
		on btn_UpNode pressed  do
			(
				global upNodeObj = pickObject prompt:"Pick Object for Up Node"
				btn_UpNode.caption = upNodeObj.name
			)
		on spn_NormThresh changed val do
			global normThresh = spn_NormThresh.value
		on spn_FcArea changed val do
			global areaThresh = spn_FcArea.value
		on chk_FcNorm changed state do
		(
			chk_Invt.enabled = chk_FcNorm.checked
			 ddl_Axis.enabled = chk_FcNorm.checked
			spn_NormThresh.enabled = chk_FcNorm.checked
		)
		on chk_FcArea changed state do
			spn_FcArea.enabled = chk_FcArea.checked
		on btn3 pressed do
		(
			fSel = #()
			for i in 1 to $.faces.count do (if (polyOp.getFaceEdges $ $.faces[i].index).count == 3 then (fSel += #(i)))
			$.selectedFaces = fSel
		)
		on btn4 pressed do
		(
			fSel = #()
			for i in 1 to $.faces.count do (if (polyOp.getFaceEdges $ $.faces[i].index).count == 4 then (fSel += #(i)))
			$.selectedFaces = fSel
		)
		on btn5 pressed do
		(
			fSel = #()
			for i in 1 to $.faces.count do (if (polyOp.getFaceEdges $ $.faces[i].index).count == 5 then (fSel += #(i)))
			$.selectedFaces = fSel
		)
		on btn6 pressed do
		(
			fSel = #()
			for i in 1 to $.faces.count do (if (polyOp.getFaceEdges $ $.faces[i].index).count == 6 then (fSel += #(i)))
			$.selectedFaces = fSel
		
		)
		on btn_SelFaces pressed do
		(
			if selection.count == 1 then
			(
				if chk_FcArea.checked == true and $.selectedFaces.count != 0 then
				(
					global areaThresh = spn_FcArea.value
					allFcSel = polyOp.getFaceSelection $ as array
					fArea = 0
					for i in 1 to allFcSel.count do
					(
						fArea += polyOp.getFaceArea $ allFcSel[i]
					)--End For
					fArea = fArea/allFcSel.count
					tolHi = fArea + (fArea * areaThresh)
					tolLo = fArea - (fArea * areaThresh)
					global fcAreaSel = #()
					for i in 1 to $.numFaces do
					(
						if (polyOp.getFaceArea $ i) < tolHi and (polyOp.getFaceArea $ i) > tolLo then
						(
							fcAreaSel += #(i)
						)--End If
					)--End For
					$.selectedFaces = fcAreaSel
				)--End If 				
				if chk_FcNorm.checked == true then
				(
					normThresh = spn_NormThresh.value
					dirArry = #([1,0,0], [0,1,0], [0,0,1])
					
					if ddl_Axis.selection != 4 then
					(
						UpDir = dirArry[ddl_Axis.selection]
					)--End If
					if upNodeObj != undefined and ddl_Axis.selection == 4 then
					(
						upDir = upNodeObj.dir
					)--End If
					if upNodeObj == undefined and ddl_Axis.selection == 4 then
					(
						upDir = [0,0,1]
					)--End If
					if chk_Invt.checked == true then
					(
						upDir = -upDir
					)--End If
					
					if $.selectedFaces.count == 0 then (usedFaces = $.faces) else (usedFaces = $.selectedFaces)
					normLst = for i in usedFaces collect (polyOp.getfacenormal $ i.index)
					global normFcSel = #()
					for i in 1 to usedFaces.count do
					(
						if (dot upDir normLst[i] >= normThresh) == true then
						(
							normFcSel += #(usedFaces[i].index)
						)
					)--End For
					$.selectedFaces = normFcSel	
				)--End If
				if chk_FcArea.checked == true and chk_FcNorm.checked == true and normFcSel != undefined then
				(
					combFcSel = #()
					for i in 1 to $.selectedFaces.count do
					(
						fcChk = findItem normFcSel fcAreaSel[i]
						if fcChk != 0 then
						(
							combFcSel += #(fcAreaSel[i])
						)--End if
					)--End For
					$.selectedFaces = combFcSel
				)--End if
			)--End If Selected
		)--End Btn
	)--End Rollout
	addRollout Rollout_PlyTools fltr_PlyTools
)--End Macro 