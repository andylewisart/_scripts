macroScript LineUp category:"CTools" toolTip:"Line up Objects"
(
allObjs = $
rows = sqrt allObjs.count as integer
aoMin = allObjs[1].min
aoMax = allObjs[1].max
aoSze = aoMin - aoMax
for i in 2 to allObjs.count do
(
	aoMin = allObjs[i].min
	aoMax = allObjs[i].max
	crntSze = aoMin - aoMax
	allObjs[i].pos = allObjs[i-1].pos + [((abs aoSze.x / 2) + (abs crntSze.x / 2)),0,0]
	
	aoSze = aoMin - aoMax
)--End For
) 