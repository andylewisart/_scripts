macroScript Pnt2Pvt category:"CTools" tooltip:"Point at Pivot"
(
	p = Point()
	for i in 1 to selection.count do
	(
		if selection.count > 1 then
		(
			p = instance p
		)
		p.name = ("CP_" + selection[i].name)
		p.size = (distance selection[i].max selection[i].min)/4
		p.transform = selection[i].transform
		p.wireColor = selection[i].wireColor
	)
)
