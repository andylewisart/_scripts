macroScript ImgPlnSize category:"CTools" toolTip:"Image Plane Proportion Size"
(
	bMap = ()
	tMap = ()
	for i in selection do
	(
		if classOf i == Plane then
		(
			theMat = i.material
			if classOf theMat.diffuseMap == Bitmaptexture then
			(
				tMap = theMat.diffuseMap
			)
			if tMap == undefined and classOf theMat.opacityMap == Bitmaptexture then
			(
				tMap = theMat.opacityMap
			)
			if tMap != undefined then
			(
				bMap = (openBitMap tMap.fileName)
				i.width = (i.length / (bMap.height / bMap.width as float))
			)
		)
	)
) 