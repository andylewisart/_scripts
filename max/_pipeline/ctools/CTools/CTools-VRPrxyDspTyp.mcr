macroScript VRPrxyDspTyp category:"CTools" toolTip:"VRay Proxy Display Type"
(
	if selection.count > 0 then
	(
		for i in selection where classOf i == VRayProxy do
		(
			if selection[1].display == 0 then (i.display = 1)
			if selection[1].display == 1 then (i.display = 3)
			if selection[1].display == 3 then (i.display = 0)
		)
	)
	else
	(
		if $VrayProxy*[1].display == 0 then ($VRayProxy*.display = 1)
		if $VrayProxy*[1].display == 1 then ($VRayProxy*.display = 3)
		if $VrayProxy*[1].display == 3 then ($VRayProxy*.display = 0)
	)
) 