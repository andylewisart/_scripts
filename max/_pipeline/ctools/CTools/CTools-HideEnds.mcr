macroScript HideEnds category:"CTools" toolTip:"Hide Bone Ends"
(
	bnEnds = for i in $*End* where (classOf i == BoneGeometry) collect i
	if bnEnds[1].isHidden == false then (hide bnEnds) Else (unhide bnEnds)
)  