macroScript trkrSet category:"CTools" toolTip:"Tracker setup (SynthEyes)"
(
	$Tracker*.centerMarker = off
	$Tracker*.wirecolor = color 255 255 0
	$Tracker*.size = 0.5
	setTransformLockFlags $Tracker* #all
) 