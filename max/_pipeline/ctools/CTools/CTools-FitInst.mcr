macroScript FitInst category:"CTools" tooltip:"Fit with Instance"
(
obj = Pickobject ()
oSize = [abs(obj.max.x - obj.min.x),abs(obj.max.y - obj.min.y),abs(obj.max.z - obj.min.z)]
	for i in selection do
	(
		objInst = instance obj
		objInst.transform = i.transform
		iSize = [abs(i.max.x - i.min.x),abs(i.max.y - i.min.y),abs(i.max.z - i.min.z)]
		scale objInst (iSize / oSize)
	)--End For
	delete selection
) 