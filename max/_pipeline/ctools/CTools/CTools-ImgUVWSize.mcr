macroScript ImgUVWSize category:"CTools" toolTip:"Image UVW Proportion Size"
(
	for i in selection do
	(
		theMat = i.material
		difMap = theMat.diffuseMap
		global bMap = (openBitMap difMap.fileName)
		i.modifiers[#UVW_Mapping].width = (i.modifiers[#UVW_Mapping].length / (bMap.height / bMap.width as float))
	)
) 