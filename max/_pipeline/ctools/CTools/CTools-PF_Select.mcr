macroScript PF_Select category:"CTools" toolTip:"PF Select Source"
(
	allSrc = for n in $* where (classOf n == PF_Source) collect n
	nms = #()
	for i in allSrc do (nms += #(i.name))
	rollout rollout_SS "PF Sources"
	(
		listBox lbxSS "PFlow Sources" items:nms
		on lbxSS selected itm do
			(
				select allSrc[itm]
			)
		on lbxSS doubleClicked itm2 do
		(
			destroyDialog rollout_SS
		)
	)--End Rollout
	if allSrc.count > 1 then
	(
		createDialog rollout_SS
	)--End If
	Else
	(
		Select allSrc
	)
) 