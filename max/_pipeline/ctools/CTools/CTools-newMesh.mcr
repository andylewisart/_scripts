macroScript newMesh category:"CTools" toolTip:"Make New Mesh"
(
	for i in 1 to selection.count do
	(
		nObj = mesh()
		nObj.transform = selection[i].transform
		nObj.mesh = selection[i].mesh
		nObj.name = selection[i].name
		update nObj
	)
	delete $
) 