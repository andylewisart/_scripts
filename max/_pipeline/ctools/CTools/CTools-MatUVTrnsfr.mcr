macroScript MatUVTrnsfr category:"CTools" toolTip:"Material and UV Transfer"
(
	obj1 = selection
	obj2 = pickObject promt:"Get Object Material"
	obj1.material = obj2.material
	for i in 1 to selection.count do
	(
			for i2 in 1 to obj2.modifiers.count do
		(
			if obj2.modifiers[i2].name == "UVW Mapping" then
			(
				if (findItem obj1[i].modifiers obj2.modifiers[i2]) == 0 then
				(
					addModifier obj1[i] obj2.modifiers[i2]
				)
			)--End If
		)--End For
	)--End For
) 