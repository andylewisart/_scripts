macroScript Rot2Shp category:"CTools" toolTip:"Rotate to Shape"
(
	fn shapeFilt o = (superClassOf o.baseObject == Shape)
	shpObj = (pickObject filter:ShapeFilt).baseObject
	pt1 = getKnotPoint shpObj 1 1
	pt2 = getKnotPoint shpObj 1 2
	for i in selection do (i.dir = normalize (pt1 - pt2))
)