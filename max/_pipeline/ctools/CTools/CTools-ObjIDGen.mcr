macroScript ObjIDGen category:"CTools" toolTip:"ObjID Generate"
(
n = 1
for i in $* do
	(
		i.gbufferChannel = n
		n += 1
	)
) 