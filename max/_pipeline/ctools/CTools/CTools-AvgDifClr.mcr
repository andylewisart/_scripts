macroScript AvgDifClr category:"CTools" toolTip:"Medit-Averages Diffuse Bitmap"
(
mat = meditMaterials[medit.getActiveMtlSlot()]
bMap = undefined
while bMap == undefined do
(
	try(bMap = (mat.diffuseMap.bitmap))catch()
	try(bMap = (mat.diffuseMap.map1.bitmap))catch()
	try(bMap = (mat.diffuseMap.map2.bitmap))catch()
	try(bMap = (mat.diffuseMap.map1.map1.bitmap))catch()
	try(bMap = (mat.diffuseMap.map1.map2.bitmap))catch()
	try(bMap = (mat.diffuseMap.map2.map1.bitmap))catch()
	try(bMap = (mat.diffuseMap.map2.map2.bitmap))catch()
	if bMap == undefined then (bMap = 1)
)
if bMap != 1 then
(
		allPxls = getPixels bMap [0,(bMap.height / 10)] (bMap.width)
		allClrs = allPxls[1]
		for i in 2 to allPxls.count do
		(
			allClrs += allPxls[i]
		)--End For
		avgClr = (allClrs / allPxls.count)
		mat.diffuseColor = avgClr
)
print bMap
) 