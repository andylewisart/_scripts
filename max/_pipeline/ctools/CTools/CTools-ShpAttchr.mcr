macroScript ShpAttchr category:"CTools" tooltip:"Shape Attacher"
(
	if selection.count > 0 then
	(
		explodeGroup selection
		--selection.material = none
		
		numobj = selection.count
		badobj = 0
		for cntr in 1 to numobj do
			(
				if superclassof $selection[cntr - badobj] != Shape then
				(
				deselect $selection[cntr - badobj]
				badobj = badobj + 1
				)-- End If
			)--End For
		
			numobj = selection.count
			numcnt = numobj
			
			if numobj > 1 then
				(
				convertToSplineShape selection
					for i in 2 to numcnt do
						(
						addAndWeld selection[1] selection[1+1] .01
						
						)-- End IF
		
				)--End If
				--messagebox "You need to select at least TWO objects."
				gc ()
	)
)
