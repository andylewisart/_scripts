macroScript prpTrnsf category:"CTools" toolTip:"Transfer Properties"
(
	rollout rollout_prpTrnsf  "Untitled" width:144 height:208
	(
		label lblTM "Transform:" pos:[16,24] width:56 height:16
		groupBox grp1 "Property Copy" pos:[8,8] width:128 height:192
		label lblTMObj "None" pos:[16,40] width:112 height:16
		button btnTMCopy "Copy" pos:[16,56] width:48 height:16
		button btnTMPaste "Paste" pos:[80,56] width:48 height:16
		label lblWC "WireColor:" pos:[16,80] width:56 height:16
		label lblWCObj "None" pos:[16,96] width:112 height:16
		button btnWCCopy "Copy" pos:[16,112] width:48 height:16
		button btnWCPaste "Paste" pos:[80,112] width:48 height:16
		label lblMtl "Material:" pos:[16,136] width:56 height:16
		label lblMtlObj "None" pos:[16,152] width:112 height:16
		button btnMtlCopy "Copy" pos:[16,168] width:48 height:16
		button btnMtlPaste "Paste" pos:[80,168] width:48 height:16
		
		on btnTMCopy pressed do
		(
			global tmCopy = selection[1].transform
			lblTMObj.caption = selection[1].name
		)
		on btnTMPaste pressed do
		(
			$.transform = tmCopy
		)
		on btnWCCopy pressed do
		(
			global wcCopy = selection[1].wirecolor
			lblWCObj.caption = selection[1].wirecolor as string
		)
		on btnWCPaste pressed do
		(
			$.wirecolor = wcCopy
		)
		on btnMtlCopy pressed do
		(
			global mtlCopy = selection[1].material
			lblMtlObj.caption = selection[1].material.name
		)
		on btnMtlPaste pressed do
		(
			$.material = mtlCopy
		)
	)
	createDialog rollout_prpTrnsf

) 