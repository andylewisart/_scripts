macroScript Mat2WClr category:"CTools" toolTip:"Material diffuse to wirecolor"
(
	obj = pickObject()
	if obj.material != undefined then
	(
		$.wirecolor = obj.material.diffuseColor
	)
) 