macroScript Face2Obj category:"CTools" toolTip:"Create New Mesh From Faces"
(
	selMesh = meshOp.detachFaces $.mesh $.mesh.selectedFaces delete:False asMesh:true
	nMesh = Editable_mesh()
	nMesh.mesh = selMesh
	nMesh.transform = $.transform
	nMesh.name = "FacesFrom-" + $.name
	update nMesh
	subObjectLevel = 0
	select nMesh
) 