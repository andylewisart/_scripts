macroScript CrtOps category:"CTools" toolTip:"Roof_Create Operands"
(
	for i in 1 to selection.count do
	(
		selection[i].selectedEdges = (polyOp.getOpenEdges selection[i])
		polyOp.createShape selection[i] selection[i].selectedEdges smooth:false name:("Operand_" + selection[i].name)
	)--End For
select $'Operand_*'
		move selection [0,0,-60]
		addModifier selection (Extrude())
		selection[1].modifiers[1].amount = 120
)
