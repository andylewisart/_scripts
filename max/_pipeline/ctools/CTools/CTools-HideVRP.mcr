macroScript HideVRP category:"CTools" toolTip:"Hide VRayProxy"
(
	if $VRayProxy*[1].ishidden == false then (hide $VRayProxy*) else (unhide $VRayProxy*)
)
