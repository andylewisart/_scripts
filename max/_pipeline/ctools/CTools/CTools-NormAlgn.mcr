macroScript NormAlgn category:"CTools" toolTip:"Align to Selected Face Normal"
(
obj = pickObject()
	if (getFaceSelection obj.mesh as array).count == 0 then (faceId = 1) else (faceId = (getFaceSelection obj.mesh as array)[1])
p = (meshOp.getFaceCenter obj.mesh faceId) + obj.pos
r = getFaceNormal obj.mesh faceId
$.pos = p
$.dir = r
) 