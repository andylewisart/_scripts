macroScript ObjPaint Category:"CTools" toolTip:"ObjPaint"
(
	sObjSel = $
	alignToSurf = true --if True then the Objects will be aligned to the surface direction
	spcr = 5  --This spacer is time based the larger the number the greater the delay..thus the greater the distance
	if selection.count > 1 then
	(
		global mnNum = 1
		global mxNum = sObjSel.count
	)--End If >
	fn geoFilt gFlt = superClassOf gFlt == GeometryClass
	srf = pickObject prompt: "Pick Surface" filter: geoFilt
	fn paint_Objects msg ir obj faceNum shift ctrl alt =
	(
		if msg == #mouseMove and ir != undefined then
		(
			shldPaint = mod (timestamp()) spcr
			if shldPaint == 1.0 then
			(
				if selection.count > 1 then
				(
					indx = random mnNum mxNum
					sObj = instance sObjSel[indx] pos:ir.pos wireColor:sObjSel[indx].wireColor
					sObj.xray = sObjSel[indx].xray
				)--End If >
				else
				(
					sObj = instance sObjSel pos:ir.pos wireColor:sObjSel.wireColor
					sObj.xray = sObjSel.xray
				)--End Else
				if alignToSurf == true then
					(
						sObj.dir = ir.dir
					)--End If alignToSurf
				)--End If shldPaint
		)--End If msg
		if msg == #mouseAbort then ok else #continue
	)--End paintObjFn
	
	mouseTrack on:srf trackCallBack:paint_Objects prompt:"Click and Drag to Spray Objects"
) 