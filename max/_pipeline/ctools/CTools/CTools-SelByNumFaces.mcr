macroScript SelByNumFaces category:"CTools" tooltip:"SelNumFaces"
(
oFCnt = selection[1].mesh.numFaces
nSel = #()
Max select all
for i in 1 to selection.count do
(
	if superClassOf selection[i] == GeometryClass and classOf selection[i] != Targetobject then
	(
		if selection[i].mesh.numFaces == oFCnt and selection[i].ishidden == false then
		(
			nSel += #(selection[i])
		)--End If
	)
)--End For
select nSel
)--End SelByNumFaces 