macroScript select_obj_by_matname
category:"Daves Tools"
tooltip:"Select Objects By Material Name"
buttontext:"Select Objects By Material Name"
silentErrors:False
(
	on execute do ( -- run script
		local scriptfile = "\\SelectObjectByMaterialName.ms"
		filein ((getdir #scripts) + scriptfile)
	)
)