rollout select_mats_wildcards_roll "Select Objects by Material Name" (
	group "Parameters" (
		edittext edt_name "" text:"Enter a material name with a *"
		checkbox chk_newMat "Assign New Material" across:2
		button btn_go "Select Objects" align:#right
	)
	progressbar prg
	
	fn calcPercent val total = (
		ret = 100.0*val/total
		prg.value = ret
	)
	fn getMatsByName = (
		collectedMats = #()
		tot = sceneMaterials.count
		inc = 0
		for m in sceneMaterials do(
			if (matchPattern m.name pattern:edt_name.text) == True then(
				append collectedMats m
			)
			inc += 1
			calcPercent inc tot
		)
		collectedMats
	)
	fn getObjects mats = (
		objs = #()
		tot = mats.count
		inc = 0
		for m in mats do (
			n = refs.dependentNodes m
			for i in n do(
				if (superClassOf i) == GeometryClass then(
					append objs i
				)
			)
			inc += 1 
			calcPercent inc tot
		)
		print objsxx
		try(select objs)catch()
		if chk_newMat.state == True then(
			a = StandardMaterial()
			for o in objs do(o.material = a )
		)
	)
	
	on btn_go pressed do(
		ret = getMatsByName()
		getObjects ret
	)
	
	
)

createDialog select_mats_wildcards_roll 300 100