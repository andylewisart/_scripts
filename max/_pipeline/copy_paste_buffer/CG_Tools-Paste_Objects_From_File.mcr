macroScript Paste_Objects_From_File Category:"Infuse" toolTip:"Paste Objects from File"
(
	if keyboard.shiftPressed then
	(

		fileIn "\\\\thanos\3D\_LIBRARY\_scripts\max\_pipeline\copy_paste_buffer\infuse_copyPaste_UI_v0.1.ms"
	)
	else
	(
		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = "\pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete stringq

		mergemaxfile (thecopypastedir + thecopypastefile) #select --this merges ("pastes") and selects objects from file
	)
)