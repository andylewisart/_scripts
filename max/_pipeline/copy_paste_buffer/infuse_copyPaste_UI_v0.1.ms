
--Destroy dialog if it already exists.
try(destroyDialog infuseCopyPaste)catch()

--Create a rollout
rollout infuseCopyPaste "Infuse Copy Paste" width:150
(
	local files = #()
	
	--Create the dotNet listview control
	dotNetControl lv "system.windows.forms.listView" height:200
	

	fn initLv theLv=
	(
		--Setup the forms view
		theLv.view=(dotNetClass "system.windows.forms.view").details
		theLv.FullRowSelect=true		--Set so full width of listView is selected and not just first column.
		theLv.GridLines=true			--Show lines between the items. 
		theLv.MultiSelect=false			--Allow for multiple selections. 
	)
	
	fn addColumns theLv columnsAr=
	(
		w=(theLv.width/columnsAr.count)-4
		for x in columnsAr do
		(
			theLv.columns.add x w
		)
	)
		
	
	fn populateList theLv=
	(
		rows=#()			--Empty array to collect rows of data
		files = getFiles "\\\\thanos\\Infuse\\People\\AndyL\\_networkCopy\\*.max"

		for i in files do
		(
			truncName = (trimright (filenamefrompath i) "_pastefile.max")
			if truncName == "Nathan" do truncName = "Chris"
			li=dotNetObject "System.Windows.Forms.ListViewItem" truncName
			append rows li	
-- 			print (trimright (filenamefrompath i) "_pastefile.max")
		)

		theLv.items.addRange rows		--Add the array of rows to the listView control. 
	)
	on lv mouseDoubleClick arg do
	(
-- 		clearListener()

		hit=(lv.HitTest (dotNetObject "System.Drawing.Point" arg.x arg.y))
-- 		print hit.item.text
-- 		print hit.item.subItems.count
-- 		print files[hit.item.index+1]
		mergemaxfile (files[hit.item.index+1]) #select --this merges ("pastes") and selects objects from file
	)
	
	
	
	on infuseCopyPaste open do
	(
		initLv lv
		addColumns lv #("User Name")
		populateList lv
	)
)

--Create a dialog and assign the rollout to it. 
createDialog infuseCopyPaste		