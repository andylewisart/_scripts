macroScript Copy_Objects_To_File Category:"Infuse" toolTip:"Copy Objects to File"
(
	if keyboard.shiftPressed then
	(
		print "network copy"
		userName = ((dotNetClass "System.Environment").GetEnvironmentVariable "UserName")

		thecopypastedir = "X:\People\AndyL\_networkCopy\\" -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = username+"_pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete string
		
		if $ != undefined do 
			saveNodes $ thecopypastestring --this saves ("copies") the objects as a file
	)
	else
	(
		print "local copy"
		thecopypastedir = getdir #autoback -- CHANGE THIS TO ANY FOLDER YOU WANT
		thecopypastefile = "\pastefile.max" --this is the filename to save as...
		thecopypastestring = thecopypastedir + thecopypastefile --this is the complete string
		if $ != undefined do 
			saveNodes $ thecopypastestring --this saves ("copies") the objects as a file
	)

	
	
)
