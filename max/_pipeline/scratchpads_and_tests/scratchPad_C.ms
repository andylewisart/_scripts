for i in (selection as array) do
(
	show i.modifers[#skin_wrap]
)

--randomize UVW offsets on UVW mods
for i in (selection as array) do
(
	rand = random 0 20
	i.modifiers[#UVWmap].gizmo.position = [rand,0,0]
)

-- do a bunch of morphs
origSel = (selection as array)
morphSel = (selection as array)
theHold.begin()
for x=1 to origSel.count do
(
	theMorpher = Morpher ()
	select origSel[x]
	modPanel.addModToSelection (theMorpher) ui:on
	WM3_MC_BuildFromNode origSel[x].morpher 1 morphSel[x]
	morphtargetmaxscriptName = getPropNames origSel[x].modifiers[#Morpher]
	paramWire.connect $crossSection_morph_ctrl.modifiers[#Attribute_Holder].Custom_Attributes[#MorphAmt] origSel[x].modifiers[#Morpher][1]"MorphAmt"
)
theHold.accept()

--Copy controllers from one hierarchy to another identical hierarchy
selA = (selection as array)
selB = (selection as array)
for i=1 to selA.count do
(
	selA[i].transform.controller = selB[i].transform.controller
)

--toggle modifiers
for i in (selection as array) do
(
	for o in i.modifiers do
	(
		if classof o == Path_Deform2 do o.enabled = not o.enabled
	)
)


-- wire path constraint parameters
mainCtrl = $
for i in (selection as array) do
(
	paramWire.connect $.pos.controller[#Percent] $stent_parent_ctrl_02.pos.controller[#Percent] "Percent+.04"
)


--add array of objects evenly spaced over a spline via path constraints
theSpline = $
theNodes = (selection as array)
itr = 100/theNodes.count
curNum = 0
for i in theNodes do
(
	pathcon = Path_constraint()
	i.position.controller = pathcon
	pathCon.follow = on
	pathCon.allowUpsideDown = on
	pathCon.path = theSpline
	pathCon.percent = curNum
	pathCon.axis = 2
	curNum += itr
)

--get world position of object
x = $.transform.pos.x
y = $.transform.pos.y
z = $.transform.pos.z 


-----------------------
--add noise to groups of objects
parents = (selection as array)
for i in parents do
(
	thisMod = Noisemodifier()
	kids =  i.children
	for o in kids do
	(
		select o
		addmodifier o thisMod
	)
)
-----------------------
--add objects to onemesh
onemesh = $
for i in (selection as array) do
(
	append onemesh.SourceObjs_Tab i
)

--Add path deform for a series of objects and splines
objs = (selection as array)
newObjs = #()
for i in objs do
(
	thisObj = copy i
	append newObjs thisObj
)
splines = (selection as array)
for i in splines do
(
-- 	select a random object in the array
	rand = random 1 (newObjs.count)
	thisObj = reference newObjs[rand]
	thisMod = Path_Deform2()
	addmodifier thisObj thisMod
	thisMod.spline = i
	thisMod.auto_stretch = on
	thisMod.autoAmount = .5	
)


--projects UVs from old meshes to new (retopologized) meshes
originalMeshes = (selection as array)
newMeshes = (selection as array)

for i in newMeshes do
(
	for j in originalMeshes do
	(
		if (matchPattern i.name pattern:(j.name+"_new"))==true do 
		(	
-- 			thisMod = Projection ()
-- 			addmodifier j thisMod
			thisMod = addPModObjects j true true objList:i
			select j
			j.modifiers[1].addRegisteredProjectionType 1
			j.modifiers[1].projectAll() 
			i.material=j.material
		)
	)
)






--macroscript RandomUvwOffset category:"decon tools"
-- randomize uv coordinate offset, usage: 
-- 1.map polys
-- 2.add unwrap_uvw mod.
-- 3.select obj
-- 4.run script
--
-- by decon at www.deconstudio.net 2008
-- lots of copy/paste from michael brempel's normalize_uv_elements.ms 2004

--global RandUVOffs
try(destroyDialog  RandUVOffs)catch()
rollout RandUVOffs "Random UV Offset"
(
	radiobuttons UVaxis labels:#("U","V","UV") default:3
	button btnOffset "Randomize!"
	button btnReset "Reset to Original Values"
	
	fn randOffset k = (
		offset = [0,0,0]
		if k == 3 do (offset[1] = random 0.0 1.0; offset[2] = random 0.0 1.0; return offset)
		offset[k] = random 0.0 1.0
		return offset
	)


	on btnOffset pressed do
	(
		if selection.count == 1 and (try($.unwrapuvw)catch(undefined)) != undefined  do (
		local moduvw = selection[1].modifiers[#unwrap_uvw]
		local vcnt=moduvw.unwrap.numbervertices()
		moduvw.unwrap.selectVertices #{1..vcnt}

		-- count uv elements
		local vertelemarray=#()
		for i=1 to vcnt do vertelemarray[i]=0
		local elem=0
		modPanel.setCurrentObject moduvw
		subobjectlevel = 1
		---------------------------------------------------
		for v=1 to vcnt do
		(
			if(vertelemarray[v]==0)then
			(	-- vertex v is not in array yet
				moduvw.unwrap.selectVertices #{v}
				moduvw.unwrap2.SelectElement()
				elem+=1
				elemverts=moduvw.unwrap.getselectedvertices()

				for i in elemverts do
				(	-- which vertices go to which element, vert[1]=vert[2]= ... =1 , vert[7]=vert[8]= ... =2 , ...
					vertelemarray[i]=elem
				)
			)
			
		)
		subobjectlevel = 0
		--amount of elements
		--print elem as string
		-----------------------------------------------------
		local elemarray=#()
		for e=1 to elem do
		(
			local notFound = true
			for v=1 to vertelemarray.count while notFound do
			(
				if vertelemarray[v]==e then
				(	-- found vertex index 
					elemarray[e]=v
					notFound = false
				)
			)
		)
		--with this we get element from vertex
		--print elemarray as string
		---------------------------------------------------
		modPanel.setCurrentObject moduvw
		subobjectlevel = 1

		for i=1 to elemarray.count do
		(	-- go through all elements
			moduvw.unwrap.selectVertices #{elemarray[i]}
			moduvw.unwrap2.SelectElement()
			elemverts=moduvw.unwrap.getselectedvertices()
			koe = [0,0,0]
			-- new offset to every element
			offset = randOffset UVaxis.state
			for v in elemverts do
			(	-- go through all vertices in element and offset them
				uvwpos = moduvw.unwrap.getVertexPosition 0 v
				uvwpos += offset
				moduvw.unwrap.setVertexPosition 0 v uvwpos
			)
		)
		subobjectlevel = 0
	)--if selection
	)--button

	on btnReset pressed do
	(
		local moduvw = selection[1].modifiers[#unwrap_uvw]
		moduvw.unwrap.reset()
	)

)--rollout
CreateDialog RandUVOffs pos:[20,100] width:200 height:100

---------------------------------
--apply UVW map to selection
for i in (selection as array) do
(
	thisMod = Uvwmap ()
	addmodifier i thisMod
	thisMod.maptype = 4
)

---------------------------------
sels = (selection as array)
sels[1].transform = sels[2].transform


sels1 = (selection as array)
sels2 = (selection as array)
for x=1 to sels1.count do
(
	sels1[x].transform = sels2[x].transform
)


------------------------------------
notEnd = true
for x=1 to 1000 while notEnd do
(
	bananas = $[4][4][2][5][2][8][x]
	if  bananas != undefined then
	(
		if classof bananas.controller == SpringPoint3Controller do
		(
				bananas.controller.spring.setmass 100
				bananas.controller.spring.setdrag  100
				bananas.controller.spring.setDampening 1 .01
				bananas.controller.spring.setTension 1 .1
		)
	)
	else
	(
		print "done"
		notEnd = false
	)
)


--------------------------------------
--spline knot selection stuff
getknotselection $ 1

indexArray = #()
for x=61 to 86 do
(
	append indexArray x
)
setknotselection $ 1 indexArray 



--------------------------------
--traincar-like path constrants
thisPath = $
for i in (selection as array) do
(
	pathCon = Path_Constraint ()
	i.controller.pos.controller = pathCon
	pathCon.follow = on
	pathCon.path = thisPath
	
	addmodifier thisPath ('PEN_Attribute_Holder 2'())
	
)

for i in (selection as array) do(
-- 	bob = (format "[%.controller.position.controller.y_position*.001,%.controller.position.controller.y_position*.001,%.controller.position.controller.y_position*.001] \n 0" ("$"+i.name) ("$"+i.name) ("$"+i.name) )
	bob =  "[$"+i.name+".controller.position.controller.y_position*.005,"+"$"+i.name+".controller.position.controller.y_position*.005,"+"$"+i.name+".controller.position.controller.y_position*.005] \n "
-- 	print bob
		i.controller.scale.controller.script = bob
-- 	(format "[%.controller.position.controller.y_position*.01,%.controller.position.controller.y_position*.01,%.controller.position.controller.y_position*.01]" ("$"+i.name) ("$"+i.name) ("$"+i.name))
)



-------------------------------------
--add script controller
for i in (selection as array) do
(
	i.controller.scale.controller = scale_script()
)

for i in (selection as array) do(
-- 	bob = (format "[%.controller.position.controller.y_position*.001,%.controller.position.controller.y_position*.001,%.controller.position.controller.y_position*.001] \n 0" ("$"+i.name) ("$"+i.name) ("$"+i.name) )
	bob =  "[$"+i.name+".controller.position.controller.y_position*.005,"+"$"+i.name+".controller.position.controller.y_position*.005,"+"$"+i.name+".controller.position.controller.y_position*.005] \n "
-- 	print bob
		i.controller.scale.controller.script = bob
-- 	(format "[%.controller.position.controller.y_position*.01,%.controller.position.controller.y_position*.01,%.controller.position.controller.y_position*.01]" ("$"+i.name) ("$"+i.name) ("$"+i.name))
)
--add bezier controller
for i in (selection as array) do
(
i.controller.scale.controller =bezier_scale ()
i.controller.scale = [1,1,1]
)
-----------------------------------
thisarray = (selection as array)
for x=2 to (thisarray.count) do
(
	thisarray[x].controller.position.controller.y_position = thisarray[x-1].controller.position.controller.y_position +500

)


-----------------------------------
selObj = selection as array
for j in selObj do (
	faces = polyOp.getNumFaces j
	theArea = 0
	for x=1 to faces do(
		theArea += polyop.getFaceArea j x
	)
	print theArea
	if theArea < 300 do delete j
)
----------------------
for x=9 to 56 do
(
	skinOps.SelectBone $PM3D_Sphere3D_2.modifiers[#Skin] x
	skinOps.blendSelected $PM3D_Sphere3D_2.modifiers[#Skin]
)
------------------------
-- wave modifier actions
for i in (selection as array) do
(
-- 	i.modifiers[#Wave].wavelength = .5
	i.modifiers[#Wave].amplitude1 = 2
)


-----------------------
-- script controller formula for trajectory constraint
at time (F+20) obj.position
-- then do a look constraint to object with script controller

---------------
-- reselect random percentage of selected objects
newArray=#()
for i in (selection as array) do
(
	rand = random 0 100
	if rand > 50 do append newArray i
)
select newArray

------------
-- add instance of initial selection as child of post-selection
thisObj = $
for i in (selection as array) do
(
	maxops.clonenodes thisObj actualNodeList:&c newNodes:&d clonetype:#instance
	d.transform = i.transform
	d.parent = i
)



---------------
--modify spring controller params
for i in (selection as array) do
(
	i.pos.controller.spring.setmass 300
	i.pos.controller.spring.setdrag  1
	i.pos.controller.spring.setDampening 1 .5
	i.pos.controller.spring.setTension 1 .5
)


---------------------------------------------
-- add link constraint without losing position
initialParent = $parentCtrl_cart_01
for i in (selection as array) do
(
	thisPos = i.transform
	i.transform.controller = Link_Constraint()
	i.transform.controller.AddTarget initialParent 0
	i.transform = thisPos
)

addParent = $
for i in (selection as array) do
(
	i.transform.controller.AddTarget addParent 2100
)

---
for i in (selection as array) do
(
	thisPos = i.position.controller.position.controller
	i.position.controller = thisPos
-- 	i.position = thisPos
)
---------------------------------------------
-- change parent without losing position on animated objects
newParent = $
for i in (selection as array) do
(
	thisPos = i.transform
	i.parent = newParent
	i.transform = thisPos
)
--------------------------------------------
--change pos controller to posXYZ without losing position
-- change parent without losing position on animated objects
newParent = $
for i in (selection as array) do
(
	thisPos = i.transform
	i.position.controller = position_xyz()
	i.parent = newParent
	i.transform = thisPos
)
---------------------------------------------
--save envelopes from skin modifiers
for i in(selection as array) do
(
	for o in i.modifiers do
	(
		print (classof o)
		if classof o == Skin do
		(
			print "DERP"
			modPanel.setCurrentObject o
			skinOps.saveEnvelope o ("\\\\thanos\\3d\\Merz\\1710_ULS_VR_App_Phase_1\\Max\\Assets\\env\\"+i.name+".env")
		)
	)
)
-----------------------------
--add new skin to object, add bones, add saved envelopes
fn ListSkinBones obj skinmod toNode:false = 
( 
    skinMod
    bonesList = #()
    result

    clearSelection()
    max modify mode
    hiddenState = obj.ishidden 
    obj.ishidden = false
    select obj
--     skinMod = obj.modifiers[#skin]
--     modPanel.setCurrentObject obj.modifiers[#Skin]
	modPanel.setCurrentObject skinMod
    bonesList = for i=1 to (skinOps.GetNumberBones skinMod) collect (skinOps.GetBoneName skinMod i 0)
    obj.ishidden = hiddenState 
    result = bonesList 
    if toNode == true do 
    (
        result = for i in bonesList collect (getNodebyName(i) ) 
    )
    result
)

for i in (selection as array) do
(
	theseBones = #()
	isSkin = false
	select i
	for o in i.modifiers do
	(
		if classof o == Skin do
		(
			o.enabled = off
			o.name = o.name+"_old"
			theseBones = (ListSkinBones i o toNode:true)
			isSkin = true
-- 			print theseBones
		)
	)
	if isSkin do
	(
		newBonesLayer = layermanager.getlayerfromname("MerzMan_exportBones")
		newBonesLayer.nodes &newBones
		newBonesList = #()
		for k in theseBones do
		(
			for j in newBones do
			(
				if j.name == k.name do append newBonesList j 
			)
			
		)
-- 		select newBonesList
		derp = Skin ()
		derp.name+="_new"
		modPanel.addModToSelection (derp)
		for o in newBonesList do
		(
			skinOps.addBone derp o 1
		)
		skinOps.loadEnvelope derp ("\\\\thanos\\3d\\Merz\\1710_ULS_VR_App_Phase_1\\Max\\Assets\\env\\"+i.name+".env")
	)
)
------------------------------------------------
-- parent a bone to each object in array
for i in (selection as array) do
(
	thisbone = boneSys.createBone [0,0,0] [0,5,0] [0,0,5]
	
	thisbone.name = i.name--+"_new"
-- 	thisbone = i.transform
	thisbone.transform.controller = Link_Constraint()
	thisbone.transform.controller.AddTarget i 0
	thisbone.transform = i.transform
-- 	i.transform = thisPos
-- 	thisbone.parent = i
)
--------------------------------------
showproperties $.rotation.controller.LookAt_Constraint


$.transform.controller.AddTarget $Object038 78

for i in $.modifiers do
(
	modName = i
	print( classof i)
)

--remove all groups
for i in objects do
(
	if isGroupMember i do
	(
		print i.name
		ungroup i
	)
)

thismod = $.modifiers[1]
for i in (selection as array) do
(
	derp = Uvwmap ()
	select i
	modPanel.addModToSelection (derp)
	derp.maptype = 4
)



--select skinning bones
skinBones = (selection as array)
--select skin mod objects
for i in (selection as array) do
(
	select i
	for o in skinBones do skinOps.addbone i.modifiers[#Skin] o 0
)

for i in (selection as array) do
(
	select i
	skinOps.voxelWeighting $.modifiers[#Skin] 1 0.7 4 0 off on
)

--randomize transform values
for i in (selection as array) do
(
	randLimit = 20
	xrand = random (randLimit*-1) randLimit
	yrand = random (randLimit*-1) randLimit
	zrand = random (randLimit*-1) randLimit
	i.position = i.position + [xrand,yrand,zrand]
-- 	randLimit = 360
-- 	xrand = random (randLimit*-1) randLimit
-- 	yrand = random (randLimit*-1) randLimit
-- 	zrand = random (randLimit*-1) randLimit
-- 	i.rotation = i.rotation + [xrand,yrand,zrand]
-- 	randLimit = .1
-- 	rotate i (eulerangles (random 0 360) (random 0 360) (random 0 360))
-- 	xrand = random (randLimit*-1) randLimit
-- 	yrand = random (randLimit*-1) randLimit
-- 	zrand = random (randLimit*-1) randLimit
-- 	i.scale = i.scale+[xrand,yrand,zrand]
-- 	i.scale = i.scale+[xrand,xrand,xrand]
)

for i in (selection as array) do
(
	randLimit = 1
	yrand = random (randLimit*-1) randLimit
	i.position.controller.y_position= i.position.controller.y_position + yrand
)

-- set up path constraint controller for group of objects
thisPath = $Line001
for i in (selection as array) do
(
	i.pos.controller = Position_XYZ ()
	newListCon = position_list ()
	newPathCon = Path_Constraint ()
-- 	newPosCon = 
	i.pos.controller = newListCon
	newListCon.available.controller = newPathCon
	newPathCon.path = thisPath
	newPathCon.percent = (random 0.0 10.0)
)

thisPath = $
for i in (selection as array) do
(
	newListCon = position_list ()
	newPosCon = Position_XYZ ()
-- 	newPosCon = 
	i.pos.controller = newListCon
	newListCon.available.controller = newPosCon

)

--wire path constraint amount to attribute holder
for i in (selection as array) do
(
-- 	paramWire.connect i.pos.controller.weight[1] $stentPath.modifiers[#PEN_Attribute_Holder_2].Custom_Attributes.Pathconstraint_Amt "PathConstraintAmt"
	paramWire.connect2way i.pos.controller[#Weights][#Weight__Path_Constraint] $stentPath.modifiers[#PEN_Attribute_Holder_2].Custom_Attributes[#Pathconstraint_Amt] "Pathconstraint_Amt" "Weight__Path_Constraint"
)

for i in (selection as array) do
(
	select i
-- 	print selection
	itr = 1
	for o in $.modifiers do(
		print o
		if classof o ==  Vol__Select do
		(
			o.falloff = 1
			o.pinch = .8
		)
			itr+=1
			
	)
)

for i in (selection as array) do
(
	select i
	print selection
	itr = 1
	for o in $.modifiers do(
		print o
		if classof o ==  Skin_Wrap do deletemodifier o
			itr+=1
			
	)
)

for o in geometry do if superClassOf o == geometryClass do 
(
	for theModifier in o.modifiers where classOf theModifier == opensubdiv do
	(
		deleteModifier o theModifier
	)
)


for i in (selection as array) do
(
	select i
-- 	print selection
	itr = 1
	for o in $.modifiers do(
		print o
		if classof o ==  Morpher do o[1].value = 50
		itr+=1
			
	)
)
for i in (selection as array) do
(
	select i
-- 	print selection
	itr = 1
	for o in $.modifiers do(
		print o
		if classof o ==  Morpher do o.reloadSelected 1
		itr+=1
			
	)
)

for i in (selection as array) do
(
	select i
	itr = 1
	for o in $.modifiers do(
		print o
		if classof o ==  VRayDisplacementMod do o.amount = 3
		itr+=1
			
	)
)

for i in (selection as array) do
(
	select i
	i.modifiers[1].pressure = 1

)

for i in (selection as array) do
(	
	select i
	itr = 1
	for i in $.modifiers do(
		if i.name == "OpenSubdiv" do 
		(
			turbo = turbosmooth()
			deletemodifier $ itr
			addmodifier $ turbo
			turbo.iterations = 0
			turbo.useRenderIterations = on
			turbo.renderIterations = 2
			itr+=1
		)
	)
	
-- 	modPanel.addModToSelection (planarMod)
)

results = #()
for o in (selection as array) do
(	
	select o
	for i in $.modifiers do(
		if i.name == "OpenSubdiv" do i.enabled = true
			
	)
	
-- 	modPanel.addModToSelection (planarMod)
)
print results


for o in (selection as array) do
(	
	select o
	if o.modifiers[2].name == "Skin" do deletemodifier $ 2
)


usedBones = #()
clearListener()
for o in (selection as array) do
(	
	for i in o.modifiers do(
		if i.name == "Skin" do 
		(
			for y=1 to (skinOps.GetNumberBones i)  do
			(
				stopIt = 1
				print 
				for x=1 to (skinOps.GetNumberVertices i) while stopIt !=0 do
				(
-- 					if (skinOps.GetVertexWeight i x y) != 0 do append usedBones (skinOps.GetBoneName i y 1)
-- 					stopIt = 0
					print y
					print(skinOps.GetVertexWeightBoneID i x y)
-- 					print (skinOps.GetVertexWeight i x (skinOps.GetVertexWeightBoneID i x y))
-- 					stopIt = 0
				)
			)
		)
			
	)
)
print usedBones

derp = #(1,2,3)
if (findItem derp 1)==0 do print "FART"



clearlistener()
usedBones = #()
for o in (selection as array)do
(
	select o
 	skinMod = $.modifiers["skin"]
 	for i = 1 to (skinOps.GetNumberVertices skinMod) do
 	(
--  		format ("Vertex % \n")i
 		for j = 1 to (skinOps.GetVertexWeightCount skinMod i) do
 		(	
 			boneID = (skinOps.GetVertexWeightBoneID skinMod i j)
 			boneName = (skinOps.GetBoneName skinMod boneID 0)
 			VertWeight = (skinOps.GetVertexWeight skinMod i j)
--  			format ("   Bone% % %\n")boneID boneName VertWeight
-- 			print (findItem usedBones boneID)
			if (findItem usedBones boneName)==0 do append usedBones boneName
 		)
--  		format("\n")
 	)
	thisNum=0
	for i=1 to skinOps.getNumberBones skinMod do
	(
		thisNum+=1
		if (findItem usedBones (skinOps.getBoneName skinMod thisNum 0))==0 do
		(

			skinOps.selectBone skinMod thisNum
			skinOps.removeBone skinMod
			thisNum-=1
		)
	)
	usedBones = #()
 )


fn removeNotBones sk = 
(
	print sk
nodes = for n in (refs.dependson sk) where isvalidnode n and not iskindof n BoneGeometry collect n
names = for k=1 to (skinops.getnumberbones sk) collect (skinops.getbonename sk k 0)

for node in nodes where (k = finditem names node.name) != 0 do
(
skinops.removebone sk k
deleteitem names k
)
)
removeNotBones $.modifiers[#Skin]

for i in (selection as array) do
(	
	select i
	itr = 1
	for i in $.modifiers do(
		if i.name == "Skin" do deletemodifier $ itr
			itr+=1
			
	)
	
-- 	modPanel.addModToSelection (planarMod)
)


clearlistener()
for o in (selection as array)do
(
	select o
	thisFile = "\\\\pacman\\Projects\\Perineologic\\1601_Animation\\assets\\envelopes\\v001\\"
 	skinMod = $.modifiers["skin"]
	skinOps.saveEnvelope skinMod (thisFile+o.name+".env")
)

clearlistener()
for o in (selection as array)do
(
	select o
	thisFile = "\\\\pacman\\Projects\\Perineologic\\1601_Animation\\assets\\envelopes\\v001\\"
 	skinMod = $.modifiers["skin"]
	skinOps.loadEnvelope skinMod (thisFile+o.name+".env")
	gc()
)

disableSceneRedraw();
undo off
for o in (selection as array)do
(
	thisFile = "\\\\pacman\\Projects\\Perineologic\\1601_Animation\\assets\\envelopes\\v001\\"
 	skinMod = o.modifiers["skin"]
	suspendEditing which:#modify
	skinOps.loadEnvelope skinMod (thisFile+o.name+".env")
	resumeEditing which:#modify
	gc()
)
undo on
enableSceneRedraw() 

thismod = Noisemodifier()
for i in (selection as array) do
(	
		select i
	itr = 1
	hasIt = False
	for i in $.modifiers do(
		if i.name == "Noise" do hasIt = True
		itr+=1
	if hasIt == False do modPanel.addModToSelection (thismod)
)

thismod = Noisemodifier()
for i in (selection as array) do
(	
	modPanel.addModToSelection (thismod)
)


for i in (selection as array) do
(
	select i
	skinOps.voxelWeighting $.modifiers[#Skin] 1 0.7 4 0 off on
)


--Convert skin wraps to skins
theHold.begin()
didIt=0
for i in (selection as array) do
(	
	try
	(
		select i
		itr = 1
		for i in $.modifiers do(
			print $.name
			print i.name
			if i.name == "Skin Wrap" do 
			(
				if (InstanceMgr.CanMakeModifiersUnique $ i) and didIt != 1 then
				(
					InstanceMgr.MakeModifiersUnique $ i #individual
	-- 				didIt=1
				)
			)
		)
		for i in $.modifiers do(
			print $.name
			print i.name
			if i.name == "Skin Wrap" do 
			(
				i.meshDeformOps.convertToSkin off
				deletemodifier $ i
				$.modifiers[#Skin].cross_radius = 5.25
				$.modifiers[#Skin].mirrorEnabled = off
	-- 			addModifier $ (Skin ())
			)
		)
	)
	catch()
-- 	modPanel.addModToSelection (planarMod)
)
theHold.Accept "UndoExecution"



for i in (selection as array) do
(	
-- 	select i
	hasIt = False
	itr = 1
	modCount = i.modifiers.count
	print modCount
	for j in i.modifiers do(
		print j.name
		if j.name == "UVW Mapping Paste" then
		(
			theIndex = itr
			hasIt = True
			
		)
		else (itr+=1)
	)
	if hasIt == True do
	(
		if modCount > 1 do
		(
			firstMod = i.modifiers[1]
			deletemodifier i 1
			addModifier i  (firstMod) before:modCount
			
		)
		maxOps.CollapseNodeTo i modCount on
		print theIndex
		print "%%"
	)
)


show $.modifiers[1].Custom_Attributes
getpropnames $.modifiers[1].Custom_Attributes
print $.modifiers[1].Custom_Attributes[1].value

==================
--select fur and run this line:
fur = selection as array
print fur
--select fur source nodes and run the rest of the code
sel = selection as array
print sel
for j in fur do
(
	for i in sel do
	(
		newHair = copy j
		newHair.transform = i.transform
		j.sourcenode = i
	)	
)

==================
addMod = $.modifiers[1]

for i in (selection as array) do
(	
	select i


	addModifier $ addmod
	

)
==================
modArray = #()
for i in $.modifiers do
(
	append modArray i
)
print modArray
for i in (selection as array) do
(	
	select i
	itr = 1
	for j in modArray do
	(
		addModifier $ j
	)

)



==================
--boneShape stuff
for i in (selection as array) do
(
	i.controller.bone.limb.layerIKFKRatio = 1
)

==================
--parents first array to second array
sel1 = selection as array
sel2 = selection as array
itr=0
for i in sel1 do 
(
	itr+=1
	sel1[itr].parent = sel2[itr]
)
==================
--resets xform on selected and collapses stack	
sel = selection as array
for i in sel do
(
	resetxform i
	maxOps.CollapseNode i off;
)

=========
for i in (selection as array) do
(
	con = CATHDPivotTrans ()
	i.transform.controller = con
)

--Relocates selection's CATHDPivots to a specified node

thisPivot = $Star001

sel = selection as array
for i in sel do
(
	i.transform.controller.pivot = (thisPivot.transform * (inverse i.transform)).pos
)

--return CATHDpivot to object's own pivot
for i in sel do
(
	i.transform.controller.pivot = [0,0,0]
)


===============
testBone = $
for i in (selection as array) do
(
	thisBone = copy testBone
	thisBone.transform = i.transform
	thisBone.parent  = i
	
)



==========
for i in (selection as array) do
(	
	select i
	for i in $.modifiers do(
		deletemodifier $ $.modifiers.Count

	)
	for i in $.modifiers do(
		deletemodifier $ $.modifiers.Count

	)
	
	$.controller.position = [0,0,0]
	$.controller.rotation = (quat 0 0 0 1)
	$.controller.scale = [1,1,1]
	
-- 	modPanel.addModToSelection (planarMod)
)

print $.modifiers.Count

show $.controller.scale


======================
--- replace selection with instances of thisObj
thisObj = $
thisArray = (selection as array)

for i in thisArray do
(
	newObj = copy thisObj
	newObj.parent = i.parent
	newObj.transform = i.transform
	
)




=========================

thisObj = $
for i in (selection as array) do
(
	instanced = instance thisObj
	instanced.transform = i.transform
	delete i)

======================
--batch importer

fn checkDialog = (
local BM_SETCHECK = 0xF1
local BM_CLICK = 0xF5
local hwnd = dialogMonitorOps.getWindowHandle()

if (uiAccessor.getWindowText hwnd == "Import Name Conflict") then (
	uiAccessor.PressButtonByName hwnd "OK"
)
if (uiAccessor.getWindowText hwnd == "OBJ Import Summarys") then (
	clearlistener()
	print (UIAccessor.GetChildWindows hwnd )
	uiAccessor.PressButtonByName hwnd "Close"
)
if (uiAccessor.getWindowText hwnd == "OBJ Import Options") then (
	clearlistener()
-- 	print "DERP"
-- 	print (UIAccessor.GetChildWindows hwnd )
	windows.sendMessage (windows.getChildrenHWND hwnd)[7][1] BM_CLICK 0 0
	windows.sendMessage (windows.getChildrenHWND hwnd)[7][1] BM_SETCHECK 1 0
-- 	windows.sendMessage (windows.getChildrenHWND hwnd)[38][1] BM_CLICK 0 0
-- 	windows.sendMessage (windows.getChildrenHWND hwnd)[38][1] BM_SETCHECK 1 0
-- 	print (windows.getChildrenHWND hwnd)
-- 	uiAccessor.PressButtonByName hwnd "Import"
	dialogMonitorOps.unRegisterNotification id:#test
	dialogMonitorOps.enabled = false
)
true
)



makeLayers = False
files = getFiles "N:\Projects\_library\_models\_kitbash\Decimated_Mech_Set_02_OBJs\*.obj"
print files
for f in files do (
if makeLayers do
(	
	thisLayer = LayerManager.newLayer() 
	thisLayer.current = True
)
dialogMonitorOps.enabled = true
dialogMonitorOps.unRegisterNotification id:#test
dialogMonitorOps.registerNotification checkDialog id:#test
importFile f --#noPrompt
dialogMonitorOps.enabled = false
) 
=========

sel = selection as array
dimensionCount = (floor(sqrt sel.count))+1
format "dimensionCount % "dimensionCount 
itr = 1
xDim = 0
yDim = 0

for x=1 to dimensionCount do
(
	for y=1 to dimensionCount do
	(
		format "itr % \n"itr 
		format "sel[itr] % \n"sel[itr] 
-- 		thisPos = [(xDim as float),(yDim as float),0.0]
		try(
		sel[itr].controller.position.controller.x_position = (xDim as float)
		sel[itr].controller.position.controller.y_position = (yDim as float)
		sel[itr].controller.position.controller.z_position = 0.0
		)
		catch()
		itr+=1
		
		yDim+=50
	)
	yDim = 0
	xDim+=50
)
show $.controller.position.controller.x_position



=========
--instance replace.
thisNode = $
sel = (Selection as array)
for obj in sel do instancereplace obj thisNode


============
+++++++++++++=
--Setup a scene
b1 = box name:"box01" pos:[-32.5492,-21.2796,0] -- create two boxes
b2 = box name:"box02" pos:[51.3844,-17.2801,0]
animate on at time 100 b1.pos = [-48.2522,167.132,0]-- animate position of one box
--
--Assign a reactor, pick the react to object, and create reactions
cont = b2.pos.controller = position_Reactor ()
--
--you can either react to a controller
reactTo cont b1.pos.controller
--or a node (the World Space position of the box)
--reactTo cont b1
--
slidertime = 100
createReaction cont
slidertime = 50
createReaction cont
deleteReaction cont 3
--
--Set the reaction parameters
setReactionState cont 2 [65.8385,174.579,0]
selectReaction cont 1
setReactionInfluence cont 1 100
setReactionStrength cont 1 1.2
setReactionFalloff cont 1 1.0
setReactionValue cont 1 [-40.5492,-20.0,0]
setReactionName cont 1 "My Reaction"
--
--get the reaction parameters
getReactionInfluence cont 1
getReactionStrength cont 1
getReactionFalloff cont 1
getReactionState cont 1
getReactionValue cont 1
getSelectedReactionNum cont
getReactionCount cont
getReactionName cont 1



print $.controller.keys
print $.pos.x_position.controller.keys