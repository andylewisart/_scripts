fn setUICheckboxState hwnd state =
(
/*******************************************************************************
    <DOC> toggle an UI checkbox's checked state via UI messages/notifications
    Arguments:
        <int> hwnd:         HWND of the control
        <int> state:        checked state.  1 = check, 0 = uncheck
        <skin modifier> curObj:     The selected skin modifier (must be in modify panel)
    Return:
        <ok>  should check/uncheck the checkbox and have its change effected
*******************************************************************************/
    local BN_CLICKED = 0 -- clicky message ID
    local BM_SETCHECK = 241 -- checkbutton toggle message ID
    local WM_COMMAND = 273 -- windows command message
 
    local parent = UIAccessor.getParentWindow hwnd
    local id = UIAccessor.getWindowResourceID hwnd
    windows.sendMessage hwnd BM_SETCHECK state 0
    windows.sendMessage parent WM_COMMAND ((bit.shift BN_CLICKED 16) + id) hwnd
    OK
),
 
 
fn confirmLoadEnvelopes removeIncomingPrefix:0 removeCurrentPrefix:0 =
(
/*******************************************************************************
    <DOC> Manipulate the Load Envelopes dialog via the UI Accessor.
    Arguments:
        <int> removeIncomingPrefix:     Corresponds to the "Remove Incoming Prefix" checkbox.  0 is false, 1 is true
        <int> removeCurrentPrefix:      Corresponds to the "Remove Current Prefix" checkbox.  0 is false, 1 is true
    Return:
        <bool> true (needed for DialogMonitorOps)
*******************************************************************************/
    --local BM_SETCHECK = 241
    local hwnd = dialogMonitorOps.getWindowHandle()
    if (uiAccessor.getWindowText hwnd == "Load Envelopes") then
    (
        local children = windows.getChildrenHWND hwnd
        for child in children do
        (
            if (child[5] == "Remove Incoming Prefix") then
            (
                setUICheckboxState child[1] 1
            )
            else if (child[5] == "Remove Current Prefix") then
            (
                setUICheckboxState child[1] 1
            )
        )
        
        UIAccessor.PressButtonByName hwnd "Match by Name"
        forceCompleteRedraw()
        UIAccessor.PressButtonByName hwnd "OK"
        --UIAccessor.PressDefaultButton()
    )
    true
),
 
fn confTT = (confirmLoadEnvelopes removeIncomingPrefix:1 removeCurrentPrefix:1),
fn confTF = (confirmLoadEnvelopes removeIncomingPrefix:1 removeCurrentPrefix:0),
fn confFT = (confirmLoadEnvelopes removeIncomingPrefix:0 removeCurrentPrefix:1),
fn confFF = (confirmLoadEnvelopes removeIncomingPrefix:0 removeCurrentPrefix:0),
 
fn loadEnvelope theSkin envFile removeIncomingPrefix:false removeCurrentPrefix:false =
(
/*******************************************************************************
    <DOC> Load an .env file.  There is no function for silently loading an .env, so this
    is a UI Accessor workaround.
    Arguments:
        <skin modifier> theSkin:        The selected skin modifier (must be in modify panel)
        <string>    envFile:                    Filename of the .env file.
    Return:
        <ok>
*******************************************************************************/
    --determine which confirmLoadEnvelopes to use
    local confirmFn = case of
    (
        (removeIncomingPrefix and removeCurrentPrefix):confTT
        (removeIncomingPrefix and not removeCurrentPrefix):confTF
        (not removeIncomingPrefix and removeCurrentPrefix):confFT
        (not removeIncomingPrefix and not removeCurrentPrefix):confFF
    )
    
    DialogMonitorOps.Enabled = true --DialogMonitorOps.Enabled = false
    DialogMonitorOps.RegisterNotification confirmFn id:#pressSkinOK
    skinOps.LoadEnvelope theSkin envFile
    DialogMonitorOps.unRegisterNotification id:#pressSkinOK
    DialogMonitorOps.Enabled = false
    ok
),




python.execute("

from PySide.QtCore import QFile, SIGNAL, Qt,QCoreApplication
from PySide.QtGui import QBrush,QDialog,QListWidget, QApplication,QFileDialog,QPushButton,QLineEdit,QLabel,QRadioButton,QComboBox,QProgressBar,QPlainTextEdit,QMessageBox,QToolTip,QFont
from PySide.QtUiTools import QUiLoader

for i in sys.path:
	print i
import sys

try:
    sys.path.remove(r'S:/outfit/developers/andylewis/git/fusefxpipeline3-0/FFX/standalone/assetPublishing/')

except:
    sys.path.append(r'S:/outfit/developers/andylewis/git/fusefxpipeline3-0/FFX/standalone/assetPublishing/')


sys.path.append(r'S:/outfit/developers/andylewis/git/fusefxpipeline3-0/FFX/standalone/assetPublishing/')


import tests.pyside.pysideTest_v001
reload(tests.pyside.pysideTest_v001)

import MaxPlus
import os

#filename = os.path.basename(fm.GetFileNameAndPath())

app = QApplication.instance() 

if app == None: 
    app = QApplication([''])
app.setStyle('plastique')
ui = tests.pyside.pysideTest_v001.PlayblastTool()

#ui.testString = '\" +myArg + \"'

#ui.setWindowTitle('%s'%(filename))
#ui.show()

")