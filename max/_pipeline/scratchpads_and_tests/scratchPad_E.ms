--Setup a scene
b1 = box name:"box01" pos:[-32.5492,-21.2796,0] -- create two boxes
b2 = box name:"box02" pos:[51.3844,-17.2801,0]
animate on at time 100 b1.pos = [-48.2522,167.132,0]-- animate position of one box
--
--Assign a reactor, pick the react to object, and create reactions
cont = b2.pos.controller = position_Reactor ()
--
--you can either react to a controller
reactTo cont b1.pos.controller
--or a node (the World Space position of the box)
--reactTo cont b1
--
slidertime = 100
createReaction cont
slidertime = 50
createReaction cont
deleteReaction cont 3
--
--Set the reaction parameters
setReactionState cont 2 [65.8385,174.579,0]
selectReaction cont 1
setReactionInfluence cont 1 100
setReactionStrength cont 1 1.2
setReactionFalloff cont 1 1.0
setReactionValue cont 1 [-40.5492,-20.0,0]
setReactionName cont 1 "My Reaction"
--
--get the reaction parameters
getReactionInfluence cont 1
getReactionStrength cont 1
getReactionFalloff cont 1
getReactionState cont 1
getReactionValue cont 1
getSelectedReactionNum cont
getReactionCount cont
getReactionName cont 1




bezierController = Bezier_Float()
if $.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller == undefined do $.modifiers[#Attribute_Holder].Custom_Attributes.Param2.controller = bezierController
print $.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller

cont = $Teapot001.pos.controller = position_Reactor ()
reactTo cont $Circle001.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller

createReaction cont

setReactionState cont 1 [65.8385,174.579,0]

print $.modifiers[#Bend].BendAngle
bezierController = Bezier_Float()
if $.modifiers[#Bend].BendAngle.controller == undefined do $.modifiers[#Bend].BendAngle.controller = bezierController

cont = $.modifiers[#Bend].BendAngle.controller = Float_Reactor ()
reactTo cont $Circle001.modifiers[#Attribute_Holder].Custom_Attributes.Param2.controller

createReaction cont

setReactionState cont 2 [-65.8385,-174.579,0]
setReactionValue cont 2 100



fn recursiveExample prop propName carryOn = (

	propName = propName + (prop as string)
	propName = substituteString propName "Controller:" "."
-- 	if prop != undefined do print(getpropnames prop)
	if carryOn == false do
	(
		print propName
		show prop
	)		
	try 
	(
		bob = getpropnames prop
		for i in bob do
		(
			bill = getPropertyController prop i 
			if bill != undefined then recursiveExample bill propName carryOn
			else
			(				
				carryOn = False
				recursiveExample i ("."+propName) carryOn
			)
		)
	)
	catch()
)

propName = ""
recursiveExample $.transform.controller propName true

for item in refs.dependsOn bob do
(
	
bob =  $.pos.controller.Position_XYZ.controller.X_Position.controller
bob =  float_Reactor ()
print bob
refs.dependents bob

getSubAnimNames bob
refs.dependents bob[1]
show bob
bob = getpropnames $.transform.controller.position.controller.position_xyz.controller.x_position.controller

for i in bob do
(
	print i
	print (getPropertyController $.transform.controller i)
)



getPropNames (modPanel.getCurrentObject())


fn getControllers obj ctrls:#() parentAnim:#()= 
(
	for item in refs.dependsOn obj do
	(
		if isController item AND item.keyable do
		(
-- 			for j in (getpropnames item) do print j
-- 			append ctrls item
			if findItem parentAnim (refs.dependents item)[1] == 0 do
			(
				
				for j in (getSubAnimNames (refs.dependents item)[1]) do
				(			
						append ctrls j 
				)
				append parentAnim (refs.dependents item)[1]
			)
		)
		if superclassof item == modifier do
			for i in (getpropnames item) do append ctrls i
		getControllers item ctrls:ctrls parentAnim:parentAnim
	)
	return ctrls
)

bob = getControllers $
print bob

for i in bob do print(getPropertyController  $.transform.controller i)

$.controller[#height] 

getSubAnim $.transform.controller.position.controller 1


fn getControllers obj ctrls:#() = 
(
	for item in refs.dependsOn obj do
	(

		if isController item AND item.keyable then
		(
			carryOn = True
			for o in refs.dependents item do
			(
				if superclassof o == modifier do
				(
					carryOn = False
					continue
				)
			)
			if carryOn ==  True do
			(
				try
				(
					append ctrls item
				)
				catch(
					try
					(
						var = exprForMAXObject item + "= Scale_Reactor ()"
						execute var
						print var
						append ctrls item
					)
					catch()
				)
			)
		)
		else
		(
			
			if superclassof item == modifier do
			(
-- 				print item
-- 				print (superclassof item)
				append ctrls item
	-- 			for i in (getpropnames item) do append ctrls i
			)
		)
		getControllers item ctrls:ctrls
	)
	return ctrls
)

controllerList = getControllers $

for i in controllerList do
(

	if superclassof i == modifier then
	(
		for j in (getpropnames i) do print j
		
	)
	else
	(
		stringArray = filterString (exprForMAXObject i) "."
		print stringArray[stringArray.count - 1]
	)
	
)

struct testing (attr1, attr2)

bob = testing attr1:0 attr2:0
print bob.attr1

structArray = #()
for i in selection do
(
	execute (format "% = testing attr1: "
)
		
		

refs.dependson $.pos.controller.X_Position.controller

