
for i in geometry do
(
	if findString i.name "Geo_Body"  != undefined  then i.gbufferchannel = 3
	else if findString i.name "Geo_L_Feather"  != undefined  then i.gbufferchannel = 1
	else if findString i.name "Geo_L_Wing_FeathShort"  != undefined  then i.gbufferchannel = 1
	else if findString i.name "Geo_R_Feather"  != undefined  then i.gbufferchannel = 2
	else if findString i.name "Geo_R_Wing_FeathShort"  != undefined  then i.gbufferchannel = 2
	else if findString i.name "VRayFur_Crow"  != undefined  then i.gbufferchannel = 3
	else if findString i.name "Geo_Eyes"  != undefined  then i.gbufferchannel = 4
)