fn tempTest =
(
    st = @"

import MaxPlus
	
## get names of selected objects (GetNodes() delivers a MaxPlus.INode)
currSelection = MaxPlus.SelectionManager.GetNodes()
for i in range(0, len(currSelection)):
	print (currSelection[i].GetName())
	name = currSelection[i].GetName()

r = MaxPlus.Core.EvalMAXScript('$'+name) 
print r.GetType(), MaxPlus.FPTypeGetName(r.GetType()), r.Get()
    " 
print st	
python.execute st
)
tempTest()

fn tempTest2 =
(
    st = @"
def getCameras():

    nodes = []
    for node in MaxPlus.SelectionManager.GetNodes():
       # if 'cam' in node.Object.GetClassName().lower():
		nodes.append(node)
 
    return nodes

cams = getCameras()
print cams

for i in cams:
	print i.GetAnimHandle()
	print i.GetObject()
	print i.GetName()
	
	#help(i)

    " 
print st	
python.execute st
)
tempTest2()

fn tempTest3 =
(
    st = @"
import MaxPlus
anim = MaxPlus.Animation
print anim.GetAnimRange().Start()/200
print anim.GetAnimRange().End()/200

    " 
print st	
python.execute st
)
tempTest3()