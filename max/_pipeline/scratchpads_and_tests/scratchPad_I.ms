﻿thisSel = $Teapot001
measureArray = (selection as array)
scaleCA= attributes thisScaleAttr
(
	parameters main rollout:params
	(
		theScale type:#integer ui:thisScale default:-10 
	)
	 
	rollout params "Scale"
	(
		spinner thisScale "10 to the power of:" type: #integer range:[-100,100, 0]
	)	
)
firstAttr = -35
for i in measureArray do
(
	i.name += "_"+(firstAttr as string)
	custAttributes.add i scaleCA
	i.baseObject.thisScaleAttr.theScale = firstAttr

	firstAttr = firstAttr +=1
)


--add pre-existing script controller to master_sphere and its children
scriptController = $.pos.controller.Y_Position.controller
select $MASTER_sphere...*
sphereSel = selection as array
for i in sphereSel do
( 
	i.pos.controller.Y_Position.controller = scriptController
)






-- delete custom attributes
a=$
b=custattributes.getdef a 1
custAttributes.delete a b

--replace obj with new sphere
for i in (selection as array) do
(
	a = sphere()
	a.transform = i.transform
	delete i
)

--cascading parenting
thisSel = (selection as array)
for x=2 to thisSel.count do
(
	thisSel[x].parent = thisSel[x-1]
)


powerSpheres = (selection as array)
thisHeight = 500
keepGoing = True
for x=1 to powerspheres.count while keepGoing do
(
	formula = thisHeight/(pow 10 powerspheres[x].baseObject.thisScaleAttr.theScale)
	print formula
	if formula < 1 do
	(
		keepGoing = False
		betweenA = powerspheres[x-1]
		betweenB = powerspheres[x]
	)
		
)

thisObj = $Teapot001
posCtrl = position_constraint()
thisObj.controller.position.controller = posCtrl
posConstraintInterface = posCtrl.constraints
posConstraintInterface.appendTarget betweenA 50.0
posConstraintInterface.appendTarget betweenB 50.0

----------------------
--points on each vertex with attachment constraints
thisObj = $
theseVerts = thisObj.verts

for x=1 to theseVerts.count dp
(
	thisPoint = point()
	
)

------------------------------

replaceCtrl = $.position.controller

for i in (selection as array) do
(
	copyReplace = copy replaceCtrl
	prevCtrl = i.position.controller
	i.position.controller = copyReplace
	i.pos.controller.Position_XYZ.controller=prevCtrl
)


replaceCtrl = $.rotation.controller

for i in (selection as array) do
(
	copyReplace = copy replaceCtrl
	prevCtrl = i.rotation.controller
	i.rotation.controller = copyReplace
	i.rotation.controller.Euler_XYZ.controller=prevCtrl
)

replaceCtrl = $.scale.controller

for i in (selection as array) do
(
	copyReplace = copy replaceCtrl
	prevCtrl = i.scale.controller
	i.scale.controller = copyReplace
	i.scale.controller.bezier_scale.controller=prevCtrl
)
---------------------
displayControlDialog $.custom_attributes.scriptCtrl.controller "beer"