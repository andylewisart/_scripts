--macroscript RandomUvwOffset category:"decon tools"
-- randomize uv coordinate offset, usage: 
-- 1.map polys
-- 2.add unwrap_uvw mod.
-- 3.select obj
-- 4.run script
--
-- by decon at www.deconstudio.net 2008
-- lots of copy/paste from michael brempel's normalize_uv_elements.ms 2004

--global RandUVOffs
try(destroyDialog  RandUVOffs)catch()
rollout RandUVOffs "Random UV Offset"
(
	radiobuttons UVaxis labels:#("U","V","UV") default:3
	button btnOffset "Randomize!"
	button btnReset "Reset to Original Values"
	
	fn randOffset k = (
		offset = [0,0,0]
		if k == 3 do (offset[1] = random 0.0 1.0; offset[2] = random 0.0 1.0; return offset)
		offset[k] = random 0.0 1.0
		return offset
	)
	fn detachObj &objName = (
-- 		facesToDetach = polyop.getFaceSelection newObj
-- 		polyop.detachFaces newObj facesToDetach asNode:true delete:false name:objName
		
		print objName
		$.modifiers[#Edit_Poly].DetachToObject objName

		)

	on btnOffset pressed do
	(
		disableSceneRedraw()
		
		if selection.count == 1 and (try($.unwrapuvw)catch(undefined)) != undefined  do (
		local moduvw = selection[1].modifiers[#unwrap_uvw]
		local vcnt=moduvw.unwrap.numbervertices()
		moduvw.unwrap.selectVertices #{1..vcnt}

		-- count uv elements
		local vertelemarray=#()
		for i=1 to vcnt do vertelemarray[i]=0
		local elem=0
		modPanel.setCurrentObject moduvw
		subobjectlevel = 1
		---------------------------------------------------
		for v=1 to vcnt do
		(

			if(vertelemarray[v]==0)then
			(	-- vertex v is not in array yet
				moduvw.unwrap.selectVertices #{v}
				moduvw.unwrap2.SelectElement()
				elem+=1
				elemverts=moduvw.unwrap.getselectedvertices()

				for i in elemverts do
				(	-- which vertices go to which element, vert[1]=vert[2]= ... =1 , vert[7]=vert[8]= ... =2 , ...
					vertelemarray[i]=elem
				)
			)
			
		)
		
		subobjectlevel = 0
		--amount of elements
		--print elem as string
		-----------------------------------------------------
		local elemarray=#()
		for e=1 to elem do
		(
			print e
			local notFound = true
			for v=1 to vertelemarray.count while notFound do
			(
				if vertelemarray[v]==e then
				(	-- found vertex index 
					elemarray[e]=v
					notFound = false
				)
			)
		)
		--with this we get element from vertex
		--print elemarray as string
		---------------------------------------------------
		modPanel.setCurrentObject moduvw
		subobjectlevel = 1

		for i=1 to elemarray.count do
		(	-- go through all elements
			print i
			print elemarray[i]
			moduvw.unwrap.selectVertices #{elemarray[i]}

			moduvw.unwrap2.SelectElement()

			moduvw.unwrap2.vertToFaceSelect() 

-- 			moduvw.unwrap2.selectFaces(moduvw.unwrap2.getSelectedFaces())
-- 			sel = polyop.getVertSelection  $
			editPoly = Edit_Poly()
			modPanel.addModToSelection (editPoly) ui:on
			subobjectlevel = 4
			$.modifiers[#Edit_Poly].ButtonOp #DetachFace
			local detachName = "surfaceObjectDetached"+(i as string)
			
			detachObj detachName
			print 1
			
			print 2
			deleteModifier $ 1
-- 			sel = polyop.getVertSelection  $ 
-- 			print( (sel as string) + " SEL")
-- 			polyop.detachFaces $ sel
-- 						
-- 			print (moduvw.unwrap2.getSelCenter())
		)

		--subobjectlevel = 0
	enableSceneRedraw()
	completeRedraw()
	)--if selection
	)--button

	on btnReset pressed do
	(
		local moduvw = selection[1].modifiers[#unwrap_uvw]
		moduvw.unwrap.reset()
	)

)--rollout
CreateDialog RandUVOffs pos:[20,100] width:200 height:100






