sel = $Heart007
firstOne = True
for i in (selection as array) do
(
	if firstOne then
	(
		WM3_MC_BuildFromNode sel.morpher 1 i
		firstOne = False
	)
	else
	(
		WM3_AddProgressiveMorphNode sel.morpher 1 i
	)
)


bob = WM3_GetProgressiveMorphNode $.morpher 1 1

WM3_GetProgressiveMorphNode $.morpher 1 25

WM3_SetProgressiveMorphTension $.morpher 1 0.0
bob = WM3_GetProgressiveMorphNode $.morpher 1 1
WM3_SetProgressiveMorphWeight $.Morpher 1 bob .001

$.morpher[1].keys[3] = 106f

moveKey $.morpher[1].controller 3 -.999

moveKey $.morpher[2].controller ($.morpher[2].keys.count) -.999