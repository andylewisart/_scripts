macroScript incrementalSave category:"Infuse" internalCategory:"Infuse" toolTip:"Incremental Save" buttonText:"Save+"
(
    if maxFileName != "" then (
        max saveplus
        TheFile = maxFilePath + (trimRight (getFilenameFile maxFileName) "1234567890") + ".max"
        if doesFileExist TheFile do deleteFile TheFile
        copyFile (maxFilePath + maxFileName) TheFile
    ) else checkForSave()
)

