
fn getRelativeScriptPath = 
(
	relativePath =getThisScriptFilename()
	pattern =@".*MASTER"
	rgx = dotnetObject "System.Text.RegularExpressions.Regex" pattern
	result = rgx.match relativePath
	result.value
)

