try(destroydialog snapshotMorpher)catch()
rollout snapshotMorpher "LoomFire Snapshot Morpher"
(
	spinner startTime_spn "Start Frame" type:#integer range:[-9999,9999,0]
	spinner endTime_spn "End Frame" type:#integer range:[-9999,9999,10]
	spinner everyNth_spn "Nth Frame" type:#integer range:[1,9999,1]
	spinner baseDiv_spin "base division" type:#integer range:[1,23,23]
	checkbox addKeys_chx "Keyframe morpher" checked:true
	checkbox useSingle_chx "Use single morpher channel" checked:true
	button ok_btn "OK"
	local snapshotArray = #()
	local morphedObj
	
	fn setMorpherKeys thisFrame thisTargetIndex setPreviousToZero isLast veryLast= 
	(
		with animate on 
		(
			if isLast then 
			(
				print "isLast"
				format "frame: % target: % setPreviousToZero: %  isLast:%\n" thisFrame thisTargetIndex setPreviousToZero isLast
				slidertime = thisFrame*everyNth_spn.value
				WM3_MC_SetValue snapshotArray[1].morpher (thisTargetIndex-1) 100.0 
			)
			else if setPreviousToZero then
			(
				print "setPreviousToZero"
				format "frame: % target: % setPreviousToZero: %  isLast:%\n" thisFrame thisTargetIndex setPreviousToZero isLast
				slidertime = (thisFrame-1)*everyNth_spn.value
				WM3_MC_SetValue snapshotArray[1].morpher thisTargetIndex 0.0 
				slidertime = ((thisFrame-1)*everyNth_spn.value)+1
				WM3_MC_SetValue snapshotArray[1].morpher (thisTargetIndex-1) 0.0
-- 				moveKey $.morpher[thisTargetIndex-1].controller ($.morpher[thisTargetIndex-1].keys.count) -.999
			)
			else if veryLast then
			(
				print "veryLast"
				format "frame: % target: % setPreviousToZero: %  isLast:%\n" thisFrame thisTargetIndex setPreviousToZero isLast
				slidertime = thisFrame*everyNth_spn.value
				WM3_MC_SetValue snapshotArray[1].morpher thisTargetIndex 100.0 
			)
			else
			(
				print "else part"
				format "frame: % target: % setPreviousToZero: %  isLast:%\n" thisFrame thisTargetIndex setPreviousToZero isLast
				slidertime = thisFrame*everyNth_spn.value
				WM3_MC_SetValue snapshotArray[1].morpher 1 0.0 
			)
			
		)
	)
	
	fn makeSnapshots = 
	(
		LayerManager.newLayerFromName ("_snapshotMorphs_"+$.name)
		thisLayer= LayerManager.getLayerFromName("_snapshotMorphs_"+$.name)
		thisLayer.current=True
		if useSingle_chx.checked then
		(
			print "single checked"
			if endTime_spn.value/23 < 1 then progressiveMorphLimit = 1 else progressiveMorphLimit = endTime_spn.value/23
			for t in startTime_spn.value to (endTime_spn.value/progressiveMorphLimit) do at time (t*progressiveMorphLimit) snapshot $
		)
		else
		(
			print "single unchecked"
			for t in startTime_spn.value to (endTime_spn.value/everyNth_spn.value) do at time (t*everyNth_spn.value) snapshot $
		)
		thisLayer.nodes &theNodes
		for idx = theNodes.count to 1 by -1 do (append snapshotArray theNodes[idx])
-- 		for o in snapshotArray do print o.name
	)
	
	fn makeMorpher = 
	(
		morphedObj = snapshotArray[1]
		select morphedObj
		modPanel.addModToSelection (Morpher ()) ui:on

		firstOne = True
		nextIndex = false
		targetIndex = 1
		for x=2 to snapshotArray.count do
		(
			--progressive morph limit is 24. move to new target when reaching this point.
			if (mod (x-1) (baseDiv_spin.value as float)) == 0 do
			(
				nextIndex = true
				targetIndex += 1

			)
			if firstOne then
			(
				
				WM3_MC_BuildFromNode morphedObj.morpher targetIndex snapshotArray[x]
				WM3_SetProgressiveMorphTension morphedObj.morpher targetIndex 0.0
				if addKeys_chx.checked do
				(
					setMorpherKeys (x-2) targetIndex false false false
				)
				firstOne = False
			)
			else if nextIndex then
			(
				WM3_MC_BuildFromNode morphedObj.morpher targetIndex snapshotArray[x-1]
				WM3_SetProgressiveMorphTension morphedObj.morpher targetIndex 0.0

				WM3_AddProgressiveMorphNode morphedObj.morpher targetIndex snapshotArray[x]
				if addKeys_chx.checked do
				(
					setMorpherKeys (x-3) targetIndex false true false
					setMorpherKeys (x-2) targetIndex true false false
				)
				nextIndex = false
			)
			else if x == (snapshotArray.count) then
			(
				WM3_AddProgressiveMorphNode morphedObj.morpher targetIndex snapshotArray[x]
				if addKeys_chx.checked do
				(
					setMorpherKeys (x-2) targetIndex false false true
				)
			)
			else
			(
				WM3_AddProgressiveMorphNode morphedObj.morpher targetIndex snapshotArray[x]
			)
		)
		for y=1 to targetIndex do
		(
			for u in morphedObj.morpher[y].keys do
			(
				if u.value == 100.0 then u.outtangentType = #linear
				else u.outtangentType = #linear
				u.intangentType = #linear			
			)				
		)
		for h=1 to targetIndex do
		(
			bob = WM3_GetProgressiveMorphNode morphedObj.morpher h 1
			print bob
-- 			WM3_SetProgressiveMorphWeight morphedObj.Morpher h bob .001
		)

		for z=2 to snapshotArray.count do delete snapshotArray[z]
		
	)
	
	on ok_btn pressed do
	(
		clearlistener()
		if selection.count <1 then messagebox "Please select an object."
		else
		(
			for sel in (selection as array) do
			(
				select sel
				makeSnapshots()
				makeMorpher()
			)
			destroyDialog snapshotMorpher
		)
		
	)
)
createDialog snapshotMorpher