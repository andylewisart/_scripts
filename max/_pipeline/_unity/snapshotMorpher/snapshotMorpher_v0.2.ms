try(destroydialog snapshotMorpher)catch()
rollout snapshotMorpher "LoomFire Snapshot Morpher"
(
	spinner startTime_spn "Start Frame" type:#integer range:[-9999,9999,0]
	spinner endTime_spn "End Frame" type:#integer range:[-9999,9999,10]
	spinner everyNth_spn "Nth Frame" type:#integer range:[1,9999,1]
	checkbox addKeys_chx "Keyframe morpher" checked:true
	button ok_btn "OK"
	local snapshotArray = #()
	local morphedObj
	
	fn setMorpherKeys thisFrame thisTargetIndex lastOne = 
	(
		with animate on 
		(
			format "thisFrame: % thisTargetIndex: % everyNth_spn.value: %\n"thisFrame thisTargetIndex everyNth_spn.value
			slidertime = thisFrame*everyNth_spn.value
			WM3_MC_SetValue snapshotArray[1].morpher thisTargetIndex 0.0 
			slidertime = thisFrame*everyNth_spn.value+everyNth_spn.value
			WM3_MC_SetValue snapshotArray[1].morpher thisTargetIndex 100.0
			slidertime = (thisFrame+1)*everyNth_spn.value+everyNth_spn.value+1
			if lastOne == false do
			(
				WM3_MC_SetValue snapshotArray[1].morpher thisTargetIndex 0.0
				
			)
			try(moveKey $.morpher[thisTargetIndex-1].controller ($.morpher[thisTargetIndex-1].keys.count) -1.0)catch()

		)
	)
	
	fn makeSnapshots = 
	(
		snapshotArray = #()
		LayerManager.newLayerFromName ("_snapshotMorphs_"+$.name)
		thisLayer= LayerManager.getLayerFromName("_snapshotMorphs_"+$.name)
		thisLayer.current=True
		for t in startTime_spn.value to (endTime_spn.value/everyNth_spn.value) do at time (t*everyNth_spn.value) snapshot $
		thisLayer.nodes &theNodes
		for idx = theNodes.count to 1 by -1 do (append snapshotArray theNodes[idx])
-- 		for o in snapshotArray do print o.name
	)
	
	fn makeMorpher = 
	(
		morphedObj = snapshotArray[1]
		select morphedObj
		modPanel.addModToSelection (Morpher ()) ui:on

		lastOne = False
		nextIndex = false
		targetIndex = 1
		for x=2 to snapshotArray.count do
		(
			if x == snapshotArray.count do lastOne = True
			WM3_MC_BuildFromNode morphedObj.morpher targetIndex snapshotArray[x]
			if addKeys_chx.checked do
			(
				if lastOne then
				(
					setMorpherKeys (x-2) targetIndex true
					targetIndex+=1
				)
				else
				(
					setMorpherKeys (x-2) targetIndex false
					targetIndex+=1
				)
			)

		)
		for y=1 to targetIndex-1 do
		(
			for u in morphedObj.morpher[y].keys do
			(
				if u.value == 100.0 then u.outtangentType = #auto
				else u.outtangentType = #auto
				u.intangentType = #auto			
			)				
		)


		for z=2 to snapshotArray.count do delete snapshotArray[z]
		
	)
	
	on ok_btn pressed do
	(
		clearlistener()
		if selection.count <1 then messagebox "Please select an object."
		else
		(
			for sel in (selection as array) do
			(
				select sel
				actionMan.executeAction 0 "197"
				makeSnapshots()
				makeMorpher()
			)
			actionMan.executeAction 0 "261"
			destroyDialog snapshotMorpher
		)
		
	)
)
createDialog snapshotMorpher