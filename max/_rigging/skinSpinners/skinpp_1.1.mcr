macroScript skinplus category: "Mohamed Sami Hadef"
(
rollout skinpp "Skin++" width:143 height:500
(
	fn DKN_filter obj = classof obj.modifiers[1] == skin
	fn DKN_filter2 obj = classof obj == Biped_Object
	Global selected_obj
	Global selected_obj_skin
	Global BONES_Coll =#()
	global vertxcount
	global selected_bone
	global callbackItem
	global mirroritems1 = #()
	global mirroritems2 = #()
	global mirrori7x7 = #()
	global bnm_1
	global bnm_2
	global signalvalu
	global boxmodeb1
	global boxmodeb2
	global listofouter = #()
	global listofinner = #()
	global vvcolor = 0
	global bippdSRC
	global bippdObj
	global f = 1
	
	spinner Sspn5 "Outer : " pos:[1,10] width:134 height:16 enabled:false range:[0,100000,100] scale:1
	spinner Sspn6 " Inner : " pos:[0,30] width:135 height:16 enabled:false range:[0,100000,100] scale:1

	listbox lbx1 "" pos:[6,172] width:131 height:13
	button btn5_add "Add" pos:[54,349] width:31 height:21 enabled:false
	button btnNx7 " Next >" pos:[89,349] width:45 height:21 enabled:false
	button btn6_rem " X " pos:[8,349] width:21 height:21 enabled:false
	checkbox chk1 "Mirror On/Off" pos:[8,154] width:114 height:16 enabled:false
	
	spinner spnsel "Selection :" pos:[8,58] width:108 height:16 enabled:false range:[0,1,0] scale:0.01
	button btn_incl "Incl" pos:[66,78] width:34 height:21 enabled:false
	button btn_excl "Excl" pos:[103,78] width:34 height:21 enabled:false
	checkbox chk4inv "" pos:[120,59] width:15 height:15 enabled:false
	button btn88 "slow out" pos:[6,78] width:54 height:21 enabled:false
	
	button btn7_wgh "Weight all" pos:[8,420] width:62 height:21 enabled:false
	button btn7_res "Back all" pos:[74,420] width:62 height:21 enabled:false
	button btninirad "Initialize" pos:[6,450] width:63 height:21 enabled:false
	pickbutton btnloadrad "Copy" pos:[73,450] width:63 height:21 enabled:false filter:DKN_filter
	pickbutton btn17 "Pick skin" pos:[6,474] width:131 height:21 filter:DKN_filter
	
	ImgTag imgimg "" pos:[36,138] width:64 height:14
	checkbox chk113 "Show colored vertices" pos:[9,383] width:126 height:15 checked:true enabled:false
	checkbox chk114 "Shaded (default)" pos:[9,401] width:126 height:15 checked:true enabled:false

	button btn18_sel " Mirror" pos:[97,109] width:40 height:21 enabled:false
	dropDownList ddl1_sel "" pos:[55,109] width:39 height:21 items:#("X", "Y", "Z") enabled:false
	spinner spn1_sel "" pos:[5,112] width:48 height:16 range:[0,999999,0.1] enabled:false
	
	GroupBox grp3 "" pos:[3,139] width:137 height:234
	GroupBox grp1 "" pos:[3,-1] width:137 height:51
	GroupBox grp2 "" pos:[3,48] width:137 height:54
	GroupBox grp14 "" pos:[3,373] width:137 height:72
	GroupBox grp8 "" pos:[3,100] width:137 height:33
	label lbl10 "  [  - ] Mirror  " pos:[36,138] width:64 height:14

	Timer tmr1 "Timer1" pos:[78,414] width:24 height:24 interval:300 active:false
	Timer tmr2 "Timer2" pos:[87,420] width:24 height:24 interval:300 active:false
	Timer tmr3 "Timer3" pos:[96,423] width:24 height:24 interval:300 active:false
	Timer tmr4 "Timer4" pos:[106,423] width:24 height:24 interval:150 active:false
	
	fn loaderb = 
	(
		---------------------------------------------------------------------------
		 biped.setTransform (biped.getNode bippdObj #pelvis link:1) #rotation (biped.getTransform  (biped.getNode bippdSRC #pelvis link:1) #rotation) false 
		---#spine------------------------------------------------------------------------
		i = 1
		nde = (biped.getNode bippdSRC #spine link:i)
		nde2 = (biped.getNode bippdObj #spine link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #spine link:i)
			nde2 = (biped.getNode bippdObj #spine link:i)
		)
		--#neck-------------------------------------------------------------------------
		i = 1
		nde =  (biped.getNode bippdSRC #neck link:i)
		nde2 = (biped.getNode bippdObj #neck link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #neck link:i)
			nde2 = (biped.getNode bippdObj #neck link:i)
		)
		--#head -----------------------------------------------------------------
			biped.setTransform (biped.getNode bippdObj #head  link:1) #rotation (biped.getTransform  (biped.getNode bippdSRC #head  link:1) #rotation) false 
		---------------------------------------------------------------------------
		i = 1
		nde =  (biped.getNode bippdSRC #larm link:i)
		nde2 = (biped.getNode bippdObj #larm link:i) 
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform  nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #larm link:i)
		nde2 = (biped.getNode bippdObj #larm link:i) 
		)
		i = 1
		nde = (biped.getNode bippdSRC #rarm link:i)
		nde2 = (biped.getNode bippdObj #rarm link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #rarm link:i)
		nde2 = (biped.getNode bippdObj #rarm link:i)
		)
		---------------------------------------------------------------------------
		i = 1
		nde =(biped.getNode bippdSRC #lfingers link:i)
		nde2 = (biped.getNode bippdObj #lfingers link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #lfingers link:i)
		nde2 = (biped.getNode bippdObj #lfingers link:i)
		)
		i = 1
		nde =  (biped.getNode bippdSRC #rfingers link:i)
		nde2 = (biped.getNode bippdObj #rfingers link:i) 
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #rfingers link:i)
		nde2 = (biped.getNode bippdObj #rfingers link:i) 
		)
		---------------------------------------------------------------------------
		i = 1
		nde =  (biped.getNode bippdSRC #lleg link:i)
		nde2 = (biped.getNode bippdObj #lleg link:i) 
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #lleg link:i)
		nde2 = (biped.getNode bippdObj #lleg link:i) 
		)
		i = 1
		nde = (biped.getNode bippdSRC #rleg link:i)
		nde2 = (biped.getNode bippdObj #rleg link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #rleg link:i)
		nde2 = (biped.getNode bippdObj #rleg link:i)
		)
		---------------------------------------------------------------------------
		i = 1
		nde =(biped.getNode bippdSRC #ltoes link:i)
		nde2 = (biped.getNode bippdObj #ltoes link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #ltoes link:i)
		nde2 = (biped.getNode bippdObj #ltoes link:i)
		)
		i = 1
		nde =(biped.getNode bippdSRC #rtoes link:i)
		nde2 = (biped.getNode bippdObj #rtoes link:i)
		while ((nde != undefined) and (nde2 != undefined)) do
		(
			biped.setTransform nde2 #rotation (biped.getTransform  nde #rotation) false 
			i=i+1
			nde = (biped.getNode bippdSRC #rtoes link:i)
		nde2 = (biped.getNode bippdObj #rtoes link:i)
		)
	)
	fn getboneFromSkin theSkin boneID=
	(
		local boneArray=for o in objects where (refs.dependencyLoopTest theSkin o) collect o
		local boneName=(skinOps.GetBoneName theSkin boneID 1)
		local notFound = true
		local i = 0
		while notFound and i < boneArray.count do
		 (
			 i+=1
			 if boneArray[i].name == boneName then notFound = false
		  )
		  if notFound then undefined else boneArray[i]
	)
	fn mainCbFn ev nd =
	(
		local ob = (GetAnimByHandle nd[1])
		if isValidNode ob do
		(
			if ((selected_obj == ob) and (modPanel.getCurrentObject() == selected_obj_skin)) do
			(
				cnb =(skinOps.GetNumberBones selected_obj_skin)
				if (cnb == BONES_Coll.count) then
				(
					sb = skinOps.GetSelectedBone selected_obj_skin
					if (sb != selected_bone) do
					(
						selected_bone = sb
						vdo = skinOps.GetOuterRadius selected_obj_skin  selected_bone 1
						vdo = (vdo * 100)/listofouter[selected_bone]
						Sspn5.value = vdo
						vdo = skinOps.GetInnerRadius selected_obj_skin  selected_bone 1
						vdo = (vdo * 100)/listofinner[selected_bone]
						Sspn6.value = vdo
						foffval = skinOps.getSelectedBonePropFalloff selected_obj_skin
						if (foffval == 2) then
							btn88.caption ="sinual"
						else if (foffval == 3) then
							btn88.caption ="fast out"
						else if (foffval == 4) then
							btn88.caption ="slow out"
						else if (foffval == 1) then
							btn88.caption ="linear"	
					)
				)
				else
				(
					bnecn7 = BONES_Coll.count
					for i= (bnecn7 +1) to cnb do
						append BONES_Coll (getboneFromSkin selected_obj_skin i)
					
					for i= (bnecn7 +1) to cnb do
					(
						append listofouter (skinOps.GetOuterRadius selected_obj_skin  i 1)
						append listofinner (skinOps.GetInnerRadius selected_obj_skin  i 1)
						 epn = (skinOps.getendPoint selected_obj_skin i)
						 spn = (skinOps.getstartPoint selected_obj_skin i)
						b= epn - spn
						skinOps.setEndPoint selected_obj_skin i (epn - (b *0.52))
						skinOps.setStartPoint selected_obj_skin i (spn + (b *0.52))
					)
					sb = skinOps.GetSelectedBone selected_obj_skin
					if (sb != selected_bone) do
					(
						selected_bone = sb
						vdo = skinOps.GetOuterRadius selected_obj_skin  selected_bone 1
						vdo = (vdo * 100)/listofouter[selected_bone]
						Sspn5.value = vdo
						vdo = skinOps.GetInnerRadius selected_obj_skin  selected_bone 1
						vdo = (vdo * 100)/listofinner[selected_bone]
						Sspn6.value = vdo
						foffval = skinOps.getSelectedBonePropFalloff selected_obj_skin
						if (foffval == 2) then
							btn88.caption ="sinual"
						else if (foffval == 3) then
							btn88.caption ="fast out"
						else if (foffval == 4) then
							btn88.caption ="slow out"
						else if (foffval == 1) then
							btn88.caption ="linear"	
					)
				)
			)
		)
	)
	fn skinneeded =
	(
		mdpngco = modPanel.getCurrentObject()
		if (mdpngco == undefined) do
		(
			max modify mode
			modPanel.setCurrentObject  selected_obj_skin
		)
		if (mdpngco != selected_obj_skin)do
			modPanel.setCurrentObject  selected_obj_skin
	)
	fn envlopsubobj =
	(
		mdpngco = modPanel.getCurrentObject()
		if (mdpngco == undefined) do
		(
			max modify mode
			modPanel.setCurrentObject  selected_obj_skin
		)
		if (mdpngco != selected_obj_skin)do
			modPanel.setCurrentObject  selected_obj_skin
		 
		if (subObjectLevel == 0) do
			subObjectLevel = 1
	)
	on btn18_sel pressed do
	(
		try
		(
		ar = #()
		versel = #()
		a = (getPolygonCount selected_obj)[2]
		skinneeded()
		for u = 1 to a do
			if ((skinOps.IsVertexSelected selected_obj_skin u) ==1)do
				append versel u
		
		selbone0 = skinOps.GetSelectedBone selected_obj_skin
		suspendediting()
		selected_obj_skin.enabled = false
			epolymd = edit_poly()
		addModifier selected_obj epolymd
		in coordsys local
		(
			if (ddl1_sel.selection == 1) then
				(
					for i = 1 to versel.count do
					(
						v1 = (getPointPos selected_obj versel[i])
						--print v1
						for j = 1 to a do
						(
							v2 = (getPointPos selected_obj j)
							--print v2
							if ((v1.x != v2.x)and(abs(v1.x+v2.x) <= spn1_sel.value)and(int(v1.y)==int(v2.y))and(int(v1.z)==int(v2.z)))do
								append ar j
						)
					)
				)
			else if (ddl1_sel.selection == 2)then
				(
					for i = 1 to versel.count do
					(
						v1 = (getPointPos selected_obj versel[i])
						for j = 1 to a do
						(
							v2 = (getPointPos selected_obj j)
							if ((v1.y != v2.y)and(abs(v1.y+v2.y) <= spn1_sel.value)and(int(v1.x)==int(v2.x))and(int(v1.z)==int(v2.z)))do
							append ar j
						)
					)
				)
			else if (ddl1_sel.selection == 3)then
				(
					for i = 1 to versel.count do
					(
						v1 = (getPointPos selected_obj versel[i])
						for j = 1 to a do
						(
							v2 = (getPointPos selected_obj j)
							if ((v1.z != v2.z)and(abs(v1.z+v2.z) <= spn1_sel.value)and(int(v1.y)==int(v2.y))and(int(v1.x)==int(v2.x)))do
							append ar j
						)
					)
				)
		)
		deleteModifier  selected_obj 1
		selected_obj_skin.enabled = true
		resumeediting()
		envlopsubobj()
		skinOps.SelectBone selected_obj_skin selbone0
		if (ar.count > 0) then
			skinOps.SelectVertices selected_obj_skin ar
		else
			skinOps.SelectVertices selected_obj_skin versel
		
			)
		catch
		(
			resumeediting()
			print (getCurrentException())
		)
	)
	on spn1_sel changed val do
	(
		if (val > (spn1_sel.range.y/2))do
			spn1_sel.range.y = spn1_sel.range.y*2
	)
	on imgimg click do
	(
		if (chk1.visible == true) then
		(
			chk1.visible = false
			btnNx7.visible = false
			btn5_add.visible = false
			lbx1.visible =false
			btn6_rem.visible =false
			grp3.height = 20 --visible =false
			lbl10.caption ="  [ + ] Mirror  "
			------------
			chk113.pos.y -= 210
			chk114.pos.y -= 210
			grp14.pos.y -= 210
			btn17.pos.y -= 210
			btn7_wgh.pos.y -= 210
			btn7_res.pos.y -= 210
			btninirad.pos.y -= 210
			btnloadrad.pos.y -= 210
			skinpp.height -= 210
			/*btn12_bip.pos.y -= 210
			btn13_bip.pos.y -= 210
			tmr1_bip.pos.y -= 210
			chk1_bip.pos.y -= 210 
			ckb1_bip.pos.y -= 210
			grp3_bip.pos.y -= 210*/
		)   
		else
		(
			chk1.visible =true
			btnNx7.visible =true
			btn5_add.visible =true
			lbx1.visible =true
			btn6_rem.visible =true
			grp3.height = 234 --.visible =true
			lbl10.caption ="  [  - ] Mirror  "
			------------
			chk113.pos.y += 210
			chk114.pos.y += 210
			grp14.pos.y += 210
			btn17.pos.y += 210
			btn7_wgh.pos.y += 210
			btn7_res.pos.y += 210
			btninirad.pos.y += 210
			btnloadrad.pos.y += 210
			skinpp.height += 210
			/*btn12_bip.pos.y += 210
			btn13_bip.pos.y += 210
			tmr1_bip.pos.y += 210
			chk1_bip.pos.y += 210 
			ckb1_bip.pos.y += 210
			grp3_bip.pos.y += 210*/
		)
	)
	on skinpp open do
	(
		bippdSRC = undefined
		bippdObj = undefined
	)
	on skinpp close do
	(
		--print "closed"
		unRegisterTimeCallback changebiped
	 try(
			if (callbackItem != undefined) do
				callbackItem = undefined ; gc light:true
		)
		catch
			(
				print (getCurrentException())
				)
	)
	on Sspn5 changed val do
	(
		envlopsubobj()
		vdo = (listofouter[selected_bone] * val)/100
		skinOps.SetOuterRadius selected_obj_skin selected_bone 1 vdo
		skinOps.SetOuterRadius selected_obj_skin selected_bone 2 vdo
		tmr1.active = false
		tmr1.active = true
	)
	on Sspn6 changed val do
	(
		envlopsubobj()
		vdo = (listofinner[selected_bone] * val)/100
		skinOps.SetInnerRadius selected_obj_skin selected_bone 1 vdo
		skinOps.SetInnerRadius selected_obj_skin selected_bone 2 vdo
		tmr2.active = false
		tmr2.active = true
	)
	on btninirad pressed do
	(
		listofouter = #()
		listofinner = #()
		envlopsubobj()
		for i= 1 to BONES_Coll.count do
		(
			 d = (distance (BONES_Coll[i].min) (BONES_Coll[i].max))
				 append listofouter d
				 append listofinner (d/2)
			 skinOps.SetInnerRadius selected_obj_skin i 1 (d/2)
			 skinOps.SetInnerRadius selected_obj_skin i 2 (d/2)
			 skinOps.SetOuterRadius selected_obj_skin i 1 d
			 skinOps.SetOuterRadius selected_obj_skin i 2 d
		)
	)
	on btnloadrad picked obj do
	(
		try (
		if (obj != undefined) do
		if (obj.modifiers[1] != undefined) do
		if ((classof obj.modifiers[1]) == Skin) do
		(
			a1_ = #()
			a2_ = #()
			select obj
			selobjskin = obj.modifiers[1]
			max modify mode
			modPanel.setCurrentObject  selobjskin
			for i= 1 to (skinOps.GetNumberBones selobjskin) do
			(
				append a1_ (skinOps.GetOuterRadius selobjskin i 1)
				append a2_ (skinOps.GetInnerRadius selobjskin i 1)
			)
				
			skinneeded()
			bnscoun = skinOps.GetNumberBones selected_obj_skin
			if (bnscoun == a1_.count) then
				for i= 1 to bnscoun do
				(
			-- 		print selected_obj_skin
			-- 		print i
			-- 		print a1_[i]
					skinOps.SetOuterRadius selected_obj_skin i 1 a1_[i]
					skinOps.SetOuterRadius selected_obj_skin i 2 a1_[i]
					skinOps.SetInnerRadius selected_obj_skin i 1 a2_[i]
					skinOps.SetInnerRadius selected_obj_skin i 2 a2_[i]
				)
			else
				(
					a01_ = #()
					a02_ = #()
					select obj
					selobjskin = obj.modifiers[1]
					for i= 1 to (skinOps.GetNumberBones selobjskin) do
						append a01_ (skinOps.GetBoneName selobjskin i 1)
					
					skinneeded()
					for i= 1 to bnscoun do
						append a02_ (skinOps.GetBoneName selected_obj_skin i 1)
					if (a01_.count > a02_.count) then
					(
						for n=1 to (a01_.count - a02_.count) do
							append a02_ " "
					)
					else
					(
						for n=1 to (a02_.count - a01_.count) do
							append a01_ " "
					)
						for i = 1 to a01_.count do
						print (a01_[i] + " :: " + a02_[i])
					
					messagebox "Bones count not equals"
				)
		)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on btn17 picked obj do
	(
		
		if (obj != undefined) then
		if (obj.modifiers[1] != undefined) then
		if ((classof obj.modifiers[1]) == Skin) then
		(
			nbrbnes = skinOps.GetNumberBones obj.modifiers[1]
			if nbrbnes > 0 do
			(
				Sspn5.enabled = true
				Sspn6.enabled = true
				spnsel.enabled = true
				btn_incl.enabled = true
				btn_excl.enabled = true
				btn5_add.enabled = true
				btn88.enabled = true
				btnNx7.enabled = true
				btn7_wgh.enabled = true
				btn7_res.enabled = true
				chk1.enabled = true
				chk4inv.enabled = true
				btninirad.enabled = true
				chk113.enabled = true
				chk114.enabled = true
				btnloadrad.enabled = true
				btn18_sel.enabled = true
				ddl1_sel.enabled = true
				spn1_sel.enabled = true
				
				if (callbackItem != undefined) do
					callbackItem = undefined ; gc light:true
				 
				callbackItem = NodeEventCallback mouseUp:false delay:128 subobjectSelectionChanged:mainCbFn
				selected_obj = obj
				btn17.caption = obj.name
				selected_obj_skin = obj.modifiers[1]
				envlopsubobj()
				BONES_Coll =#()
				mirrori7x7 =#()
				mirroritems1 = #()
				mirroritems2 = #()
				for i= 1 to nbrbnes do
				 append BONES_Coll (getboneFromSkin selected_obj_skin i)
				 vertxcount = numPoints obj
				 --selected_obj_skin.showNoEnvelopes = true
				 selected_obj_skin.envelopesAlwaysOnTop = false
				skinOps.SelectBone selected_obj_skin 1
				selected_bone = 1
				listofouter = #()
				listofinner = #()
				
				for i= 1 to BONES_Coll.count do
				(
					 append listofouter (skinOps.GetOuterRadius selected_obj_skin  i 1)
					 append listofinner (skinOps.GetInnerRadius selected_obj_skin  i 1)
					 epn = (skinOps.getendPoint selected_obj_skin i)
					 spn = (skinOps.getstartPoint selected_obj_skin i)
					 db2 = (distance epn spn)
					if (db2 > 0.02) do
					(
						b= epn - spn
						 k001 = (epn - (b *0.52))
						 k002 = (spn + (b *0.52))
						skinOps.setEndPoint selected_obj_skin i k001
						skinOps.setStartPoint selected_obj_skin i k002
					)
				)
				chk114.checked = true
				selected_obj_skin.shadeweights = true
				selected_obj.xray = false
				selected_obj.backFaceCull = false
				vvcolor = 2
			)
		 )
	)
	on spnsel changed val do
	(
		skinneeded()
		tmr3.active = false
		tmr3.active = true
	)
	on chk4inv changed state do
	(
		skinneeded()
		tmr3.active = false
		tmr3.active = true
	)
	on btn_incl pressed do
	(
		skinneeded()
		skinOps.buttonInclude selected_obj_skin
	)
	on btn_excl pressed do
	(
		skinneeded()
		skinOps.buttonExclude selected_obj_skin
		vlr02 = skinOps.GetouterRadius selected_obj_skin  selected_bone 1
		skinOps.SetOuterRadius selected_obj_skin selected_bone 1 (vlr02 - 0.1)
		skinOps.SetOuterRadius selected_obj_skin selected_bone 2 (vlr02 - 0.1)
		skinOps.SetOuterRadius selected_obj_skin selected_bone 1 (vlr02 + 0.1)
		skinOps.SetOuterRadius selected_obj_skin selected_bone 2 (vlr02 + 0.1)
		skinOps.SetOuterRadius selected_obj_skin selected_bone 1 vlr02
		skinOps.SetOuterRadius selected_obj_skin selected_bone 2 vlr02
	)
	on btn88 pressed  do
    (
		try (
		skinneeded()
		foffval = skinOps.getSelectedBonePropFalloff selected_obj_skin
		if (foffval == 1) then
		(
			foff = 2
			btn88.caption ="sinual"
		)
		else if (foffval == 2) then
		(  
			foff = 3
			btn88.caption ="fast out"
		)
		else if (foffval == 3) then
		(
			foff = 4
			btn88.caption ="slow out"
		)
		else if (foffval == 4) then
		(
			foff = 1
			btn88.caption ="linear"	
		)
		skinOps.setSelectedBonePropFalloff selected_obj_skin foff
		vlr02 = skinOps.GetInnerRadius selected_obj_skin  selected_bone 1
		skinOps.SetInnerRadius selected_obj_skin selected_bone 1 (vlr02 - 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 2 (vlr02 - 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 1 (vlr02 + 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 2 (vlr02 + 0.1)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on lbx1 selected sel do
	(
		try (
		if (lbx1.selection != undefined) do
		if (lbx1.selection > 0) do
		(
			btn6_rem.enabled = true
			if(tmr4.active == true) do
			(
				 tmr4.active = false
				 bnm_1.boxMode = boxmodeb1
				 bnm_2.boxMode = boxmodeb2
			)
			selcilem = lbx1.selection
			bnm_1 = mirroritems1[selcilem]
			bnm_2 = mirroritems2[selcilem]
			boxmodeb1 = bnm_1.boxMode  
			boxmodeb2 = bnm_2.boxMode
			bnm_1.boxMode = not(bnm_1.boxMode)
			signalvalu = 1
			tmr4.active = true
		)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on lbx1 doubleClicked arg do
	(
		if (lbx1.selection != undefined) do
		if (lbx1.selection > 0) do
			print mirrori7x7[lbx1.selection]
	)
	on btnNx7 pressed do
	(
		try (
			
		if (selection.count == 2) then
		(
			if keyboard.controlPressed then
			(
				if (((findItem BONES_Coll selection[1]) >0) and ((findItem BONES_Coll selection[2]) >0)) then
				(
					c = mirrori7x7.count
					appendifunique mirrori7x7 (selection[1].name + " :: " + selection[2].name)
					if (mirrori7x7.count != c) do
					(
						append mirroritems1 selection[1]
						append mirroritems2 selection[2]
						lbx1.items = mirrori7x7
					)
				)
				else
				messagebox "Selected bones not in list"
			)
				aa = selection[1]
				bb = selection[2]
				if ((aa.children.count > 0) and (bb.children.count > 0)) then
				(
					select #(aa.children[1],bb.children[1])
				)
				else
				(
					mnin = aa
					while (aa.parent.children.count == 1) do
					(
						mnin = aa
						aa = aa.parent
						bb = bb.parent
					)
					mnin = aa
					aa = aa.parent
					bb = bb.parent
					mninNbr  = 0
					for i=1 to aa.children.count do
					(
						if (aa.children[i] == mnin) do 
							mninNbr = i
					)
					if (((mninNbr+1) <= aa.children.count) and ((mninNbr+1) <= bb.children.count)) do
					(
						select #(aa.children[mninNbr+1],bb.children[mninNbr+1])
					)
				)
			)
	  	)
		catch
		(
			print (getCurrentException())
		)
	)
	on btn5_add pressed do
	(
		try(
		if (selection.count == 2) then
		(
			if (((findItem BONES_Coll selection[1]) >0) and ((findItem BONES_Coll selection[2]) >0)) then
			(				
				c = mirrori7x7.count
					appendifunique mirrori7x7 (selection[1].name + " :: " + selection[2].name)
					if (mirrori7x7.count != c) do
					(
						append mirroritems1 selection[1]
						append mirroritems2 selection[2]
						lbx1.items = mirrori7x7
					)
					
				if keyboard.controlPressed then
				(
					aa = selection[1]
					bb = selection[2]
					if ((aa.children.count > 0) and (bb.children.count > 0)) then
					(
						select #(aa.children[1],bb.children[1])
					)
					else
					(
						mnin = aa
						while (aa.parent.children.count == 1) do
						(
							mnin = aa
							aa = aa.parent
							bb = bb.parent
						)
						mnin = aa
						aa = aa.parent
						bb = bb.parent
						mninNbr  = 0
						for i=1 to aa.children.count do
						(
							if (aa.children[i] == mnin) do 
								mninNbr = i
						)
						if (((mninNbr+1) <= aa.children.count) and ((mninNbr+1) <= bb.children.count)) do
						(
							select #(aa.children[mninNbr+1],bb.children[mninNbr+1])
						)
					)
				)
			)
			else
				messagebox "Selected bones not in list"
		)
		else
		 messagebox "Select 2 bones"
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on btn6_rem pressed do
	(
		if (lbx1.selection != undefined) do
		if (lbx1.selection > 0) do
		(
			selcilem = lbx1.selection
			deleteItem mirroritems1 selcilem
			deleteItem mirroritems2 selcilem
			deleteItem mirrori7x7 selcilem
			lbx1.items = mirrori7x7
		)
	)
	on chk1 changed State do
	(
		try(
				if (mirrori7x7.count == 0) do
				(
					bippdSRC = BONES_Coll[1]
					if (classof(bippdSRC) == Biped_Object) then
					(
					--chk1.checked = false
					i = 1
					nde =  (biped.getNode bippdSRC #larm link:i)
					nde2 = (biped.getNode bippdSRC #rarm link:i)
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #larm link:i)
						nde2 = (biped.getNode bippdSRC #rarm link:i)
					)
					-----------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lfingers link:i)
					nde2 = (biped.getNode bippdSRC #rfingers link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lfingers link:i)
						nde2 = (biped.getNode bippdSRC #rfingers link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lleg link:i)
					nde2 = (biped.getNode bippdSRC #rleg link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lleg link:i)
						nde2 = (biped.getNode bippdSRC #rleg link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #ltoes link:i)
					nde2 = (biped.getNode bippdSRC #rtoes link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #ltoes link:i)
						nde2 = (biped.getNode bippdSRC #rtoes link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lUparmTwist link:i)
					nde2 = (biped.getNode bippdSRC #rUparmTwist link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lUparmTwist link:i)
						nde2 = (biped.getNode bippdSRC #rUparmTwist link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lfArmTwist link:i)
					nde2 = (biped.getNode bippdSRC #rfArmTwist link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lfArmTwist link:i)
						nde2 = (biped.getNode bippdSRC #rfArmTwist link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lThighTwist link:i)
					nde2 = (biped.getNode bippdSRC #rThighTwist link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lThighTwist link:i)
						nde2 = (biped.getNode bippdSRC #rThighTwist link:i) 
					)
					 --------------------------------------------------------------------------------------------------------
					 i = 1
					nde =  (biped.getNode bippdSRC #lCalfTwist link:i)
					nde2 = (biped.getNode bippdSRC #rCalfTwist link:i) 
					while (( nde != undefined) and ( nde2 != undefined)) do
					(
						if (((findItem BONES_Coll nde) >0) and ((findItem BONES_Coll nde2) >0)) then
						(
							append mirroritems1 nde
							append mirroritems2 nde2
							append mirrori7x7 (nde.name + " :: " + nde2.name)
						)
						i=i+1
						nde = (biped.getNode bippdSRC #lCalfTwist link:i)
						nde2 = (biped.getNode bippdSRC #rCalfTwist link:i) 
					)
				   )
				   else
				   (
					   allbnes = #()
					   allbnes = deepcopy BONES_Coll
					   Lbnes = #()
					   Rbnes = #()
					   c = allbnes.count 
					   
					  for i = 1 to c do
						(
						   b01 = allbnes[i]
						   if (((findItem Lbnes i) == 0) and (((findItem Rbnes i) == 0))) do
						   (
							   
							   if(i+1 <= c) do
							    (
								   for j = (i+1) to c do
								   (
									   b02 = allbnes[j]
									   n1 = b01.name
									   n2 = b02.name
									   
									   if(n1.count == n2.count) do
									   (
										   p = substituteString n1 "Right" "?????"
										   p = substituteString p "right" "?????"
										   p = substituteString p "RIGHT" "?????"
										   p = substituteString p "Left" "????"
										   p = substituteString p "left" "????"
										   p = substituteString p "LEFT" "?????"
										   p = substituteString p "R" "?"
										   p = substituteString p "r" "?"
										   p = substituteString p "L" "?"
										   p = substituteString p "l" "?"
										   
										   if(matchPattern n2 pattern:p) do
										   (
											   append Lbnes i
											   append Rbnes j
											   append mirroritems1 BONES_Coll[i]
											   append mirroritems2  BONES_Coll[j]
											   append mirrori7x7 (BONES_Coll[i].name + " :: " + BONES_Coll[j].name)
										   )
									   )
								   )
								)
							 )
						 )
						 free allbnes
						 free Lbnes
						 free Rbnes
				   )
					 --------------------------------------------------------------------------------------------------------
					lbx1.items = mirrori7x7
				)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on btn7_res pressed do
	(
		try(
		skinneeded()
		if ((queryBox "Assign weight back to envelopes for all verts ?" title:"") == true) do
		(
			ar = #()
			a = (getPolygonCount selected_obj)[2]
			for u = 1 to a do
				append ar u
			skinOps.SelectVertices selected_obj_skin ar
			skinOps.resetSelectedVerts selected_obj_skin
		)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on btn7_wgh pressed do
	(
		try(
		skinneeded()
		if ((queryBox "Assign weight for all verts ?" title:"") == true) do
		(
			for vx = 1 to vertxcount do
			(
				bn0 = skinOps.GetVertexWeightCount selected_obj_skin vx
				ub =0
				for u = 1 to bn0 do
				(
					d = skinOps.GetVertexWeightBoneID selected_obj_skin vx u
					wgh = (skinOps.GetVertexWeight selected_obj_skin vx u)
					skinOps.SetVertexWeights selected_obj_skin vx d wgh
				)
			)
		)
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on tmr1 tick do
	(
		try(
		if (Sspn5.value > (Sspn5.range.y/2))do
			Sspn5.range.y = Sspn5.range.y *2
		if ((chk1.state == true) and (mirrori7x7.count > 0)) do
		(
		   for bb=1 to mirroritems1.count do 
			(
				if (mirroritems1[bb] == BONES_Coll[selected_bone] ) do
				(
					bb2 = findItem BONES_Coll mirroritems2[bb]
					if bb2 > 0 do
					(
						vdo = (listofouter[selected_bone] * Sspn5.value)/100
						skinOps.SetOuterRadius selected_obj_skin bb2 1 vdo
						skinOps.SetOuterRadius selected_obj_skin bb2 2 vdo
					)
				)
			)
		)
		tmr1.active = false
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on tmr2 tick do
	(
		try(
		if (Sspn6.value > (Sspn6.range.y/2))do
			Sspn6.range.y = Sspn6.range.y *2
		if ((chk1.state == true) and (mirrori7x7.count > 0)) do
		(
		   for bb=1 to mirroritems1.count do 
			(
				if (mirroritems1[bb] == BONES_Coll[selected_bone] ) do
				(
					bb2 = findItem BONES_Coll mirroritems2[bb]
					if bb2 > 0 do
					(
						vdo = (listofinner[selected_bone] * Sspn6.value)/100
						skinOps.SetInnerRadius selected_obj_skin bb2 1 vdo
						skinOps.SetInnerRadius selected_obj_skin bb2 2 vdo
					)
				)
			)
		)
		tmr2.active = false
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on tmr3 tick do
	(
		try(
		vrx = #()
		spnvl = spnsel.value
		if ( spnvl > 0) do
		(
			for vx = 1 to vertxcount do
			(
				bn0 = skinOps.GetVertexWeightCount selected_obj_skin vx
				ub =0
				for u = 1 to bn0 do
				(
					d = skinOps.GetVertexWeightBoneID selected_obj_skin vx u
					if (d == selected_bone) do
					 ub =u
				)
				if (ub > 0) do
				(
					wg = (skinOps.GetVertexWeight selected_obj_skin vx ub)
					if (chk4inv.state == false) then
					(
						if ( spnvl == 0.01) do
							 spnvl = 0.0001
						if ( wg >= spnvl ) do
							append vrx vx
					)
					else
					(
						if ((wg <= spnvl ) and (wg > 0)) do
							append vrx vx
					)
				)
			)
		)
		skinOps.SelectVertices selected_obj_skin vrx
		tmr3.active = false
			)
		catch
		(
			print (getCurrentException())
		)
	)
	on tmr4 tick do
	(
		if (signalvalu==1) then
		(
			bnm_1.boxMode = not(bnm_1.boxMode)
			signalvalu = 2
		)
		else if (signalvalu==2) then
		(
			bnm_1.boxMode  = not(bnm_1.boxMode)
			signalvalu =3
		)
		else if (signalvalu==3) then
		(
			bnm_1.boxMode  = not(bnm_1.boxMode)
			signalvalu =4
		)
		else if (signalvalu==4) then
		(
			--bnm_1.boxMode  = not(bnm_1.boxMode)
			bnm_2.boxMode  = not(bnm_2.boxMode)
			tmr4.interval = 250
			signalvalu =5
		)
		else if (signalvalu==5) then
		(
			bnm_2.boxMode  = not(bnm_2.boxMode)
			tmr4.active = false
			bnm_1.boxMode  = boxmodeb1
			bnm_2.boxMode  = boxmodeb2
			tmr4.interval = 150
		)
	)
	on chk113 changed state do
	(
		selected_obj_skin.draw_vertices  = state
	)
	on chk114 changed state do
	(
		try(
		envlopsubobj()
		if (vvcolor == 1) then
		(
			chk114.checked = true
			selected_obj_skin.shadeweights = not(selected_obj_skin.shadeweights)
			selected_obj_skin.shadeweights = true
			selected_obj.xray = false
			selected_obj.backFaceCull = false
			chk114.caption = "Shaded (default)"
			vvcolor = 2
		)
		else if (vvcolor == 2) then
		(
			chk114.checked = true
			selected_obj.vertexColorsShaded = false
			selected_obj.xray = false
			selected_obj.backFaceCull = false
			chk114.caption = "Colored"
			vvcolor = 3
		)
		else if (vvcolor == 3) then
		(
			chk114.checked = true
			selected_obj.vertexColorsShaded = false
			selected_obj.xray = true
			selected_obj.backFaceCull = true
			chk114.caption = "See through"
			vvcolor = 4
			vlr02 = skinOps.GetInnerRadius selected_obj_skin  selected_bone 1
		skinOps.SetInnerRadius selected_obj_skin selected_bone 1 (vlr02 - 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 2 (vlr02 - 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 1 (vlr02 + 0.1)
		skinOps.SetInnerRadius selected_obj_skin selected_bone 2 (vlr02 + 0.1)
		)
		else if (vvcolor == 4) then
		(
			chk114.checked = false
			selected_obj_skin.shadeweights = false
			selected_obj.xray = false
			selected_obj.backFaceCull = false
			chk114.caption = "Off"
			vvcolor = 1
		)
			)
		catch
		(
			print (getCurrentException())
		)
	)
)
createdialog skinpp
)