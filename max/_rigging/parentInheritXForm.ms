e
========
$.transform = $.parent.transform
========
for i in (selection as array) do
(
	i.transform = i.parent.transform
)
========
--Parentizes selection
Selobj = selection as array
for x = 2 to (Selobj.count)  do
(
	Selobj[x].parent = Selobj[x-1]
)


=======

Selobj = selection as array
for x = 1 to (Selobj.count)  do
(
	PT= point()
	PT.name = "xform_" + Selobj[x].name
	PT.transform = Selobj[x].transform
	--PT.size = Selobj[x].width * 6
	PT.box = true
	PT.cross = false
	PT.centermarker = false
	PT.axistripod =false
	PT.wirecolor = (color 0 255 0)
	PT.size = 3
	
	PT.parent = SelObj[x].parent
	SelObj[x].parent = PT
	/*SelObj[x].parent = CirShp
	CirShp.parent= PT
	PT.parent = PTz*/
	
	
)


--only select two for this one! makes a child/parent between the two.
--parent first, child second
Selobj = selection as array

PT= point()
PT.name = "PTx_" + Selobj[1].name
PT.transform = Selobj[1].transform
--PT.size = Selobj[x].width * 6
PT.box = true
PT.cross = false
PT.centermarker = false
PT.axistripod =false
PT.wirecolor = (color 14 255 2)

-- PT.parent = SelObj[1].parent
PT.parent = SelObj[1]
SelObj[2].parent = PT
/*SelObj[x].parent = CirShp
CirShp.parent= PT
PT.parent = PTz*/
	
	
==================================
--Select obj to parent to, then object(with offset parent in heirarchy) being parented
Selobj = selection as array
PT = point()
PT.cross = true
PT.name = "PTx_"+Selobj[2].name
Pt.transform = Selobj[1].transform
Pt.parent = Selobj[1]
posCon = position_Constraint()
rotCon = orientation_constraint()
Selobj[2].parent.pos.controller.available.controller = posCon

Selobj[2].parent.rotation.controller.available.controller = rotCon
posCon.appendTarget Pt 100
rotCon.appendTarget Pt 100
Selobj[2].transform = Selobj[1].transform
++++++++++++++++++++++++++++++++++
--parents entire selection to point that is parented to first object's parent in selection.
Selobj = selection as array
print selobj.center

PT= point()
PT.name = "parentCtrl_" + Selobj[1].name
select selobj
PT.position = $.center
--PT.size = Selobj[x].width * 6
PT.box = true
PT.cross = false
PT.centermarker = false
PT.axistripod =false
PT.wirecolor = (color 14 255 2)
PT.size = 1

PT.parent = SelObj[1].parent
for x in SelObj do x.parent = PT

/*SelObj[x].parent = CirShp
CirShp.parent= PT
PT.parent = PTz*/
	
	


==================================
--Adds parent between original parent and children. Used to add a ctrl shape to a point parent.
ctrlShape = selection as array
xformPoints = selection as array

for x in xformPoints do
(
	kids = x.children
	newCtrl = copy ctrlShape[1] 
	newCtrl.transform = x.transform
	kids.parent = newCtrl
	newCtrl.parent = x
	
)



==================================
--Replaces obj with new and improved obj.
newObj = $
oldObjs = (selection as array)

for i in oldObjs do
(
	bob = instance newObj
	
	bob.parent = i.parent
	
	bob.name = i.name
	bob.transform.controller = i.transform.controller
	bob.transform = i.transform
	i.children.parent = bob
	delete i
)

==================================
sels = (selection as array)

sels[1].parent = sels[2].parent


==================================

ptArray = #()
circleArray = #()
for i in (selection as array) do
(

	if classof i == Point do append ptArray i
	if classof i == Circle do append circleArray i
)

for i in circleArray do
(
	for o in ptArray do
	(
		if findString o.name i.name  != undefined  do
		(
			print "match"
			posCon = position_Constraint()
			rotCon = orientation_constraint()
			i.parent.pos.controller.available.controller = posCon
			i.parent.rotation.controller.available.controller = rotCon
			posCon.appendTarget o 100
			rotCon.appendTarget o 100
			i.transform =o.transform
		)
	)
)


==================================
for i in (selection as array) do
(	
	randPhase = random -100.0 100.0
	randOffset = random .5 1.2
	randFreq = random 40.0 60.0
	randAmp = random 4.0 8.0
	i.modifiers[#Attribute_Holder].WingControls.phase = randPhase
	i.modifiers[#Attribute_Holder].WingControls.offset = randOffset
	i.modifiers[#Attribute_Holder].WingControls.freq = randFreq
	i.modifiers[#Attribute_Holder].WingControls.amp = randAmp
)

selectArray = #()
for i in (selection as array) do
(
	
	if findString i.name "Settings" != undefined  do append selectArray i
		
)

select selectArray
-------------------
--Add child points
for i in (selection as array) do
(
	thisPoint = Point size:.5 name:(i.name+"_child")
	thisPoint.box = True
	thisPoint.transform = i.transform
	thisPoint.parent = i
)
----------------
-- instance controllers 

instancedCon = $.transform.controller
for i in (selection as array) do
(
	i.controller = instancedCon
)