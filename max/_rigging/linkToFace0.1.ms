
 if MainFloater != undefined then (closerolloutfloater mainfloater)
 
 MainFloater = newRolloutFloater "Link Tools" 180 182
    
 rollout face_linker "Link to Face"
 (
    Group "Link to Faces"
    (
 	   button getFaceBtn "Get Selected Face(s)"
 	   edittext dummyTxt "Name: " fieldWidth:100 text:"attDummy_"
 	   button createDummiesBtn "Create attachDummies"
 	   checkbox alignDummies "Align to face normal" checked:true
 	)
    Group "Link to Vertices"
 	(
 		--we do this next
 	)
 
 --locals
 	local theNode
 	local theFaces
 	
 --ui
 	on getFaceBtn pressed do
 	(
 		if selection.count != 1 then (return undefined)
 		theNode = $
 		theFaces = polyop.getFaceSelection theNode
 		getFaceBtn.text = ((theFaces as array).count as string + " Faces on " + theNode.name)
 	)
 	   
 	on createDummiesBtn pressed do
 	(
 		undo "createDummies" on
 		(
 			i=1
 			for face in theFaces do
 			(
 				theFace = ((polyop.getFaceSelection theNode as array)[i])
 				local new
 				if alignDummies.checked == true then
 				(
 					new = point name:(dummyTxt.text + i as string) position:(polyop.getFaceCenter theNode theFace) dir:(polyop.getFaceNormal theNode theFace) size:1
 				)
 				else
 				(
 					new = point name:(dummyTxt.text + i as string) position:(polyop.getFaceCenter theNode theFace) size:1
 				)
				new.position.controller = position_script()
				new.position.controller.script = ("dependson $" + theNode.name +"\npolyop.getFaceCenter $'" + theNode.name +"' " + theFace as string)
				i += 1
 			)
 		)
 	)
 )
 addrollout face_linker MainFloater