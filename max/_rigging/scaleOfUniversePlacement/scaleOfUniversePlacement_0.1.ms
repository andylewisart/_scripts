﻿
Try(DestroyDialog uniPlace)catch()

rollout uniPlace "Scale Of Universe Placement Tool" 
(
	local sphereArray
	pickbutton theObj_btn "Object of Interest"
	label heightLbl "Object Height:"
	spinner theObjHeight_spin "" range:[0,100,1] across:3 align:#left width:40
	label lblTimesTen " * 10 ^" align:#center
	spinner theObjPow_spin "" range:[-100,100,1] width:40 type:#integer
	label lblMeters "meters"
	button placeObj_btn "OK" width:100
	button testBtn "TEST"


	fn placeObj =
	(
		keepGoing = True
		for x=1 to sphereArray.count while keepGoing do
		(
			formula = (theObjHeight_spin.value*(pow 10 theObjPow_spin.value))/(pow 10 sphereArray[x].baseObject.thisScaleAttr.theScale)
			print formula
			if formula == 1 then
			(
				keepGoing = False
				betweenA = sphereArray[x]
				betweenB = sphereArray[x+1]
			)				
			else if formula < 1 then
			(
				keepGoing = False
				betweenA = sphereArray[x-1]
				betweenB = sphereArray[x]
			)
			
		)
		
		calcHeight = theObjHeight_spin.value*(pow 10 theObjPow_spin.value)
		betweenAValue = pow 10 betweenA.baseObject.thisScaleAttr.theScale
		betweenBValue = pow 10 betweenB.baseObject.thisScaleAttr.theScale
		format "N:% A:% B:%\n" calcHeight betweenAValue betweenBValue
		blendPercent = ((calcHeight-betweenAValue)/(betweenBValue-betweenAValue))*100
		print blendPercent
		print "&&&&&"
		thisObj = theObj_btn.object
		posCtrl = position_constraint()
		thisObj.controller.position.controller = posCtrl
		posConstraintInterface = posCtrl.constraints
		posConstraintInterface.appendTarget betweenA (100-blendPercent)
		posConstraintInterface.appendTarget betweenB blendPercent
		
	)
	
	fn scaleobj =
	(
		target = $MASTER_sphere
		obj = theObj_btn.object
		ratios = (target.max - target.min) / (obj.max - obj.min)
		the_scale = amin #(ratios.x, ratios.y, ratios.z)

		scale obj (the_scale * [1, 1, 1])
	)
	
	fn updateMeters = 
	(
		lblMeters.text = (theObjHeight_spin.value* pow 10 theObjPow_spin.value) as string +" meters"
	)
	
	on theObjHeight_spin entered do
	(
		updateMeters()
	)
	
	on theObjPow_spin entered do
	(
		updateMeters()
	)
	
	on theObj_btn picked obj do
	(
	--see if the user did not cancel the picking...
		if obj != undefined do
		(
			--if he did not, make the box's wireframe red:
			obj.wirecolor = red
			--and display the name of the object on the button:
			theObj_btn.text = obj.name
		)
	)

	on placeObj_btn pressed do
	(
		if theObj_btn.object != undefined then
		(
			clearlistener()
			scaleobj()
			placeObj()
		)
		else
		(
			messagebox "Please select object of interest."
		)
	)
	
	on testBtn pressed do
	(
		clearlistener()
		show theObjHeight_spin.value
	)
	
	on uniPlace open do
	(
		updateMeters()
		select $MASTER_sphere...*
		sphereArray = (selection as array)
		deselect sphereArray
	)
	
)

createDialog uniPlace width:200