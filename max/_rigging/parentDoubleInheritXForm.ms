fn makeParent = 
(

	PT= point()
	PT.name = "CTRL_" + $.name
	PT.transform = $.transform
	--PT.size = Selobj[x].width * 6
	PT.box = true
	PT.cross = false
	PT.centermarker = false
	PT.axistripod =false
	PT.wirecolor = (color 24 176 203)
	
	PT.parent = $.parent
	$.parent = PT
	/*SelObj[x].parent = CirShp
	CirShp.parent= PT
	PT.parent = PTz*/
)
fn makeSecondParent = 
(

	PT= point()
	PT.name = "XFORM_" + $.name
	PT.transform = $.transform
	--PT.size = Selobj[x].width * 6
	PT.box = false
	PT.cross = false
	PT.centermarker = false
	PT.axistripod =true
	PT.wirecolor = (color 120 7 7)
	
	PT.parent = $.parent
	$.parent = PT
	/*SelObj[x].parent = CirShp
	CirShp.parent= PT
	PT.parent = PTz*/
)
		
	

Selobj = selection as array
for x = 1 to (Selobj.count)  do
(
	select SelObj[x]
	makeParent()
	select SelObj[x].parent
	makeSecondParent()
)