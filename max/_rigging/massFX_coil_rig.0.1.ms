﻿--add instanced rigid body modifier to selected
modPanel.addModToSelection (MassFX_RBody ())
--create Hingeconstrint
    --loop throught the selection array aligning and assinging the parent and child to the constraint.
    v= 10000
for i =1 to (selection.count-1) do
    (
        parent = selection[i+1]
        child = selection[i]
        TheHindgeJoint = UConstraint()
        TheHindgeJoint.parent = parent
        TheHindgeJoint.body1 = parent
        TheHindgeJoint.body0 = child
        TheHindgeJoint.helpersize = 8.98711
        TheHindgeJoint.position = child.position
        TheHindgeJoint.rotation = child.rotation
        rotate TheHindgeJoint (angleaxis 180 [0,0,1])
        TheHindgeJoint.swing1Angle = 10
		TheHindgeJoint.swing1Mode = 2
        TheHindgeJoint.swing2Mode = 2
        TheHindgeJoint.swing2Angle = 10
        TheHindgeJoint.helpersize = 8.98711
        TheHindgeJoint.posDamping = v
        TheHindgeJoint.swingDamping = v
        TheHindgeJoint.twistDamping = v
        TheHindgeJoint.swing1Restitution = v
        TheHindgeJoint.swing2Restitution = v
        TheHindgeJoint.posSpring = v
        TheHindgeJoint.swingSpring = v
        TheHindgeJoint.twistSpring = v
    )
	
	
	
selObj = (selection as array)
for i in selObj do
(
	i.modifiers[#MassFX_Rigid_Body].meshRadius = 4.5
)


v= 100
selObj = (selection as array)
for TheHindgeJoint in selObj do
(

	TheHindgeJoint.swing1Angle = 10
	TheHindgeJoint.swing1Mode = 2
	TheHindgeJoint.swing2Mode = 2
	TheHindgeJoint.swing2Angle = 10
	TheHindgeJoint.helpersize = 5
	TheHindgeJoint.posDamping = v
	TheHindgeJoint.swingDamping = v
	TheHindgeJoint.twistDamping = v
	TheHindgeJoint.swing1Restitution = v
	TheHindgeJoint.swing2Restitution = v
	TheHindgeJoint.posSpring = v
	TheHindgeJoint.swingSpring = v
	TheHindgeJoint.twistSpring = v
)



selObj = (selection as array)
thisForce = $
for i in selObj do
(
	append i.modifiers[#MassFX_Rigid_Body].forcesList thisForce 
)