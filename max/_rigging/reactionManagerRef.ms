--Setup a scene
b1 = box name:"box01" pos:[-32.5492,-21.2796,0] -- create two boxes
b2 = box name:"box02" pos:[51.3844,-17.2801,0]
animate on at time 100 b1.pos = [-48.2522,167.132,0]-- animate position of one box
--
--Assign a reactor, pick the react to object, and create reactions
cont = b2.pos.controller = position_Reactor ()
--
--you can either react to a controller
reactTo cont b1.pos.controller
--or a node (the World Space position of the box)
--reactTo cont b1
--
slidertime = 100
createReaction cont
slidertime = 50
createReaction cont
deleteReaction cont 3
--
--Set the reaction parameters
setReactionState cont 2 [65.8385,174.579,0]
selectReaction cont 1
setReactionInfluence cont 1 100
setReactionStrength cont 1 1.2
setReactionFalloff cont 1 1.0
setReactionValue cont 1 [-40.5492,-20.0,0]
setReactionName cont 1 "My Reaction"
--
--get the reaction parameters
getReactionInfluence cont 1
getReactionStrength cont 1
getReactionFalloff cont 1
getReactionState cont 1
getReactionValue cont 1
getSelectedReactionNum cont
getReactionCount cont
getReactionName cont 1




bezierController = Bezier_Float()
if $.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller == undefined do $.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller = bezierController

cont = $Teapot001.pos.controller = position_Reactor ()
reactTo cont $Circle001.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller

createReaction cont

setReactionState cont 1 [65.8385,174.579,0]
setReactionState cont 2 [-65.8385,-174.579,0]
setReactionValue cont 2 100

-- bezierController = Bezier_Float()
-- if $.modifiers[#Bend].BendAngle.controller == undefined do $.modifiers[#Bend].BendAngle.controller = bezierController

-- cont = $.modifiers[#Bend].BendAngle.controller = Float_Reactor ()
-- reactTo cont $Circle001.modifiers[#Attribute_Holder].Custom_Attributes.Param1.controller

-- createReaction cont

