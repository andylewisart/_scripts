/*
	AttachmentAlign_06
	Author: Benedikt Rey
	Website: brey.bplaced.net
	Date: 14.02.2014
	Version: unpolished v06
*/


-- macroScript AttachmentAlign
-- Category:"BR Tools"
-- ToolTip:"Creates an alinged attachment constrained helper."
-- (
	
	P1 = undefined
	P2 = undefined
	t_Plane = undefined
	Triangles = undefined
	MAXAKeyVar = undefined

	distanceArray = #()
	DetailDistance = #()
	Coords = #()
	FaceCoords = #()

	fn CreateAttachmentPoint Plane1:t_Plane = 
		(
			ThePoint = Point centermarker:true box:true size:5.0 name:"Point_Helper_Attachment_Pivot" wirecolor:orange
			ThePoint.pos.controller = Position_List()
			ThePoint.pos.controller.available.controller = Attachment()
			ThePoint.pos.controller.active = 2
			ThePoint.pos.controller.Attachment.controller.node = Plane1
			
			addNewKey ThePoint.pos.controller.Attachment.controller 0f
			ThePoint
		)

	fn LowestDistance ArrayToCheck =
	(
		checkVar = amin ArrayToCheck
		index = (findItem ArrayToCheck checkVar)
		#(index, checkVar)
	)

	fn FindCoords acc:10 progressBool:false UsedArray1:DetailDistance UsedArray2:Coords Point1:P1 Point2:P2 MAXAKeyIs:MAXAKeyVar =
	(
		If acc == 10 or acc == 100 or acc == 1000 then
		(
			if progressBool do progressStart "post-processing"
			
			for i = 1 to acc do
			(
				for n = 1 to acc do
				(
					MAXAKeyIs.coord = [i/acc as float,n/acc as float]
					UsedArray1[i*acc+n-acc] = distance Point1.pos Point2.pos
					UsedArray2[i*acc+n-acc] = MAXAKeyIs.coord
				)
				if progressBool do progressUpdate (100.0*i/acc)
			)
			MAXAKeyIs.coord = UsedArray2[(LowestDistance UsedArray1)[1]]
			
			free UsedArray1
			free UsedArray2
			if progressBool do progressEnd()
		)
		else
		(
			Messagebox "Could not find a correct position ... you need to enter a accuracy of 10, 100 or 1000!"
		)
	)

	fn CheckThreePoints acc:10 progressBool:false UsedArray1:DetailDistance UsedArray2:Coords Point1:P1 Point2:P2 MAXAKeyIs:MAXAKeyVar =
	(
		MAXAKeyIs.coord = [0.0,0.0]
		UsedArray1[1] = distance Point1.pos Point2.pos
		UsedArray2[1] = MAXAKeyIs.coord
		MAXAKeyIs.coord = [1.0,0.0]
		UsedArray1[2] = distance Point1.pos Point2.pos
		UsedArray2[2] = MAXAKeyIs.coord
		MAXAKeyIs.coord = [0.0,1.0]
		UsedArray1[3] = distance Point1.pos Point2.pos
		UsedArray2[3] = MAXAKeyIs.coord
		MAXAKeyIs.coord = [0.333,0.333]
		UsedArray1[3] = distance Point1.pos Point2.pos
		UsedArray2[3] = MAXAKeyIs.coord
		
		MAXAKeyIs.coord = UsedArray2[(LowestDistance UsedArray1)[1]]
	)

	fn FindExactPoint progressBool:true acc:100 UsedArray1:DetailDistance UsedArray2:Coords Point1:P1 Point2:P2 MAXAKeyIs:MAXAKeyVar =
	(
		if progressBool do progressStart "post-process 1"
		free UsedArray1
		free UsedArray2
		
		if acc == 10 or acc == 100 then
		(
			start = timeStamp()
			run1 = acc
			
			for i = 1 to run1 do
			(
				for n = 1 to run1 do
				(
					MAXAKeyIs.coord = [i/run1 as float,n/run1 as float]
					UsedArray1[i*run1+n-run1] = distance Point1.pos Point2.pos
					UsedArray2[i*run1+n-run1] = MAXAKeyIs.coord
				)
				if progressBool do If getProgressCancel() do exit
				if progressBool do progressUpdate (100.0*i/run1)
			)
			MAXAKeyIs.coord = UsedArray2[(LowestDistance UsedArray1)[1]]
			free UsedArray1
			free UsedArray2
			end = timeStamp()
			format "Processing took % seconds\n" ((end - start) / 1000.0)
		)
		else
		(
			start = timeStamp()
			run1 = acc
			
			for i = 1 to run1 do
			(
				for n = 1 to run1 do
				(
					MAXAKeyIs.coord = [i/run1 as float,n/run1 as float]
					UsedArray1[i*run1+n-run1] = distance Point1.pos Point2.pos
					UsedArray2[i*run1+n-run1] = MAXAKeyIs.coord
				)
				if progressBool do If getProgressCancel() do exit
				if progressBool do progressUpdate (100.0*i/run1)
			)
			MAXAKeyIs.coord = UsedArray2[(LowestDistance UsedArray1)[1]]
			free UsedArray1
			free UsedArray2
			end = timeStamp()
			format "Processing took % seconds\n" ((end - start) / 1000.0)
		)
		if progressBool do progressEnd()
	)

	fn FinalFacesIndex ArrayToCheck =
	(
		TempArray = #()
		for i = 1 to ArrayToCheck.count do 
		(
			TempArray[i] = ArrayToCheck[i]
		)
		sort TempArray
		
		resultArray = #()
		
		for i = 1 to 9 do
		(
			resultArray[i] = (findItem ArrayToCheck TempArray[i])
		)
		
		resultArray
	)

	fn FindCorrectPos TriangleCount:Triangles accuracy:1000 accTest:10 UsedArray3:distanceArray UsedArray4:FaceCoords Point1:P1 Point2:P2 MAXAKeyIs:MAXAKeyVar =
	(
		start_all = timeStamp()
		progressStart "pre-processing"
		for i = 1 to TriangleCount do
		(
			if i == 1 then MAXAKeyIs.face = 0
			else MAXAKeyIs.face = i-1
			
			CheckThreePoints()
			
			UsedArray3[i] = distance Point1.pos Point2.pos
			UsedArray4[i] = MAXAKeyIs.coord
			
			progressUpdate (100.0*i/TriangleCount)
		)
		MAXAKeyIs.face = (LowestDistance UsedArray3)[1]-1
		MAXAKeyIs.coord = UsedArray4[(LowestDistance UsedArray3)[1]]
		progressEnd()
		
		FindExactPoint acc:accuracy
		Point2.pos.controller.Attachment.controller.manupdate = true 
		AttachCtrl.update Point2.pos.controller.Attachment.controller
		Point2.pos.controller.Attachment.controller.manupdate = false
		end_all = timeStamp()
		format "All Processing took % seconds\n" ((end_all - start_all) / 1000.0)
	)

	fn EditablePolyFilter obj = ((classOf obj) == Editable_Poly)
	fn HelperFilter obj = ((classOf obj) == Point)

	fn CountTris surface1 =
	(
		a = #((polyop.getNumFaces surface1),(polyop.getNumEdges surface1))
		(a[2] - a[1]) * 2
	)

	rollout AttachToTheRightPoint "Align Attachment"	width:200
	(
		Button btnHelp "Help" width:95 pos:[5,5]
		Button btnAbout "About" width:90 pos:[105,5]
		pickbutton pckTarget "Pick Target" pos:[5,30] width:95 filter:HelperFilter
		label lblTarget "	<<" pos:[105,35] width:100
		pickbutton pckSurface "Pick Surface" tooltip:"Needs to be an Editable Poly" filter:EditablePolyFilter pos:[5,55] width:95
		label lblSurface "	<<" pos:[105,60] width:100
		listbox lstAccuracy "Accuracy" items:#("right on the point","near the point","same face") height:3 pos:[5,85] tooltip:"\"Best\" could take a bit ..." width:190
		Button btnProceed "Proceed" tooltip:"The more poly's the longer does it take ...\nkeep that in mind ..." width:190
		
		on AttachToTheRightPoint open do 
		(
			lstAccuracy.selection = 2
		)
		
		on btnAbout pressed do
		(
			Rollout rA "About" width:150 height:50
			(
				label lbl "Author: Benedikt Rey\ndate: 14.02.2014" width:180 height:140 pos:[10,5]
				label lbl1 "brey.bplaced.net" width:180 height:140 pos:[10,35] color:orange
			)
			createdialog rA
		)
		
		on btnHelp pressed do 
		(
			Rollout rH "Help" width:200 height:470
			(
				label lbl "" pos:[10,5] width:180 height:460
				on rH open do
				(
					HelpString = (
						"Make sure your Mesh is an EditablePoly.\nPlace with AutoGrid on, or how ever you want, your target (Point_Helper) on the mesh, or near to it.\n" +
						"Pick both into this script and proceed.\nThe script will place a helper and attachment constrain it to your mesh." +
						
						"This script will not work on every mesh perfectly, so try the lowest accuracy before you chose a higher.\n\n" +
						"On my Computer, the highest accuracy took 320.079 seconds to post-process and it is 0.00047196 units far away from the target.\n\n" +
						"The middle accuracy took 3.346 seconds to post-process and it is 0.0063131 units far away.\n\n" +
						"The lowest accuracy took 0.067 seconds to post-process and it is 0.168603 units far away.\n\n" +
						"The preprocessing took 5.5 seconds on a 3080 triangle mesh. The more triangles the more seconds ...\n\n" +
						"I think the best solution is to use the default accuracy.\n\n" +
						"Total process of a 18360 triangles mesh took 180 seconds.")
					lbl.caption = HelpString
				)
			)
			createdialog rH
		)
		
		on pckTarget picked obj do
		(
			lblTarget.caption = obj.name
			P1 = obj
		)
		
		on pckSurface picked obj do
		(
			lblSurface.caption = obj.name
			t_Plane = obj
			
			Triangles = CountTris obj
		)
		
		on btnProceed pressed do
		(
			if pckSurface.object != undefined and pckTarget.object != undefined do 
			(
				sel = selection as array
				for i in sel do
				(
					P1 = i
					ref = #(1000,100,10)
					
					P2 = CreateAttachmentPoint()
					MAXAKeyVar = AttachCtrl.getKey P2.pos.controller.Attachment.controller 1
					MAXAKeyVar.coord = [0.333,0.333]
					FindCorrectPos accuracy:ref[lstAccuracy.selection] TriangleCount:Triangles accTest:10
				)
				destroydialog AttachToTheRightPoint
			)
		)
	)

	createdialog AttachToTheRightPoint
-- )
