rollout CRSC "CRSC" width:176 height:210 (
	button btnCreate "Create Controller" pos:[18,10] width:140 height:32 toolTip:"Create Attribute Holder with attribute to control animation"
	editText etModName "name" fieldWidth:120 text:"RotationAniController"
	button btnAddX "Add Attribute X" pos:[34,72] width:110 height:28 enabled:false toolTip:"Add new attribute to existed Attribute Holder"
	button btnAddY "Add Attribute Y" pos:[34,102] width:110 height:28 enabled:false toolTip:"Add new attribute to existed Attribute Holder"
	button btnAddZ "Add Attribute Z" pos:[34,132] width:110 height:28 enabled:false toolTip:"Add new attribute to existed Attribute Holder"
	button btnGet "Get Objects To Control" pos:[18,170] width:140 height:32 enabled:false toolTip:"Select objects to be controlled by previously created attribute"
	
	local controller
	local percent
	local ca
	local m
	local currentAtt = ""

	on btnCreate pressed do (
		m = EmptyModifier()
		m.name = etModName.text
		controller = selection as array
		controller = controller[1]
		btnCreate.text = controller.name
		addModifier controller m
		btnAddX.enabled = true
		--btnAddY.enabled = true
		btnAddZ.enabled = true
	)
	
	on btnGet pressed do (
		local skipped = 0
		if ca != undefined then(
			if currentAtt == "X" then (
				for ob in selection do (
					ob.rotation.controller.X_Rotation.controller = float_script ()
					ob.rotation.controller.X_Rotation.controller.script = "degToRad ((getNodeByName \""+ob.name+"\").rotation.x_rotation + (getNodeByName \""+controller.name+"\").modifiers[#"+etModName.text+"].aniAtt"+currentAtt +".rSpeed)"
				)
			)else if currentAtt == "Y" then (
				for ob in selection do (
					ob.rotation.controller.Y_Rotation.controller = float_script ()
					ob.rotation.controller.Y_Rotation.controller.script = "degToRad ((getNodeByName \""+ob.name+"\").rotation.y_rotation + (getNodeByName \""+controller.name+"\").modifiers[#"+etModName.text+"].aniAtt"+currentAtt +".rSpeed)"
				)
			)else if currentAtt == "Z" then (
				for ob in selection do (
					ob.rotation.controller.Z_Rotation.controller = float_script ()
					ob.rotation.controller.Z_Rotation.controller.script = "degToRad ((getNodeByName \""+ob.name+"\").rotation.z_rotation + (getNodeByName \""+controller.name+"\").modifiers[#"+etModName.text+"].aniAtt"+currentAtt +".rSpeed)"
				)
			)
		)else(
			messagebox "Controller undetected! Action can't be done!"
		)
	)
	
	on btnAddX pressed do (
		if isProperty m #aniAttX then(
			messagebox "X attribute for this controller already exist!"
		)else (
			ca = attributes aniAttX (
				parameters paramAni rollout:rolloutAni (
					rSpeed type:#float ui:(srSpeed)
				)
				rollout rolloutAni "Rotation Animation" (
					spinner srSpeed "X" fieldWidth:100 range:[-1000,1000,0] type:#float
				)
			)
			currentAtt  = "X"
			btnCreate.text = controller.name+":aniAtt"+currentAtt
		)
		custAttributes.add m ca
		btnGet.enabled = true
	)
	
	on btnAddY pressed do (
		if isProperty m #aniAttY then(
			messagebox "Y attribute for this controller already exist!"
		)else (
			ca = attributes aniAttY (
				parameters paramAni rollout:rolloutAni (
					rSpeed type:#float ui:(srSpeed)
				)
				rollout rolloutAni "Rotation Animation" (
					spinner srSpeed "Y" fieldWidth:100 range:[-1000,1000,0] type:#float
				)
			)
			currentAtt  = "Y"
			btnCreate.text = controller.name+":aniAtt"+currentAtt
		)
		custAttributes.add m ca
		btnGet.enabled = true
	)
	
	on btnAddZ pressed do (
		if isProperty m #aniAttZ then(
			messagebox "Z attribute for this controller already exist!"
		)else (
			ca = attributes aniAttZ (
				parameters paramAni rollout:rolloutAni (
					rSpeed type:#float ui:(srSpeed)
				)
				rollout rolloutAni "Rotation Animation" (
					spinner srSpeed "Z" fieldWidth:100 range:[-1000,1000,0] type:#float
				)
			)
			currentAtt  = "Z"
			btnCreate.text = controller.name+":aniAtt"+currentAtt
		)
		custAttributes.add m ca
		btnGet.enabled = true
	)
)
createDialog CRSC