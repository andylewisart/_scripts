
Try(DestroyDialog timeShift)catch()

rollout timeShift "hT Time Shifter"
(
	local animated_props = #()
	local animated_objs = #()
	group "Slide animated keys"
	(
		spinner offset_spin "Offset" type:#integer range: [1,9999,10] align:#center
		radiobuttons whichObj_radio labels:#("Selected Objects", "All objects")
		button offsetTime_btn "Offset Time" 
	)       
	 group "Animated Tracks Only"
	(
		button selectAnimatedTracks "Select" 
		button keyAnimatedTracks "Key" 
	)
		
	fn getAnimatedProps theObject =
	(
		
	  if try (theObject.isAnimated) catch (false) do
		append animated_props theObject
	  for i = 1 to theObject.numSubs do
		getAnimatedProps theObject[i]
	)
	
	fn copyAndShiftKeysForProps = 
	(
		for i in animated_props do
		(
			try
			(
					--get child subanims
				if i.numsubs == 0 do
				(
					
					with animate on
					(
						curValue = i.value
						i.value = curValue
						insertTime  i currenttime (offset_spin.value as time)
						i.value = curValue
					)
				)
			)
			catch(getCurrentException())
	
		)
	)
	fn setKeyOnAnimated = 
	(
		for i in animated_props do
		(
			try
			(
				--get child subanims
				if i.numsubs == 0 do
				(
					with animate on
					(
						curValue = i.value
						i.value = curValue
					)
				)
			)
			catch(getCurrentException())
	
		)
	)
	
	fn checkIfLinkConstraint obj = 
	(
		if (classof obj.controller) == Link_Constraint do
			(
				numTargets = (obj.controller.getNumTargets())
				if numTargets> 0 do
				(
					for y=1 to numTargets do 
					(
						curFrameNo = obj.controller.getframeno(y)
						obj.controller.setFrameNo (y) (offset_spin.value + curFrameNo)
							
					)
				)
			)
	)
	
	on selectAnimatedTracks pressed do
	(
		theHold.Begin()
		
		if whichObj_radio.state ==1 then
		(
			for o in (selection as array) do
			(
				getAnimatedProps o
				if animated_props.count > 0 do append animated_objs o
				animated_props = #()
			)
			select animated_objs
			
		)
		else if whichObj_radio.state ==2 then
		(
			for o in objects do
			(
				getAnimatedProps o
				if animated_props.count > 0 do append animated_objs o
				animated_props = #()
			)
			select animated_objs
		)
		animated_objs = #()
		theHold.Accept "UndoExecution"
	)
	on keyAnimatedTracks pressed do
	(
		theHold.Begin()
		
		if whichObj_radio.state ==1 then
		(
			for o in (selection as array) do
			(
				getAnimatedProps o
				setKeyOnAnimated()
				animated_props = #()
			)
		)
		else if whichObj_radio.state ==2 then
		(
			for o in objects do
			(
				getAnimatedProps o
				setKeyOnAnimated()
				animated_props = #()
			)
		)
		
		theHold.Accept "UndoExecution"
	)
	
	on offsetTime_btn pressed do 
	(
		
		theHold.Begin()
		
		if whichObj_radio.state ==1 then
		(
			for o in (selection as array) do
			(
				checkIfLinkConstraint o
				getAnimatedProps o
				copyAndShiftKeysForProps()
				animated_props = #()
			)
		)
		else if whichObj_radio.state ==2 then
		(
			for o in objects do
			(
				checkIfLinkConstraint o
				getAnimatedProps o
				copyAndShiftKeysForProps()
				animated_props = #()
			)
		)
		
		theHold.Accept "UndoExecution"
		
	)
        
)

createDialog timeShift --width:560 height:390

	
