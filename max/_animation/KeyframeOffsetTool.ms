--Espenkielland@gmail.com
gapDistance = 5

utility KeyframeTool "Keyframe Tool"
(
	
group "Keyframe Settings"
(
	spinner OffsetBySpn "Offset by" type:#integer
	checkbox OffsetSelectedBt "Only selected keys"
	button OffsetFramesBt "Offset Keyframes" 
	
	
	
	on OffsetFramesBt pressed  do
	(
		
	
		--keysPos = $.position.controller.keys
		--objectArray = #($)
		objectArray = #()
		
		for i = 1 to selection.count by 1 do
		(
			append objectArray selection[i]
		)

		for i = 1 to objectArray.count by 1 do
		(
			finalDistance= OffsetBySpn.value * (i-1)
			movekeys objectArray[i] finalDistance
		)
		--addNewKey keysPos 2
		--moveKeys $box01.pos.controller 5
		--moveKeys $ 1f
		--keysRot = $.rotation.controller.keys
		--keyLol = $.controller.keys
	)
)
Button resetb "Reset"
)