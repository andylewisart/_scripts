/*HeaderStart*******************************************************
:Created By:			Paul Neale
:Company:				PEN Productions Inc.
:Site:					http://penproductions.ca
:Email:				info@penproductions.ca
:Client:				PEN Productions Inc.

:Purpose:
	Track Editor Macros
:History:

:Todo:

:Bugs:

:Tests:

*******************************************************HeaderEnd*/

macroScript TrackEditor 
	category:"PEN Animation"
	toolTip:"Toggle Track Editor"
(
	on execute do
	(
		if trackEditor != undefined do
		(
			trackEditor.run()
		)
	)
)
