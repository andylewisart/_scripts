

/*
	---------------------------------------------------
	BakeTM v.1.1 [2010-09-05]
	---------------------------------------------------
	
	Bake animation transform from an object to another object
	
	Author: 	Anubis [http://project3d.narod.ru]
	Started: 	v.1.0 [2009-09-09]
	Updated: 	v.1.2 [2010-12-01]
	Compatibility: 3ds Max 9 and higher
	* (currently tested on 3ds Max 2009)
*/
if classOf infuse_reverseAnim == RolloutClass do DestroyDialog infuse_reverseAnim
rollout infuse_reverseAnim "Bake TM"
(
	local originalAnimatedObject, counterAnimatedObject, rObj, null_A
	fn BakeTransform sourceObj targecounterAnimatedObject strFrame endFrame subStep forceUpdate = (
		animate on (
			for t = strFrame to endFrame by subStep do
			(
				if forceUpdate do sliderTime = t
				at time t
					targecounterAnimatedObject.transform = sourceObj.transform
			)
		)
	)
	group "Bake Transform"
	(
		pickbutton pb_originalAnimatedObject "Pick Original Anim" width:140 autoDisplay:true align:#right
		pickbutton pb_counterAnimatedObject "Pick Counter Anim" width:140 autoDisplay:true align:#right
		spinner sp_strFrame "Start Frame: " range:[-10000,10000,animationRange.start] type:#integer fieldWidth:50
		spinner sp_endFrame "End Frame: " range:[-10000,10000,animationRange.end] type:#integer fieldWidth:50
		spinner sp_subStep "Step sampler: " range:[0.01,1000,1] fieldWidth:50 -- float
		checkbox cbxUpdate "Force Update (*slow)" checked:off
		button bt_bakeTM "Bake Transform" width:140 enabled:false
	)
-- 	group "Reduce Keys"
-- 	(
-- 		pickbutton pb_obj "Pick Object" width:140 autoDisplay:true align:#right
-- 		spinner sp_intFrame "Start Frame: " range:[-10000,10000,animationRange.start] type:#integer fieldWidth:50
-- 		spinner sp_outFrame "End Frame: " range:[-10000,10000,animationRange.end] type:#integer fieldWidth:50
-- 		spinner sp_sStep "Step sampler: " range:[0.01,1000,1] fieldWidth:50 -- float
-- 		spinner sp_thres "Threshold: " range:[0.01,1000,0.5] fieldWidth:50 -- float
-- 		button bt_redKeys "Reduce Transform Keys" width:140 enabled:false
-- 	)
	on sp_strFrame changed val do
		if val >= sp_endFrame.value do sp_strFrame.value = sp_endFrame.value - 1
	on sp_endFrame changed val do
		if val <= sp_strFrame.value do sp_endFrame.value = sp_strFrame.value + 1
	on sp_intFrame changed val do
		if val >= sp_outFrame.value do sp_intFrame.value = sp_outFrame.value - 1
	on sp_outFrame changed val do
		if val <= sp_intFrame.value do sp_outFrame.value = sp_intFrame.value + 1
	on pb_originalAnimatedObject picked obj do
	(
		if isValidNode obj do
		(
			originalAnimatedObject = obj
			if isValidNode counterAnimatedObject do
			(
				if counterAnimatedObject != originalAnimatedObject then
					bt_bakeTM.enabled = true
				else bt_bakeTM.enabled = false
			)
		)
	)
	on pb_originalAnimatedObject rightclick do
	(
		originalAnimatedObject = pb_originalAnimatedObject.object = undefined
		bt_bakeTM.enabled = false
	)
	on pb_counterAnimatedObject picked obj do
	(
		if isValidNode obj do
		(
			counterAnimatedObject = obj
			if isValidNode originalAnimatedObject do
			(
				if originalAnimatedObject != counterAnimatedObject then
					bt_bakeTM.enabled = true
				else bt_bakeTM.enabled = false
			)
		)
	)
	on pb_counterAnimatedObject rightclick do
	(
		counterAnimatedObject = pb_counterAnimatedObject.object = undefined
		bt_bakeTM.enabled = false
	)
-- 	on pb_obj picked obj do
-- 	(
-- 		if isValidNode obj do
-- 		(
-- 			rObj = obj
-- 			bt_redKeys.enabled = true
-- 		)
-- 	)
-- 	on pb_obj rightclick do
-- 	(
-- 		rObj = pb_obj.object = undefined
-- 		bt_redKeys.enabled = false
-- 	)
	on bt_bakeTM pressed do
	(
		theHold.Begin()
		null_A = Point()
		null_A.name = null_A.name+"_null_A"
		null_A.parent = counterAnimatedObject
		null_A.transform.controller = Link_Constraint ()
		null_A.transform.controller.AddTarget counterAnimatedObject 0
		null_A.transform = counterAnimatedObject.transform
		targetNull = Point()
		targetNull.name += "_ANIM"
		targetNull.parent = originalAnimatedObject
		BakeTransform null_A targetNull sp_strFrame.value sp_endFrame.value sp_subStep.value cbxUpdate.state
		newParent = copy originalAnimatedObject
		newParent.name += "_PARENT"
		newCounter = copy counterAnimatedObject
		newCounter.name+="_PATH"
		select newParent
		macros.run "Animation Tools" "DeleteSelectedAnimation"
		null_A.parent = newParent
		targetNull.parent = newParent
		newCounter.parent = targetNull
		delete null_A
		theHold.Accept()
		
	)
	on bt_redKeys pressed do
	(
		max create mode -- needed to fix max2009 bug!
		--setWaitCursor()
		reduceKeys rObj.transform.controller sp_thres.value sp_sStep.value (interval sp_intFrame.value sp_outFrame.value)
		--setArrowCursor()
		DestroyDialog infuse_reverseAnim -- needed to fix max2009 bug!
	)
)
CreateDialog infuse_reverseAnim width:166 \
style:#(#style_titlebar,#style_sysmenu,#style_minimizebox)
