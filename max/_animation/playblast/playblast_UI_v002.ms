try(destroydialog makePreviewUI)catch()
fileIn(master_script_path+"\max\stateSets\stateSetOps.ms")
fileIn(master_script_path+"\max\_pipeline\_python\executeFileParser.ms")
python_file_parser_cmd = "get_render_preview_path"
execute_file_parser()
file_parser_result

rollout makePreviewUI "Loomfire Make Preview" 
(
	local renderPreviewPath
	edittext inputText "Input Name" labelontop:True
-- 	dotNetControl stateSetName "Label" text:"No State Active"
	dotNetControl lv "system.windows.forms.listView" height:200 
	
	group ""(
	radiobuttons time_type labels:#("Active Time Segment", "Range")
	spinner startFrame "Start Frame" type:#integer range:[-99999,99999,0]
	spinner endFrame "End Frame" type:#integer range:[-99999,99999,0]
	checkbox openFolder "Open folder afterwards" 
-- 	checkbox overwrite_chx "Overwrite previous" checked:true
	button ok_btn "OK" width:100
	)
	
	fn GetAllSubDirs_fn MyDirectory =
	(
		temp = #();
		s = 1;
		folders = getDirectories (MyDirectory + "/*");
		t = folders.count;
		sort folders
		newFolders=#()
		for i in folders do
		(
			filtered = (filterString i @"\")
			append newFolders (filtered[filtered.count])
		)
		return newFolders;
	)
		
	 fn editValue hwnd val = (
		WM_CHAR 	= 0x0102
		VK_RETURN 	= 0x000D

		uiaccessor.setwindowtext hwnd val
		windows.sendMessage hwnd WM_CHAR VK_RETURN 0 -- press ENTER key
	)	

	fn getPreviewPath = (
		print "@@@@@@@@@@@@@@@@"
		python_file_parser_cmd = "get_explicit_folder get_render_preview_path " + inputText.text
		execute_file_parser()
		python_file_parser_cmd = "set_version_folder " + file_parser_result
		execute_file_parser()
		renderPreviewPath = file_parser_result
		print renderPreviewPath
		print "%%%%%%%%%%%%%%"
	)
	fn saveOpUIAccess  = (
        local WindowHandle = DialogMonitorOPS.GetWindowHandle()
        local title =  (UIAccessor.GetWindowText WindowHandle)
        
		print windowHandle
		print title
-- 		fileIn(master_script_path+"\max\stateSets\stateSetOps.ms")
-- 		curStateSet = getCurrentStateSetName()
-- 		fileIn(master_script_path+"\max\_pipeline\_python\executeFileParser.ms")
-- 		python_file_parser_cmd = "get_explicit_folder get_render_preview_path " + inputText.text
-- 		execute_file_parser()
-- 		python_file_parser_cmd = "set_version_folder " + file_parser_result
-- 		execute_file_parser()
-- 		renderPreviewPath = file_parser_result
        if title == "Make Preview" do
        (
		
			print (windows.getChildrenHWND WindowHandle)
-- 			local TCM_SETCURFOCUS = 0x1330
			editValue ((windows.getChildrenHWND WindowHandle)[21])[1] (100 as string)
			
			if time_type.state == 2 then
			(
				print "!!!!!!!!!!!!!!!!!!!!!!!"
				UIAccessor.PressButtonByName WindowHandle "Custom Range:"
			)
			else
			(
				print "??????????????"
				UIAccessor.PressButtonByName WindowHandle "Active Time Segment"
			)
			
			editValue ((windows.getChildrenHWND WindowHandle)[5])[1] (startFrame.value as string)
			editValue ((windows.getChildrenHWND WindowHandle)[9])[1] (endFrame.value as string)
-- 			UIAccessor.SetWindowText ((windows.getChildrenHWND WindowHandle)[5])[1] (startFrame.value as string)
-- 			UIAccessor.SetWindowText ((windows.getChildrenHWND WindowHandle)[9])[1] (endFrame.value as string)

			filetypeUI = ((windows.getChildrenHWND WindowHandle)[21])
			filetypeHwnd = ((windows.getChildrenHWND WindowHandle)[21])[1]
			if filetypeUI != undefined then (
				WM_LBUTTONDOWN = 0x0201
				WM_LBUTTONUP = 0x0202

				windows.sendMessage filetypeHwnd WM_LBUTTONDOWN 0 -1	
				windows.sendMessage filetypeHwnd WM_LBUTTONUP 0 -1		
			)
			
			UIAccessor.PressButtonByName WindowHandle "Custom File Type"
			filetypeUI = ((windows.getChildrenHWND WindowHandle)[33])
			filetypeHwnd = ((windows.getChildrenHWND WindowHandle)[33])[1]
			if filetypeUI != undefined then (
				WM_LBUTTONDOWN = 0x0201
				WM_LBUTTONUP = 0x0202

				windows.sendMessage filetypeHwnd WM_LBUTTONDOWN 0 -1	
				windows.sendMessage filetypeHwnd WM_LBUTTONUP 0 -1		
			)
			

			UIAccessor.PressButtonByName WindowHandle "File..."
            UIAccessor.PressButtonByName WindowHandle "Create"
        )
		
		if title == "Create Animated Sequence File..." do
		(
			print (windows.getChildrenHWND WindowHandle)
			local edits = for c in (windows.getchildrenhwnd WindowHandle) where c[4] == "Edit" collect c[1]
-- 			editValue edits[1] "derp"	
			local filetypeUI 
			local filetypeHwnd
			for i in (windows.getChildrenHWND WindowHandle) where matchPattern i[5] pattern:"*AVI File (*.avi)*" do (
				filetypeUI = i
				filetypeHwnd =i[1]
			)
			if filetypeUI != undefined then (

				
				WM_COMMAND = 0x111 -- Windows Message: Command
				WM_LBUTTONDOWN = 0x0201
				WM_LBUTTONUP = 0x0202
				CB_GETCOUNT = 0x0146 -- ComboBox message: Get number of items
				CB_SHOWDROPDOWN = 0x014F
				CB_SETCURSEL = 0x014E -- ComboBox message: Set current selection
				CBN_SELENDOK = 9 -- ComboBox notification: Selection ended, OK		

				windows.sendMessage filetypeHwnd CB_SHOWDROPDOWN 1 0
				windows.sendMessage filetypeHwnd CB_SETCURSEL 7 0
				windows.sendMessage filetypeHwnd WM_LBUTTONDOWN 0 -1	
				windows.sendMessage filetypeHwnd WM_LBUTTONUP 0 -1		
				windows.sendMessage filetypeHwnd CB_SHOWDROPDOWN 0 0		

			)
			local setupBtn = for c in (windows.getchildrenhwnd WindowHandle) where c[4] == "Setup..." collect c[1]
			print "^^^^^^^^^^^^^^"
			editValue edits[1] (renderPreviewPath	+ "\\" + "test" + ".mov")
			print "***********"
			UIAccessor.PressButtonByName WindowHandle "&Save"
			
			DialogMonitorOPS.unRegisterNotification id:#saveOpUIAccess
			DialogMonitorOPS.enabled = false	
			
		)	
			
		if title == "JPEG Image Control" do
        (
		
			UIAccessor.PressButtonByName WindowHandle "OK"

        )

        true
    )
	
	fn addColumns theLv columnsAr=
	(
		w=(theLv.width/columnsAr.count)-5
		for x in columnsAr do
		(
			theLv.columns.add x w
		)
	)
	fn populateList theLv foldernames =
	(
		rows=#()			--Empty array to collect rows of data
		for i in foldernames do
		(
				li=dotNetObject "System.Windows.Forms.ListViewItem" i	--Create a listViewItem object and name it. 
			
				append rows li	
				
		)
		theLv.items.addRange rows		--Add the array of rows to the listView control. 
	)
	fn initLv theLv=
	(
		--Setup the forms view
		theLv.view=(dotNetClass "system.windows.forms.view").details
		theLv.FullRowSelect=true		--Set so full width of listView is selected and not just first column.
		theLv.GridLines=true			--Show lines between the items. 

	)
	
	fn startProcess =(
		setappdata globalTracks 50 (startFrame.value as string)
		setappdata globalTracks 51 (endFrame.value as string)
		setappdata globalTracks 52 (time_type.state as string)
		
		clearListener()
		DialogMonitorOPS.enabled = true
		DialogMonitorOPS.RegisterNotification saveOpUIAccess id:#saveOpUIAccess
		try
		(
		max preview
		if openFolder.checked do shellLaunch "explorer.exe" renderPreviewPath
		
		destroydialog makePreviewUI
		)
		catch
		(
			DialogMonitorOPS.unRegisterNotification id:#saveOpUIAccess
			DialogMonitorOPS.enabled = false	
		)
	)
	on makePreviewUI open do
	(
		if maxfilename !="" then
		(
			initlv lv
			addColumns lv #("Existing Preview Names")
			python_file_parser_cmd = "get_render_preview_path"
			execute_file_parser()
			folderNames = (GetAllSubDirs_fn file_parser_result)
			populateList lv folderNames
-- 			stateSetName.text =getCurrentStateSetName()
-- 			stateSetName.backColor = (dotNetClass "System.Drawing.Color").fromArgb 0	255 0
			--retrieve previously entered numbers
			if getappdata globalTracks 50 != undefined then
			(	
				startFrame.value = (getappdata globalTracks 50) as integer
				endFrame.value = (getappdata globalTracks 51) as integer
				time_type.state = (getappdata globalTracks 52) as integer
			)
			else
			(
				startFrame.value = rendStart as integer
				endFrame.value = rendEnd as integer
			)
			if time_type.state == 1 then 
			(
				startFrame.enabled = false
				endFrame.enabled = false
			)
		)
		else
		(
			messagebox "Please save your file in an appropriate directory."
			destroydialog makePreviewUI
		)

	)
	
	on time_type changed thisState do
	(
		if time_type.state == 1 then 
		(
			startFrame.enabled = false
			endFrame.enabled = false
		)
		else
		(
			startFrame.enabled = true
			endFrame.enabled = true
		)
	)
	
	on lv mouseDown arg do (

		hitInfo = (lv.hitTest arg.location)
		inputText.text = hitInfo.item.text

	)
	
	on ok_btn pressed do
	(
		clearlistener()
		if inputtext.text !="" then
		(
			getPreviewPath()

			print renderPreviewPath
			print "&&&&&&&&&&&&&&&"
-- 			if doesFileExist renderPreviewPath and overwrite_chx.checked == False then
-- 			(
-- 				if querybox "Looks like you might be overwriting a previous playblast. Continue?" do startProcess()
-- 			)
-- 			else(startProcess())
			startProcess()
		)
		else(messagebox "Please provide input name")
	)
)
createDialog makePreviewUI


