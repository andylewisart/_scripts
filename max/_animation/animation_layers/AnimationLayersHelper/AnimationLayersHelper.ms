macroScript AnimationLayersHelper
category:"mikes scripts"
toolTip:""
--<void>AnimLayerManager.copyLayerNodes <index>listIndex <&node array>nodes  copies the animimation
--AnimLayerManager.pasteLayerNodes <index>listIndex <&node array>nodes
-- use multilist box to isolated multiple layers at once
/* Author: Michael Burgoyne
This script should make it quicker to work with animation layers by quickly isolating the selected animation layers*/
(
global AniLayers
try destroyDialog AniLayers catch()
	
local aniLayersArray =#()

	rollout AniLayers "Animation Layers Manager"
	(
		ListBox AniLayersListBox "Select the animation layer:" items:aniLayersArray 
		button addAniLayer "+" tooltip:"Add animation layer" across:5 enabled:true
		button copyAniLayer "C" tooltip:"Copy animation layer" enabled:false
		button pasteAniLayer "P" tooltip:"Paste animation layer" enabled:false
		button collapseAniLayer "_" tooltip:"Colapse animation layer" enabled:false
		button deleteAniLayer "-" tooltip:"Delete animation layer" enabled:true
		button enableAniLayer "Enable" tooltip:"Enable animation layer" enabled:false across:2
		button disableAniLayer "Disable" tooltip:"Disable animation layer" enabled:false
		
		--##################### FUNCTIONS ###################################
		--if $ == undefined then (AniLayersListBox.enabled = false) else (AniLayersListBox.enabled = true)

	--selection function
	on AniLayersListBox selected nameIndex do
	(
		--print AniLayersListBox.selected
		--print AniLayersListBox.selection
		--set all the others weight to 0
		LayerWeightCount = AnimLayerManager.getLayerCount()
		for i = 1 to LayerWeightCount do
		(
		AnimLayerManager.setLayerWeight i 0 0 --sets the weight to 0
		AnimLayerManager.setLayerOutputMute i true
		AnimLayerManager.setLayerMute i true
		)
		
		AnimLayerManager.setLayerWeight nameIndex 0 1 --sets the weight to 100
		AnimLayerManager.setLayerActive nameIndex --sets the layer to active
		AnimLayerManager.setLayerOutputMute nameIndex false --sets the output track to off
		AnimLayerManager.setLayerMute nameIndex false
	)
	--Open the layers properties dialog box
	on AniLayersListBox doubleClicked DobleClickIndex do
	(
		AnimLayerManager.animLayerPropertiesDlg()
	)
-- 	on AniLayersListBox rightClick  do
-- 	(
-- 		GetAniLayerNames()
-- 		updateList()
-- 	)		


	fn GetAniLayerNames =
	(
		aniLayersArray =#()
		LayerCount = AnimLayerManager.getLayerCount()
			for i = 1 to layerCount do
			(
			 append aniLayersArray (AnimLayerManager.getLayerName i)
			)
	)
	
	fn updateList =
	(
		GetAniLayerNames()
		ListLayerNames = for n in aniLayersArray collect n
		AniLayersListBox.items = ListLayerNames	
	)
		on addAniLayer pressed do
		(
			AnimLayerManager.addLayerDlg $
			updateList()
		)
		on copyAniLayer pressed do
		(
			--print AniLayersListBox.selection
			AnimLayerManager.copyLayerNodes AniLayersListBox.selection $
		)
		on pasteAniLayer pressed do
		(
			--print AniLayersListBox.selection
			AnimLayerManager.pasteLayerNodes  AniLayersListBox.selection $
		)
		on enableAniLayer pressed do
		(
			AnimLayerManager.enableLayersDlg $
			updateList()
		)
		
		on disableAniLayer pressed do
		(
			AnimLayerManager.disableLayerNodes $
			updateList()
		)
		
		on deleteAniLayer pressed do
		(
			AnimLayerManager.deleteLayer AniLayersListBox.selection
			updateList()
		)	
	On AniLayers open do 
	( 
		GetAniLayerNames()
		updateList()
	)
)

createDialog AniLayers 200 215  lockWidth:true lockHeight:true style:#(#style_toolwindow, #style_sysmenu, #style_resizing)

)