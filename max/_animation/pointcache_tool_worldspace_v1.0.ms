﻿macroScript PoinCacheToolWS category:"Sameh Scripts"
(
		try destroyDialog ::PointCacheToolWS catch()
		rollout PointCacheToolWS "PC Tool WS V1.0" 
		(
			
			
			button _about "Help" width:110
			on _about pressed do 
			(
			messagebox "This script Records Point Cache Files of all the selected objects \n with objects Names.\n\n To use: \n 1- Choose Save Folder where th PC files will go \n 2- Select the objects or use Select Per Mod Button \n 3- Click Bake Deformation \n Note that it will overwrite all similar Point Cache files every time you click Bake botten.\n \n Choose modifier order you would like to delet and click Delet modifier.\n Point Cach Shifter , will adjust the range of the point cache playback for the selected objects "
			)
			edittext PCPath "Path"
			button _SaveFolder "Choose Save Folder" width:110
			on _SaveFolder pressed do 
			(
			
				FolderPath=getSavePath caption:"Choose Point Cache Folder" initialDir:maxfilepath
				if folderpath != undefined do
				PCpath.text= folderPath
				
			)
			--Select Per Modifier Tool
			Button _SelectPerModifier "Select Per Mod" width:110
			
			on _SelectPerModifier pressed do 
			(
				
				try destroyDialog ::test_dialog catch()
			
		rollout test_dialog "Select" width:200 height:220
		(
			
		local modifierClasses = #() -- array to hold all the unique modifier names
		local objectSets = #() -- array to hold corresponding object sets
		label ScriptCreated "Script Created By: Swordslayer"
		fn collectmodifierClasses =
		(
		for obj in objects do 
		(
		for objMod in obj.modifiers do
		(
		local modIndex = findItem modifierClasses (classOf objMod as string) -- is the modifier already there?
		if modIndex == 0 then -- if this is the first such modifier we see
		(
		append modifierClasses (classOf objMod as string) -- save its name
		append objectSets #(obj) -- and save the object to a new object set
		-- as a result, the objectSet index will be the same as the index of the modifier name
		)
		else append objectSets[modIndex] obj -- otherwise append it to the corresponding object set
		)
		)
		)

		fn selectObjectsByMod modStr =
		select objectSets[findItem modifierClasses modStr] -- find the corresponding objectSet and select it

		listbox modifiersToSelect "Modifiers:"
		button btnSelect "Select modifiers"

		on test_dialog open do
		(
		collectmodifierClasses()
		modifiersToSelect.items = sort (for item in modifierClasses collect item as string)
		)

		on modifiersToSelect doubleClicked item do
		(
		selectObjectsByMod modifiersToSelect.items[item]
			DestroyDialog (test_dialog)
		)

		on btnSelect pressed do
		(
		selectObjectsByMod modifiersToSelect.selected
		DestroyDialog (test_dialog)
		)
		)
		createDialog test_dialog
				
				
			)
			
			Label devider3 ""
			-- Baking Point Cache Files
			Button _BakePC "<Bake Deformation>" width:110
			progressbar Bake_prog color:red
			on _BakePC pressed do 
			(
				if PCpath.text=="" then 
					messagebox "Please select output folder to save point cache files"
				else 		
				if $==undefined then
				messagebox"Please Select the baked objects"
				else
				(
					 for i = 1 to selection.count do
					(
					A= selection as array
					OBJname = "PC_" +A[i].name + ".xml"
					FilePathName= PCpath.text
					PointCacheName= FilePathName +@"\"+ OBJname
					addmodifier A[i] (Point_CacheSpacewarpModifier ())
					A[i].modifiers[#Point_CacheSpacewarpModifier].filename=PointCacheName
					cacheOps.recordcache A[i].modifiers[#Point_CacheSpacewarpModifier] 
					Bake_prog.value = 100.*i/A.count

						)
							Messagebox "Point Cache files Has been Generated"
						
					)
			)
			
			
			--Delet Modifire Tool
			Label devider1 "________________________"
			Label DeleteMod "Delete Modifiers"
			dropdownlist ModOrder "Order" Items:#("1","2","3","4","5","6","7","8","9") width:40 align:#center selection:0
			on ModOrder selected i do 
			Order=ModOrder.items[i]
			Button _DeleteModefires "Delete Modifier" width:110
			on _DeleteModefires pressed do 
			(
				
				if queryBox ("Are you sure ?") beep:true then
					deleteModifier $ ModOrder.selection
					
			)
			
			Label devider2 "________________________"
			
			--Point Cache Shifter tool 
			
			Label ShifterLable "Point Cache Shifter"
			dropdownlist PBType  Items:#("Original Range","Custom Start","Custom Range")
			spinner Start "Start Frame:" range:[-10000,10000,0] type:#integer enabled:false
			Spinner End "End Frame:" range:[-10000,10000,100] type:#integer enabled:false

			button _Shifter "Shift" width:110
			
			on PBType selected j do
			(
				
				if j == 1 then
					(	Start.enabled = false
						End.enabled = false
					)
					else
					if j == 2 then
					( Start.enabled = true
						End.enabled = false
					)
					else
					(
						Start.enabled = true
						End.enabled = true
					)
				print (PBtype.selection-1)
			
			)
					
				on _Shifter pressed do 
				(
				for i = 1 to selection.count do
					(
					A= selection as array
					
					A[i].modifiers[#Point_CacheSpacewarpModifier].playbackType= (PBtype.selection-1)
					A[i].modifiers[#Point_CacheSpacewarpModifier].playbackStart = Start.value
					A[i].modifiers[#Point_CacheSpacewarpModifier].playbackend = End.value
					)
				messagebox (" Playback start frame now is: " + Start.value as string)
				)
				
		)
		createdialog PointCacheToolWS
	)
	