
theHold.begin()
thisCam = $
listCon = position_list ()
thisCam.pos.controller = listCon
noiseCon = Noise_position ()
listCon.available.controller = NoiseCon

noiseAmount = .05
noiseFreq = 0.041

noiseCon.noise_strength = [noiseAmount,noiseAmount,0]
noisecon.frequency = noiseFreq
listCon.cut 2
listCon.paste 1
listCon.active = 2

listCon = rotation_list ()
thisCam.rotation.controller = listCon
noiseCon = Noise_rotation ()
listCon.available.controller = NoiseCon

noiseAmount = .05
noiseFreq = 0.053

noiseCon.noise_strength = [noiseAmount,noiseAmount,noiseAmount]
noisecon.frequency = noiseFreq
listCon.cut 2
listCon.paste 1
listCon.active = 2
theHold.accept()