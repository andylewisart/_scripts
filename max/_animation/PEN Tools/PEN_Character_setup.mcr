/*
	PEN Character Setup Floater
	Created By Paul Neale
	Use and modify at your own risk
	Last updated 05/03/2001
*/

macroScript Character_Setup category:"PEN Tools"
(
conlistarray = #(bezier_position(),space,euler_xyz(),local_euler_xyz(),float_expression())
bones_array = #()
fins_array = #()
bones_state_array = #()

global float_size = 150
global float_size_02 = float_size
global character_setup_floater
global character_setup_floater_pos
global expcon = float_expression()
global tcbcon = tcb_rotation()
global pos_xyz_con = position_xyz()
global pos_bezier_con = bezier_position()
global eulerxyz_con = euler_xyz()
global addcontroller_roll
global bones_util_roll
global renamer_roll

global oldVal=0.0
global val=0.0
global changeVal=0.0
global cur_sidesize=0.0
global bone_array = #()


rollout sel_util_roll "Select Utility" --------------------Utils rollout -----Start
(
	checkbutton addcontroller_but "Controllers" width:130 height:15 highlightcolor:blue
	checkbutton bone_util_but "Bones Utils" width:130 height:15 highlightcolor:blue
	checkbutton renamer_but "Renamer" width:130 height:15 highlightcolor:blue
	button version_bt "Ver:" width:30 height:15 align:#right
------------------------------------------------------------reply to Utils rollout

on version_bt pressed do
(messagebox "Created By: Paul Neale \npen_productions@yahoo.com \nLast update: 05/03/2001" title:"Version") 

on addcontroller_but changed state do
(
	if addcontroller_but.state == true then
	(
		float_size_02 += 470
		character_setup_floater.size = [180,float_size_02]
		addrollout addcontroller_roll character_setup_floater
	)else
	(
		float_size_02 -= 470
		removerollout addcontroller_roll character_setup_floater
		character_setup_floater.size = [180,float_size_02]
	)
)

on bone_util_but changed state do
(
	if bone_util_but.state == true then
	(
		float_size_02 += 357
		character_setup_floater.size = [180,float_size_02]
		addrollout bones_util_roll character_setup_floater
	)else
	(
		float_size_02 -= 357
		removerollout bones_util_roll character_setup_floater
		character_setup_floater.size = [180,float_size_02]
		callbacks.removeScripts id:#bone_spin
	)
)

on renamer_but changed state do
(
	if renamer_but.state == true then
	(
		float_size_02 += 296
		character_setup_floater.size = [180,float_size_02]
		addrollout renamer_roll character_setup_floater
	)else
	(
		float_size_02 -= 296
		removerollout renamer_roll character_setup_floater
		character_setup_floater.size = [180,float_size_02]
	)
)


)--****************************************************************Utils rollout-------End

--*******************************************************Controllers rollout	Start
rollout addcontroller_roll "Controllers" 
(

group "Remove Lists:"
(
	button removeposlist "Remove Position List" width:130
	button removerotlist "Remove Rotation List" width:130
)

group "Position / Rotation Lists:"
(
	button addposlist "Add Position List" width:130
	button addposlistexp "Add Pos List w\ exp" width:130
	button addrotlist "Add Rotation List" width:130
	button addrotlistexp "Add Rotation List w\ exp" width:130
)

group "Set List Active:"
(
	button setlistactive01 "Set 1st List Con Active" width:130
	button setlistactive02 "Set 2st List Con Active" width:130
)

group "Zero Out Objects:"
(
	button zeropos "Zero Out Position" width:130
	button zerorot "Zero Out Rotation" width:130
)

group "Resets:"
(
	button resetpos "Reset Position" width:130
	button resetrot "Reset Rotation" width:130
)

--**************************************Reply

on removeposlist pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	poscon_var = bezier_position()
	
	for x = 1 to obj_sel01.count do
	(
		poscon = obj_sel01[x].position.controller
		if (classof poscon) == position_list do
		(
			curpos = obj_sel01[x].position
			obj_sel01[x].position.controller = copy poscon_var
			obj_sel01[x].position = curpos
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)

on removerotlist pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	rotcon_var = tcb_rotation()
	
	for x = 1 to obj_sel01.count do
	(
		rotcon = obj_sel01[x].rotation.controller
		if (classof rotcon) == rotation_list do
		(
			currot = obj_sel01[x].rotation.controller.value
			obj_sel01[x].rotation.controller = copy rotcon_var
			obj_sel01[x].rotation.controller.value = currot
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)

on addposlist pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	poslistcon_var = position_list()
	posXYZcon_var = position_xyz()
	
	for x = 1 to obj_sel01.count do
	(
		poslistcon = obj_sel01[x].position.controller
		if (classof poslistcon) != position_list do
		(
			obj_sel01[x].position.controller = position_list()
			obj_sel01[x].position.controller[2].controller = copy posXYZcon_var
			listctrl.setname obj_sel01[x].position.controller 1 "Zero"
			listctrl.setname obj_sel01[x].position.controller 2 "Animation"
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)	

on addposlistexp pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	poslistcon_var = position_list()
	posXYZcon_var = position_xyz()
	posexpcon_var = float_expression()
	
	for x = 1 to obj_sel01.count do
	(
		poslistcon = obj_sel01[x].position.controller
		if (classof poslistcon) != position_list do
		(
			obj_sel01[x].position.controller = position_list()
			obj_sel01[x].position.controller[2].controller = copy posXYZcon_var
			listctrl.setname obj_sel01[x].position.controller 1 "Zero"
			listctrl.setname obj_sel01[x].position.controller 2 "Animation"
			for y = 1 to 3 do
			(
				obj_sel01[x].position.controller[2][y].controller = copy posexpcon_var
			)
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)	

on addrotlist pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	rotlistcon_var = rotation_list()
	rotXYZcon_var = euler_xyz()
	
	for x = 1 to obj_sel01.count do
	(
		rotlistcon = obj_sel01[x].rotation.controller
		if (classof rotlistcon) != rotation_list do
		(
			obj_sel01[x].rotation.controller = rotation_list()
			obj_sel01[x].rotation.controller[2].controller = copy rotXYZcon_var
			listctrl.setname obj_sel01[x].rotation.controller 1 "Zero"
			listctrl.setname obj_sel01[x].rotation.controller 2 "Animation"
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)	

on addrotlistexp pressed do
(
	disablesceneredraw()

	obj_sel01 = getcurrentselection()
	rotlistcon_var = rotation_list()
	rotXYZcon_var = euler_xyz()
	rotexpcon_var = float_expression()
	
	for x = 1 to obj_sel01.count do
	(
		rotlistcon = obj_sel01[x].rotation.controller
		if (classof rotlistcon) != rotation_list do
		(
			obj_sel01[x].rotation.controller = rotation_list()
			obj_sel01[x].rotation.controller[2].controller = copy rotXYZcon_var
			listctrl.setname obj_sel01[x].rotation.controller 1 "Zero"
			listctrl.setname obj_sel01[x].rotation.controller 2 "Animation"
			for y = 1 to 3 do
			(
				obj_sel01[x].rotation.controller[2][y].controller = copy rotexpcon_var
			)
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)	

	
on setlistactive01 pressed do
	(
		for x in selection do
		(
			try(x.position.controller.setactive 1)catch()
			try(x.rotation.controller.setactive 1)catch()
		)
	)

on setlistactive02 pressed do
	(
		for x in selection do
		(
			try(x.position.controller.setactive 2)catch()
			try(x.rotation.controller.setactive 2)catch()
		)
	)

on zeropos pressed do
(
	disablesceneredraw()
	posXYZcon_var = position_XYZ()
	obj_sel01 = getcurrentselection()
	
	for x = 1 to obj_sel01.count do
	(
		poscon = obj_sel01[x].position.controller
		if (classof poscon) != position_list do
		(
			poslistcon_var = position_list()
			obj_sel01[x].position.controller = poslistcon_var
			obj_sel01[x].position.controller[2].controller = copy posXYZcon_var
			obj_sel01[x].position.controller.setactive 2
			listctrl.setname obj_sel01[x].position.controller 1 "Zero"
			listctrl.setname obj_sel01[x].position.controller 2 "Animation"
		)
	)
	
	for x = 1 to obj_sel01.count do
	(
		if obj_sel01[x].position.controller.numsubs == 2 do
		(
			obj_sel01[x].position.controller[2].controller = copy posXYZcon_var
			obj_sel01[x].position.controller.setactive 2
			listctrl.setname obj_sel01[x].position.controller 1 "Zero"
			listctrl.setname obj_sel01[x].position.controller 2 "Animation"
		)
		
		poslistcon2 = obj_sel01[x].position.controller[2].controller
		if (classof poslistcon2) == position_XYZ do
		(
			obj_sel01[x].position.controller[1].value += obj_sel01[x].position.controller[2].value
			obj_sel01[x].position.controller[2].value = [0,0,0]
		)
	)
	select obj_sel01
	enablesceneredraw()
	redrawviews()
)

on zerorot pressed do
(
disablesceneredraw()

	obj_sel = getcurrentselection()
	rotTCBcon = tcb_rotation()
	rotXYZcon = euler_XYZ()
	rotfloatcon = bezier_float()
	
	for x = 1 to obj_sel.count do
	(
		rotlistcon1 = obj_sel[x].rotation.controller --check for list controller
		if (classof rotlistcon1) == rotation_list then
		(
			rotlistcon2 = obj_sel[x].rotation.controller[2].controller --check for euler_xyz controller
			if (classof rotlistcon2) == euler_XYZ do
			(
				float_count = 0
				for m = 1 to 3 do --check for bezier_float controller
				(
					rotlistcon2con = obj_sel[x].rotation.controller[2][m].controller 
					if (classof rotlistcon2con) == bezier_float do
					(float_count +=1)
					print float_count
				)
					if float_count == 3 do
					(
						--copy current controllers
						currotcon = #()
						for y = 1 to 3 do (append currotcon obj_sel[x].rotation.controller[2][y].controller)
						
						--replace list controller with TCB
							currot = obj_sel[x].rotation.controller.value
							obj_sel[x].rotation.controller = copy rotTCBcon
							obj_sel[x].rotation.controller.value = currot
						
						--recreate list controller
						rotlistcon = rotation_list()
						obj_sel[x].rotation.controller = rotlistcon
						obj_sel[x].rotation.controller[2].controller = copy rotXYZcon
						for z = 1 to 3 do 
						(
							obj_sel[x].rotation.controller[2][z].controller = currotcon[z]
							obj_sel[x].rotation.controller[2][z].controller.value = 0
						)
						
						--set second controller active
						obj_sel[x].rotation.controller.setactive 2
						
						--name controllers
						listctrl.setname obj_sel[x].rotation.controller 1 "Zero"
						listctrl.setname obj_sel[x].rotation.controller 2 "Animation"
						
					)--end if float_count
			)--end if rotlistcon2
		)
		else
		(
			--Create 1st and 2nd list controller
			rotlistcon = rotation_list()
			obj_sel[x].rotation.controller = rotlistcon
			obj_sel[x].rotation.controller[2].controller = copy rotXYZcon
			for y = 1 to 3 do
			(obj_sel[x].rotation.controller[2][y].controller = copy rotfloatcon)
			
			--set second controller active
			obj_sel[x].rotation.controller.setactive 2

			--name controllers
 			listctrl.setname obj_sel[x].rotation.controller 1 "Zero"
			listctrl.setname obj_sel[x].rotation.controller 2 "Animation"

		)--end if rotlistcon1
	)--end x loop

select obj_sel
enablesceneredraw()
redrawviews()
)

on resetpos pressed do
(
	disablesceneredraw()
	for x in selection do
	(
	try(for y = 1 to 3 do
		(
			x.position.controller[2][y].controller.value = 0
		))catch()
	)
	enablesceneredraw()
	redrawviews()	
)

on resetrot pressed do
(
	disablesceneredraw()
	for x in selection do
	(
	try(for y = 1 to 3 do
		(
			x.rotation.controller[2][y].controller.value = 0
		))catch()	
	)
	enablesceneredraw()
	redrawviews()
)

)--********************************************************End Controllers Roll

--**********************************************************Bones_Rollout------Start

rollout bones_util_roll "Bones Utilities"
(
	group "Fins:"
	(
		checkbox bone_reletive "Reletive Spinners:" checked:true enabled:false
	
		button bone_sidefinson "Side ON" width:65 offset:[-5,0] across:2
		button bone_sidefinsoff "Side OFF" width:65 offset:[5,0]
		spinner bone_sidesize "" fieldwidth:38 range:[-10000,10000,0] across:3
		spinner bone_sideSTtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[5,0]
		spinner bone_sideEDtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[10,0]
		label lb_sp01 ""
		button bone_frontfinson "Front ON" width:65 offset:[-5,0] across:2
		button bone_frontfinsoff "Front OFF" width:65 offset:[5,0]
		spinner bone_frontsize "" fieldwidth:38 range:[-10000,10000,0] across:3
		spinner bone_frontSTtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[5,0]
		spinner bone_frontEDtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[10,0]
		label lb_sp02 ""
		button bone_backfinson "Back ON" width:65 offset:[-5,0] across:2
		button bone_backfinsoff "Back OFF" width:65 offset:[5,0]
		spinner bone_backsize "" fieldwidth:38 range:[-10000,10000,0] across:3
		spinner bone_backSTtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[5,0]
		spinner bone_backEDtaper "" fieldwidth:38 range:[-10000,10000,0] offset:[10,0]

	)
	
	group "Copy / Paste Fins:"
	(
		button bone_copy "Copy" width:60 across:2 enabled:false
		button bone_paste "Paste" width:60 enabled:false
	)
	
	group "Bone Fins ON/OFF:"
	(
		checkbutton fins_onoff "ON / OFF" highlightcolor:red
	)
--*****************************************Reply to Bones Roll
Fn bone_spin_fn = --callback fn to set spinner values
(
	bone_sel_fn()
	cur_sidesize = 0
	if bones_util_roll.bone_reletive.state == false do
	(	
		for x = 1 to bone_array.count do
		(
			bones_util_roll.bone_sidesize.value = (cur_sidesize += (bone_array[x].sidefinssize)/bone_array.count)
		)
	)
)

Fn bone_sel_fn = --Fn to sort bones in selection
(
	obj_sel = getcurrentselection()
	global bone_array = #()
	for x = 1 to obj_sel.count do
	(
		if classof obj_sel[x] == boneGeometry do
		(append bone_array obj_sel[x])
	)
)


on bone_reletive changed state do
(
	bone_sel_fn()
	if bone_reletive.state == true then
	(
--		callbacks.removeScripts id:#bone_spin
		bone_sidesize.range = [-10000,10000,0]
		bone_sidesize.value = 0
	)
	else
	(
--		callbacks.addScript #selectionsetchanged "bone_spin_fn()" id:#bone_spin
		cur_sidesize = 0
		for x = 1 to bone_array.count do
		(
			bone_sidesize.range = [0,10000,0]
			bone_spin_fn()
		)
	)
)

--************************Sidefins
on bone_sidefinson pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].sidefins = true)catch()
		)
	)

on bone_sidefinsoff pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].sidefins = false)catch()
		)
	)

on bone_sidesize changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].sidefinssize += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].sidefinssize = val	)
	)
)
on bone_sidesize buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_sidesize.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_sideSTtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].sidefinsstarttaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].sidefinsstarttaper = val	)
	)
)
on bone_sideSTtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_sideSTtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_sideEDtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].sidefinsendtaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].sidefinsendtaper = val	)
	)
)
on bone_sideEDtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_sideEDtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

--************************Frontfins
on bone_frontfinson pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].frontfin = true)catch()
		)
	)
on bone_frontfinsoff pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].frontfin = false)catch()
		)
	)

on bone_frontsize changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].frontfinsize += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].frontfinsize = val	)
	)
)
on bone_frontsize buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_frontsize.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_frontSTtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].frontfinstarttaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].frontfinstarttaper = val	)
	)
)
on bone_frontSTtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_frontSTtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_frontEDtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].frontfinendtaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].frontfinendtaper = val	)
	)
)
on bone_frontEDtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_frontEDtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)
	
--************************Backfins
on bone_backfinson pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].backfin = true)catch()
		)
	)
on bone_backfinsoff pressed do
	(
		obj01 = selection
		for x = 1 to obj01.count do
		(
			try(obj01[x].backfin = false)catch()
		)
	)

on bone_backsize changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].backfinsize += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].backfinsize = val	)
	)
)
on bone_backsize buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_backsize.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_backSTtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].backfinstarttaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].backfinstarttaper = val	)
	)
)
on bone_backSTtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_backSTtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)

on bone_backEDtaper changed val do
(
	bone_sel_fn()
	
	if bone_reletive.state == true then
	(
		global changeVal=0.0
		changeVal = val - oldVal

		for x = 1 to bone_array.count do
		(
			bone_array[x].backfinendtaper += changeVal	
		)
		
		oldVal = val
	)else
	(
		for x = 1 to bone_array.count do
		(	bone_array[x].backfinendtaper = val	)
	)
)
on bone_backEDtaper buttonup do 
(
	if bone_reletive.state == true do
	(
		bone_backEDtaper.value = 0
		oldVal = 0
		changeVal=0.0
	)
)
	

--Fins ON/OFF.Will remember which bones were turned off and turn them back on again. 
--Need to add persistant globals to the arrays incase the script is closed before the bones are turned back on.
on fins_onoff changed state do
	(
		if fins_onoff.state == true then
		(
			bones_sel =  selection as array
			for x = 1 to bones_sel.count do
			(
				if classof bones_sel[x] == BoneGeometry do
				(
					append bones_array bones_sel[x]
				)
			)
			
			for y = 1 to bones_array.count do
			(
				append bones_state_array bones_array[y].sidefins
				append bones_state_array bones_array[y].frontfin
				append bones_state_array bones_array[y].backfin
				append fins_array bones_state_array
				bones_state_array = #()
				bones_array[y].sidefins = false
				bones_array[y].frontfin = false
				bones_array[y].backfin = false
				
			)
		
		)
		else
		(
			for z = 1 to bones_array.count do
			(
				bones_array[z].sidefins = fins_array[z][1]
				bones_array[z].frontfin = fins_array[z][2]
				bones_array[z].backfin = fins_array[z][3]
				

			)
				bones_array = #()
				fins_array = #()
				bones_state_array = #()
		)
	)

)
--********************************************************Bones Rollout    End

--*****************************************************Renamer Roll-----------------Start
Rollout Renamer_Roll "Object Renamer" 
(
	button aboutBtn "About" width:35 height:15
	group "Affect"(
		radiobuttons selectionInfo Labels:#("All", "Selected") default:2
		)
	group "Method"(
		radiobuttons nameOptions Labels:#("Prepend", "Replace")
		)
	group "Name"(
		Label lblName "New name"
		edittext newName
		)
	group "Renamer"(
		checkbox groupit "Group when done?"
		button rename "Rename!" width:100
		)
	on rename pressed do (
	ObjArray = #()
	if selectionInfo.state == 1 do(
		ObjArray = for obj in objects collect obj
		)
	if selectionInfo.state == 2 do(
		ObjArray = for obj in selection collect obj
		)
	if nameOptions.state == 1 do(
		for obj in ObjArray do(
			tmpname = newName.text + "_" + obj.name
			obj.name = tmpname
			)
		if groupit.state == true do (
			group ObjArray name:newName.text
			)
		)
	if nameOptions.state == 2 do(
		i = 1
		for obj in ObjArray do(
			if i < 10 do (
				i = i as string
				i = "0" + i
				)
			i = i as string
			tmpname = newName.text + i
			obj.name = tmpname
			i = i as integer
			i = i + 1
			)
			if groupit.state == true do (
				group ObjArray name:newName.text
				)
			)
		)
	on aboutBtn pressed do (
		messagebox "Object Renamer\nBy Adam Silverthorne\ndoogie@slip.net\nwww.slip.net/~doogie" title:"About Renamer"		
		)
	)



if character_setup_floater != undefined do
								(
								closerolloutfloater character_setup_floater
								)
		
			character_setup_floater = newRolloutFloater "Character Setup" 180 float_size_02 10 100
			addrollout sel_util_roll character_setup_floater 

)--End Macro