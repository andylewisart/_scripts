/*
	Duplicate Name Finder
	By Paul Neale
	Created 23/03/2001
*/
macroscript Duplicate_Name_Finder category:"PEN Tools"
(--Start macro
global obj_array = #()
global objname_array = #()
global duplist_array = #()


--********************************************************Rollouts
rollout dup_roll "Dup Name Finder"
(
	button dupfind_bt "Search for Duplicates" width:126
	multilistbox duplist_mlb "Duplicates:" height:15
	button seldup_bt "Select" width:126

--*******************************************************return
on dupfind_bt pressed do
(
	obj_array = #()
	objname_array = #()
	duplist_array = #()
	
	for x = 1 to $objects.count do (append obj_array $objects[x])
	for x = 1 to $objects.count do (append objname_array $objects[x].name)
	sort objname_array
	
	for y = 1 to (objname_array.count - 1) do
	(
		if objname_array[y] == objname_array[(y+1)] do
		(
			append duplist_array objname_array[y]
			print objname_array[y]
		)
	)
	duplist_mlb.items = duplist_array
	
)--end on dupfind_bt

on seldup_bt pressed do
(
	deselect $*
	for i = 1 to duplist_array.count do
	(
		if dup_roll.duplist_mlb.selection[i] == true do
		(
			for k = 1 to obj_array.count do
			(
				if duplist_array[i] == obj_array[k].name do 
				(
					selectmore obj_array[k]
--					print obj_array[k].name
				)
			)
		)
	)	
)-- end seldup_bt

)-- end dup_roll

--***********************************************************Floater
if dup_float != undefined do (closerolloutfloater dup_float)

	dup_float = newrolloutfloater "Duplicate Name Finder" 180 350 10 100
	addrollout dup_roll dup_float

)--End Macro