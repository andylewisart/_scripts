/*
	Macro Script addons
	Created By Paul Neale
	
	Use and modify at your own risk
	Last updated 24/02/2000
*/


--********************************************************************Modifier Pannel

macroscript Show_End_Result category:"PEN Modeling"
(
	if showendresult==true then
	(showendresult=false)else(showendresult=true)
)

--**********************************************************************MeshSmooth

macroscript MeshSmooth_0 category:"PEN Modeling"
(for x = 1 to selection.count do try(selection[x].meshsmooth.iterations = 0)catch())

macroscript MeshSmooth_1 category:"PEN Modeling"
(for x = 1 to selection.count do try(selection[x].meshsmooth.iterations = 1)catch())

macroscript MeshSmooth_2 category:"PEN Modeling"
(for x = 1 to selection.count do try(selection[x].meshsmooth.iterations = 2)catch())

macroScript MS_On category:"PEN Modeling"	 
(for x = 1 to selection.count do  	
	try((selection[x].meshsmooth.enabledinviews = true 		 
	     selection[x].meshsmooth.enabled = true))catch()) 
		 
macroScript MS_Off category:"PEN Modeling"	 
(for x = 1 to selection.count do
  	try((selection[x].meshsmooth.enabled = false))catch()) 

macroScript MS_Off_In_Views category:"PEN Modeling"	 
(for x = 1 to selection.count do  	
	try((selection[x].meshsmooth.enabled = true 		 
		 selection[x].meshsmooth.enabledinviews = false))catch()) 
	
Macroscript MS_Render_Off category:"PEN Modeling"
(for x = 1 to selection.count do try((selection[x].meshsmooth.userenderiterations = false))catch())

Macroscript MS_Render_1 category:"PEN Modeling"
(for x = 1 to selection.count do try
	(selection[x].meshsmooth.userenderiterations = true
	selection[x].meshsmooth.renderiterations = 1)catch())
	
Macroscript MS_Render_2 category:"PEN Modeling"
(for x = 1 to selection.count do try
	(selection[x].meshsmooth.userenderiterations = true
	selection[x].meshsmooth.renderiterations = 2)catch())

Macroscript MS_Render_3 category:"PEN Modeling"
(for x = 1 to selection.count do try
	(selection[x].meshsmooth.userenderiterations = true
	selection[x].meshsmooth.renderiterations = 3)catch())

--************************************************************Mirror

Macroscript Mirror_On_Off category:"PEN Modeling"
(try(if $.mirror.enabled == true then ($.mirror.enabled = false)else($.mirror.enabled = true))catch())

macroScript Mirror_Setup category:"PEN Modeling" 
	(try($.mirror.mirror_axis = 2  
	 $.mirror.copy = true)catch()) 

--**********************************************************Surface Tools

Macroscript Surface_1 category:"PEN Modeling"
(try($.surface.steps = 1)catch())

Macroscript Surface_2 category:"PEN Modeling"
(try($.surface.steps = 2)catch())

Macroscript Surface_3 category:"PEN Modeling"
(try($.surface.steps = 3)catch())

Macroscript Surface_4 category:"PEN Modeling"
(try($.surface.steps = 4)catch())


--********************************************************Isolator
Macroscript Isolator category:"PEN Tools"
(
iso_floater = 
	rollout isogroup "Unisolate"
(
	checkbutton iso01 "Isolated" checked:on highlightcolor:green
	on iso01 changed state do
		(
			try(
			unhide $isogroup
			select $isogroup
			count=selection.count
			for x=1 to count do (unhide selection[x])
			ungroup $isogroup
			max select invert
			)catch()
			closerolloutfloater iso_floater
		)
)
	if iso_floater != undefined do
								(

								)
	try(    max select invert
			group selection name:"isogroup"
			count=selection.count
			for x=1 to count do (hide selection[x])
			max select invert
			deselect $isogroup
		)catch()
		
			iso_floater = newRolloutFloater "Isolator" 70 100 20 100
			addRollout isogroup iso_floater
)

--*****************************************************************Fix Edges
--Macroscript Fix_Edges category:"PEN Tools"
--(
--	edgeselset=#()
--	for face = 1 to obj.numfaces do
--	for edge = 1 to 3 do
--	if(getedgevis obj face edge)then
--	append edgeselset (((face-1)*3)+edge)
--	setedgeselection obj edgeselset
--	update obj
--	meshOps.visibleEdge obj
--)

--****************************************************************Filters

Macroscript All_Obj category:"PEN Tools"
(SetSelectFilter 1)

Macroscript Goemetry_Obj category:"PEN Tools"
(SetSelectFilter 2)

Macroscript Shapes_Obj category:"PEN Tools"
(SetSelectFilter 3)

Macroscript Lights_Obj category:"PEN Tools"
(SetSelectFilter 4)

Macroscript Cameras_Obj category:"PEN Tools"
(SetSelectFilter 5)

Macroscript Helpers_Obj category:"PEN Tools"
(SetSelectFilter 6)

Macroscript Warps_Obj category:"PEN Tools"
(SetSelectFilter 7)

Macroscript Bones_Obj category:"PEN Tools"
(SetSelectFilter 8)

Macroscript IK_Chains_Obj category:"PEN Tools"
(SetSelectFilter 9)

Macroscript Point_Obj category:"PEN Tools"
(SetSelectFilter 10)


--*****************************************************************Control Object

Macroscript Control_Object category:"PEN Tools"
(
	circle name:"CNTsphere01" radius:20 wirecolor:yellow
	circle name:"CNTsphere02" radius:20 wirecolor:yellow
	$CNTsphere02.rotation.x_rotation = 90
	circle name:"CNTsphere03" radius:20 wirecolor:yellow
	$CNTsphere03.rotation.y_rotation = 90
	converttosplineshape $CNTsphere01
	converttosplineshape $CNTsphere02
	converttosplineshape $CNTsphere03
	addandweld $CNTsphere01 $CNTsphere02 0
	addandweld $CNTsphere01 $CNTsphere03 0
	$CNTsphere01.position.controller = position_list()
	$CNTsphere01.position.controller[2].controller = position_xyz()
	$CNTsphere01.rotation.controller = rotation_list()
	$CNTsphere01.rotation.controller[2].controller = euler_xyz()
	select $CNTsphere01
)

--****************************************************************Controllers
--Resets second controller to zero
macroscript Reset_Pos_Rot Category:"PEN Animation"
(	for x = 1 to selection.count do
	(	for y = 1 to 3 do
		(	try(selection[x].position.controller[2][y].controller.value = 0)catch()
			try(selection[x].rotation.controller[2][y].controller.value = 0)catch()
		)))

--**********************************************************************************
--Create List controllers
--If list controller exists it add second controller to first then zeros out second.

macroscript Set_1st_Controller_Active category:"PEN Animation"
(	for x in selection do
	(	try(x.position.controller.setactive 1)catch()
		try(x.rotation.controller.setactive 1)catch()
	))

macroscript Set_2st_Controller_Active category:"PEN Animation"
(	for x in selection do
	(	try(x.position.controller.setactive 2)catch()
		try(x.rotation.controller.setactive 2)catch()
	))
	

