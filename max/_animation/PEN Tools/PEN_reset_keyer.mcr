/*
*****************************************
	Multi Purpose Reset and keyer
	Paul Neale
	27/02/2001
*****************************************

*Currently only keys and resets objects with a list controller active in the second controller &
objects on path controller with list controller & 2nd list active in percent track.

Things to add.
* Saving selections of objects and access to selection sets.
* Keying/Reseting all controllers.
* Deselect from list button.
* Key and reset selected in list only.
* Scale support
*/

Macroscript Reset_Keyer category:"PEN Animation"
(
global sel_name = #()
global obj_list_roll 
global obj_sel_bt
global obj_set_run_fn
global size = [170,220]
global Multi_purpose_float

Fn Obj_set_fn =
(		
	sel = getcurrentselection()
	sel_name = #()
	for x = 1 to sel.count do (append sel_name sel[x].name)
	sort sel_name
	str = "";
	str += "rollout obj_list_roll \"Selected Objects\"\n"
	str += "(\n"
		
		if sel.count > 0 do
			(
				str += "Multilistbox obj_listbox items:sel_name width:140 height:20 offset:[-11,0]"
			)
	
	str += ")\n"

	execute str

)

rollout Multi_purpose_roll "Reset/Keyer"
(
--*************************************************************interface

group "Key Selected"
(
	button pos_key "Pos" width:55 height:20 align:#left across:2
	button rot_key "Rot" width:55 height:20 align:#right
	
)

group "Reset Selected"
(
	button pos_reset "Pos" width:55 height:20 align:#left across:2
	button rot_reset "Rot" width:55 height:20 align:#right
)

group ""
(
	checkbutton obj_sel_bt "Display Selection" checked:false width:115 height:20 highlightcolor:blue
)
	button ver01 "Ver:" width:25 height:13 pos:[105,7]

--****************************************************************reply

on pos_key pressed do
(
	obj_sel01 = selection as array
	for x = 1 to obj_sel01.count do
	(
		poscon = obj_sel01[x].position.controller
		if (classof poscon) == position_list then
		(
			poslistcon = obj_sel01[x].position.controller[2].controller
			if (classof poslistcon) == position_xyz then
			(
				for y = 1 to 3 do (obj_sel01[x].position.controller[2][y].value = obj_sel01[x].position.controller[2][y].value)
				print (obj_sel01[x].name + " -- Has been Keyed at" + " " + (obj_sel01[x].position.controller[2].value as string))
				
			)else(print (obj_sel01[x].name + " -- Has NO Position_XYZ controller in 2nd list"))
		)else(print (obj_sel01[x].name + "--Has NO Position_List controller"))
		
		try(obj_sel01[x].position.controller.percent.controller[2].value = obj_sel01[x].position.controller.percent.controller[2].value)catch()
	)
)

on rot_key pressed do
(
	obj_sel01 = selection as array
	for x = 1 to obj_sel01.count do
	(
		rotcon = obj_sel01[x].rotation.controller
		if (classof rotcon) == rotation_list then
		(
			rotlistcon = obj_sel01[x].rotation.controller[2].controller
			if (classof rotlistcon) == euler_xyz then
			(
				for y = 1 to 3 do (obj_sel01[x].rotation.controller[2][y].value = obj_sel01[x].rotation.controller[2][y].value)
				print (obj_sel01[x].name + " -- Has been Keyed at" + " " + (obj_sel01[x].rotation.controller[2].value as string))
				
			)else(print (obj_sel01[x].name + " -- Has NO Euler_XYZ controller in 2nd list"))
		)else(print (obj_sel01[x].name + "--Has NO Rotation_List controller"))
	)
)


on pos_reset pressed do
(
	obj_sel01 = selection as array
	
	for x = 1 to obj_sel01.count do
	(
		poscon = obj_sel01[x].position.controller
		if (classof poscon) == position_list then
		(	
			poslistcon = obj_sel01[x].position.controller[2].controller
			if (classof poslistcon) == position_xyz do
			(
				for y = 1 to 3 do (obj_sel01[x].position.controller[2][y].value = 0)
			)
		)else(print (obj_sel01[x].name + "--Does Not have Position_List Controller"))
		
		try(obj_sel01[x].position.controller.percent.controller[2].value = 0)catch()
	)
	
	
)

on rot_reset pressed do
(
	obj_sel01 = selection as array
	
	for x = 1 to obj_sel01.count do
	(
		rotcon = obj_sel01[x].rotation.controller
		if (classof rotcon) == rotation_list then
		(	
			rotlistcon = obj_sel01[x].rotation.controller[2].controller
			if (classof rotlistcon) == euler_xyz do
			(
				for y = 1 to 3 do (obj_sel01[x].rotation.controller[2][y].value = 0)
			)
		)else(print (obj_sel01[x].name + "--Does Not have Rotation_List Controller"))
	)
)

on obj_sel_bt changed state do
(
	if obj_sel_bt.state == true then
	(

		Obj_set_run_fn()
		callbacks.addScript #selectionsetchanged "Obj_set_run_fn()" id:#Obj_set_id
		Multi_purpose_float.size = [170,525]

	)else
	(
		callbacks.removeScripts id:#Obj_set_id
		removerollout obj_list_roll Multi_purpose_float
		Multi_purpose_float.size = [170,220]
	)
)

on ver01 pressed do (messagebox "Created By:Paul Neale \nUpdated: 27/02/2001" title:"Version")

)--End Multi_purpose_roll

--Function to add an remove the obj_list_roll and run obj_set_fn
Fn obj_set_run_fn =
(
		removerollout obj_list_roll Multi_purpose_float
		obj_set_fn()
	
		addrollout obj_list_roll Multi_purpose_float
)

if Multi_purpose_float != undefined do
	(closerolloutfloater Multi_purpose_float)

	 Multi_purpose_float = newrolloutfloater "Reset/Keyer" 170 220 10 100
	 addrollout Multi_purpose_roll Multi_purpose_float
	 Obj_set_fn()

)--End Macroscript

--******************************************************test area
--test line to filter name of morph target
/*
morph_name = $.morpher[1] as string

morph_name = filterstring morph_name "_"

morph_name[3]

-- or --morph_name = ( filterstring ($.morpher[1] as string ) "_")[3]

*/