plugin simpleMeshMod mcg_clone
	name:"mcg_clone"
	classID:#(0x5369F377, 0x386FCECF)
	category:"Max Creation Graph"
	usePBValidity:true
(
	local pluginDefinitionFile -- the plugin definition file
	local pluginInstance -- the plugin instance created from the plugin definition
	local _meshWrapper -- the dotnet wrapper around the stack mesh pointer
	local argsType = dotNetClass "System.Object[]" -- used when creating args array
	local dotnet_ValueToDotNetObject = dotnet.ValueToDotNetObject
	local tmHandler
	--simulation locals
	local _lastTime = 0.0
	local interactiveMode = false

	-- function wrappers
	local _meshWrapper_SetValue
	local pluginInstance_UpdateMesh

	-- value wrapper local and function wrapper declarations
	local meshWrapper, meshWrapper_SetValue


	parameters main rollout:params
	(
		Num_Clones ui:Num_Clones default:1 type:#integer animatable:true
		X_Offset ui:X_Offset default:10 type:#float animatable:true
		Y_Offset ui:Y_Offset default:10 type:#float animatable:true
		Z_Offset ui:Z_Offset default:10 type:#float animatable:true
		X_Rot ui:X_Rot default:0 type:#float animatable:true
		Z_Rot ui:Z_Rot default:0 type:#float animatable:true
		Y_Rot ui:Y_Rot default:0 type:#float animatable:true
		Scale ui:Scale default:1 type:#float animatable:true
		_dummy type:#boolean -- this exists to enable easy invalidation of the object
		pluginGraph type:#filename assettype:#MaxCreationGraph readOnly:true enumAsAsset:true 
		pluginGraphDependencies type:#filenametab assettype:#MaxCreationGraph readOnly:true enumAsAsset:true tabSize:0 tabSizeVariable:true
	)



	fn RunSimLoop updateFxn = (
		local result = ok
		try (
			result = updateFxn ()
		)
		catch()
		return result
	)

	fn initialize = 
	(
		pluginDefinitionFile = @"N:/Google_Drive/_MASTER/max/_mcg/mcg_clone.maxtool"
		local c = dotNetClass "Viper3dsMaxBridge.ModifierPluginInstance"
		local cid = this.classId
		local refMakerWrapperClass = dotnetclass "Autodesk.Max.MaxPlus.ReferenceMaker"
		refMakerWrapper = refMakerWrapperClass._CreateWrapper this
		pluginInstance = c.CreateInstance cid[1] cid[2] pluginDefinitionFile refMakerWrapper
		pluginInstance_UpdateMesh = pluginInstance.UpdateMesh
		local meshWrapperClass = dotnetclass "Autodesk.Max.MaxPlus.Mesh"
		_meshWrapper = meshWrapperClass._CreateWrapper undefined
		_meshWrapper_SetValue = _meshWrapper._SetValue
		pluginGraph = pluginDefinitionFile
		pluginGraphDependencies = #(@"C:\Program Files\Autodesk\3ds Max 2017\MaxCreationGraph\Compounds\CloneAndTransformMesh.maxcompound",@"C:\Program Files\Autodesk\3ds Max 2017\MaxCreationGraph\Compounds\TransformMesh.maxcompound",@"C:\Program Files\Autodesk\3ds Max 2017\MaxCreationGraph\Compounds\DeformMeshPoints.maxcompound")

		-- value wrapper local initializations
		local meshWrapperClass = dotNetClass "Autodesk.Max.MaxPlus.Mesh"
		meshWrapper = meshWrapperClass._CreateWrapper (undefined)
		meshWrapper_SetValue = meshWrapper._SetValue

	)

	on create do 
	(
		initialize()
	)

	on clone fromObj do 
	(
		initialize()
	)

	on load do 
	(
		initialize()
	)

	on update do 
	(
		initialize()
	)

rollout params "LoomFire Clone Tool"
	(
		spinner Num_Clones "Num Clones" range:[1, 100, 1] type:#integer
group""(
		spinner X_Offset "X Offset" range:[0, 100, 10] type:#float
		spinner Y_Offset "Y Offset" range:[0, 100, 10] type:#float
		spinner Z_Offset "Z Offset" range:[0, 100, 10] type:#float
)
group""(
		spinner X_Rot "X Rot" range:[-100, 100, 0] type:#float
		spinner Y_Rot "Y Rot" range:[-100, 100, 0] type:#float
		spinner Z_Rot "Z Rot" range:[-100, 100, 0] type:#float
)
group""(
		spinner Scale "Scale" range:[0.001, 10, 1] type:#float
)

	)

	fn updateMesh = 
	(

		if (pluginInstance == undefined) then ( return undefined )
		local args = dotnet_ValueToDotNetObject #(meshWrapper, Num_Clones, X_Offset, Y_Offset, Z_Offset, X_Rot, Z_Rot, Y_Rot, Scale) argsType
		pluginInstance_UpdateMesh (currentTime as integer) _meshWrapper args
		ok
	)
	
	on modifyMesh do
	(
		_meshWrapper_SetValue mesh
		meshWrapper_SetValue mesh

		if (PreInvokeMethod != undefined ) then PreInvokeMethod()
		RunSimLoop updateMesh
		_meshWrapper_SetValue undefined

		if (PostInvokeMethod != undefined ) then PostInvokeMethod()
		ok
	)

)
