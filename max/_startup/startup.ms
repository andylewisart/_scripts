--global for python file parser method. Stored as a string.
global python_file_parser_cmd=""
global MASTER_SCRIPT_PATH
--Get master script directory
fn getRelativeMasterScriptPath scriptFileName = 
(
	pattern =@".*MASTER"
	rgx = dotnetObject "System.Text.RegularExpressions.Regex" pattern
	result = rgx.match scriptFileName
	result.value
)
MASTER_SCRIPT_PATH = getRelativeMasterScriptPath (getThisScriptFilename())

--initialize python imports and paths 
fn  init_loomfire_python =
(
	python.execute ("
import MaxPlus
import sys
relative_path = (MaxPlus.Core.EvalMAXScript(\"MASTER_SCRIPT_PATH\")).Get() + \"\python\"

if not relative_path in sys.path:
		sys.path.append(relative_path)

	")
)
init_loomfire_python()

--import Infuse Menu
fileIn @"\\thanos\3D\_LIBRARY\_scripts\max\_pipeline\infuse_menu\infuse_menu_v0.2.ms"


print "Infuse startup script activated!"