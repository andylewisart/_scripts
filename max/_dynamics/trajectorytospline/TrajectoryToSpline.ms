--------------------------------------------------------------------------------------
-- Script Name:	TrajectoryToSpline.ms
-- Compatible:		Max 2012 and up
-- Version:		v1.0
-- Release date:   04 July 2016
-- Code by:		Hernan A. Rodenstein  - www.rode3d.com.ar

-- Part of this script is based on "PflowToSplines" script by Martin Geupel (http://www.racoon-artworks.de)
------------------------------------------------------------------------------------

macroScript TrajectoryToSpline
category:"Rode3D"
tooltip:"Trajectory to Spline"
buttontext:"Trj->Spline"
(
	global AA_floater
	global AA_pos = [407,77]
	
	rollout RL_Trj2Spline "Trajectory to Spline V1.0"
	(		
		local sourceObjsArr = #()
		local sourceType = 1 -- object/s
		local GeneratedSplines = #()
		local astart = animationRange.start.frame as integer
		local aend = animationRange.end.frame as integer
		
		--Filter functions for pick button
		fn allObjs_filt obj = true
		fn pflow_filt obj = classOf obj == PF_Source
			
		local objSelectionFilter = allObjs_filt
		
		group "Trajectory Source"
		(
			radiobuttons rbApplyTo labels:#("Animated object/s", "PFlow particles") align:#left default:sourceType
			button btnPickObjs "Pick Object/s" width:130 height:30 align:#center offset:[0,5]
		)		
		group "Frame Range"
		(
			radiobuttons useTimeRange labels:#("Active Range", "Set Range") align:#left default:1
			spinner sframe "Start Time....." range:[-1000,1000000,astart] type:#integer fieldwidth:50 align:#left enabled:false
			spinner eframe "End Time ......" range:[-1000,1000000,aend] type:#integer fieldwidth:50 align:#left enabled:false
		)
		group "Curve type"
		(
			radiobuttons rbKnotType columns:2 labels:#("Linear", "Curve") offsets:#([0,0], [5,0]) align:#left default:2
		)
		group "Sample Rate"
		(
			spinner stepCount "Every.........." range:[1,100,1] type:#integer fieldwidth:30 align:#left across:2
			label txt1 "f" offset:[-11,0] align:#right
		)
		checkbox attachSplines "Attach all splines" checked:false offset:[0,10]

		button generateSplines "GENERATE SPLINES" width:150 height:30 align:#center offset:[0,10]
		progressbar progBar width:150 height:5 color:blue align:#center offset:[0,3]

		-- Create a spline from PFlow particle trajectory
		fn CreateSplineFromParticle startFrame endFrame PositionArray knottype spPrecision =(
			srcPos = 1  -- loop position in positionarray
			lifeSpan = (   -- count total amount of positions which are not undefined 
				tmpCounter = 0
				for cnt in PositionArray where cnt != undefined do tmpCounter +=1
				tmpCounter
			)
			while PositionArray[srcPos] == undefined AND srcPos <= (endFrame-startFrame) do(  -- if current pos in positionArray is undefined step 1 forward
				srcPos += 1
			)
			firstFrameSv = srcPos
			
			if PositionArray[srcPos] != undefined do(   -- if cached particle is not in supplied range it would fail otherwise
				anSpl = SplineShape pos:PositionArray[srcPos]  
				anSpl.wirecolor = green
				anSpl.optimize = true
				anSpl.steps = 6
				addNewSpline anSpl
				
				for iter = 1 to lifeSpan do(	-- create either corner or smooth knots
					case knottype of(
						1: (if PositionArray[iter]!=undefined then addKnot anSpl 1 #corner #line PositionArray[iter])
						2: (if PositionArray[iter]!=undefined then addKnot anSpl 1 #smooth #curve PositionArray[iter])
					)
				)
				if numKnots anSpl < 2 then (
					try(delete anSpl)catch()  -- important: if particles are born on the last frame in range, spline wouldn't have more than 1 knot --> error!
				)else(
					append GeneratedSplines anSpl  -- collect generated splines in an array
					updateShape anSpl  -- update shape after knot creation
				)
			)
		)

		fn PFlowToSplines PFSource spPrecision knottype =
		(
			PCache = #()
			progBar.value = 0.0

			undo off(
				maxOps.getDefaultTangentType &inTangent &outTangent
				maxops.setDefaultTangentType #linear #linear  -- linear interpolation between keys, important for steps
				start = timeStamp()
				PContainer = PFSource
				slidertime = aend   -- set slidertime to endframe to..
				totalParticleNum = PContainer.numParticlesGenerated()  -- ... get the total amount of generated particles
				
				for pCnt = 1 to totalParticleNum do(   -- fill PCache array with empty Array for each generated particle  
					append PCache #()
					PCache[pCnt][(aend-astart)+1] = undefined
				)
				
				counterN = -1
				for cTime = astart to aend by spPrecision while keyboard.escPressed == false do(  -- loop through each frame and collect positions
					counterN += 1
					slidertime = cTime
					if mod (cTime as integer) 5 == 0.0 then (  -- on each 5th frame do:
						(dotnetClass "Application").doEvents()  -- force max to update ui elements
						progBar.value = 100.0*(cTime-astart)/(aend-astart)  -- update Progressbar
					)
					for pCnt = 1 to totalParticleNum do(   -- for each particle...
						thIndex = 0   -- variable to store the ORIGINAL particle birth ID (ids will reset when particles die!)
						PContainer.hasParticleID pCnt &thIndex  -- get the birth particle ID for correct position in cachearray
						SubCache = PCache[pCnt]
						if PContainer.getParticleID thIndex != 0 then(  -- if particle is still alive..
							SubCache[(counterN)+1] = (PContainer.getParticlePosition thIndex)   --  ... append the position in PCache array at the index of current particle
						)
						else(
							SubCache[(counterN)+1] = undefined  -- if dead: append undefined
						)
					)
				)
				progBar.value = 0.0
				
				for cCount = 1 to PCache.count while keyboard.escPressed == false do(   -- for each array in PCache (means: for each particle) do... while esc not holding...
					CreateSplineFromParticle astart aend PCache[cCount] knottype spPrecision-- call spline generation function and supply StartFrame, EndFrame, cached Positions of the particle, knottype
				)
				
				end = timeStamp()
				slidertime = animationrange.start
				gc()
				maxops.setDefaultTangentType inTangent outTangent
			) -- end of undo
		)
		
		-- Get Object/s positions at every frame and store them in array
		function getObjsPosArr objsArr =
		(
			local objsPosArr=#()
			for b=1 to objsArr.count do (
				append objsPosArr #()
				for f=astart to aend do (
					at time f append objsPosArr[b] objsArr[b].transform.pos
				)
			)
			return objsPosArr
		)
		
		-- Draw a spline based on points array
		fn drawSpline pointsArr sampleRate knotType =
		(
			minVertsDistance = 0.05
			if knotType==1 then (
				knType=#corner
				cvType=#line
			) else (
				knType=#smooth
				cvType=#curve
			)
			ss = SplineShape pos:pointsArr[1]
			addNewSpline ss		
			prevPt = pointsArr[1]+100.0
			for p=1 to pointsArr.count by sampleRate do (				
				if (distance pointsArr[p] prevPt) > minVertsDistance then (
					addKnot ss 1 knType cvType pointsArr[p]
				)
				prevPt = pointsArr[p]
			)
			updateShape ss
			ss
		)
		
		-- Attach all the splines created
		fn attachAllSplines spArr =
		(
			for s=2 to spArr.count do ( addAndWeld spArr[1] spArr[s] 0 )
			updateShape spArr[1]
		)
		
		
		on rbApplyTo changed state do
		(
			if state==1 then objSelectionFilter = allObjs_filt else objSelectionFilter = pflow_filt
		)
		
		on btnPickObjs pressed do
		(
			pickedObjsArr = selectByName title:"Select Object/s" buttonText:"OK" filter:objSelectionFilter showHidden:false single:false		
			if pickedObjsArr!=undefined then (
				if pickedObjsArr.count>0 then (				
					sourceObjsArr = pickedObjsArr
					sourceType = rbApplyTo.state
					btnPickObjs.text = pickedObjsArr[1].name+"..."
				)
			)
		)		
		
		on useTimeRange changed state do
		(
			if useTimeRange.state == 2 then (
				sframe.enabled = true
				eframe.enabled = true
			) else (
				sframe.enabled = false
				eframe.enabled = false
				astart = animationRange.start.frame as integer
				aend = animationRange.end.frame as integer
			)			
		)

		on sframe changed val do astart = val
		
		on eframe changed val do aend = val
		
		on generateSplines pressed do
		(
			free GeneratedSplines
			if sourceType==1 then ( -- source is object/s
				objsPosArr = getObjsPosArr sourceObjsArr
				progBar.value = 0.0
				undo off (
					for arr=1 to objsPosArr.count do (
						sp = drawSpline objsPosArr[arr] stepCount.value rbKnotType.state
						appendIfUnique GeneratedSplines sp
						if mod (arr as integer) 5 == 0.0 then (  -- on each 5th frame do:
							(dotnetClass "Application").doEvents()  -- force max to update ui elements
							progBar.value = 100.0*(arr-astart)/(aend-astart)  -- update Progressbar
						)
					)
				)
				progBar.value = 0.0
			) else ( -- source is particle system				
				PFlowToSplines sourceObjsArr[1] stepCount.value rbKnotType.state
			)
			if attachSplines.checked==true then attachAllSplines GeneratedSplines
		) -- end on
	) -- end rollout

	try
	(
		closerolloutfloater aa_floater
		aa_pos = aa_floater.pos
	)catch()
	aa_floater = newrolloutfloater "Trajectory to Spline V1.0" 170 420
	AA_floater.pos = AA_pos
	addrollout RL_Trj2Spline AA_floater rolledup:false
)

