fn getCurrentStateSetName = 
(	
	try(
		stateSetsDotNetObject = dotNetObject "Autodesk.Max.StateSets.Plugin" --Get StateSets dotNet object. 
		stateSets = stateSetsDotNetObject.Instance --Get the state sets instance from the dotNet object.  

		masterState = stateSets.EntityManager.RootEntity.MasterStateSet

		return  masterState.CurrentState[1].name
	)
	catch(return "MasterState")

)


