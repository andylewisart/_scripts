stateSetsDotNetObject = dotNetObject "Autodesk.Max.StateSets.Plugin" --Get StateSets dotNet object. 
stateSets = stateSetsDotNetObject.Instance --Get the state sets instance from the dotNet object.  

stateSets.ShowMainFrame() --Open the State Sets window. 

masterState = stateSets.EntityManager.RootEntity.MasterStateSet

stateSet = masterState.AddState()
stateSet.Name = "MyStateSet"

show masterState.CurrentState

previousCurrentStateSet = masterState.CurrentState.Items[0] --Record current state set.
masterState.CurrentState = #(stateSet) --Assign current state set. 
stateSet.BeginRecording() --Begin recording state set.  
stateSet.EndRecording() --End recording state set. 
masterState.CurrentState = #(previousCurrentStateSet) --Assign previous state set. 


subStateSet = stateSet.AddState()
subStateSet = stateSet.Render #(stateSet) --Render state set, note that we specify a path because this state set could be located under multiple parents.

scriptedState = stateSet.AddScriptedState() --Add a scripted state. 
scriptedState.ApplyScript = "-- MaxScript executed when applying state set" --Assign the apply script.
scriptedState.RevertScript = "-- MaxScript executed when reverting state set" --Assign the revert script. 
stateSet.CreateTemplate() --Create a template for this state set  


masterState.RemoveChild stateSet --Removes the state set 
masterState.RemoveChild masterState.Children.Item[0] --Removes the first child 

masterState.RenderAllStates()

objectStateSet = masterState.ObjectStateSet



showproperties masterState.CurrentState[1].name


============

stateSetsDotNetObject = dotNetObject "Autodesk.Max.StateSets.Plugin" --Get StateSets dotNet object. 
stateSets = stateSetsDotNetObject.Instance --Get the state sets instance from the dotNet object.  

masterState = stateSets.EntityManager.RootEntity.MasterStateSet

showproperties masterState.CurrentState[1].name