(
if fusMaxFilename == undefined then (
	messagebox "!!! Startup script is missing. !!!"
)
	
fn editValue hwnd val = (
	WM_CHAR 	= 0x0102
	VK_RETURN 	= 0x000D

	uiaccessor.setwindowtext hwnd val
	windows.sendMessage hwnd WM_CHAR VK_RETURN 0 -- press ENTER key
)	
fn getChildHwnd papaHwnd name:"" = (
  for i in (windows.getChildrenHWND papaHwnd) where matchPattern i[5] pattern:name do return i[1]
  0
)

global fusePreviewPath = ""
local makingPreview = false
local whatismaxversion = (maxversion())[1]
local cancelled = false
fn pastePreviewPath = (
	local WindowHandle = DialogMonitorOPS.GetWindowHandle()
	local title =  (UIAccessor.GetWindowText WindowHandle)

	if title == "Make Preview" then (
		local CustomFileTypeHwnd = getChildHwnd WindowHandle name:"Custom File Type"
		UIAccessor.PressButton CustomFileTypeHwnd
		if whatismaxversion == 17000 then (
			UIAccessor.PressButtonByName WindowHandle "File..."
		)
		local dontstopHwnd = getChildHwnd WindowHandle name:"Don't Stop"
		if dontstopHwnd> 0 then (
			cancelled = true
		)
		print(dontstopHwnd)
	)
			
-- 	if title == " AVI File Compression Setup " then (
-- 		UIAccessor.CloseDialog WindowHandle
-- 	)

 	if title == "Create Animated Sequence File..." then (
		-- PASTE
		local edits = for c in (windows.getchildrenhwnd WindowHandle) where c[4] == "Edit" collect c[1]
		editValue edits[1] fusePreviewPath		
		
		-- SET JPG
		if whatismaxversion == 17000 then (
			local filetypeUI 
			local filetypeHwnd
			for i in (windows.getChildrenHWND WindowHandle) where matchPattern i[5] pattern:"*AVI File (*.avi)*" do (
				filetypeUI = i
				filetypeHwnd =i[1]
			)
			if filetypeUI != undefined then (
				WM_COMMAND = 0x111 -- Windows Message: Command
				WM_LBUTTONDOWN = 0x0201
				WM_LBUTTONUP = 0x0202
				CB_GETCOUNT = 0x0146 -- ComboBox message: Get number of items
				CB_SHOWDROPDOWN = 0x014F
				CB_SETCURSEL = 0x014E -- ComboBox message: Set current selection
				CBN_SELENDOK = 9 -- ComboBox notification: Selection ended, OK		

				windows.sendMessage filetypeHwnd CB_SHOWDROPDOWN 1 0
				windows.sendMessage filetypeHwnd CB_SETCURSEL 7 0
				windows.sendMessage filetypeHwnd WM_LBUTTONDOWN 0 -1	
				windows.sendMessage filetypeHwnd WM_LBUTTONUP 0 -1		
				windows.sendMessage filetypeHwnd CB_SHOWDROPDOWN 0 0		
			)
		)	
		-- SAVE
		local saveHwnd 
		for i in (windows.getChildrenHWND WindowHandle) where matchPattern i[5] pattern:"&Save" do (
			saveHwnd = i[1]
		)
		UIAccessor.PressButton saveHwnd
		makingPreview = true
	)

	if title == "JPEG Image Control" then (
		UIAccessor.PressButtonByName WindowHandle "OK"
		makingPreview = true
 	)
	true
)

parsedMaxFile = fusMaxFilename()

local doContinue = true
if parsedMaxFile.valid == false then (
	doContinue = (queryBox ("Invalid Filename.\nDo you want to make preview in " + (getdir #preview) + "?"))
)

if doContinue == true then (
	fusePreviewPath = parsedMaxFile.dirPreview + parsedMaxFile.name + "..jpg"
	if parsedMaxFile.valid == false then (
		previewMaxfilename = parsedMaxFile.name
		if previewMaxfilename == "" then (
			previewMaxfilename = "Untitled"
		)
		fusePreviewPath = (getdir #preview) + previewMaxfilename + "\\" + previewMaxfilename + "..jpg"
	)
	local doesShiftPressed = keyboard.shiftPressed 
	DialogMonitorOPS.enabled = true
	DialogMonitorOPS.RegisterNotification pastePreviewPath id:#pastePreviewPath
	local currentOutGamma = fileOutGamma
	fileOutGamma = 2.2
	makedir (getFilenamePath fusePreviewPath) all:true
	max preview
	fileOutGamma = currentOutGamma
	sliderTime = animationRange.start
	if makingPreview == true then (
		if doesShiftPressed == false and cancelled == false then (
-- 			makedir  (maxfilepath + "preview\\") all:true
-- 			saveMaxFile ((getFilenamePath fusePreviewPath)  + maxfilename) useNewFile:false clearNeedSaveFlag:false
		)
-- 		previewSeqPath = "\"" + ((getFilenamePath fusePreviewPath) + (getFilenameFile fusePreviewPath) + "####" + (getFilenameType fusePreviewPath)) + "\""
-- 		pdPlayerPath = "\"" + @"C:\Program Files\Pdplayer 64\pdplayer64.exe" + "\""
-- 		pdPlayerOption = "--frame_base=1 --fps=24 --automatic_preload=1 --play_forward"
-- 		-- ShellLaunch pdPlayerPath (previewSeqPath + " " + pdPlayerOption)
-- 		pdPlayerCommand = ((pdPlayerPath + " " + previewSeqPath + " " + pdPlayerOption))
-- 		print(pdPlayerCommand)
-- 		hiddendoscommand pdPlayerCommand donotwait:true
		if cancelled == false then (
			local parsedFusMaxfile = fusMaxFilename()
			local previewSeqPath = (getFilenamePath fusePreviewPath)
			local RVcommand = (@"explorer rvlink://" + previewSeqPath + " -screen 0 -play")
-- 				print(RVcommand)``````````````````````````````
-- 				HiddenDOSCommand (@"explorer rvlink://S:/episodic/fus/fus500/renders/preview/010x005/fus500_010x005_train-ani_v002 -screen 0")
			HiddenDOSCommand RVcommand
		)
	)
	DialogMonitorOPS.unRegisterNotification id:#pastePreviewPath
	DialogMonitorOPS.enabled = false
	makingPreview = false
)
)