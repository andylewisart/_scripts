--****************************************************************************
-- Copy Render to Clipboard, v2.0
-- Author:   Christopher Grant 
-- CREDIT: the extension that copies a bitmap to the windows system clipboard is by Larry Minton, and Bobo gave me a cool function to optimize copying the Alpha channel...
-- Email: scripts@scriptspot.com
--
-- Description:
--	Simply copies your last render to the clipboard. I've separated it into two macroscripts,
--  one "CopyRenderToClipboard" copies the RGB data to the clipboard and the other one 
--  "CopyRenderAlphaToClipboard" copies the Alpha data to the clipboard. Note these will both
--  work with 3rd party renderers as long as you are rendering through the 3ds max frame buffer
--  (the image you would see when clicking "Show Last Rendering")...
--
-- Get more info and the latest version of Larry Minton's AVGuard Extension at http://www.scriptspot.com
-- AVGuard v6.08 (3ds max 6) is available at http://www.scriptspot.com/extensions/web_upload/Larry%20Minton/avg_dlx60_8.zip
-- AVGuard v7.06 (3ds max 7) is available at http://www.scriptspot.com/extensions/web_upload/Larry%20Minton/avg_dlx70_6.zip
--
-- Installation: 
--	Step 1: INSTALL APPROPRIATE AVGUARD EXTENSION INTO YOUR PLUGINS FOLDER...
--	Step 2: Run script then find it under "CG_Tools" in the customize ui category
--
--	Usage:
--	 Just put it in your quad menu / menu bar / keyboard shortcut... There isn't any interface...
--
-- Revision History
--  9/27/2004: development began
--  12/7/2004: just updated header text for appropriate max 7 info...
--  3/24/2005: Thanks to Bobo's optimization I've added Alpha channel copying...
--
--****************************************************************************/

macroScript Copy_Render_To_Clipboard
buttontext:"Copy Render"
Category:"CG_Tools" 
tooltip:"Copy Render To Clipboard" 
(
if setclipboardbitmap != undefined then		--check whether command is undefined... if so then avg not installed or using pre 3ds max 7...
	(
		therender = getlastrenderedimage() 	--new to max 6...
		if therender != undefined do 
			setclipboardbitmap therender	--clipboard access via AVG extension (in version 6 at least)
		
		therender = 0
		gc()								--making sure it doesn't sit in memory any longer than needed...
	)
else
	(
		theerrormsg = "This script requires Larry Minton's AVGuard extension. You can download it from scriptspot.com or your favorite max plugin site."
		messagebox theerrormsg title: "AVGuard Extension Not Found"
	)
)

macroScript Copy_Render_Alpha_To_Clipboard
buttontext:"Copy Render ALPHA"
Category:"CG_Tools" 
tooltip:"Copy Render ALPHA To Clipboard" 
(

fn getLastRenderedAlpha = --THANK YOU BOBO!!
(
	theImage = getlastrenderedimage()	
	if theImage != undefined then	
	(
		deleteFile (GetDir #image + "\A_alphaSplit_Temp.tga") --del any old file	
		gc light:true --makes sure the image doesn't get cached
		oldCD = targa.getColorDepth()	
		targa.setColorDepth 32	
		oldAS = targa.getAlphaSplit()	
		targa.setAlphaSplit true	
		theImage.filename = GetDir #image + "\alphaSplit_Temp.tga"	
		save theImage	
		close theImage	
		targa.setAlphaSplit oldAS 	
		targa.setColorDepth oldCD 	
		deleteFile (GetDir #image + "\alphaSplit_Temp.tga")	
		try (openBitmap (GetDir #image + "\A_alphaSplit_Temp.tga"))
		catch(undefined)
	) 	
	else undefined
)

if setclipboardbitmap != undefined then		--check whether command is undefined... if so then avg not installed or using pre 3ds max 7...
	(
		thealpha = getLastRenderedAlpha() 	--function to get alpha via saving to disk first
		if thealpha != undefined do 
		(	
			setclipboardbitmap thealpha --clipboard access via AVG extension (in version 6 at least)
			thealpha = 0
		)			
	)
else
	(
		theerrormsg = "This script requires Larry Minton's AVGuard extension. You can download it from scriptspot.com or your favorite max plugin site."
		messagebox theerrormsg title: "AVGuard Extension Not Found"
	)
)