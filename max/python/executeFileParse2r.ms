getThisScriptFilename()
global renderPreviewPath = undefined


currentStateName
-- fn python_preview_path_for_current_state  = 
-- (
	python.execute ("
import MaxPlus
import re		
import sys
		
relative_path = MaxPlus.Core.EvalMAXScript('getThisScriptFilename()')
relative_path = re.findall(r'.*MASTER', relative_path.Get())

if not relative_path[0] in sys.path:
		sys.path.append(relative_path[0])

for i in sys.path:
		print i
		
		
import python.fileParser
reload(python.fileParser)
from python.fileParser import FileParser

thisFilePath = MaxPlus.Core.EvalMAXScript(\"maxfilepath\")
thisFileName = MaxPlus.Core.EvalMAXScript(\"maxfilename\")
thisStateName = MaxPlus.Core.EvalMAXScript(\"currentStateName\")

derp = FileParser(thisFilePath.Get(), thisFileName.Get())
print derp.get_render_preview_path()
bob = derp.set_version_folder(derp.get_explicit_folder(derp.get_render_preview_path(), thisStateName.Get()))

MaxPlus.Core.EvalMAXScript('renderPreviewPath = @\"%s\"'%bob)
		
	")
-- )
-- python_preview_path_for_current_state()



renderPreviewPath
