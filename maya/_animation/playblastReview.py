# -- Fuse Playblast Tool for Maya
# -- written by Andy Lewis
# -- 2014
# ----------------imports-----------------

import os
import re
import maya.cmds as cmds
import maya.mel as mel
import glob
import subprocess
from maya import OpenMayaUI as omui
from shiboken import wrapInstance 
from PySide import QtCore, QtGui, QtUiTools
from PySide.QtCore import *
from PySide.QtGui import *

# ----------Paths--------------
UI_PATH = "S:/outfit/artists/Andy/_maya/_scripts/UI/playblastReview.ui"
# -------------------------------
mayaMainWindowPtr = omui.MQtUtil.mainWindow() 
mayaMainWindow= wrapInstance(long(mayaMainWindowPtr), QWidget) 
# ------------Load UI ------------
def loadUi(path, parent=None):
    loader = QtUiTools.QUiLoader()
    file = QtCore.QFile(path)
    file.open(QtCore.QFile.ReadOnly)
    widget = loader.load(file, parent)
    file.close()
    return widget
# ---------------class-----------------
class PlayblastReview(QtGui.QMainWindow):  
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        playblastReviewUI = UI_PATH
        self.ui = loadUi(playblastReviewUI, self)

        #variables
        self.previewPath = ""
        self.fileDir = ""
        self.renderDir = ""
        self.shotName = ""
        self.fileName = ""
        self.mayaFileName = ""
        self.shotNumber = ""
        self.truncFileName = ""
        self.playblastShotDir = ""
        self.playblastRenderDir = ""
        self.playblastFullName = ""
        self.fullFileName = ""

        #initial setup
        self.ui.main_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.initParse()
        self.addTableItem()

        #connections

        self.ui.main_table.doubleClicked.connect(self.playPlayblast)
        self.ui.play_btn.clicked.connect(self.playPlayblast)
        self.ui.open_btn.clicked.connect(self.openFolder)
        self.ui.submit_btn.clicked.connect(self.submit)

        #self.ui.play_btn.clicked.connect(self.addTableItem)

    def submit(self):
        row = self.ui.main_table.currentRow()

        if self.ui.main_table.model().index(row, 1).data() != "No jpg":

            #item = self.ui.main_table.itemFromIndex(row)

            rowData = self.ui.main_table.model().index(row, 0).data()



            path = self.playblastShotDir + "/" + rowData + "/"


            print path
            #return path


            path.replace("/", "\\")
            #prvPath = getFilenamePath  fName
            #checkFileNumbers (prvPath+"*.jpg")
            #print prvPath
            pythonEXE = "C:\\Python26\\python.exe"
            pythonUI = "S:\\outfit\\shared\\pythonPipeline\\SubmitForReviewPreview\\submitForreview_ffx3_v001.py"
            #-- filesFolders = @"S:\\episodic\\fus\\fus500\\renders\\preview\\010x005\\fus500_010x005_train-ani_v002"
            bypass = "False"
            #print prvPath
            mainApp = pythonEXE+" "+pythonUI+" "+path+" "+bypass
            #hiddenDOSCommand  (mainApp)
            print mainApp, " Main App"
            os.system(mainApp)

        else:
            cmds.warning("No jpg sequence found for this version. Nothing submitted")

    def parseFrameRange(self, item, row):
        print item.text(), "TEXT"

        path = self.playblastShotDir + "/" + item.text()

        print path, "PATH"

        frameArray = []

        for i in glob.glob(path + '/' +'*.jpg'):

            frameArray.append(i)

        frameArray.sort()

        if frameArray != []:
            print frameArray[0]

            frameStart = frameArray[0].split(".")[-2]
            print frameStart
            frameEnd = frameArray[-1].split(".")[-2]

            print frameStart, frameEnd

            frameItem = QtGui.QTableWidgetItem(frameStart+"-"+frameEnd)
            self.ui.main_table.setItem(row,1,frameItem)
        else:
            frameItem = QtGui.QTableWidgetItem('No jpg')
            self.ui.main_table.setItem(row,1,frameItem)


    def playPlayblast(self):

        row = self.ui.main_table.currentRow()

        if self.ui.main_table.model().index(row, 1).data() != "No jpg":

            #item = self.ui.main_table.itemFromIndex(row)

            rowData = self.ui.main_table.model().index(row, 0).data()



            path = self.playblastShotDir + "/" + rowData + "/"


            for i in glob.glob(path + '/' +'*.jpg'):
                print i, "GOGOGO"
                os.system("start %s"%i)
                break



            

        else:
            cmds.warning("No jpg sequence found for this version")

    def openFolder(self):

        row = self.ui.main_table.currentRow()
        print row, "row"
        rowData = self.ui.main_table.model().index(row, 0).data()

        path = self.playblastShotDir + "/" + rowData + "/"

        for i in glob.glob(path + '/' +'*.jpg'):
            print i, "GOGOGO"
            i = i.replace("/", "\\")
            subprocess.Popen(r'explorer /select, "%s"'%i)
            break

    def addTableItem(self):

        rowNum = -1

        for i in os.listdir(self.playblastShotDir):
            print i
            rowNum += 1
            self.ui.main_table.insertRow(rowNum)
            item = QtGui.QTableWidgetItem(i)
            self.ui.main_table.setItem(rowNum,0,item)

            self.parseFrameRange(item, rowNum)


    def initParse(self):

        self.fileDir = cmds.file(q= True, loc = True)
        self.renderDir = self.fileDir.split('projects')[0] + 'renders/preview'
        self.shotName = (self.fileDir.split('projects')[1]).split("/")[1]
        self.fileName = (self.fileDir.split('/')[-1]).split(".")[0]
        self.mayaFileName = self.fileName[:-5] + "May" + self.fileName[-5:]


        self.shotNumber = (self.fileDir.split('projects')[0]).split("/")[-2]
        self.truncFileName = self.fileName.replace(self.shotName+"_", '')
        self.playblastShotDir = self.renderDir+"/"+self.shotName.replace(self.shotNumber+"_", "")
        self.playblastRenderDir = self.playblastShotDir+"/" + self.mayaFileName
        self.playblastFullName = self.playblastRenderDir + "/" + self.mayaFileName #+ ".9999.jpg"
        self.fullFileName = ""

        print self.fileDir, "fileDir"
        print self.renderDir, "renderDir"
        print self.shotName, "shotName"
        print self.fileName, "fileName"
        print self.mayaFileName, "mayaFileName"
        print self.shotNumber, "shotNumber"
        print self.truncFileName, "truncFileName"
        print self.playblastShotDir, "playblastShotDir"
        print self.playblastRenderDir, "playblastRenderDir"
        print self.playblastFullName, "playblastFullName"

  


def launchUIMaya():

    if (cmds.window('mayaPlayblastReview', exists=True) == True):
        cmds.deleteUI('mayaPlayblastReview')

    if cmds.file(q= True, loc = True) != 'unknown':
        app = PlayblastReview(parent=mayaMainWindow)

        cmds.showWindow( 'mayaPlayblastReview' )
        return app
    else:
        cmds.warning("Please save file in proper location before using this script.")

if __name__ == '__main__':
    launchUIMaya()

