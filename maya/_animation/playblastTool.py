# -- Fuse Playblast Tool for Maya
# -- written by Andy Lewis
# -- 2014
# ----------------imports-----------------

import os
import re
import maya.cmds as cmds
import maya.mel as mel
import shutil
from maya import OpenMayaUI as omui
from shiboken import wrapInstance 
from PySide import QtCore, QtGui, QtUiTools
from PySide.QtCore import *
from PySide.QtGui import *

# ----------Paths--------------
UI_PATH = "S:/outfit/artists/Andy/_maya/_scripts/UI/playblastTool.ui"
# -------------------------------
mayaMainWindowPtr = omui.MQtUtil.mainWindow() 
mayaMainWindow= wrapInstance(long(mayaMainWindowPtr), QWidget) 
# ------------Load UI ------------
def loadUi(path, parent=None):
    loader = QtUiTools.QUiLoader()
    file = QtCore.QFile(path)
    file.open(QtCore.QFile.ReadOnly)
    widget = loader.load(file, parent)
    file.close()
    return widget
# ---------------class-----------------
class PlayblastTool(QtGui.QMainWindow):  
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        playblastToolUI = UI_PATH
        self.ui = loadUi(playblastToolUI, self)

        #variables
        self.playblastString = ""
        self.fileDir = ""
        self.renderDir = ""
        self.shotName = ""
        self.fileName = ""
        self.mayaFileName = ""


        self.shotNumber = ""
        self.truncFileName = ""
        self.playblastShotDir = ""
        self.playblastRenderDir = ""
        self.playblastFullName = ""
        self.fullFileName = ""

        #initial setup
        self.initVariables()
        self.setInitialCustomFrames()
        self.overwriteSetup()
        

        #connections
        self.ui.specTime_radio.toggled.connect(self.specFrangeToggle)
        self.ui.displaySettings_combo.currentIndexChanged.connect(self.customSizeToggle)
        self.ui.cancel_btn.clicked.connect(self.closeApp)
        self.ui.create_btn.clicked.connect(self.checklist)
        self.ui.scale_slider.sliderMoved.connect(self.sliderValueLineEdit)
        self.ui.sliderScale_spin.valueChanged.connect(self.setSliderValue)


    def initVariables(self):

        self.playblastString = ""
        self.fileDir = cmds.file(q= True, loc = True)
        self.renderDir = self.fileDir.split('projects')[0] + 'renders/preview'
        self.shotName = (self.fileDir.split('projects')[1]).split("/")[1]
        self.fileName = (self.fileDir.split('/')[-1]).split(".")[0]
        self.mayaFileName = self.fileName[:-5] + "May" + self.fileName[-5:]


        self.shotNumber = (self.fileDir.split('projects')[0]).split("/")[-2]
        self.truncFileName = self.fileName.replace(self.shotName+"_", '')
        self.playblastShotDir = self.renderDir+"/"+self.shotName.replace(self.shotNumber+"_", "")
        self.playblastRenderDir = self.playblastShotDir+"/" + self.mayaFileName
        self.playblastFullName = self.playblastRenderDir + "/" + self.mayaFileName #+ ".9999.jpg"
        self.fullFileName = ""

    def createFolders(self):

        print self.fileDir, "fileDir"
        print self.renderDir, "renderDir"
        print self.shotName, "shotName"
        print self.fileName, "fileName"
        print self.mayaFileName, "mayaFileName"
        print self.shotNumber, "shotNumber"
        print self.truncFileName, "truncFileName"
        print self.playblastShotDir, "playblastShotDir"
        print self.playblastRenderDir, "playblastRenderDir"
        print self.playblastFullName, "playblastFullName"


        if not os.path.exists(self.playblastShotDir):
            os.makedirs(self.playblastShotDir)
        if not os.path.exists(self.playblastRenderDir):
            os.makedirs(self.playblastRenderDir)


    def checkForJpg(self):
        for thisFile in os.listdir(self.playblastRenderDir):
            if thisFile.endswith('.jpg'):
                cmds.warning("Playblast files already exist. Version up or check 'Override Existing' to playblast anyway. Aborting...")
                return 0

        return 1
                


    def checklist(self):

        self.initVariables()
        #safeguards
        if not '/projects/' in self.fileDir:
            cmds.warning("File doesn't appear to be saved in proper location")

        elif (self.ui.startTime_spin.value() > self.ui.endTime_spin.value()) or (self.ui.endTime_spin.value() < self.ui.startTime_spin.value()):
            cmds.warning("Enter a valid frame range")

        #if overwrite checkbox isn't checked, check to see if there are already jpgs in folder. If it is checked, we don't care if they're already there.
        elif self.ui.overwrite_chx.isChecked() == 0:
            self.createFolders()
            if self.checkForJpg() == 1:
                self.generatePlayblast()


        else:
            self.createFolders()
            self.generatePlayblast()

    def generatePlayblast(self):

        #temporarily swap render global image format to jpg. Otherwise playblast will render iff.

        origGlobalFormat = cmds.getAttr("defaultRenderGlobals.imageFormat") 
        cmds.setAttr("defaultRenderGlobals.imageFormat", 8)

        #builds a tag string for playblast command

        
        self.playblastString += "filename = '%s', "%self.playblastFullName
        self.playblastString += "format = 'image', viewer = 0, "

        if self.ui.specTime_radio.isChecked():
            self.playblastString += "startTime = %s, "%self.ui.startTime_spin.value()
            self.playblastString += "endTime = %s, "%self.ui.endTime_spin.value()

        if self.ui.ornaments_chx.isChecked() == 0:
            self.playblastString += "showOrnaments = 0, "

        if self.ui.offscreen_chx.isChecked():
            self.playblastString += "offScreen = 1, "

        if self.ui.displaySettings_combo.currentIndex() == 0:
            self.playblastString += "width = %s, "%mel.eval('getAttr "defaultResolution.width"')
            self.playblastString += "height = %s, "%mel.eval('getAttr "defaultResolution.height"')

        elif self.ui.displaySettings_combo.currentIndex() == 2:
            self.playblastString += "width = %s, "%self.ui.customWidth_spin.value()
            self.playblastString += "height = %s, "%self.ui.customHeight_spin.value()

        self.playblastString += "percent = %s"%int(self.ui.sliderScale_spin.value())

        print self.playblastString

        playblastCmd = "cmds.playblast(%s)"%self.playblastString

        exec playblastCmd


        cmds.setAttr("defaultRenderGlobals.imageFormat", origGlobalFormat)

        grabFile = self.playblastRenderDir +"/" + os.listdir(self.playblastRenderDir)[0]

        #mayaFile = 

        #cmds.file(v, ea = True, type = 'mayaBinary', f = True)

        os.system("start %s"%grabFile)

        self.copyFileToPlayblastDir()

        self.playblastString = ""

    def copyFileToPlayblastDir(self):
        curDir = cmds.file(q=1, sceneName=1)
        curDir = curDir.replace("/", "\\")
        fileName = cmds.file(q=1, sceneName=1, shortName = 1)
        print curDir, " curDir"
        print self.playblastRenderDir, " playblastRenderDir"
        print fileName, " fileName"
        windowsNaming = self.playblastRenderDir.replace("/", "\\")

        #shutil.copyfile(curDir, windowsNaming)
        try:
            os.system("copy %s %s" %(curDir, windowsNaming+"\\" + fileName))
        except:
            cmds.warning("Could not save a copy of maya file in playblast directory.")
    def overwriteSetup(self):
        if not '$fusePlayblastOverwrite' in mel.eval('env'):
            mel.eval('global int $fusePlayblastOverwrite = 0;')
        else:
            if mel.eval('$tempMelVar = $fusePlayblastOverwrite')==1:
                self.ui.overwrite_chx.setChecked(1)


    def sliderValueLineEdit(self):
        self.ui.sliderScale_spin.setValue(self.ui.scale_slider.sliderPosition()/10)

    def setSliderValue(self):
        self.ui.scale_slider.setValue(self.ui.sliderScale_spin.value()*10)

    def setInitialCustomFrames(self):
        self.ui.startTime_spin.setValue(int(cmds.playbackOptions(minTime = 1, query = 1)))
        self.ui.endTime_spin.setValue(int(cmds.playbackOptions(maxTime = 1, query = 1)))

    def specFrangeToggle(self):
        if self.ui.specTime_radio.isChecked():
            self.ui.startTime_spin.setEnabled(1)
            self.ui.startFrame_label.setEnabled(1)
            self.ui.endFrame_label.setEnabled(1)
            self.ui.endTime_spin.setEnabled(1)
        else:
            self.ui.startTime_spin.setEnabled(0)
            self.ui.startFrame_label.setEnabled(0)
            self.ui.endFrame_label.setEnabled(0)
            self.ui.endTime_spin.setEnabled(0)

    def customSizeToggle(self):

        if self.ui.displaySettings_combo.currentIndex() == 2:
            self.ui.customWidth_label.setEnabled(1)
            self.ui.customHeight_label.setEnabled(1)
            self.ui.customWidth_spin.setEnabled(1)
            self.ui.customHeight_spin.setEnabled(1)
        else:
            self.ui.customWidth_label.setEnabled(0)
            self.ui.customHeight_label.setEnabled(0)
            self.ui.customWidth_spin.setEnabled(0)
            self.ui.customHeight_spin.setEnabled(0)

    def closeApp(self):
        cmds.deleteUI('mayaPlayblastTool')


def launchUIMaya():

    if (cmds.window('mayaPlayblastTool', exists=True) == True):
        cmds.deleteUI('mayaPlayblastTool')

    if cmds.file(q= True, loc = True) != 'unknown':
        app = PlayblastTool(parent=mayaMainWindow)

        cmds.showWindow( 'mayaPlayblastTool' )
        return app
    else:
        cmds.warning("Please save file in proper location before using this script.")

if __name__ == '__main__':
    launchUIMaya()

