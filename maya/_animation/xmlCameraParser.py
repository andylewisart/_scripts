# -*- coding: utf-8 -*-

from xml.dom import minidom
import re
from pymel.all import *
import pymel.core.datatypes as dt
import maya.cmds as cmds
# cam = 'Cam_gle601_48x001_starBoard_v001FBXASC0910FBXASC045134FBXASC093'
# xmlDoc = minidom.parse("S:/episodic/rnd/rnd000/assets/000_000/cam/Render/v001/Cam_rnd000_000_000_Render_v001[1-100].xml")

def createMayaCamera(xmlPath):
	xmlDoc = minidom.parse(xmlPath)
	camName = xmlPath.split('/')[-1]
	camName = camName.split(".")[0]
	camName = camName.replace("[", "_").replace("-", "_").replace("]", "")

	propertyList = xmlDoc.getElementsByTagName('Properties')[0]

	renderWidth = propertyList.attributes['renderWidth'].value
	renderHeight = propertyList.attributes['renderHeight'].value
	renderRange = propertyList.attributes['range'].value
	
	cam = cmds.camera()[0]
	print cam, camName
	cam = cmds.rename(cam, camName)
	itemList = xmlDoc.getElementsByTagName('frame')

	for l in itemList:

		frameNum = l.attributes['f'].value
		cmds.currentTime(frameNum)
		xfArray = l.attributes['xf'].value

		xfArray = re.sub('\(matrix3 ', '', xfArray)
		xfArray = re.findall(r"[-+]?\d*\.*\d+", xfArray)

		for i in xfArray:
			i = re.sub('u', '', i)

		xfArrayFloat = []
		for i in xfArray:
			a = float(i)
			xfArrayFloat.append(a)

		xfArrayFloat.insert(3, 0.0)
		xfArrayFloat.insert(7, 0.0)
		xfArrayFloat.insert(11, 0.0)
		xfArrayFloat.insert(15, 1.0)

		mayaArray = [1,0,0,0,0,0,1,0,0,-1,0,0,0,0,0,1]
		mayaInverseArray = [1,0,0,0,0,0,-1,0,0,1,0,0,0,0,0,1]

		mayaMatrix = dt.Matrix(mayaArray)
		mayaInverseMatrix = dt.Matrix(mayaInverseArray)
		xfMatrix = dt.Matrix(xfArrayFloat)

		#multipliedMatrix = mayaMatrix * xfMatrix * mayaInverseMatrix
		multipliedMatrix = xfMatrix * mayaInverseMatrix

		multipliedArray = []
		for i in multipliedMatrix:
			multipliedArray = multipliedArray + list(i)

		print multipliedArray
		cmds.xform(cam, m=multipliedArray)



		camFov = l.attributes['fov'].value

		cmds.setAttr("%s.focalLength"%cam, float(camFov))

		cmds.setKeyframe(cam)






