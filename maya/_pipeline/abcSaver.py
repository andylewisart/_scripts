import maya.cmds as cmds
import os

fileDir = cmds.file(q= True, loc = True)

if not '/projects/' in fileDir:
    cmds.warning("File doesn't appear to be saved in proper location")
else:
	sel = cmds.ls(sl=1)
	if len(sel)<=0:
		cmds.warning("Please select object(s) for export")
	else:
		assetDir = fileDir.split('projects')[0] + 'assets'
		shotName = (fileDir.split('projects')[1]).split("/")[1]
		fileName = (fileDir.split('/')[-1]).split(".")[0]
		shotNumber = (fileDir.split('projects')[0]).split("/")[-2]
		userAssetName = "Sparky"
		truncFileName = fileName.replace(shotName+"_", '')
		frameStart = 1
		frameEnd = 120



		abcShotDir = assetDir+"/"+shotName.replace(shotNumber+"_", "")
		abcAssetDir = abcShotDir+"/abc"
		print assetDir, "assetDir"
		print shotName, "shotName"
		print truncFileName, "truncFileName"
		print abcShotDir, "abcShotDir"
		if not os.path.exists(abcShotDir):
			print "doesn't exist 1"
			print abcShotDir
			os.makedirs(abcShotDir)

		if not os.path.exists(abcAssetDir):
			print "doesn't exist 2"
			print abcAssetDir
			os.makedirs(abcAssetDir)

		print abcAssetDir+"/"+userAssetName+"_v001_"+truncFileName+".abc"

		version = 1

		while True:
			v = abcAssetDir+"/"+userAssetName+ '_v' + str('%03d' % version)+"_"+truncFileName+".abc"
			print v
			if os.path.isfile(v):
				print 'abc exists'
				version +=1
			else:
				print "abc doesn't exist"
				cmds.AbcExport(j = '-fr %s %s -file %s'%(frameStart, frameEnd, v))
				break