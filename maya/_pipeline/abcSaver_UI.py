# -- ABC saver
# -- written by Andy Lewis
# -- 2014
# ----------------imports-----------------

import os
import re
import maya.cmds as cmds
from maya import OpenMayaUI as omui
from shiboken import wrapInstance 
from PySide import QtCore, QtGui, QtUiTools
from PySide.QtCore import *
from PySide.QtGui import *

# ----------Paths--------------
UI_PATH = "S:/outfit/artists/Andy/_maya/_scripts/UI/abcSaver.ui"
# -------------------------------
mayaMainWindowPtr = omui.MQtUtil.mainWindow() 
mayaMainWindow= wrapInstance(long(mayaMainWindowPtr), QWidget) 
# ------------Load UI ------------
def loadUi(path, parent=None):
	loader = QtUiTools.QUiLoader()
	file = QtCore.QFile(path)
	file.open(QtCore.QFile.ReadOnly)
	widget = loader.load(file, parent)
	file.close()
	return widget
# ---------------class-----------------
class abcSaver(QtGui.QMainWindow):	
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        abcSaverUI = UI_PATH
        self.ui = loadUi(abcSaverUI, self)

        #variables
        self.exportString = ""
        self.verboseCheck = 0
        self.fileDir = cmds.file(q= True, loc = True)
        self.assetDir = self.fileDir.split('projects')[0] + 'assets'
        self.shotName = (self.fileDir.split('projects')[1]).split("/")[1]
        self.fileName = (self.fileDir.split('/')[-1]).split(".")[0]
        self.shotNumber = (self.fileDir.split('projects')[0]).split("/")[-2]
        self.truncFileName = self.fileName.replace(self.shotName+"_", '')
        self.abcShotDir = self.assetDir+"/"+self.shotName.replace(self.shotNumber+"_", "")
        self.abcAssetDir = self.abcShotDir+"/abc"
        self.userAssetName = ""
        self.fullFileName = ""

        #initial setup
        self.setInitialFrames()
        self.ui.export_btn.setEnabled(False)

        #connections
        self.ui.export_btn.clicked.connect( self.exportAbc )
        self.ui.assetName_lnEdit.editingFinished.connect( self.getAssetVers )


    def setInitialFrames(self):

        self.ui.startFrame_spin.setValue(int(cmds.playbackOptions(minTime = 1, query = 1)))
        self.ui.endFrame_spin.setValue(int(cmds.playbackOptions(maxTime = 1, query = 1)))


    def getAssetVers(self):
        if re.match('^[a-zA-Z0-9_]+$', self.ui.assetName_lnEdit.text()):
            self.ui.export_btn.setEnabled(True)
            version = 1
            self.userAssetName = self.ui.assetName_lnEdit.text()

            while True:
                self.fullFileName = self.abcAssetDir+"/"+self.userAssetName+ '_v' + str('%03d' % version)+"_"+self.truncFileName+".abc"
                if os.path.isfile(self.fullFileName):
                    version +=1
                else:
                    self.ui.export_btn.setText(self.userAssetName+ '_v' + str('%03d' % version))
                    break
        else:
            self.ui.export_btn.setEnabled(False)
     
    def makeExportString(self):

        self.exportString = "-fr %s %s "%(self.ui.startFrame_spin.value(), self.ui.endFrame_spin.value())

        if self.ui.verb_btn.isChecked():
            self.verboseCheck = 1

        if self.ui.noNorm_btn.isChecked():
            self.exportString += "-noNormals "

        if self.ui.renderable_btn.isChecked():
            self.exportString += "-ro "

        if self.ui.namespace_btn.isChecked():
            self.exportString += "-stripNamespaces "

        if self.ui.uvWrite_btn.isChecked():
            self.exportString += "-uvWrite "

        if self.ui.writeColor_btn.isChecked():
            self.exportString += "-writeColorSets "

        if self.ui.writeFace_btn.isChecked():
            self.exportString += "-writeFaceSets "

        if self.ui.wholeFrame_btn.isChecked():
            self.exportString += "-wholeFrameGeo "

        if self.ui.worldSpace_btn.isChecked():
            self.exportString += "-worldSpace "

        if self.ui.writeViz_btn.isChecked():
            self.exportString += "-writeVisibility "

        if self.ui.euler_btn.isChecked():
            self.exportString += "eulerFilter "

        if self.ui.ogawa_radio.isChecked():
            self.exportString += "-dataFormat ogawa "

        for i in cmds.ls(sl=1):
            self.exportString += "-root %s "%i

        print self.exportString + "-file "+self.fullFileName, "<----EXPORT STRING"


    def createFolders(self, name):

        if not os.path.exists(name):
            os.makedirs(name)


    def exportAbc(self):
        
        sel = cmds.ls(sl=1)

        #safeguards
        if not '/projects/' in self.fileDir:
            cmds.warning("File doesn't appear to be saved in proper location")
        elif self.ui.assetName_lnEdit.text() == "":
            cmds.warning("Please provide an asset name")
        elif (self.ui.startFrame_spin.value() > self.ui.endFrame_spin.value()) or (self.ui.endFrame_spin.value() < self.ui.startFrame_spin.value()):
            cmds.warning("Enter a valid frame range")
        elif len(sel)<=0:
            cmds.warning("Please select object(s) for export")
        elif self.fullFileName == "":
            cmds.warning("Can't find proper place to save")
        else:

            self.createFolders(self.abcShotDir)
            self.createFolders(self.abcAssetDir)

            self.makeExportString()

            cmds.AbcExport(verbose = self.verboseCheck, j = self.exportString + "-file "+ self.fullFileName)

            cmds.deleteUI('mayaAbcSaver')

def launchUIMaya():
    if (cmds.window('mayaAbcSaver', exists=True) == True):
        cmds.deleteUI('mayaAbcSaver')

    if cmds.file(q= True, loc = True) != 'unknown':
        app = abcSaver(parent=mayaMainWindow)

        cmds.showWindow( 'mayaAbcSaver' )
        return app
    else:
        cmds.warning("Please save file in proper location before using this script.")

if __name__ == '__main__':
    launchUIMaya()

