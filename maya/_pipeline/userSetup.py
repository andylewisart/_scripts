import sys
import maya.cmds as cmds
import maya.mel as mel

# -- directories
pythonPath = "S:\outfit\shared\mayaPipeline\scripts\python"
sys.path.append( pythonPath )
cmds.evalDeferred("from mayaSceneTool import *")
cmds.evalDeferred("run()")