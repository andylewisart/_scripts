def fileParser():

    fileDir = cmds.file(q= True, loc = True)
    renderDir = fileDir.split('projects')[0] + 'renders/preview'
    shotName = (fileDir.split('projects')[1]).split("/")[1]
    fileName = (fileDir.split('/')[-1]).split(".")[0]
    mayaFileName = fileName[:-5] + "May" + fileName[-5:]


    shotNumber = (fileDir.split('projects')[0]).split("/")[-2]
    truncFileName = fileName.replace(shotName+"_", '')
    playblastShotDir = renderDir+"/"+shotName.replace(shotNumber+"_", "")
    playblastRenderDir = playblastShotDir+"/" + mayaFileName
    playblastFullName = playblastRenderDir + "/" + mayaFileName #+ ".9999.jpg"
    fullFileName = ""