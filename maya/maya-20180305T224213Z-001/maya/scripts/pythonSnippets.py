import maya.cmds as cmds
import random

for i, d in enumerate(cmds.ls(type = "VRayMesh")):
    if cmds.listConnections('%s.output' %d, d=True ) == None: #checks to see if there is a connection
        print 'beer'
        continue
    print i
    curShape = cmds.listConnections('%s.output' %d, d=True, plugs=True)[0]
    print curShape
    cmds.disconnectAttr("%s.output" %d, curShape)


shape = cmds.ls(sl=True)
random.shuffle(shape)

prox = cmds.ls(sl=True)

def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

splitList = chunks(shape, len(shape)/len(prox))
print splitList


for i, spl in enumerate(splitList):
    for j, d in enumerate(splitList[i]):
        if cmds.listConnections('%s.inMesh' %d, s=True ) == True: #checks to see if there is a connection
            print 'already connected'
            continue
        cmds.connectAttr("%s.output" %prox[i], "%s.inMesh" %d)




import maya.cmds as cmds
import random as rand
import maya.mel as mel

randomize = rand.uniform(0,360)

for i in cmds.ls(sl=True):
    randomizeScale = rand.uniform(.4, .75)
    #cmds.setAttr( str(i)+'.rotateY', rand.uniform(0, 360)  )   
    cmds.setAttr( str(i)+'.scaleX', randomizeScale )
    cmds.setAttr( str(i)+'.scaleY', randomizeScale )
    cmds.setAttr( str(i)+'.scaleZ', randomizeScale )
print randomize


for i in cmds.ls(type = 'VRayMesh'):
    print cmds.nodeType(i)
    cmds.setAttr( str(i)+'.animOffset', rand.uniform(0,85))
    
    

    
    
for i in cmds.ls(sl=True):
    print cmds.polyEvaluate(i, b =  True)
    
itr = 0
for i in cmds.ls(sl=True):
    print itr
    itr+=1
    
list = cmds.ls(sl=True)
rand.shuffle(list)
cmds.select(list[len(list)/2:])

curShape = cmds.ls(sl=True)
mel.eval('vray addAttributesFromGroup ' + curShape+ ' vray_objectID 1')
#mel.eval('vrayAddAttr ' + curShape+ ' vray_objectID')
cmds.setAttr("%s.vrayObjectID" %curShape, 5)
mel.eval("vrayUpdateAEJob")




  vray addAttributesFromGroup $node vray_roundedges 1;
 

  vrayAddAttr $node vrayRoundEdges;
 

  vrayAddAttr $node vrayRoundEdgesRadius;            //  3 lines add Vray arrtibutes
 

  setAttr ( $node + ".vrayRoundEdgesRadius") 0.12;