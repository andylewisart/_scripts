'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re
import time

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm

import pyside_util
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'VRay Attribute Editor'
WINDOW_VERSION = 0.04
WINDOW_NAME = 'vray_attribute_editor'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'vrayAttrEditor.0.04.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
  def __init__( self, parent = pyside_util.get_maya_window(), *args ):
    super( Reference_Tool, self ).__init__( parent )
    self.setupUi( self )

    self.objectID_add_btn.clicked.connect( self.addObjID )
    self.objectID_remove_btn.clicked.connect( self.removeObjID )
    self.selectID_btn.clicked.connect( self.selectByID )

    self.subdiv_add_btn.clicked.connect( self.addSubdiv )
    self.subdiv_remove_btn.clicked.connect( self.removeSubdiv )
    self.selectSubdiv_btn.clicked.connect( self.selectSubdiv )

    self.subdivDisp_add_btn.clicked.connect( self.addSubdivDisp )
    self.subdivDisp_remove_btn.clicked.connect( self.removeSubdivDisp )
    self.selectSubdivDisp_btn.clicked.connect( self.selectSubdivDisp )

    self.dispControl_add_btn.clicked.connect( self.addDispControl )
    self.dispControl_remove_btn.clicked.connect( self.removeDispControl )
    self.selectDispControl_btn.clicked.connect( self.selectDispControl )

    self.rndEdges_add_btn.clicked.connect( self.addRndEdges )
    self.rndEdges_remove_btn.clicked.connect( self.removeRndEdges )
    self.selectRndEdges_btn.clicked.connect( self.selectRndEdges )

    self.show()


  def addObjID(self):
    idNumber = int(self.objID_lnEdit.text())
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      print idNumber, obj
      #mel.eval('vray addAttributesFromGroup ' + obj +' vray_objectID 1')
      #mel.eval("setAttr \"%s.vrayObjectID\" %s;"%(obj, idNumber))
      #cmds.evalDeferred(lambda object=obj,object_id=idNumber: cmds.setAttr(object + '.vrayObjectID', idNumber))
      #mel.eval('vray addAttributesFromGroup {0} vrayObjectID 1;'.format(obj))
      mel.eval('vrayAddAttr {0} vrayObjectID;'.format(obj))
      cmds.setAttr (('{0}.vrayObjectID'.format(obj)), idNumber)
      mel.eval("vrayUpdateAE")
                  

  def removeObjID(self):
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      #mel.eval('vray addAttributesFromGroup ' + obj +' vrayObjectID 0')
      mel.eval('vrayDeleteAttr {0} vrayObjectID;'.format(obj))
      mel.eval("vrayUpdateAE")

  def selectByID(self):
    myObj = cmds.ls (dag=True, type='mesh') 
    selectionList = []
    for i, obj in enumerate(myObj):
      try:
        ID = cmds.getAttr("{0}.vrayObjectID".format(obj))
        if ID == int(self.selectID_lnEdit.text()):
          selectionList.append(obj)
        print ID, selectionList
      except:
          print "{0} doesn't have an obj ID".format(obj)
    if selectionList == []:
      cmds.select(cmds.ls(sl=1), deselect = 1)
    else:
      cmds.select(selectionList)


  def addSubdiv(self):
    renderSubdivSurface = int(self.renderSubdivSurface_chxBox.isChecked())
    subdivUVBorder = int(self.subdivUVBorder_chxBox.isChecked())
    subdivUV = int(self.subdivUV_chxBox.isChecked())
    staticGeo = int(self.staticGeo_chxBox.isChecked())

    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      #mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 1')
      #mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj))
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 1')
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 1')
      #cmds.evalDeferred(lambda object=obj: mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj)))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vraySubdivEnable'.format(Obj)), renderSubdivSurface))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vraySubdivUVsAtBorders'.format(Obj)), subdivUVBorder))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vraySubdivUVs'.format(Obj)), subdivUV))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayStaticSubdiv'.format(Obj)), staticGeo))
      mel.eval("vrayUpdateAE")

  def removeSubdiv(self):
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 0')
      mel.eval('vrayDeleteAttr {0} vray_subdivision;'.format(obj))
      mel.eval("vrayUpdateAE")

  def selectSubdiv(self):
    myObj = cmds.ls (dag=True, type='mesh') 
    selectionList = []
    for i, obj in enumerate(myObj):
      if cmds.attributeQuery("vraySubdivEnable", node = obj, ex = 1) == 1:
        selectionList.append(obj)
    if selectionList == []:
      cmds.select(cmds.ls(sl=1), deselect = 1)
    else:
      cmds.select(selectionList)


  def addSubdivDisp(self):
    overrideGlobal = int(self.overrideGlobalSettings_chxBox.isChecked())
    viewDep = int(self.viewDependent_chxBox.isChecked())
    edgeLen = float(self.edgeLen_lnEdit.text())
    maxSubdiv = int(self.maxSubdiv_lnEdit.text())

    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      #mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 1')
      #mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj))
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subquality 1')
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subquality 1')
      #cmds.evalDeferred(lambda object=obj: mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj)))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayOverrideGlobalSubQual'.format(Obj)), overrideGlobal))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayViewDep'.format(Obj)), viewDep))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayEdgeLength'.format(Obj)), edgeLen))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayMaxSubdivs'.format(Obj)), maxSubdiv))
      mel.eval("vrayUpdateAE")

  def removeSubdivDisp(self):
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_subquality 0')
      mel.eval('vrayDeleteAttr {0} vray_subquality;'.format(obj))
      mel.eval("vrayUpdateAE")

  def selectSubdivDisp(self):
    myObj = cmds.ls (dag=True, type='mesh') 
    selectionList = []
    for i, obj in enumerate(myObj):
      if cmds.attributeQuery("vrayOverrideGlobalSubQual", node = obj, ex = 1) == 1:
        selectionList.append(obj)
    if selectionList == []:
      cmds.select(cmds.ls(sl=1), deselect = 1)
    else:
      cmds.select(selectionList)



  def addDispControl(self):
    noDisp = int(self.noDisp_chxBox.isChecked())
    dispType = self.dispType_comboBox.currentIndex()
    dispAmt = float(self.dispAmt_lnEdit.text())
    dispShift = float(self.dispShift_lnEdit.text())
    keepContinuity = int(self.keepContinuity_chxBox.isChecked())
    waterLevel_chxBox = int(self.waterLevel_chxBox.isChecked())
    waterLevel_lnEdit = float(self.waterLevel_lnEdit.text())
    texRes = float(self.texRes_lnEdit.text())
    precision = float(self.precision_lnEdit.text())
    tightBounds = int(self.tightBounds_chxBox.isChecked())
    filterTex = int(self.filterTex_chxBox.isChecked())
    filterBlur = float(self.filterBlur_lnEdit.text())
    dispBounds = self.dispBounds_comboBox.currentIndex()
    minValue = float(self.minValue_lnEdit.text())
    maxValue = float(self.maxValue_lnEdit.text())

    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):

      mel.eval('vray addAttributesFromGroup ' + obj +' vray_displacement 1')
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_displacement 1')

      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementNone'.format(Obj)), noDisp))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementType'.format(Obj)), dispType))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementAmount'.format(Obj)), dispAmt))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementShift'.format(Obj)), dispShift))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementKeepContinuity'.format(Obj)), keepContinuity))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayEnableWaterLevel'.format(Obj)), waterLevel_chxBox))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayWaterLevel'.format(Obj)), waterLevel_lnEdit))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vray2dDisplacementResolution'.format(Obj)), texRes))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vray2dDisplacementPrecision'.format(Obj)), precision))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vray2dDisplacementTightBounds'.format(Obj)), tightBounds))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vray2dDisplacementFilterTexture'.format(Obj)), filterTex))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vray2dDisplacementFilterBlur'.format(Obj)), filterBlur))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementUseBounds'.format(Obj)), dispBounds))
      #cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementMinValue'.format(Obj)), minValue))
      #cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayDisplacementMaxValue'.format(Obj)), maxValue))

      mel.eval("vrayUpdateAE")

  def removeDispControl(self):
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_displacement 0')
      mel.eval('vrayDeleteAttr {0} vray_displacement;'.format(obj))
      mel.eval("vrayUpdateAE")

  def selectDispControl(self):
    myObj = cmds.ls (dag=True, type='mesh') 
    selectionList = []
    for i, obj in enumerate(myObj):
      if cmds.attributeQuery("vrayDisplacementNone", node = obj, ex = 1) == 1:
        selectionList.append(obj)
    if selectionList == []:
      cmds.select(cmds.ls(sl=1), deselect = 1)
    else:
      cmds.select(selectionList)



  def addRndEdges(self):
    enableRndEdges = int(self.enableRndEdges_chxBox.isChecked())
    rndEdgeRadius = float(self.rndEdgeRadius_lnEdit.text())


    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      #mel.eval('vray addAttributesFromGroup ' + obj +' vray_subdivision 1')
      #mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj))
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_roundedges 1')
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_roundedges 1')
      #cmds.evalDeferred(lambda object=obj: mel.eval('vrayAddAttr {0} vray_subdivision;'.format(obj)))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayRoundEdges'.format(Obj)), enableRndEdges))
      cmds.evalDeferred(lambda Obj=obj: cmds.setAttr (('{0}.vrayRoundEdgesRadius'.format(Obj)), rndEdgeRadius))

      mel.eval("vrayUpdateAE")

  def removeRndEdges(self):
    myObj = cmds.ls (sl=True, dag=True, type='mesh') 
    for i, obj in enumerate(myObj):
      mel.eval('vray addAttributesFromGroup ' + obj +' vray_roundedges 0')
      mel.eval('vrayDeleteAttr {0} vray_roundedges;'.format(obj))
      mel.eval("vrayUpdateAE")

  def selectRndEdges(self):
    myObj = cmds.ls (dag=True, type='mesh') 
    selectionList = []
    for i, obj in enumerate(myObj):
      if cmds.attributeQuery("vrayRoundEdges", node = obj, ex = 1) == 1:
        selectionList.append(obj)
    if selectionList == []:
      cmds.select(cmds.ls(sl=1), deselect = 1)
    else:
      cmds.select(selectionList)



  def testing(self):
    print self.objID_lnEdit.text()



  #self.statusBar().showMessage(sender.text() + ' was pressed')
  #btn = QtGui.QPushButton('Button', self)
          


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'vrayAttrEditor.0.04.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
