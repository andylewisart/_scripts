import pymel.core as pm
import gspread
import re
import maya.mel as mel
import maya.cmds as cmds
import time
from gspread.ns import _ns

class RenderInitializer(object):

    def __init__(self, show, episode, shot, gSpreadObj):

        self.show = show
        self.episode = episode
        self.shot = shot
        self.gSpreadObj = gSpreadObj
        self.settingsDict = None
        self.renderElements = None
        self.sprsht = None

    def getAvailableSpreadsheets(self):
        feed = self.gSpreadObj.get_spreadsheets_feed()
        feedList=[]
        for elem in feed.findall(_ns('entry')):
            elem_title = elem.find(_ns('title')).text
            feedList.append(elem_title)
        return feedList

    def setGSpreadObject(self):
        self.sprsht = self.gSpreadObj.open("Grimm Ep 312")

    def setRenderer(self, renderer):
        if renderer == 'vray':
            lambda x: pm.loadPlugin('vrayformaya.mll')
            # mel.eval("vrayInit;")
            # mel.eval("updateRendererUI;")

        pm.setAttr('defaultRenderGlobals.ren', renderer, type='string')
        lambda x: pm.mel.unifiedRenderGlobalsWindow()

    def loadRenderElements(self):
        #sh = self.gSpreadObj.open("Grimm Ep 312")
        worksheet = self.sprsht.worksheet("data")
        renderElements = worksheet.find("Render Elements")
        theShot = worksheet.find(self.shot)
        pm.importFile(worksheet.cell(theShot.row, renderElements.col).value)

    def setFrameRange(self):
        worksheet = self.sprsht.worksheet("ShotTracker")
        duration = worksheet.find("Duration")
        theShot = worksheet.find(self.shot)

        if self.show == 'Grimm':
        #Add 16 frames of handles for grimm
            try:
                frange = int(worksheet.cell(theShot.row, duration.col).value)+16
                pm.setAttr ("defaultRenderGlobals.endFrame" ,frange)
                pm.playbackOptions(maxTime=frange )
                pm.playbackOptions(aet=frange )
            except:
                pm.warning("Could not retrieve a proper frame count from spreadsheet")

    def setRenderGlobals(self, renderGlobs):
        try:
            mel.eval('source "{0}"'.format(renderGlobs))
        except:
            print "EXCEPT"
            lambda x: mel.eval('source "{0}"'.format(renderGlobs))

    def loadLighting(self):

        worksheet = self.sprsht.worksheet("data")
        lighting = worksheet.find("Lighting")
        theShot = worksheet.find(self.shot)
        print worksheet.cell(theShot.row, lighting.col).value
        pm.importFile(worksheet.cell(theShot.row, lighting.col).value)



gSpreadObj = gspread.login('inhancevfx@gmail.com', 'padthaicgi')

initialize = RenderInitializer('Grimm', '312', 'G312_03_10', gSpreadObj)

print initialize.getAvailableSpreadsheets()

initialize.setGSpreadObject()

initialize.setRenderer('vray')

initialize.loadRenderElements()

initialize.setFrameRange()

initialize.setRenderGlobals('X:/Scripts/maya/presets/init_VRaySettingsNodePreset_lwf_lanc_GI_draft.mel')

initialize.loadLighting()

