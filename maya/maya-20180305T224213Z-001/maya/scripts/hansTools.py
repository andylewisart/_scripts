import maya.cmds as cmds
import os
import maya.mel as mel
from functools import partial
import random as rand
from vray.utils import *
import fnmatch
import pymel.core as pm
import re

def setInhanceProject():
    
    #Sets project based on Inhance stipulations

    fileDir = "".join(cmds.file(q= True, sn= True).rpartition("Maya/")[:2])
    print fileDir
    cmds.workspace(dir=fileDir )
    print cmds.workspace( q=True, directory=True )


def fillFileNamePrefix():

    fileDir = cmds.file(q= True, loc = True)

    scene = fileDir.split("/")[-2]

    images = cmds.file(q= True, loc = True).rpartition("scenes")[0]+ "images/" + scene + "/"

    version = 1

    while True:
        v = images + 'v' + str('%02d' % version)
        if os.path.isdir(v) == True:
            version +=1
        else:
            break
            
    fileNamePrefix = scene+"/"+ 'v' + str('%02d' % version) + "/%l/%s_%l"
             
    cmds.setAttr('vraySettings.fileNamePrefix', fileNamePrefix, type = 'string')

def expandMyWorld():

    for i, d in enumerate(cmds.ls(type = "camera")):
        cmds.setAttr("%s.farClipPlane" %d, 100000)


def follicleScatterUI():
    
    #check to see if window exists
    if cmds.window("follicleScatterUI", exists = True):
        cmds.deleteUI("follicleScatterUI")

    #create window
    window = cmds.window("follicleScatterUI", title = "Follicle Scatter", w = 300, h = 100, mnb = False, mxb = False, sizeable = False)

    #create a main layout
    mainLayout =cmds.columnLayout(w=300, h=300)

    #banner layout
    imagePath = cmds.internalVar(upd=True) + "icons/waaaaaaa.jpg"
    cmds.image(w=300, h=100, image=imagePath)

    scatterObj = []
    terrain = []
    terrainInputField = cmds.textField("TerrainInputField")
    scatterInputField = cmds.textField("ScatterInputField")
    cmds.button(label = "Load Terrain", w=300, h = 50, command=partial(getSel, "TerrainInputField", terrain))
    cmds.button(label = "Load Scatter Object", w=300, h = 50, command=partial(getSel, "ScatterInputField", scatterObj))
    print terrain

    #create the Duplicate object button
    cmds.separator(h=15)
    cmds.button(label = "Scatter to Terrain", w=300, h = 50, c = getReady)

    #show window
    cmds.showWindow(window)

def getSel(TF, terrain, *args):

    if len(cmds.ls(sl = True)) == 0:
        print "check1"
        errorWindow("No object selected")
    elif len(cmds.ls(sl = True)) > 1:
        print "check2"
        errorWindow("Please only select one terrain")
    elif len(cmds.ls(sl = True)) == 1:
        print "check3"
        terrain = cmds.ls(sl = True)[0]
        cmds.textField(TF, edit = True, text = str(terrain))
        return terrain
    else:
        print "check4"
        errorWindow("Unknown error")

def follicleParent():

    follicles=cmds.ls(type = "follicle")
    follicles = [cmds.listRelatives(f, parent=True, type='transform') for f in follicles]

    dups = len(follicles)

    duplicates = dupSpecialIC(dups)

    if not cmds.objExists("follicle_set"):
        cmds.sets(name = "follicle_set")
    if not cmds.objExists("scattered_set"):
        cmds.sets(name = "scattered_set")

    for i in follicles:
        cmds.sets(i, add ="follicle_set")

    for i in duplicates:
        cmds.sets(i, add ="scattered_set")

    print duplicates
    print follicles
    print len(follicles)

    for i, d in enumerate(duplicates):
        cmds.parent(d,follicles[i])
        cmds.setAttr('%s.translateX' % d, 0 )
        cmds.setAttr('%s.translateY' % d, 0 )
        cmds.setAttr('%s.translateZ' % d, 0 )

    randomizeTranslations(duplicates)

def randomizeTranslations(duplicates):
    for i in duplicates:
        randomizeScale = rand.uniform(.25,1)
        cmds.setAttr( str(i)+'.rotateZ', rand.uniform(0,360) )     
        cmds.setAttr( str(i)+'.scaleX', randomizeScale )
        cmds.setAttr( str(i)+'.scaleY', randomizeScale )
        cmds.setAttr( str(i)+'.scaleZ', randomizeScale )



def getReady(*args):
    terrain = cmds.textField("TerrainInputField", q = True, text = True)
    scatterObj = cmds.textField("ScatterInputField", q = True, text = True)
    sourceFollicleConstraintMel(terrain)

def sourceFollicleConstraintMel(terrain):

    print terrain
    cmds.select(terrain)
    mel.eval('source "follicleConstraint.mel"')
    #mel.eval(follicleConstraint())
    follicleParent()


def tf_value(*args):
    value = cmds.textField("TF_num", q = True, tx = True)
    return value

def dupSpecialIC(numCopy):
    #obj = cmds.ls(sl=True)
    duplicates = []
    scatterObj = cmds.textField("ScatterInputField", q = True, tx = True)
    print scatterObj
    for i in range(int(numCopy)):
        duplicates.extend(cmds.duplicate(scatterObj, un = True))
    return duplicates


def errorWindow(msg):
    if cmds.window("dupError", exists = True):
            cmds.deleteUI("dupError")
    window = cmds.window("dupError", title="D'oh!", widthHeight=(200, 55) )
    cmds.columnLayout( adjustableColumn=True )
    cmds.text( label=str(msg), align='center' )
    cmds.button(label = "Hokay", c = ('cmds.deleteUI(\"' + window + '\", window=True)'))
    cmds.showWindow(window)



def renderCheckerUI():
    
    #check to see if window exists
    if cmds.window("renderCheckerUI", exists = True):
        cmds.deleteUI("renderCheckerUI")

    #create window
    window = cmds.window("renderCheckerUI", title = "Render Checker Thinger", w = 300, h = 100, mnb = False, mxb = False, sizeable = False)

    #create a main layout
    mainLayout =cmds.columnLayout(w=300, h=300)

    #banner layout
    imagePath = cmds.internalVar(upd=True) + "icons/goingToTheStore_01.jpg"
    print cmds.internalVar(upd=True)
    cmds.image(w=300, h=100, image=imagePath)

    versUp = cmds.checkBox(label = "versUp") 
    disablePlanes = cmds.checkBox(label = "disablePlanes")


    cmds.separator(h=15)
    cmds.button(label = "Open Deadline Submitter", w=300, h = 50, command = partial (testing, versUp, disablePlanes))

    cmds.showWindow(window)



def testing(versUp, disablePlanes, *args):

    if cmds.checkBox(versUp, q = True, value = True ) == True:
        setInhanceProject()
        fillFileNamePrefix()

    if cmds.checkBox(disablePlanes, q = True, value = True ) == True:
        disableCamPlanes()


    mel.eval('SubmitJobToDeadline')

def disableCamPlanes():
    for i, d in enumerate(cmds.ls(type = "imagePlane")):
        cmds.setAttr("%s.displayMode" %d, 0)

def camTimeline():
    cam = cmds.ls("Camera01")[0]
    lastFrame = cmds.keyframe( cam, attribute='translateX', query=True, keyframeCount=True )

    cmds.setAttr ("defaultRenderGlobals.endFrame" ,lastFrame)
    cmds.playbackOptions(maxTime=lastFrame )
    cmds.playbackOptions(aet=lastFrame )

    cmds.setAttr( '%s.translateX' %cam, lock=True )
    cmds.setAttr( '%s.translateY' %cam, lock=True )
    cmds.setAttr( '%s.translateZ' %cam, lock=True )
    cmds.setAttr( '%s.rotateX' %cam, lock=True )
    cmds.setAttr( '%s.rotateY' %cam, lock=True )
    cmds.setAttr( '%s.rotateZ' %cam, lock=True )
    cmds.setAttr( '%s.scaleX' %cam, lock=True )
    cmds.setAttr( '%s.scaleY' %cam, lock=True )
    cmds.setAttr( '%s.scaleZ' %cam, lock=True )
    cmds.setAttr( '%s.visibility' %cam, lock=True )

def fixDeformShapeNodeShading():
    brokenXForms = pm.ls(sl=True, type="transform")

    for i, d in enumerate(brokenXForms):

        shapes = pm.listRelatives(d, s = True)

        origShape = shapes[0]
        deformShape = shapes[1]
        
        

        origShapeConnections = pm.listConnections(origShape, c= True, d = True, p = True, t = 'shadingEngine')
        
        if origShapeConnections != []:

            print origShapeConnections

            origShapeSourceConnection = list(list(origShapeConnections[0]))[0]
            origShapeDestConnection = list(list(origShapeConnections[0]))[1]

        
            sourceConnectName = origShapeSourceConnection.rpartition(".")[2]

        

            pm.connectAttr(deformShape+"."+sourceConnectName, origShapeDestConnection, f = True)
        else:
             print "Original shape node for %s doesn't have a connection to shading group."%origShape


def lightSelectGenerate(name):
    LS = mel.eval('vrayAddRenderElement LightSelectElement;')
    LS = cmds.rename(LS, "LS_"+name)
    
    cmds.select(name)
    light = cmds.ls(sl=True)[0]
    
    mel.eval('sets -edit -forceElement  %s %s'%(LS, light))
    cmds.setAttr('%s.vray_name_lightselect'%LS, LS, type = 'string')
    
    
def makeLightSelects():  
    #selects lights and creates light select passes 
    sel = cmds.ls(sl=True)
    lights = []
    for i, d in enumerate(sel):
        shape = cmds.nodeType(cmds.listRelatives(d, s = 1))
        if 'Light' in shape or 'vrayLightMesh' in shape:
              lights.append(d)
    
    for i, d in enumerate(lights):
        lightSelectGenerate(d)


def greenScreenViewportBG():
    try:
      colorSwitch
    except NameError:
      colorSwitch = 1

    if colorSwitch == 1:
        cmds.displayRGBColor( 'background', 0, 1, 0 )
        colorSwitch = 2
    else:
        cmds.displayRGBColor( 'background', rs = True )
        colorSwitch = 1


class VersionUpFile(object):

    def __init__(self, versLetter):
        self.versLetter = versLetter

    def versUpFile(self):

        if re.findall(r'[a-z]', self.versLetter):
            #get file path
            fileDir = cmds.file(q= True, loc = True)

            dirname = os.path.dirname(fileDir)

            myFile = os.path.splitext(os.path.basename(fileDir))[0]
            ext = os.path.splitext(os.path.basename(fileDir))[1]
            if ext == ".ma":
                extType = "mayaAscii"
            elif ext == ".mb":
                extType = "mayaBinary"
            else:
                print "file type not recognized"


            myFileSplit = myFile.split("_")

            

            #dissect the part of filename that needs to be versioned up

            try:
                oldLetter = re.findall(r'_%s\d+'%self.versLetter, myFile)[0]
                print oldLetter + ' oldLetter'
                oldNum = oldLetter.rpartition(self.versLetter)[-1]
                newNum = str('%02d' %(int(oldNum) +1))

                newVersion = "_"+oldLetter.rpartition(self.versLetter)[1]+newNum

                newFile = myFile.replace(oldLetter, newVersion)
                
                print newFile + " TRY"

                cmds.file(rename = dirname+"/"+newFile)
                cmds.file(save=True, type=extType )
            except:

                addTag = myFile + "_" + self.versLetter +"01"
                print addTag + ext + " EXCEPT"
                
                cmds.file(rename = dirname+"/"+addTag)
                cmds.file(save=True, type=extType )
        else:
            cmds.warning("To add a tag, insert alphabetical characters to box on left side of plus")


def initVersUpFile(letter):

  vers_A = VersionUpFile(letter)
  vers_A.versUpFile()



def popConstraints():
    #Adds 'rivets' to selected verts

    verts = cmds.ls(sl=True, fl=True)
        
    #workaround for namespace issues in POP attrs
    vertObjAttrName = []
    vertObj = cmds.pickWalk(cmds.ls(verts, o=True), direction = "up")
    for i, d in enumerate(vertObj):
        try:
            vertObjAttrName.append(d.split(':')[1])
        except:
            vertObjAttrName.append(d)
    print vertObjAttrName

    circleList = []
    for i, d in enumerate(verts):
        vertUV = cmds.polyListComponentConversion(verts[i], tuv = True)
        print vertUV

        uvCoords = cmds.polyEditUV(vertUV, query = True)
        print uvCoords
        loc = cmds.spaceLocator()[0]
        circleList.append(loc)
        
        
        cmds.pointOnPolyConstraint(verts[i], loc, mo=False, w = 1)
        pop_constraint = cmds.listRelatives(loc, type = "pointOnPolyConstraint")
        
        print ("%s.%sU0"%(pop_constraint[0], vertObj[0]), uvCoords[0])
        cmds.setAttr("%s.%sU0"%(pop_constraint[0], vertObjAttrName[0]), uvCoords[0])
        cmds.setAttr("%s.%sV0"%(pop_constraint[0], vertObjAttrName[0]), uvCoords[1])
        
        cmds.rename(loc, d+'loc')
        cmds.rename(pop_constraint, d+'popConstraint')
        


def createUtilRenderLayer():
    #Create render layer for 32-bit util passes
    mel.eval('unifiedRenderGlobalsWindow;')
    cmds.createRenderLayer(name = "util")
    try:
        #switch to util render layer
        mel.eval('editRenderLayerGlobals -currentRenderLayer util;')
        
        #disable light select passes
        try:
            for i, d in enumerate(cmds.ls(type='VRayRenderElementSet')):
                mel.eval('listAddedPressed "{0}" "0"'.format(d))
        except:
            pass
        
        #disable anything but util passes
        for i, d in enumerate(cmds.ls(type='VRayRenderElement')):
            type = cmds.getAttr(d+".vrayClassType")
            if type == 'velocityChannel':
                cmds.editRenderLayerAdjustment(d+".vray_filtering_velocity")
                cmds.setAttr(d+".vray_filtering_velocity", 0)    
            elif type == 'zdepthChannel':
                cmds.editRenderLayerAdjustment(d+".vray_filtering_zdepth")
                cmds.setAttr(d+".vray_filtering_zdepth", 0)
            elif re.findall(r'uv', d, re.IGNORECASE) !=[]:
                cmds.editRenderLayerAdjustment(d+".vray_filtering_extratex")
                cmds.setAttr(d+".vray_filtering_extratex", 0)
            else:
                mel.eval('listAddedPressed "{0}" "0"'.format(d))
            #print cmds.getAttr(d+".vrayClassType")
            
        cmds.editRenderLayerAdjustment("vraySettings.giOn")
        cmds.setAttr("vraySettings.giOn", 0)
        
        cmds.editRenderLayerAdjustment("vraySettings.globopt_light_doLights")
        cmds.setAttr("vraySettings.globopt_light_doLights", 0)
        
        cmds.editRenderLayerAdjustment("vraySettings.globopt_light_doDefaultLights")
        cmds.setAttr("vraySettings.globopt_light_doDefaultLights", 0)
        
        cmds.editRenderLayerAdjustment("vraySettings.globopt_light_doShadows")
        cmds.setAttr("vraySettings.globopt_light_doShadows", 0)
        
        cmds.editRenderLayerAdjustment("vraySettings.globopt_mtl_reflectionRefraction")
        cmds.setAttr("vraySettings.globopt_mtl_reflectionRefraction", 0)
        
        cmds.editRenderLayerAdjustment("vraySettings.samplerType")
        cmds.setAttr("vraySettings.samplerType", 0)
        
        cmds.setAttr("vraySettings.fixedSubdivs", 1)
        
        cmds.editRenderLayerAdjustment("vraySettings.imgOpt_exr_bitsPerChannel")
        cmds.setAttr("vraySettings.imgOpt_exr_bitsPerChannel", 32)
        
    except:
        cmds.warning("Could not generate util render layer!")


#select bind joints from skin cluster
def selectBindJnts():
    infs = cmds.skinCluster(q=True, inf=True)
    cmds.select(infs)