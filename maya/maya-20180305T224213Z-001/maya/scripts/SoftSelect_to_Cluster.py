import maya.OpenMaya as om
import maya.cmds as cmds
import string

def softSelectionToCluster():
    cmds.ConvertSelectionToVertices()
    selection = cmds.ls(sl=True)
    if not selection:
        print "## No object selected"
        return None
        
    objType = cmds.nodeType( selection[ 0 ] )
    selType = 0 if objType == 'nurbsCurve' else 1 if objType == 'mesh' else -1
    if selType == -1:
        print "## Invaild selection of type ", objType
        return None

    # Strip selection of any attributes
    sel = string.split(selection[0], '.')

    richSel = om.MRichSelection()
    om.MGlobal.getRichSelection(richSel)

    richSelList = om.MSelectionList()
    richSel.getSelection(richSelList)

    path = om.MDagPath()
    component = om.MObject()
    richSelList.getDagPath(0, path, component)

    componentFn = om.MFnSingleIndexedComponent(component)

    cluster = cmds.cluster( rel=True )
    clusterSet = cmds.listConnections( cluster, type="objectSet" )
    cmds.select (selection[0], r=True)
   
    for i in range(0,componentFn.elementCount()):
        weight = componentFn.weight(i)
        v = componentFn.element(i)
        w = weight.influence()
        pnts = (sel[0]+'.' + ('vtx' if selType else 'cv') + '[%d]') % v
        cmds.sets(pnts, add=clusterSet[0])
        cmds.percent(cluster[0], pnts,  v=w )
      
    cmds.select(cluster)
 