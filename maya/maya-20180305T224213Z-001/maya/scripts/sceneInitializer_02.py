import pymel.core as pm
import gspread
import re
import maya.mel as mel
import maya.cmds as cmds
import time
from gspread.ns import _ns
import gspread
import os
import sys

class RenderInitializer(object):

    def __init__(self, show, gSpreadObj):

        self.show = show
        self.shot = None
        self.gSpreadObj = gSpreadObj
        self.settingsDict = None
        self.renderElements = None
        self.sprsht = None

    def getAvailableSpreadsheets(self):
        feed = self.gSpreadObj.get_spreadsheets_feed()
        feedList=[]
        for elem in feed.findall(_ns('entry')):
            elem_title = elem.find(_ns('title')).text
            feedList.append(elem_title)
        return feedList

    def setGSpreadObject(self, sheet):
        self.sprsht = self.gSpreadObj.open(sheet)

    def getShotNames(self):
        worksheet = self.sprsht.worksheet("ShotTracker")
        shot_re = re.compile(r'Shot')
        cell = worksheet.find(shot_re)
        values_list = worksheet.col_values(cell.col)
        returnList = []
        for i, d in enumerate(values_list):
            if d != None:
                if not 'Shot' in d:
                    returnList.append(d)
        return returnList

    def lockCam(self):
        try:
            cam = cmds.ls("Camera01")[0]

            self.lockAttr(cam, 'translateX')
            self.lockAttr(cam, 'translateY')
            self.lockAttr(cam, 'translateZ')

            self.lockAttr(cam, 'rotateX')
            self.lockAttr(cam, 'rotateY')
            self.lockAttr(cam, 'rotateZ')

            self.lockAttr(cam, 'scaleX')
            self.lockAttr(cam, 'scaleY')
            self.lockAttr(cam, 'scaleZ')
        except:
            pm.warning("Could not find a 'Camera01' in scene")


    def lockAttr(self, cam, attr):
        pm.setAttr( '{0}.{1}'.format(cam, attr), lock=True )

    def setRenderer(self, renderer):
        try:
            mel.eval("deleteUI unifiedRenderGlobalsWindow;")
        except:
            pass
        #pm.mel.unifiedRenderGlobalsWindow()
        if renderer == 'VRay':
            pm.loadPlugin('vrayformaya.mll')
            # mel.eval("vrayInit;")
        

        pm.evalDeferred(lambda:pm.setAttr('defaultRenderGlobals.ren', renderer.lower(), type='string'))
        #mel.eval("updateRendererUI;")
        pm.evalDeferred(lambda:pm.mel.unifiedRenderGlobalsWindow())



    def findRenderDataWorksheet(self, name):
        try:
            worksheet = self.sprsht.worksheet("Render Data")
            return worksheet
        except :
            pm.warning("{0}: Could not find 'Render Data' worksheet in Google Doc".format(name))
            raise Exception("Oh Fuck!")

    def findOnWorksheet(self, worksheet, name, category):
        try:
            findCell = worksheet.find(name)
            return findCell
        except:
            pm.warning("{1}: Could not find '{0}' on worksheet in Google Doc".format(name, category))
            raise Exception("Oh Fuck!")


    def loadRenderElements(self, shot):
        #sh = self.gSpreadObj.open("Grimm Ep 312")
        category = "Import Render Elements"
        try:
            worksheet = self.findRenderDataWorksheet(category)
            #print worksheet, "worksheet"
            #worksheet = self.sprsht.worksheet("Render Data")
            renderElements = self.findOnWorksheet(worksheet, "Render Elements", category)
            #print renderElements, "renderElements"
            # renderElements = worksheet.find("Render Elements")
            # theShot = worksheet.find(shot)
            theShot = self.findOnWorksheet(worksheet, shot, category)
            #print theShot, "theShot"
            pm.importFile(worksheet.cell(theShot.row, renderElements.col).value)
        except Exception, e:
            print "Exception:", e 
            pass

    def setFrameRange(self, show, shot):
        worksheet = self.sprsht.worksheet("ShotTracker")
        duration = worksheet.find("Duration")
        theShot = worksheet.find(shot)

        if show == 'Grimm':
        #Add 16 frames of handles for grimm
            try:
                frange = int(worksheet.cell(theShot.row, duration.col).value)+16
                pm.setAttr ("defaultRenderGlobals.endFrame" ,frange)
                pm.playbackOptions(maxTime=frange )
                pm.playbackOptions(aet=frange )
            except:
                pm.warning("Could not retrieve a proper frame count from spreadsheet")

        else:
            try:
                frange = int(worksheet.cell(theShot.row, duration.col).value)
                pm.setAttr ("defaultRenderGlobals.endFrame" ,frange)
                pm.playbackOptions(maxTime=frange )
                pm.playbackOptions(aet=frange )
            except:
                pm.warning("Could not retrieve a proper frame count from spreadsheet")

    def setRenderGlobals(self, renderGlobs):
        try:
            mel.eval('source "{0}"'.format(renderGlobs))
        except:
            #print "EXCEPT"
            pm.evalDeferred(lambda:mel.eval('source "{0}"'.format(renderGlobs)))

    def loadLighting(self, shot):
        try:
            worksheet = self.sprsht.worksheet("Render Data")
            #print worksheet, "worksheet"
            try:
                lighting = worksheet.find("Lighting")
                try:
                    theShot = worksheet.find(shot)
                    #print worksheet.cell(theShot.row, lighting.col).value
                    pm.importFile(worksheet.cell(theShot.row, lighting.col).value)
                except:
                    pm.warning("Import Lighting: Shot doesn't seem to be specified in Render Data worksheet")
            except:
                pm.warning("Import Lighting: Could not find 'Lighting' column in Render Data worksheet")
        except:
            pm.warning("Import Lighting: Could not find 'Render Data' worksheet in Google Doc")

    def importCam(self, show, epsInput, shot):
        #print epsInput, "epsInput"
        epsNum = re.findall(r'\d+', epsInput)[0]
        #print epsNum, "epsNum"

        thisShow = 'T:/'+show
        
        dirs = os.listdir(thisShow)
        eps = []
        for i, d in enumerate(dirs):
          #print d, 'd'
          if os.path.isdir(thisShow+"/"+ d) and 'EPS' in d:
              #print d, "HIT"
              eps.append(d)
        eps.sort()
        #print eps, "eps"
        for i in eps:
            #print re.findall(r'\d+', i)[0], epsNum
            if re.findall(r'\d+', i)[0] == epsNum:
                shotDir = thisShow + "/"+i+"/Maya/scenes"

        #print shotDir, "shotDir"

        if shotDir == None:
            pm.warning("Couldn't find episode directory that coorelates to Google Doc episode")
        else:
            mayaShotDirs = os.listdir(shotDir)
            #print mayaShotDirs
            for i, d in enumerate(mayaShotDirs):
                shotArray=re.findall(r'\d+', shot) 
                iArray=re.findall(r'\d+', d)
                #print shotArray, iArray, "derp"
                try:
                    #compare numbers of shots to find a match
                    for j, k in enumerate(shotArray):
                        #print int(k), int(iArray[j])
                        if int(k) != int(iArray[j]):
                            break
                        #print "herp derp"
                        #if comparison tests make it to end of array, we have a match:
                        if len(shotArray)==j+1:
                            #print "made it"
                            trackDir = shotDir+"/"+d+"/track"
                            break
                except Exception, e:
                    print e
                    pass

            try:
                latestFile = None
                latestExt = None
                #print trackDir, "trackDir"
                for f in os.listdir(trackDir): 
                    if f.endswith('.ma') or f.endswith('.mb'):
                        theFile = f.split(".")[0]
                        ext = f.split(".")[1]
                        #print theFile[-2:], latestFile, "burrito"

                        if theFile[-2:]>latestFile:
                            #print"chea"
                            latestFile = theFile
                            latestExt = ext
                            #print latestFile, latestExt, "uhhhh"
                pm.importFile(trackDir+"/"+latestFile+"."+latestExt)


            except Exception, e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pm.warning("Couldn't find track camera directory")
                return Exception

    def getImageSequence(self, show, epsInput, shot):

        #print epsInput, "epsInput"
        epsNum = re.findall(r'\d+', epsInput)[0]
        #print epsNum, "epsNum"

        thisShow = 'T:/'+show
        
        dirs = os.listdir(thisShow)
        eps = []
        for i, d in enumerate(dirs):
          #print d, 'd'
          if os.path.isdir(thisShow+"/"+ d) and 'EPS' in d:
              #print d, "HIT"
              eps.append(d)
        eps.sort()
        #print eps, "eps"
        for i in eps:
            #print re.findall(r'\d+', i)[0], epsNum
            if re.findall(r'\d+', i)[0] == epsNum:
                shotDir = thisShow + "/"+i+ "/ImageSequences/Plates/" 

        #print shotDir, "shotDir"

        if shotDir == None:
            pm.warning("Couldn't find episode directory that coorelates to Google Doc episode")
        else:
            mayaShotDirs = os.listdir(shotDir)
            #print mayaShotDirs
            for i, d in enumerate(mayaShotDirs):
                shotArray=re.findall(r'\d+', shot) 
                iArray=re.findall(r'\d+', d)
                #print shotArray, iArray, "derp"
                try:
                    #compare numbers of shots to find a match
                    for j, k in enumerate(shotArray):
                        #print int(k), int(iArray[j])
                        if int(k) != int(iArray[j]):
                            break
                        #print "herp derp"
                        #if comparison tests make it to end of array, we have a match:
                        if len(shotArray)==j+1:
                            #print "made it"
                            try:
                                imgDir = shotDir+"/"+d+'/jpg/'
                                print imgDir
                                return imgDir
                                break
                            except:
                                pm.warning("no jpg directory found")
                except Exception, e:
                    print e
                    pass




    def importImagePlane(self, imgDir):
        print "beer"
        # try:
        pm.select('Camera0Shape1')
        print "selected"

        thePlane = pm.imagePlane(c=pm.ls(sl=1)[0])

        print "thePlane"

        #imgSeqPath = getImageSequence()


        imgs = os.listdir(imgDir)

        for i, d in enumerate(imgs):
            print d, "DEE"
            fileName, fileExt = os.path.splitext(d)
            print fileExt, "fileExt"
            if fileExt == '.jpg':
                jpgFile = d
                break




        pm.setAttr("{0}.imageName".format(thePlane[1]), imgDir + jpgFile, type = 'string')

        pm.setAttr("{0}.useFrameExtension".format(thePlane[1]), 1)

        # except Exception, e:
        #     exc_type, exc_obj, exc_tb = sys.exc_info()
        #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        #     print(exc_type, fname, exc_tb.tb_lineno)
        #     pm.warning("Couldn't load camera image plane. Is there more than one object named 'Camera01'?")




# gSpreadObj = gspread.login('inhancevfx@gmail.com', 'padthaicgi')

# initialize = RenderInitializer('Grimm', gSpreadObj)

# # print initialize.getAvailableSpreadsheets()

# # initialize.setGSpreadObject()

# initialize.setRenderer('VRay')

# initialize.loadRenderElements()

# initialize.setFrameRange()

# initialize.setRenderGlobals('X:/Scripts/maya/presets/init_VRaySettingsNodePreset_lwf_lanc_GI_draft.mel')

# initialize.loadLighting()

