'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re
import time

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm

import pyside_util

import sceneInitializer_02 as scnInit
import gspread
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'Scene Init'
WINDOW_VERSION = 0.01
WINDOW_NAME = 'scene_initializer'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'sceneInitializer.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
  def __init__( self, parent = pyside_util.get_maya_window(), *args ):
    super( Reference_Tool, self ).__init__( parent )

    
    
    self.googSpr = None

    self.setupUi( self )



    self.getShows()
    self.getRenderGlobalPresets()
    self.getPlugins()


    self.show_listWidget.itemClicked.connect(self.getEpisode)

    self.checkAll_checkBox.stateChanged.connect(self.checkAll)

    self.episode_listWidget.itemClicked.connect(self.getShot)

    self.ok_btn.clicked.connect(self.okClicked)
    
    self.cancel_btn.clicked.connect(self.cancel)

    self.show()

    

    '''close on exit.... yaaaaaay!'''
    self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
    

  def cancel(self):
    self.close()

  def getPlugins(self):
    pm.loadPlugin('vrayformaya.mll')

  def okClicked(self):

      if self.shot_listWidget.selectedItems()!= []:

        
        self.googSpr.setRenderer(self.renderer_comboBox.currentText())
        if self.importLighting_checkBox.isChecked(): 
          self.googSpr.loadLighting(self.shot_listWidget.currentItem().text())

        if self.importGlobals_checkBox.isChecked(): 
          self.googSpr.setRenderGlobals('X:/Scripts/maya/presets/' +self.renderGlobals_comboBox.currentText())

        if self.importFrange_checkBox.isChecked():
          self.googSpr.setFrameRange(self.show_listWidget.currentItem().text(), self.shot_listWidget.currentItem().text())

        if self.importRE_checkBox.isChecked():
          self.googSpr.loadRenderElements(self.shot_listWidget.currentItem().text())

        if self.importCam_checkBox.isChecked():
          try:
            self.googSpr.importCam(self.show_listWidget.currentItem().text(), self.episode_listWidget.currentItem().text(), self.shot_listWidget.currentItem().text())
            imgSeq = self.googSpr.getImageSequence(self.show_listWidget.currentItem().text(), self.episode_listWidget.currentItem().text(), self.shot_listWidget.currentItem().text())
            print imgSeq, "imgSeq"
            if imgSeq != None:
              self.googSpr.importImagePlane(imgSeq)
            else:
              pm.warning("Could not locate plate directory for selected shot")
          except: 
            pass

        if self.lockCam_checkBox.isChecked():
          self.googSpr.lockCam()



      else:
        pm.warning("Please select a shot")


  def checkAll(self):

    if self.checkAll_checkBox.isChecked():
      self.importLighting_checkBox.setChecked(1)
      self.importGlobals_checkBox.setChecked(1)
      self.importFrange_checkBox.setChecked(1)
      self.importRE_checkBox.setChecked(1)
      self.lockCam_checkBox.setChecked(1)
      self.importCam_checkBox.setChecked(1)

  def getShows(self):


    dirs = os.listdir('T:')

    for i, d in enumerate(dirs):
        if os.path.isdir('T:'+ d) and d[0] !='.':
            self.show_listWidget.addItem(d)

  def getRenderGlobalPresets(self):

    presetPath = "X:\Scripts\maya\presets"

    for i, d in enumerate(os.listdir(presetPath)):
      if 'init_' in d:
        self.renderGlobals_comboBox.addItem(d)

  def getEpisode(self):
    # thisShow = 'T:/'+self.show_listWidget.currentItem().text()
    # print thisShow, "thisShow"
    # dirs = os.listdir(thisShow)
    # eps = []
    # for i, d in enumerate(dirs):
    #   print d, 'd'
    #   if os.path.isdir(thisShow+"/"+ d) and 'EPS' in d:
    #       print d, "HIT"
    #       eps.append(d)

    # eps.sort()
    # for i in eps:

    #   self.episode_listWidget.addItem(i)

    self.episode_listWidget.clear()
    self.shot_listWidget.clear()

    gSpreadObj = gspread.login('inhancevfx@gmail.com', 'padthaicgi')
    show = self.show_listWidget.currentItem().text()
    self.googSpr = scnInit.RenderInitializer(show, gSpreadObj)


    availSprSheets = self.googSpr.getAvailableSpreadsheets()

    for i, d in enumerate(availSprSheets):
      if show in d:
        self.episode_listWidget.addItem(d)


  def getShot(self):
    self.shot_listWidget.clear()

    thisEps = self.episode_listWidget.currentItem().text()
    self.googSpr.setGSpreadObject(thisEps)

    shotNames = self.googSpr.getShotNames()

    for i, d in enumerate(shotNames):
      self.shot_listWidget.addItem(d)




  def testing(self):
    pass


          


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'sceneInitializer.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
