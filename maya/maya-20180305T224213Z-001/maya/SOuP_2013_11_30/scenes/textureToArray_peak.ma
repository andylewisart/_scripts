//Maya ASCII 2011 scene
//Name: textureToArray_peak.ma
//Last modified: Sun, Mar 14, 2010 11:42:20 PM
//Codeset: UTF-8
requires maya "2011";
requires "stereoCamera" "10.0";
requires "SOuP" "1.22";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2011";
fileInfo "version" "2011 Hotfix 2 x64";
fileInfo "cutIdentifier" "201006030209-775257";
fileInfo "osv" "Linux 2.6.30.5-43.fc11.x86_64 #1 SMP Thu Aug 27 21:39:52 EDT 2009 x86_64";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.1803376386509841 0.42469967609765702 -0.27801773575074629 ;
	setAttr ".r" -type "double3" -23.738352729612675 -104.1999999999982 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1.1932637289906998;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pPlane1";
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyPlane -n "polyPlane1";
	setAttr ".sw" 50;
	setAttr ".sh" 50;
	setAttr ".cuv" 2;
createNode ramp -n "ramp1";
	setAttr -s 2 ".cel";
	setAttr ".cel[0].ep" 0;
	setAttr ".cel[0].ec" -type "float3" 1 1 1 ;
	setAttr ".cel[2].ep" 1;
	setAttr ".cel[2].ec" -type "float3" 0 0 0 ;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n"
		+ "            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n"
		+ "                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n"
		+ "                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n"
		+ "            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n"
		+ "                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n"
		+ "                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 0\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n"
		+ "                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"integer\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"integer\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n"
		+ "                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 0.935\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 5\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n"
		+ "                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 0.935\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 5\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n"
		+ "                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n"
		+ "                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n"
		+ "                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -showShapes 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 0\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -showShapes 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 0\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Hypershade\")) \n\t\t\t\t\t\"scriptedPanel\"\n\t\t\t\t\t\"$panelName = `scriptedPanel -unParent  -type \\\"hyperShadePanel\\\" -l (localizedPanelLabel(\\\"Hypershade\\\")) -mbv $menusOkayInPanels `\"\n\t\t\t\t\t\"scriptedPanel -edit -l (localizedPanelLabel(\\\"Hypershade\\\")) -mbv $menusOkayInPanels  $panelName\"\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Script Editor\")) \n\t\t\t\t\t\"scriptedPanel\"\n\t\t\t\t\t\"$panelName = `scriptedPanel -unParent  -type \\\"scriptEditorPanel\\\" -l (localizedPanelLabel(\\\"Script Editor\\\")) -mbv $menusOkayInPanels `\"\n\t\t\t\t\t\"scriptedPanel -edit -l (localizedPanelLabel(\\\"Script Editor\\\")) -mbv $menusOkayInPanels  $panelName\"\n\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\n"
		+ "viewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 100 ";
	setAttr ".st" 6;
createNode ocean -n "ocean1";
	setAttr ".wh[0]"  0 0.16 1;
	setAttr ".wtb[0]"  0 1 1;
	setAttr -s 2 ".wp[0:1]"  0 0.30000001 1 1 0.5 1;
createNode place2dTexture -n "place2dTexture1";
createNode peak -n "peak1";
	setAttr -s 2601 ".wl[0].w";
	setAttr ".wl[0].w[0:499]"  -0.018086791 -0.019100599 -0.01647608 -0.01232041 -0.0090532517 
		-0.0082834922 -0.0082817608 -0.0082268631 -0.0064843376 -0.0042553819 -0.0037182269 
		-0.0016103347 0.0024435651 0.0034423717 0.0036910474 0.0052413689 0.0052618547 0.0054678544 
		0.0062430645 0.0066218483 0.0072193299 0.00479401 0.0020018364 0.002883123 0.0038166447 
		0.00057062547 -0.00074885099 0.00052901846 -0.00095881557 -0.0027545048 -0.0025876088 
		-0.0025682694 -0.0043592169 -0.0058840988 -0.0062123127 -0.006964053 -0.0087028546 
		-0.011361536 -0.013360056 -0.013057791 -0.011295334 -0.0096874908 -0.0074720131 -0.0048332065 
		-0.0032909291 -0.0021695504 -0.0006222293 0.001763067 0.005571425 0.0085303709 0.0089747924 
		-0.017335003 -0.016979285 -0.012283145 -0.0073579848 -0.0083022276 -0.0092677921 
		-0.0089342957 -0.0080524059 -0.0055269939 -0.0026754327 -0.0015013212 0.0012480178 
		0.0062926495 0.0074422355 0.0072080623 0.0073272162 0.0057300623 0.0058786152 0.0066277008 
		0.0059215985 0.0056540202 0.0051838127 0.0031869204 0.0014532355 -0.0011398516 -0.0031833854 
		-0.00281418 -0.0022333916 -0.0041077104 -0.0045025595 -0.0035512897 -0.00438768 -0.0050983392 
		-0.0039967899 -0.005574578 -0.0090238964 -0.011200513 -0.012365198 -0.012374911 -0.010554206 
		-0.0080631189 -0.0068641626 -0.005344185 -0.003341781 -0.0028660698 -0.0027911151 
		-0.0012845347 0.0016058149 0.0041130097 0.0051762993 0.0079505686 -0.014060651 -0.012045024 
		-0.0060836384 -0.0049956008 -0.008272185 -0.0083018001 -0.0076551377 -0.0076582604 
		-0.0057579731 -0.0016957716 0.0021104664 0.0063710427 0.010540875 0.0089604966 0.0084950803 
		0.0057898588 0.0037002168 0.0056161466 0.00726591 0.0041119587 0.0025813975 0.0030679181 
		0.0014698871 -0.0014101361 -0.003850108 -0.004683061 -0.0039792787 -0.0051677185 
		-0.0072441055 -0.0066397968 -0.0048393356 -0.0059299613 -0.0061970712 -0.0047145253 
		-0.0069067958 -0.010177433 -0.011453747 -0.011555105 -0.010739055 -0.0079176724 -0.0047875014 
		-0.0041953945 -0.0034315605 -0.0020946783 -0.0024058279 -0.002748555 -0.0013380693 
		0.00091658498 0.0028925959 0.0051910835 0.0060136006 -0.0099207982 -0.0060435394 
		-0.00074361474 -0.0046833511 -0.0068211714 -0.0057675526 -0.004814927 -0.0060735177 
		-0.004604253 -0.00017459149 0.0053300597 0.011341398 0.010905169 0.0079114828 0.0079467874 
		0.0040894025 0.001824807 0.0034987461 0.0035057548 0.0010454722 0.00066213531 0.00031804061 
		-0.0024166403 -0.0039475253 -0.004391396 -0.004756439 -0.0056341505 -0.0079490375 
		-0.0096497852 -0.0086241765 -0.006451318 -0.0064522922 -0.0061798231 -0.005365557 
		-0.0082648918 -0.010296269 -0.0096182767 -0.0090386728 -0.0091085145 -0.0071503213 
		-0.0041278601 -0.0023575434 -0.00084558997 3.0507916e-05 -0.00073281548 -0.00093519455 
		-0.00031875324 -0.00035560993 0.0019356527 0.0057339207 0.0041650403 -0.0072387606 
		-0.001678579 0.00089273811 -0.0019873818 -0.0029244944 -0.0024879966 -0.0044472069 
		-0.0048194109 -0.0013122967 0.0034225709 0.0078233071 0.012279443 0.0088256858 0.0064531686 
		0.0050326874 0.0018471064 0.00074812002 0.0016292174 -0.00036636501 -0.001797513 
		-0.00080379471 -0.0025502711 -0.0060315263 -0.0065377876 -0.0052929004 -0.0062893047 
		-0.0079110311 -0.0090658152 -0.009797086 -0.0089260321 -0.0070330449 -0.0065427022 
		-0.0056412914 -0.0049412232 -0.0077484166 -0.0093600573 -0.0081642531 -0.0074269366 
		-0.0074138683 -0.005597482 -0.0026752467 4.9430237e-05 0.0021648835 0.0010761762 
		-0.00034375448 0.00062544394 0.0018413414 0.0010891618 0.0029814711 0.0046674255 
		0.0027445178 -0.0047005094 0.00054438459 0.00054921873 0.00066177594 0.001183097 
		-0.0016889586 -0.0049834666 -0.0034500069 0.0033654268 0.0085480437 0.01039122 0.01038831 
		0.0067134271 0.0041555143 0.0016834587 -0.00017093343 0.00078429497 0.0013392887 
		-0.0021555203 -0.0036676303 -0.003522946 -0.0067723431 -0.0093029533 -0.0082122488 
		-0.0065086158 -0.0084019573 -0.0089740343 -0.0080746468 -0.0081357313 -0.0076749744 
		-0.0063240384 -0.0061569135 -0.005856141 -0.0044696536 -0.0051516029 -0.0065032705 
		-0.0064818482 -0.0061233155 -0.0045822416 -0.0018293659 0.00042562955 0.0019873953 
		0.0027016173 0.00022204092 -0.00072634825 0.0018365472 0.0042586219 0.0040624612 
		0.0045390842 0.0029607615 0.0013411574 -0.001782238 0.0009697082 0.00039855239 0.00059997337 
		0.00085679942 -0.0008855979 -0.0020818929 0.001884613 0.0087610744 0.010459091 0.010498231 
		0.0081863813 0.0048304503 0.0027218438 0.00034397826 -0.00075991568 0.00081203965 
		0.00084204361 -0.0027680099 -0.0044466937 -0.0060326667 -0.01026406 -0.011677278 
		-0.0088858632 -0.0073207561 -0.0093620084 -0.0087104132 -0.0060683079 -0.0067755436 
		-0.0072686514 -0.005499091 -0.004670253 -0.0048506442 -0.0033437954 -0.0018069702 
		-0.0020751883 -0.0030713687 -0.0031120928 -0.00067836826 0.0024114074 0.002190744 
		0.0019046108 0.0019991028 0.00059586205 0.0011768305 0.0025299564 0.0029199589 0.0039031229 
		0.0048776227 0.0026937556 0.001089275 0.00026533997 0.0010350801 0.0016725917 0.00085913739 
		-0.00051452231 0.00047919882 0.0041868459 0.0086447112 0.009903877 0.011189632 0.010296846 
		0.0057003526 0.0020416738 0.00061732356 -0.0002703736 -0.00030797487 0.0010502654 
		3.9988867e-05 -0.0029990482 -0.004040427 -0.0064876438 -0.011220304 -0.012378025 
		-0.0095506636 -0.0088428278 -0.0099124741 -0.0078579541 -0.0044283923 -0.0062382864 
		-0.0067717135 -0.0043812902 -0.0022250747 -0.0014778813 -0.00015575932 0.00088892906 
		0.0012983721 0.0016794692 0.0019238335 0.0038058017 0.0050657177 0.0032635969 0.0019506416 
		0.0010798245 0.00040579971 0.00051458395 0.00062617764 0.00094902702 0.0027175366 
		0.0040993369 0.0019246329 7.4258074e-05 0.0016832793 0.0013384686 0.0032005385 0.0018378677 
		0.00030042802 0.0033173112 0.0076986784 0.0088076126 0.011542046 0.012263338 0.007411805 
		0.0023212549 -0.00072281715 -0.0013458901 -0.0010556307 6.7723195e-05 0.0020420635 
		0.00084618165 -0.0022974673 -0.0035169437 -0.006171565 -0.010542432 -0.011992233 
		-0.010710772 -0.010531901 -0.0099244984 -0.0064843921 -0.0038985044 -0.0048474018 
		-0.0039729122 -0.0019899334 -0.0014558994 0.00056073326 0.0037252824 0.0031101045 
		0.0032111923 0.0063578766 0.0078238612 0.0077373316 0.0076644141 0.0051371576 0.0018681204 
		-0.00051829102 -0.0017888179 -0.0019933237 -0.0013381302 7.5639648e-05 0.0019511658 
		0.00091345661 -0.0022975688 -0.0036901711 0.0034188449 0.0026615143 0.0039636977 
		0.0029535275 0.0043534799 0.0073190872 0.0074994941 0.0093398588 0.011132356 0.00757587 
		0.0036915764 0.00027550675 -0.0015611976 -0.0014947862 -0.0011955552 -0.0007583329 
		0.0010525929 0.0023410302 -0.00061682583 -0.0035482096 -0.0070275492 -0.010078214 
		-0.011069176 -0.010755636 -0.010584492 -0.0084139071 -0.0043789269 -0.0042473534 
		-0.0045746476 -0.0017373774 -0.00022268901 -0.0016395921 -3.9992388e-05 0.0045839651 
		0.0056808749 0.0067231101 0.011314942 0.011307885 0.0098273503 0.0095746769 0.0054493505;
	setAttr ".wl[0].w[500:999]"  0.0021035594 -8.6178828e-05 -0.0021334898 -0.0032821824 
		-0.003223869 -0.0025576805 -0.0018496071 -0.002753682 -0.0054693264 -0.0064177867 
		0.0059332163 0.0065262718 0.0064823674 0.0053764149 0.0057667978 0.0059547457 0.0067754248 
		0.008771277 0.0066607636 0.0033620936 0.0018455172 -2.32741e-05 -0.0020558015 -0.002459192 
		-0.0020786794 -0.0023506305 -0.0014196212 0.00086196803 0.0008100167 -0.0030202102 
		-0.007308091 -0.0097942725 -0.0097921444 -0.009506125 -0.0094395233 -0.0069207726 
		-0.0030625474 -0.004117738 -0.0047208052 -0.0019441652 9.4461837e-05 -0.00050489249 
		0.0013183844 0.0055687907 0.0073288386 0.0099483598 0.012989841 0.010227793 0.011415177 
		0.0093829995 0.0050085257 0.0030377046 0.0010565131 -0.001781235 -0.0032920642 -0.0041202665 
		-0.0054520927 -0.006168996 -0.0058088261 -0.0065836576 -0.0070650117 0.0092652189 
		0.010391015 0.0078526316 0.0062627443 0.0057634558 0.0046840054 0.004057094 0.0041429787 
		0.0022932037 0.00056127587 0.00087869621 -0.00029259175 -0.0038783252 -0.0053378046 
		-0.0041985945 -0.0027196417 -0.0015547797 -9.6108968e-05 -0.0001434788 -0.0026841694 
		-0.006837137 -0.0093044704 -0.009010152 -0.0084367609 -0.0084109949 -0.0061441427 
		-0.0023720907 -0.0024630427 -0.0028706552 -0.0014349676 -0.00077475433 -0.0003009924 
		0.0028779313 0.0071216077 0.0082771238 0.010337227 0.011087575 0.010325167 0.011305489 
		0.0072374237 0.0050458573 0.0044855606 0.001158106 -0.0021302353 -0.0027637684 -0.0036957313 
		-0.0068989154 -0.0088038137 -0.0083449986 -0.0077589899 -0.0078919474 0.0084251519 
		0.0079083117 0.0062819486 0.0065877149 0.0062278765 0.0020114623 -0.001099236 -0.0014831147 
		-0.0013084146 -0.0015116053 -0.00085715024 -0.0017336298 -0.0045357598 -0.0061055524 
		-0.0051020971 -0.0025410904 -0.00025241356 -8.0214639e-05 -0.0024081594 -0.0044917963 
		-0.0070286719 -0.0088887569 -0.0083244499 -0.0070240479 -0.006291966 -0.0040580095 
		-0.00091794028 -0.0011714633 -0.0016423222 -0.0010384894 -0.0012344312 -0.00045408949 
		0.0033757943 0.0082234927 0.010423379 0.012313877 0.0096761724 0.0079729306 0.0069915764 
		0.004384974 0.0052442071 0.0070402981 0.0023426781 -0.001829434 -0.0026292668 -0.0041063223 
		-0.0078630513 -0.009883225 -0.0097871255 -0.0095057208 -0.0092173694 0.0062222853 
		0.0078674844 0.0058010421 0.0041924072 0.0017871484 -0.0026854256 -0.0054862988 -0.005091269 
		-0.0035071578 -0.0028068016 -0.0029103509 -0.003868971 -0.0043596285 -0.0042129494 
		-0.0040161433 -0.0025451565 -0.00032912137 -0.00054323033 -0.0034250859 -0.0052295742 
		-0.0067233779 -0.0079318061 -0.0071687377 -0.0053200582 -0.0033646708 -0.00070296315 
		0.00068079168 -0.0014830314 -0.0025483957 -0.0020225151 -0.0011750755 0.0012826669 
		0.0060128495 0.0097355759 0.011355393 0.012887776 0.008056663 0.0052874046 0.0056887362 
		0.0043459935 0.005340517 0.0071744649 0.0035299796 -0.00067188067 -0.0029674585 -0.0062982375 
		-0.0092718378 -0.010080446 -0.010024451 -0.010397493 -0.0095147369 0.0048031989 0.0065373946 
		0.0036080657 0.00056388945 -0.0028408228 -0.0062397216 -0.0074391328 -0.0064417603 
		-0.005035962 -0.0047794124 -0.0046504689 -0.0053090607 -0.0048850318 -0.002753424 
		-0.0014670127 -0.0017920097 -0.00096255611 -0.00045436964 -0.0022039483 -0.0036213906 
		-0.0052109635 -0.0068080584 -0.0062297652 -0.0040240549 -0.0010371885 0.002196616 
		0.0017992355 -0.0008713943 -0.0022336885 -0.0029838048 -0.0030481252 0.00036602293 
		0.006612563 0.010730613 0.012126439 0.012408234 0.0068195197 0.0039656037 0.0051596193 
		0.0049246554 0.0049510314 0.0046322923 0.0018983358 -0.00022978423 -0.0029996459 
		-0.0075608864 -0.010358139 -0.010290268 -0.010127446 -0.010921368 -0.0094927773 0.0038639237 
		0.002419563 0.00046568591 -0.0016248234 -0.0051562553 -0.0078598345 -0.008085927 
		-0.0072101867 -0.0082892422 -0.0081399027 -0.0059941965 -0.004852578 -0.0044172904 
		-0.002407192 -0.00035987992 -0.00021919911 0.00058026728 0.0016585407 0.00014434237 
		-0.0021241275 -0.0044275373 -0.0057126763 -0.0043888665 -0.0013450996 0.0023300692 
		0.004063298 0.00080586009 -0.0019251904 -0.0028607999 -0.0043808357 -0.004785608 
		-0.0012260359 0.0051064803 0.010534968 0.012020109 0.0091165621 0.0050551994 0.0034624408 
		0.0034026273 0.0034886815 0.0050840108 0.0045527341 0.00039159274 -0.0019493254 -0.0044431426 
		-0.0090636145 -0.011522567 -0.010823496 -0.010376456 -0.011055394 -0.0093293479 0.0027223954 
		-0.00069636584 -0.0021080137 -0.0033563541 -0.0067916452 -0.0091467528 -0.0090478174 
		-0.0091025736 -0.010562549 -0.0098649478 -0.0060604964 -0.0035702125 -0.0035856739 
		-0.0024508636 -0.00094665872 0.00047898921 0.0035405201 0.0054202075 0.0015927698 
		-0.0016967016 -0.0030949824 -0.0029784073 -0.00079507369 0.0026734434 0.0056418884 
		0.0028519563 -0.0016815693 -0.0042344802 -0.0055261892 -0.0060789497 -0.0050269673 
		-0.0013391855 0.0037343316 0.0074283057 0.0075050336 0.0043310439 0.0022802975 0.003255859 
		0.0029458469 0.0025252656 0.0055783391 0.00465413 -0.0011334083 -0.0045193816 -0.0075704553 
		-0.011663175 -0.012879467 -0.010656368 -0.0092336964 -0.0097346706 -0.0083751688 
		0.00045956677 -0.0024008541 -0.0037386932 -0.0056156632 -0.0090369806 -0.010926231 
		-0.010454809 -0.0098890886 -0.010147574 -0.0082330797 -0.0046499185 -0.0056154113 
		-0.0061975527 -0.0041724993 -0.0014004193 0.00090705662 0.0048612091 0.0069745169 
		0.0031050839 0.00070123799 0.00024585248 0.00064207264 0.0021870208 0.0039355513 
		0.0044539291 0.0026950887 -0.0012450343 -0.0051262048 -0.0081361821 -0.0085515827 
		-0.0058886018 -0.0018798733 0.0018023184 0.0036713406 0.0032012067 0.00064937596 
		-0.00094656652 0.00071933714 0.0023952224 0.0032052528 0.0062350575 0.0023502982 
		-0.0031958073 -0.0060699475 -0.0091363881 -0.012894397 -0.012824905 -0.0092416257 
		-0.0072617345 -0.008262407 -0.0079313694 -0.0022141689 -0.0036780753 -0.0050982544 
		-0.007779784 -0.01066334 -0.011872353 -0.010853369 -0.0094966916 -0.008395208 -0.0059901495 
		-0.0058989367 -0.0081236232 -0.0085120518 -0.0056430707 -0.0013633124 0.0023436048 
		0.0054218103 0.0054129488 0.0047199712 0.0044928705 0.0026011518 0.0021104678 0.0040837689 
		0.0046096896 0.0029593394 0.0016859841 -0.0016088712 -0.0065407194 -0.0096548554 
		-0.0092472378 -0.0061472626 -0.0023479927 0.00015585741 0.00020993873 -0.0006238317 
		-0.0026353891 -0.0044084932 -0.0030372441 0.00016328954 0.0038252089 0.007065346 
		0.00084053387 -0.0041349893 -0.0062946999 -0.0090922546 -0.012251114 -0.011205872 
		-0.0073544048 -0.0062798336 -0.0074114399 -0.0071272664 -0.0030374771 -0.0049307658 
		-0.0078975037 -0.010296186 -0.011647258 -0.011716653 -0.010387538 -0.0089461375 -0.0078518474 
		-0.0072144796 -0.0074739498 -0.0078804856 -0.0070834928 -0.0047216709 -0.0021287217 
		0.0007863756 0.003698844 0.0042474307 0.0058494192 0.006361586 0.0026565786 0.0029485675 
		0.0062664365 0.005138665 0.0018323681 -0.00073820935 -0.0046574967 -0.0080692265 
		-0.0087914318 -0.0070907529 -0.0045091761;
	setAttr ".wl[0].w[1000:1499]"  -0.0022139526 -0.0015839974 -0.0029677025 -0.0043423134 
		-0.0062845689 -0.0075667226 -0.0051431605 -0.00081507361 0.0036561859 0.0069991224 
		0.0018159149 -0.0030245783 -0.0061010155 -0.0098646116 -0.01123714 -0.0088678217 
		-0.0053606099 -0.0048498586 -0.0045338795 -0.0047700861 -0.0033849319 -0.0072950283 
		-0.011006326 -0.012381969 -0.011891502 -0.011008188 -0.010116026 -0.0089682462 -0.0078668222 
		-0.0077787265 -0.0077879666 -0.0065850941 -0.0044100536 -0.0034532468 -0.0034926778 
		-0.0018649454 0.0022182376 0.0055027376 0.007420511 0.0061328872 0.0038423813 0.004978775 
		0.0056839916 0.0029404913 0.0010553455 -0.0018809349 -0.0060813325 -0.0079566874 
		-0.0066397437 -0.004387815 -0.0034285393 -0.0024504738 -0.002830514 -0.0047172355 
		-0.0066397823 -0.0089238845 -0.0095693273 -0.0058601513 -0.00035710563 0.0033147726 
		0.0048183752 0.0029676794 -0.00098737772 -0.0058895801 -0.0098988842 -0.0093476521 
		-0.0055740331 -0.0028746934 -0.0024933896 -0.00026421878 -0.0037172257 -0.0061504571 
		-0.0091279643 -0.011734528 -0.011789119 -0.010281125 -0.0097640194 -0.010515313 -0.010275079 
		-0.0084072836 -0.0066384068 -0.006594657 -0.0062105702 -0.0045523332 -0.0028597836 
		-0.0024139471 -0.00059225946 0.0037407447 0.0073056128 0.0061820173 0.0054115057 
		0.0068061869 0.0074424916 0.0037778893 0.00120979 0.00063713407 -0.0020050078 -0.0055190986 
		-0.0062961187 -0.0046494124 -0.0042098332 -0.0039751213 -0.0021527905 -0.0022443903 
		-0.0049930969 -0.0076873675 -0.0098445611 -0.0099694626 -0.0065119392 -0.0016208291 
		0.0021252923 0.0044366596 0.0033092801 4.9795199e-05 -0.0043689776 -0.0073309867 
		-0.0057911095 -0.001854212 -0.00091779727 -0.0008137261 0.0004495451 -0.0040262346 
		-0.0081051234 -0.0093006222 -0.0094322944 -0.0083922837 -0.0077776602 -0.009285518 
		-0.010703469 -0.010957064 -0.0093681999 -0.0063636983 -0.0047439947 -0.0050211647 
		-0.0039302288 -0.001368784 0.00070210826 0.0027646387 0.0055289119 0.0061048698 0.0054901829 
		0.0075352625 0.0097225457 0.0071218661 0.0034931009 0.0018598246 0.00060580432 -0.0030204346 
		-0.0052437684 -0.0045615523 -0.0032881172 -0.0042762258 -0.0038571537 -0.00065437809 
		-0.002444908 -0.0067049107 -0.00919188 -0.0096390033 -0.0085195266 -0.0060033049 
		-0.0032546162 0.00020459597 0.004964381 0.003664054 -0.00056279346 -0.0027142754 
		-0.0034364434 -0.0022260849 -0.00045146767 0.00060671882 0.001028431 -0.0010497497 
		-0.0049966718 -0.0078424644 -0.0077718617 -0.0056057922 -0.0046528056 -0.0073633497 
		-0.0089945728 -0.008912053 -0.0091648372 -0.0089186458 -0.007105324 -0.0047295322 
		-0.0030309081 -0.0011545536 0.0013185311 0.0025116773 0.0038035999 0.0052269422 0.0056364518 
		0.0077457526 0.01115673 0.0079492759 0.0047187423 0.0040056459 0.0037966259 0.0011214593 
		-0.002930739 -0.0045555448 -0.0032872027 -0.0027371156 -0.0040075351 -0.0033970356 
		-0.0024104652 -0.0055465847 -0.0085872337 -0.0097420951 -0.0086443229 -0.0062231016 
		-0.0044271904 -0.003465299 -0.00097303872 0.0031166587 0.001897016 -0.0014577867 
		-0.0014825545 2.3348024e-05 -0.00038042729 0.00029588304 0.0029373025 0.0025510839 
		-0.0022061537 -0.0058321282 -0.0061918162 -0.0052619139 -0.0019393421 -0.0024658672 
		-0.006222141 -0.006753426 -0.0057682577 -0.006610814 -0.0079634264 -0.0074747847 
		-0.0053129322 -0.0021266737 0.0013697487 0.0028277915 0.0030528589 0.0043551214 0.0050566792 
		0.0056631672 0.008429247 0.0081813997 0.0050757527 0.0052747801 0.0057810517 0.0047640977 
		0.0021594441 -0.00058956101 -0.0019235269 -0.0022744595 -0.003782541 -0.0046555656 
		-0.0047483994 -0.0065096552 -0.0082358606 -0.0083797015 -0.0078394674 -0.0072986139 
		-0.0066026333 -0.0049336012 -0.0031917819 -0.0012064138 0.00025529694 -0.00062407262 
		-0.0012073606 0.00074044877 0.0028520022 0.001417506 0.0024120631 0.0049788011 0.0015714299 
		-0.002494806 -0.0049740132 -0.0044088853 -0.0022249189 0.0012566218 0.0003059124 
		-0.00211533 -0.0027475005 -0.0044355337 -0.0069111139 -0.0073895338 -0.0059920554 
		-0.0033740837 -0.00011486615 0.0017556881 0.0010469353 0.0018515254 0.00347886 0.0034156644 
		0.0042228759 0.0065464005 0.004816751 0.0032691113 0.0064974292 0.0078479471 0.0038232442 
		0.001801218 0.0022463906 0.001439209 -0.0018287485 -0.0043109893 -0.0051458539 -0.0067668371 
		-0.0096664093 -0.010290344 -0.0079839174 -0.0060141813 -0.007547135 -0.0084338551 
		-0.0063090096 -0.0031124072 -0.001495637 -0.0016447254 -0.0016689328 -0.00012077167 
		0.0027300585 0.0031148798 0.0029692398 0.0057755332 0.0056115817 0.00097161438 -0.0013588853 
		-0.0024216732 -0.0023626722 0.0011410288 0.0029886579 0.0019294617 0.0022188413 1.9149738e-05 
		-0.0045534917 -0.0067604473 -0.0054120845 -0.0024520031 0.0001833908 0.0014736725 
		0.00031942403 -0.00062270631 0.0003690233 0.0013038093 0.00099882914 0.0019188123 
		0.0037635334 0.0032761237 0.0027389631 0.0046538222 0.0046406356 0.0029464285 0.0021133358 
		0.0046882429 0.0037917374 -0.00060546747 -0.0034109035 -0.0054587331 -0.0083572222 
		-0.010686056 -0.010659548 -0.0086172745 -0.0074301367 -0.0079202093 -0.0074787391 
		-0.0051464182 -0.0034973528 -0.003811741 -0.0034804156 -0.0017335509 0.00088857394 
		0.0024585496 0.0023363163 0.0043533705 0.0093204705 0.0071645309 0.0033154488 0.0020999326 
		-0.00089559786 -0.00066110899 0.0032001042 0.0025617285 0.00199062 0.003519957 0.00057410967 
		-0.0030318948 -0.0032067341 -0.00091932528 3.1293195e-05 0.0003696013 0.00088892609 
		0.00027753902 -0.0004469548 -0.0009764061 -0.0014656822 -0.0012127652 -0.00016651378 
		0.00081557082 0.0016079799 0.0026467168 0.002302449 0.00083972851 0.0021099371 0.0054661124 
		0.0082297148 0.0045508435 0.00092174893 -0.002354075 -0.0063420506 -0.0086606815 
		-0.0090182303 -0.0087479595 -0.0086641889 -0.008413028 -0.0075231926 -0.0056969714 
		-0.0045662192 -0.005230207 -0.0056237862 -0.0042631226 -0.0012282815 0.0016186585 
		0.0023480412 0.0033528893 0.0070950817 0.011440837 0.0088317553 0.0079445932 0.0065203654 
		0.00040649832 0.00032007357 0.0032920856 0.0023325286 0.0028785344 0.0039030295 0.0003403991 
		-0.0008176195 0.0016881815 0.0025908002 0.00067412423 0.00073262362 0.0017290812 
		0.00074072485 -0.0016555384 -0.0037779326 -0.0042168568 -0.0025420147 -0.00063592219 
		-0.00049918832 -0.00020776625 0.001242812 0.0011739626 -5.9449114e-05 0.0019157892 
		0.0083049536 0.0082287099 0.0040008686 0.002662526 -0.00099512539 -0.0054902164 -0.0067725359 
		-0.0057918276 -0.0062921899 -0.0072465893 -0.0070328428 -0.0062620779 -0.0064526149 
		-0.0067965775 -0.0062009012 -0.0048225177 -0.0023224289 0.0004268419 0.0015143461 
		0.002417773 0.0045845774 0.0080885692 0.010536875 0.0098976158 0.012047208 0.0099974368 
		0.0043306756 0.0020053922 0.0029352189 0.0020553395 0.0032446396 0.0038088204 0.0010485068 
		0.0013934327 0.0040951991 0.0038827129 0.0035345973 0.0040201219 0.0017441029 -0.0013679073 
		-0.0036428117 -0.0052714315 -0.0051758601 -0.0027459622 8.0860453e-05 -0.00026369898 
		-0.0011195273 -0.00061767799;
	setAttr ".wl[0].w[1500:1999]"  0.00032947201 0.0013002551 0.0038860715 0.0070768921 
		0.0053190654 0.0045998567 0.0052675493 0.0011774132 -0.0025685315 -0.0032932982 -0.0034933523 
		-0.0049160016 -0.004979488 -0.0040797144 -0.0055137868 -0.0074473191 -0.0080084763 
		-0.0064327838 -0.0030805722 0.00026103575 0.00075263076 0.00077014696 0.0026597525 
		0.0050650206 0.0062442878 0.0085048154 0.01273597 0.014691483 0.010658505 0.0087815486 
		0.0025995334 0.0024599922 0.0021749632 0.0031890352 0.0037780304 0.0037827026 0.0053886548 
		0.0050269798 0.0044135097 0.0064548468 0.0047485335 -0.00021650572 -0.0028859333 
		-0.0036075164 -0.0042319736 -0.0043545924 -0.0032712771 -0.0017332782 -0.0010247689 
		-0.0010325268 -0.0004912382 0.00066434068 0.002355366 0.0041839331 0.0046969801 0.0044135498 
		0.0057603708 0.0045258482 0.00099187542 -0.0004918612 -1.6383667e-05 -0.00076188124 
		-0.0021894076 -0.0020793963 -0.003828147 -0.0064425468 -0.0075237039 -0.0072764591 
		-0.0057827821 -0.0030659176 -0.00014949078 0.00057505211 0.0012469704 0.0036975336 
		0.0056841443 0.0056203688 0.0071145715 0.01127483 0.012335853 0.0096903192 0.0089498311 
		0.0020945764 0.0026023698 0.0040892106 0.005808033 0.0053935079 0.0061920122 0.0083692428 
		0.0065383418 0.0056550605 0.0064982725 0.0030545087 -0.0011901641 -0.0027914713 -0.002857055 
		-0.0037388271 -0.0048660534 -0.0049899397 -0.0042746421 -0.0027397219 -0.00037757002 
		0.0015285626 0.0022727912 0.0022652117 0.0023399852 0.0029952079 0.0042313347 0.0038678949 
		0.0014528405 0.00020558666 0.0011568464 0.0027417622 0.0027317472 0.0018111338 -0.0016255637 
		-0.0064147636 -0.0086396998 -0.0076960684 -0.0059700399 -0.0051012593 -0.0037220344 
		-0.0007524559 0.001712408 0.0023876999 0.0030168046 0.0038826445 0.005300764 0.0072667501 
		0.0088862609 0.009353851 0.0089527415 0.0065097464 0.0024034597 0.0034880398 0.0054354076 
		0.0090770833 0.0088964794 0.0076731555 0.0092278793 0.0080734668 0.0064329728 0.0048346282 
		0.001400075 -0.0014325982 -0.0026775706 -0.0042005284 -0.0062300162 -0.0068189641 
		-0.0058719623 -0.0046923952 -0.0031492126 -0.00095865858 0.00014749495 0.00046268036 
		0.0010849058 0.0019140879 0.0023893442 0.0022609502 0.0013359793 0.00033746113 0.0017117519 
		0.0040968154 0.0038648746 0.0016144465 0.00037242577 -0.0023190714 -0.0066480432 
		-0.0085390769 -0.0073149474 -0.0056116641 -0.0053794505 -0.0040840898 -0.00075961417 
		0.0019347062 0.0019104743 0.0016322103 0.0024730882 0.0049214438 0.0079563316 0.0085177757 
		0.0074195466 0.0047464557 0.0021760911 0.00081987854 0.0038136269 0.0059158783 0.009453156 
		0.011922859 0.011736489 0.010118619 0.0062937746 0.0041344119 0.0024572669 -0.00010431002 
		-0.0016546188 -0.0026377877 -0.0054295342 -0.007863313 -0.0078957556 -0.0066656936 
		-0.006114813 -0.0051139854 -0.003731787 -0.0037709381 -0.0032136894 6.0189024e-05 
		0.002660308 0.00090193137 -0.0004111462 0.00049982569 0.0012034863 0.0023777862 0.0044565639 
		0.0035926481 0.00026474835 -0.0014545871 -0.002500976 -0.0047051082 -0.006422014 
		-0.0067070448 -0.0066007818 -0.0058633387 -0.0036135647 -0.00069775415 -2.7310409e-05 
		0.00055196055 0.0020009726 0.0033306044 0.0050260308 0.0069775167 0.007185563 0.0054579419 
		0.0028069415 0.00058501266 -0.0016215757 0.0032182592 0.0071876394 0.010253377 0.011882994 
		0.011795379 0.0078194365 0.004048028 0.0027148449 0.0013181444 -0.0010163195 -0.0020028159 
		-0.0026681991 -0.0057061748 -0.0078580789 -0.0077470993 -0.0082239211 -0.0095599592 
		-0.0087708486 -0.0062251636 -0.0053635417 -0.0042944211 -0.0014031162 -0.00088653876 
		-0.0023994748 -0.0018419991 0.0012234652 0.0031446018 0.0028288474 0.00344264 0.0034209841 
		0.0012805508 -0.00094887021 -0.0025543459 -0.0042963349 -0.0056917164 -0.0064483373 
		-0.0061189067 -0.004313645 -0.0014881297 -0.0011203913 -0.0023791688 -0.0013551032 
		0.0014594531 0.0032543815 0.0047002709 0.0065568942 0.0053186771 0.0034093992 0.0027927237 
		0.0012789557 -0.002263752 0.0016968057 0.0052047586 0.0086365044 0.010993169 0.0091478275 
		0.0050445711 0.0031623107 0.0030495082 0.00078462972 -0.0018983674 -0.0023905551 
		-0.0029274886 -0.0059343642 -0.0073835757 -0.0069553466 -0.0088988636 -0.011131352 
		-0.010772547 -0.0080789356 -0.006307344 -0.0052878936 -0.0047071334 -0.0051611024 
		-0.004003915 -0.0008013238 0.0026886151 0.0036242916 0.0046301093 0.0040717633 0.0024425436 
		0.0016562098 0.00021857978 -0.0024701902 -0.0044403467 -0.0048526209 -0.004174042 
		-0.0028903682 -0.00084410055 -0.00025648752 -0.0024867402 -0.003669899 -0.0020211772 
		0.00027921028 0.0010004477 0.0034342527 0.006450193 0.0033183754 0.0012669987 0.0014538171 
		0.00061346026 -0.0019964362 0.00069904374 0.0033467007 0.00706399 0.008865783 0.0065695671 
		0.0034038089 0.0020671831 0.0017517739 -0.00068065396 -0.0025302691 -0.0018228926 
		-0.0017373565 -0.0049629048 -0.0065116524 -0.0063898056 -0.0083374986 -0.010506058 
		-0.010331923 -0.0090230061 -0.0084010717 -0.0073709744 -0.0071538948 -0.0067809364 
		-0.0034782575 0.0015351643 0.0019227456 0.0020040567 0.0054934761 0.0058043096 0.0021967329 
		0.0010839384 0.0003801632 -0.0015902143 -0.0026217594 -0.0014860841 0.00038645885 
		0.0011974841 0.00048238365 -0.0014382063 -0.0032675676 -0.0031098644 -0.0011879128 
		-0.00088511786 -0.0010020933 0.0010768746 0.0018675249 0.00037322447 -0.00034804459 
		-0.00050977978 -0.0011326708 -0.0010703731 0.0016085737 0.0040451745 0.0063523729 
		0.0041566547 0.0029186662 0.0028257561 0.0012707944 -5.5015495e-05 -0.00095895067 
		-0.0012915671 1.2793811e-05 0.00028462417 -0.0027485627 -0.0050105993 -0.006707821 
		-0.0085063986 -0.0088796215 -0.0082546426 -0.008646301 -0.0092440518 -0.0081380429 
		-0.0074627772 -0.006571813 -0.0032609885 -6.41878e-05 0.00029860402 0.0018590689 
		0.0066038384 0.0054613221 0.0029121723 0.001684245 0.00054323813 -0.00080983888 0.00020888477 
		0.0032747511 0.0048454693 0.0026937351 0.0006893191 -0.0009126151 -0.0022469144 -0.0023173022 
		-0.0028407006 -0.0027790773 -0.0017165173 -0.001055124 -0.003028098 -0.0034794405 
		-0.00237391 -0.0031405964 -0.0035967426 0.00077049958 0.0023715247 0.0042899409 0.0042878198 
		0.00087928842 0.00071347761 0.0027147662 0.00090155739 -0.00063048303 0.00038584261 
		0.0014703514 0.001082293 -0.00087829347 -0.0021615745 -0.0032789926 -0.0059961663 
		-0.0080378717 -0.0073160408 -0.0057025868 -0.0073511899 -0.0084188282 -0.0072785961 
		-0.0065999851 -0.0063416525 -0.0046869628 -0.0027068127 -0.00048967858 0.0039270688 
		0.0064456714 0.0027297914 0.00098617061 0.0018564777 0.0012898464 0.00033981132 0.0025802602 
		0.0064408635 0.0063582007 0.0040504108 0.0030376192 0.0013013961 -0.0016080167 -0.0039871931 
		-0.0050954018 -0.0039441148 -0.0015395764 -0.0031925836 -0.0066208718 -0.0070696082 
		-0.0058434214 -0.0066919941 -0.0064660087 0.002680796 0.0015869782 0.0021107676 0.0021602374 
		0.00026884169 0.00066111155 0.0017336977 -0.00019887538 -0.00065966602 0.0014904625 
		0.0027907453;
	setAttr ".wl[0].w[2000:2499]"  0.00072549947 -0.0020529097 -0.0029928586 -0.0028813253 
		-0.0042939438 -0.0060553066 -0.0057514706 -0.004867563 -0.0063754725 -0.0068437001 
		-0.0056048525 -0.0055458089 -0.006060468 -0.0051398911 -0.003193805 -0.00027276651 
		0.0029363653 0.0023414823 0.00043257663 0.00010090531 0.0012571972 0.0015879183 0.0025132624 
		0.0054507107 0.0068985932 0.0060489709 0.0057999962 0.0034033107 -6.4352782e-05 -0.0020294103 
		-0.0036170359 -0.0044391337 -0.0035862059 -0.0031181816 -0.0061782966 -0.0090198908 
		-0.0093719214 -0.0088177817 -0.0092602642 -0.0080889547 0.0037965309 0.00097283366 
		-0.00011287653 -0.00042701318 -0.0004009578 0.00025826757 -0.00055766362 -0.0011888548 
		0.00059010624 0.0026076753 0.0016594739 0.00079889322 -0.00087366183 -0.0031125078 
		-0.0046223518 -0.0053882198 -0.005847658 -0.0052717514 -0.0044240053 -0.0047635846 
		-0.0044343658 -0.0039107255 -0.0050897226 -0.0057606222 -0.0046089129 -0.0024363324 
		-0.00065298617 -0.0004784312 -0.0013362525 -0.00051636668 0.0018031661 0.0024896872 
		0.0014060584 0.0035409876 0.0070297555 0.0057586883 0.0059729395 0.0060572829 0.00084989844 
		-0.0024591617 -0.0021051413 -0.0011412875 -0.002059926 -0.0034534526 -0.0058230432 
		-0.0083883656 -0.0096995551 -0.0098560769 -0.010217389 -0.010192724 -0.0084064975 
		0.001390587 -0.00056444493 -0.0019343373 -0.0026910405 -0.0023471152 -0.001545073 
		-0.0019751599 -0.0009748154 0.0026280009 0.0032549344 -6.8970141e-05 5.2858057e-05 
		-0.00027073571 -0.0037097598 -0.0059739123 -0.0064175539 -0.006342811 -0.0051559391 
		-0.0030726444 -0.0022093514 -0.002507231 -0.0035927107 -0.0050785337 -0.0052822139 
		-0.0039005829 -0.0025763651 -0.0020496801 -0.0017910943 -0.0018265964 -0.0003161972 
		0.0026967635 0.0030214819 0.0016831512 0.003566592 0.0068982467 0.0057851095 0.0059949146 
		0.0038515083 -0.00039527635 -0.0021107476 -0.0015920476 -0.0016565282 -0.0021073092 
		-0.0035046083 -0.0061295908 -0.0078433473 -0.0084431199 -0.0096442988 -0.011265424 
		-0.010823487 -0.0086459657 -0.0022615099 -0.0031632828 -0.0034586894 -0.0040478567 
		-0.0038481238 -0.0027154789 -0.001970462 -0.00039679077 0.002030001 0.0011973709 
		-0.00042042101 -0.00025888919 -0.0012154932 -0.0044851098 -0.0053988323 -0.0049013048 
		-0.0053387191 -0.0044904309 -0.0022183754 -0.0017495729 -0.0025195449 -0.0031913125 
		-0.003777626 -0.0034563055 -0.0029415658 -0.0037580417 -0.0037901497 -0.0016478405 
		-2.3711822e-05 0.00025030712 0.00092063425 0.002273788 0.0035635568 0.0059810295 
		0.0081087379 0.0070251245 0.0041290121 0.00051656587 -0.00056943699 -8.8855159e-06 
		-0.0012507392 -0.002952795 -0.0029702995 -0.0029209778 -0.0044628237 -0.0056717582 
		-0.0072539002 -0.01031198 -0.011888303 -0.010712423 -0.0083004059 -0.0040631928 -0.004973982 
		-0.0052027977 -0.0056144097 -0.005178682 -0.0032902088 -0.0015931689 -0.00091197749 
		-0.00081050454 -0.0010151989 -0.00046477717 0.00094990199 -0.00084872643 -0.0039705285 
		-0.0037009486 -0.0024259824 -0.0033824118 -0.002800253 -0.00145524 -0.0019332318 
		-0.0017442614 -0.0014264676 -0.001833878 -0.0016470311 -0.0022986399 -0.0042561125 
		-0.0043821158 -0.001640449 0.0010266072 0.00066841021 0.0010515515 0.0035321135 0.0066407695 
		0.0083501833 0.0074498765 0.005918669 0.0036902314 0.00094873458 0.00056955731 0.00043880788 
		-0.0015936171 -0.0026421461 -0.0021710068 -0.0027460447 -0.0045489701 -0.0056041786 
		-0.0076202899 -0.01018906 -0.01065179 -0.0086824708 -0.0068099885 -0.0045357142 -0.0064236661 
		-0.0066401898 -0.006572512 -0.0055870572 -0.0031918574 -0.0017374859 -0.0024585077 
		-0.0023327973 -0.0014913587 -0.00081198616 -0.00044638134 -0.00051362341 -0.001439078 
		-0.0010147765 -0.00095625606 -0.00094913132 0.00059199077 0.00046872368 -0.00081923453 
		0.00045962783 0.00039111346 -0.0021670046 -0.0022271923 -0.0013495125 -0.0025200902 
		-0.002532911 -0.00071915507 0.00021578054 0.00068864308 0.0029270358 0.0057513872 
		0.0062365262 0.0065473155 0.0057509583 0.0050849877 0.0056709214 0.004327944 0.0016416202 
		-0.00081960089 -0.002458734 -0.0023258731 -0.0015425142 -0.0029085013 -0.0051219608 
		-0.0065664323 -0.0079228058 -0.0089188851 -0.008373295 -0.0070634931 -0.0066420846 
		-0.0060650865 -0.0071435692 -0.0058466168 -0.0050814254 -0.0041949851 -0.0025597252 
		-0.0029130375 -0.0042359396 -0.0035577272 -0.0020568939 -0.0024381878 -0.0030253315 
		-0.0018209901 0.0011856883 0.0032800187 0.0021793793 0.0026412008 0.0049914359 0.0024163609 
		0.00088542467 0.0025543235 0.0010621126 -0.0020965887 -0.001754083 0.00023867696 
		0.00026607973 0.00010813435 0.00019820762 -0.00027829362 0.00072172121 0.0035704826 
		0.0033022428 0.0025983448 0.0041559655 0.0042754863 0.0042710975 0.0055053304 0.0031970569 
		0.00084154611 0.00018690963 -0.00085126492 -0.0018322028 -0.0026945362 -0.0033749265 
		-0.0048005129 -0.0066637849 -0.0080672679 -0.0083234953 -0.0080998633 -0.0086371452 
		-0.0079215355 -0.0064204531 -0.0059153056 -0.0030042613 -0.0025052442 -0.0032451348 
		-0.0030758772 -0.0037844805 -0.00474479 -0.0045769922 -0.0047167111 -0.0047256327 
		-0.0032717714 -0.00084254006 0.0025733919 0.0063868151 0.0071372893 0.0072098915 
		0.0072704023 0.0039661173 0.002933024 0.0035829078 0.0016850036 0.00044279522 0.0015416573 
		0.0013872683 -0.00027939229 2.3430941e-05 0.00069909671 0.00074987969 0.0017152273 
		0.0018629475 -0.00061870838 -0.00096268475 0.00068982597 0.00026394374 0.0011216139 
		0.0036153744 0.0018465433 0.00074966485 0.0025528679 0.001356724 -0.0022021958 -0.0040551466 
		-0.0041788318 -0.0044496721 -0.006196409 -0.0078065335 -0.0081162006 -0.0082470849 
		-0.0090242699 -0.0078542307 -0.0042203558 -0.0033151696 -0.00090912427 -0.0026510281 
		-0.0040203668 -0.0033089006 -0.0030805706 -0.0036111705 -0.0049791923 -0.0072707231 
		-0.0071402816 -0.0032474082 0.0011851792 0.0027162188 0.005421672 0.0097874328 0.010032874 
		0.0080462992 0.0065938774 0.0057337494 0.0033970289 0.0016876375 0.003062868 0.004450805 
		0.00097740465 -0.00074849871 0.00084828469 0.00155171 -7.6769327e-05 -0.00023857914 
		-0.0011278827 -0.0035128659 -0.0036640617 -0.0026728148 -0.0032894618 -0.0021413737 
		0.00067637255 0.0016228409 0.0019817424 0.0013406425 -0.0016252662 -0.0030275234 
		-0.0035004152 -0.0034047121 -0.0035572711 -0.0048117922 -0.0065622139 -0.0076645548 
		-0.0077256467 -0.0072191204 -0.0053928387 -0.0021239731 -0.0012014885 -0.0017900057 
		-0.00412283 -0.0033836481 -0.0019230711 -0.0032294246 -0.0047960868 -0.0057758992 
		-0.0076516978 -0.0073490059 -0.0035696758 0.00024018972 0.0021888327 0.005809213 
		0.010524478 0.0093542002 0.0078355698 0.0074917329 0.0056045158 0.0033099966 0.0029200632 
		0.0047525768 0.0037893988 0.0011539271 0.0018058342 0.0032123707 -0.00038581397 -0.002965326 
		-0.0023581502 -0.0026833173 -0.0048897043 -0.0055190763 -0.0051964815 -0.0052480707 
		-0.0040571732 -0.0023175788 -0.00084653986 0.001405912 -0.00089423684 -0.0043245568 
		-0.0032953918 -0.0011436068 -0.00074371626 -0.0020043955 -0.0026652948 -0.0043755891 
		-0.0066968733 -0.006842101 -0.0045429775 -0.0015775731 -0.0017454394;
	setAttr ".wl[0].w[2500:2600]"  -0.00067308696 -0.0038560396 -0.0053751459 -0.0028516336 
		-0.0020092425 -0.0060249548 -0.0079926057 -0.0066266125 -0.0055616484 -0.0047972561 
		-0.0027029205 -0.00064763799 0.0022006296 0.0068069245 0.0079198815 0.0062167179 
		0.0062723537 0.0050553665 0.0035324458 0.0047064908 0.0064179832 0.0060061179 0.0029293923 
		0.0015066612 0.0022847063 0.001004768 -0.0022924375 -0.00329462 -0.0021161844 -0.0031979606 
		-0.0059711281 -0.0071311789 -0.0073339376 -0.0066789947 -0.0046096407 -0.0033636647 
		-0.0029390815 -0.0014679399 -0.0019023974 -0.0037538609 -0.0028548283 0.0010936169 
		0.00097318058 -0.0011220982 -0.00063078362 -0.0012667341 -0.0039649978 -0.0041767508 
		-0.0012938956 0.0020245435 -0.0018440804 -0.0021878243 -0.0059231063 -0.0070021031 
		-0.004349662 -0.0043487931 -0.0077245133 -0.0081947083 -0.0053099981 -0.0031727303 
		-0.003308703 -0.002090968 0.00026929093 0.0034835087 0.006054813 0.0045449771 0.0039967229 
		0.0049813376 0.0030804456 0.003743923 0.0066695232 0.0064800256 0.0051592682 0.0041019167 
		0.0026575644 0.00033440074 -0.0022403519 -0.0031330213 -0.0021100251 -0.0016647979 
		-0.0048495261 -0.0067962883 -0.0070005618 -0.007673596 -0.0075055151 -0.0047477032 
		-0.0027807197 -0.0038116397 -0.0038919209 -0.0033497163 -0.003117997 -0.0015194854 
		0.0012875973 0.0004286957 -0.00041696336 0.0020311268 0.0026198323 0.0006796759 0.00089125661 
		0.0030575478 0.0045364499;
	setAttr ".d" 1;
createNode tweak -n "tweak1";
createNode objectSet -n "peak1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "peak1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "peak1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode tweak -n "tweak2";
createNode objectSet -n "tweak2Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "tweak2GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "tweak2GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode textureToArray -n "textureToArray1";
	setAttr ".odt" 5;
createNode arrayToMulti -n "arrayToMulti1";
createNode expression -n "expression1";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0]=.I[0]*.025";
createNode timeToUnitConversion -n "timeToUnitConversion1";
	setAttr ".cf" 0.004;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -k on ".nds";
	setAttr -k on ".mwc";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -k on ".nds";
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
	setAttr -k on ".mbf";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "tweak2.og[0]" "pPlaneShape1.i";
connectAttr "peak1GroupId.id" "pPlaneShape1.iog.og[0].gid";
connectAttr "peak1Set.mwc" "pPlaneShape1.iog.og[0].gco";
connectAttr "groupId2.id" "pPlaneShape1.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "pPlaneShape1.iog.og[1].gco";
connectAttr "tweak2GroupId.id" "pPlaneShape1.iog.og[2].gid";
connectAttr "tweak2Set.mwc" "pPlaneShape1.iog.og[2].gco";
connectAttr "tweak2.vl[0].vt[0]" "pPlaneShape1.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "place2dTexture1.o" "ocean1.uv";
connectAttr "place2dTexture1.ofs" "ocean1.fs";
connectAttr "expression1.out[0]" "ocean1.ti";
connectAttr "peak1GroupParts.og" "peak1.ip[0].ig";
connectAttr "peak1GroupId.id" "peak1.ip[0].gi";
connectAttr "arrayToMulti1.owl[0]" "peak1.wl[0]";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "peak1GroupId.msg" "peak1Set.gn" -na;
connectAttr "pPlaneShape1.iog.og[0]" "peak1Set.dsm" -na;
connectAttr "peak1.msg" "peak1Set.ub[0]";
connectAttr "tweak1.og[0]" "peak1GroupParts.ig";
connectAttr "peak1GroupId.id" "peak1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "pPlaneShape1.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "polyPlane1.out" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "tweak2GroupParts.og" "tweak2.ip[0].ig";
connectAttr "tweak2GroupId.id" "tweak2.ip[0].gi";
connectAttr "tweak2GroupId.msg" "tweak2Set.gn" -na;
connectAttr "pPlaneShape1.iog.og[2]" "tweak2Set.dsm" -na;
connectAttr "tweak2.msg" "tweak2Set.ub[0]";
connectAttr "peak1.og[0]" "tweak2GroupParts.ig";
connectAttr "tweak2GroupId.id" "tweak2GroupParts.gi";
connectAttr "polyPlane1.out" "textureToArray1.ig";
connectAttr "ocean1.oc" "textureToArray1.iclr";
connectAttr "ocean1.oa" "textureToArray1.ia";
connectAttr "textureToArray1.orgbapp" "arrayToMulti1.iwpp";
connectAttr "timeToUnitConversion1.o" "expression1.in[0]";
connectAttr ":time1.o" "expression1.tim";
connectAttr ":time1.o" "timeToUnitConversion1.i";
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ramp1.msg" ":defaultTextureList1.tx" -na;
connectAttr "ocean1.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
// End of textureToArray_peak.ma
