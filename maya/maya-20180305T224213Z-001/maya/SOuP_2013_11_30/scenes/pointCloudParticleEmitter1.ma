//Maya ASCII 2013 scene
//Name: brickWallDestroy3.ma
//Last modified: Thu, May 10, 2012 09:05:04 PM
//Codeset: UTF-8
requires maya "2013";
requires "SOuP" "1.28 (May  2 2012)";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220220-825135";
fileInfo "osv" "Mac OS X 10.6.7";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 35.736545694381192 13.637602948864188 50.47561046039263 ;
	setAttr ".r" -type "double3" -3.3383527296136073 28.199999999993022 1.1277878642463172e-16 ;
	setAttr ".rp" -type "double3" -7.1054273576010019e-15 0 7.1054273576010019e-15 ;
	setAttr ".rpt" -type "double3" 9.4231683473420152e-15 -6.6492329815021044e-15 -8.7348006634616634e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".fcp" 1000;
	setAttr ".coi" 54.885956623816497;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 12.585820906908344 11.419877515339524 -0.17849254608154297 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fcp" 1000;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fcp" 1000;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fcp" 1000;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "frontWall";
createNode mesh -n "frontWallShape" -p "frontWall";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "frontWallShape1Orig" -p "frontWall";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "backWall";
createNode mesh -n "backWallShape" -p "backWall";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "backWallShape1Orig" -p "backWall";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "SOUP_AUX_NODES____________________1";
createNode transform -n "boundingObject1";
	setAttr ".v" no;
createNode boundingObject -n "boundingObjectShape1r" -p "boundingObject1";
	setAttr -k off ".v";
	setAttr ".tp" 3;
	setAttr ".dpcbv" no;
	setAttr -s 2 ".pgtf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".wgtf[0:1]"  0 1 1 1 0 1;
	setAttr ".clrf[0].clrfcv" -type "float3" 1 1 1 ;
	setAttr ".af[0]"  0 1 1;
	setAttr -s 2 ".rf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".nf[0:1]"  0 1 1 1 0 1;
	setAttr ".pr" 0.89041096324177627;
createNode transform -n "boundingObject2";
	setAttr ".v" no;
createNode boundingObject -n "boundingObjectShape2" -p "boundingObject2";
	setAttr -k off ".v";
	setAttr ".tp" 3;
	setAttr ".dpcbv" no;
	setAttr -s 2 ".pgtf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".wgtf[0:1]"  0 1 1 1 0 1;
	setAttr ".clrf[0].clrfcv" -type "float3" 1 1 1 ;
	setAttr ".af[0]"  0 1 1;
	setAttr -s 2 ".rf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".nf[0:1]"  0 1 1 1 0 1;
	setAttr ".pr" 2.123287668681308;
createNode transform -n "boundingObject4";
	setAttr ".v" no;
createNode boundingObject -n "boundingObjectShape4" -p "boundingObject4";
	setAttr -k off ".v";
	setAttr ".tp" 3;
	setAttr -s 2 ".pgtf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".wgtf[0:1]"  0.88695651 0 1 1 1 1;
	setAttr ".clrf[0].clrfcv" -type "float3" 1 1 1 ;
	setAttr ".af[0]"  0 1 1;
	setAttr -s 2 ".rf[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".nf[0:1]"  0 1 1 1 0 1;
	setAttr ".pr" 2.3287671195878965;
createNode transform -n "EMITTERS____________________";
createNode pointCloudParticleEmitter -n "pointCloudParticleEmitter1";
	setAttr ".rm" 100;
	setAttr ".randp" 0.20000000298023224;
	setAttr ".randd" 0.20000000298023224;
	setAttr ".rands" 0.45180723071098328;
	setAttr ".sm" 8;
createNode pointCloudParticleEmitter -n "pointCloudParticleEmitter2";
	setAttr ".rpp" -type "doubleArray" 5822 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".rm" 1.2000000476837158;
	setAttr ".randp" 0.40000000596046448;
	setAttr ".sm" 10;
	setAttr ".us1" yes;
	setAttr ".us2" yes;
createNode pointCloudParticleEmitter -n "pointCloudParticleEmitter4";
	setAttr ".rm" 15;
	setAttr ".randp" 0.30000001192092896;
	setAttr ".randd" 0.11445783078670502;
	setAttr ".rands" 0.39156627655029297;
	setAttr ".sm" 15;
createNode pointCloudParticleEmitter -n "pointCloudParticleEmitter3";
	setAttr ".rm" 3;
	setAttr ".randp" 0.5;
	setAttr ".randd" 0.19277107715606689;
	setAttr ".rands" 0.24096386134624481;
	setAttr ".sm" 20;
createNode pointCloudFluidEmitter -n "pointCloudFluidEmitter1";
	setAttr ".rat" 500;
	setAttr ".pc" -type "float3" 0.46700001 0.28253502 0.28253502 ;
	setAttr ".fdo" 0;
	setAttr ".fde" 4;
createNode pointCloudFluidEmitter -n "pointCloudFluidEmitter2";
	setAttr ".rat" 500;
	setAttr ".pc" -type "float3" 0.5 0.30250001 0.30250001 ;
	setAttr ".fdo" 0;
	setAttr ".fde" 2;
	setAttr ".efc" yes;
createNode pointEmitter -n "emitterLarge";
	setAttr ".t" -type "double3" 28.454286860391164 13 0.40513363372314753 ;
	setAttr ".emt" 0;
	setAttr ".sro" no;
	setAttr -l on ".urpp";
	setAttr ".d" -type "double3" -1 0 0 ;
	setAttr ".spr" 0.25;
	setAttr ".spd" 60;
	setAttr ".nspd" 0;
createNode pointEmitter -n "emitterSmall";
	setAttr ".t" -type "double3" 28.045104446001595 14.225308099885584 0 ;
	setAttr ".emt" 0;
	setAttr ".sro" no;
	setAttr -l on ".urpp";
	setAttr ".d" -type "double3" -1 0 0 ;
	setAttr ".spd" 50;
	setAttr ".nspd" 0;
	setAttr ".vol" 3;
	setAttr ".vsw" 49.736842233197471;
	setAttr ".afa" 0;
	setAttr ".alx" 80;
createNode transform -n "PARTICLES____________________";
createNode transform -n "smallGunShot";
createNode nParticle -n "smallGunShotShape" -p "smallGunShot";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -sn "opacityPP" -ln "opacityPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "opacityPP0" -ln "opacityPP0" -dt "doubleArray";
	addAttr -ci true -sn "radiusPP" -ln "radiusPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "radiusPP0" -ln "radiusPP0" -dt "doubleArray";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr ".lfm" 3;
	setAttr ".irbx" -type "string" "vector $pos = .I[0];\nif($pos.x<0)\n.O[0]=0;";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "";
	setAttr ".cts" 1;
	setAttr ".chw" 300;
	setAttr ".prt" 7;
	setAttr ".boce" 0.89999997615814209;
	setAttr ".fron" 0.019999999552965164;
	setAttr ".cofl" 1;
	setAttr ".cold" no;
	setAttr ".scld" no;
	setAttr -s 2 ".fsc[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".pfdo[0:1]"  0 1 1 1 0 1;
	setAttr ".vssc[0]"  0 1 1;
	setAttr ".stns[0]"  0 1 1;
	setAttr ".thr" 0;
	setAttr ".rdc[0]"  0 1 1;
	setAttr ".rci" 1;
	setAttr ".mssc[0]"  0 1 1;
	setAttr ".pfsc[0]"  0 1 1;
	setAttr ".frsc[0]"  0 1 1;
	setAttr ".stsc[0]"  0 1 1;
	setAttr ".clsc[0]"  0 1 1;
	setAttr ".bosc[0]"  0 1 1;
	setAttr ".opc[0]"  0 1 1;
	setAttr ".oci" 1;
	setAttr ".cl[0].clp" 0;
	setAttr ".cl[0].clc" -type "float3" 0.77999997 0.77999997 0.77999997 ;
	setAttr ".cl[0].cli" 1;
	setAttr ".inca[0].incap" 0;
	setAttr ".inca[0].incac" -type "float3" 0 0 0 ;
	setAttr ".inca[0].incai" 1;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan" 1;
	setAttr ".opacityPP0" -type "doubleArray" 0 ;
	setAttr ".radiusPP0" -type "doubleArray" 0 ;
createNode transform -n "dustParticles";
createNode particle -n "dustParticlesShape" -p "dustParticles";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -is true -ci true -sn "colorAccum" -ln "colorAccum" -min 0 -max 1 -at "bool";
	addAttr -is true -ci true -sn "useLighting" -ln "useLighting" -min 0 -max 1 -at "bool";
	addAttr -is true -ci true -sn "lineWidth" -ln "lineWidth" -dv 1 -min 1 -max 20 -at "long";
	addAttr -is true -ci true -sn "tailFade" -ln "tailFade" -min -1 -max 1 -at "float";
	addAttr -is true -ci true -sn "tailSize" -ln "tailSize" -dv 1 -min -100 -max 100 
		-at "float";
	addAttr -is true -ci true -sn "normalDir" -ln "normalDir" -dv 2 -min 1 -max 3 -at "long";
	addAttr -ci true -sn "opacityPP" -ln "opacityPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "opacityPP0" -ln "opacityPP0" -dt "doubleArray";
	addAttr -is true -ci true -sn "colorRed" -ln "colorRed" -at "double";
	addAttr -is true -ci true -sn "colorGreen" -ln "colorGreen" -at "double";
	addAttr -is true -ci true -sn "colorBlue" -ln "colorBlue" -at "double";
	addAttr -is true -ci true -sn "opacity" -ln "opacity" -at "double";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".usc" yes;
	setAttr ".scp" -type "string" "brickWallDestroy3_startup";
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr ".lfm" 3;
	setAttr ".lfr" 0.2;
	setAttr ".irbx" -type "string" "vector $pos = .I[0];\nif($pos.y<0)\n.O[0]=0;";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" ".O[0]=rand(0,0.3);";
	setAttr ".con" 0.99333333315948635;
	setAttr ".cts" 1;
	setAttr ".chw" 300;
	setAttr ".prt" 6;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan" 0.5;
	setAttr -k on ".colorAccum";
	setAttr -k on ".useLighting";
	setAttr -k on ".lineWidth" 2;
	setAttr -k on ".tailFade" -1;
	setAttr -k on ".tailSize" 0.30000001192092896;
	setAttr -k on ".normalDir";
	setAttr ".opacityPP0" -type "doubleArray" 0 ;
	setAttr -k on ".colorRed" 0.5;
	setAttr -k on ".colorGreen" 0.4;
	setAttr -k on ".colorBlue" 0.4;
	setAttr -k on ".opacity" 0.8;
createNode transform -n "largeGunShot";
createNode nParticle -n "largeGunShotShape" -p "largeGunShot";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr ".lfm" 3;
	setAttr ".irbx" -type "string" "vector $pos = .I[0];\nif($pos.x<0)\n.O[0]=0;";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "";
	setAttr ".cts" 1;
	setAttr ".chw" 300;
	setAttr ".prt" 4;
	setAttr ".boce" 0.89999997615814209;
	setAttr ".fron" 0.019999999552965164;
	setAttr ".cofl" 1;
	setAttr ".cold" no;
	setAttr ".scld" no;
	setAttr -s 2 ".fsc[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".pfdo[0:1]"  0 1 1 1 0 1;
	setAttr ".vssc[0]"  0 1 1;
	setAttr ".stns[0]"  0 1 1;
	setAttr ".thr" 0;
	setAttr ".ra" 1.0666667222976685;
	setAttr ".rdc[0]"  0 1 1;
	setAttr ".rci" 1;
	setAttr ".mssc[0]"  0 1 1;
	setAttr ".pfsc[0]"  0 1 1;
	setAttr ".frsc[0]"  0 1 1;
	setAttr ".stsc[0]"  0 1 1;
	setAttr ".clsc[0]"  0 1 1;
	setAttr ".bosc[0]"  0 1 1;
	setAttr ".op" 0;
	setAttr ".opc[0]"  0 1 1;
	setAttr ".oci" 1;
	setAttr ".cl[0].clp" 0;
	setAttr ".cl[0].clc" -type "float3" 0.45100001 0.45100001 0.45100001 ;
	setAttr ".cl[0].cli" 1;
	setAttr ".inca[0].incap" 0;
	setAttr ".inca[0].incac" -type "float3" 0 0 0 ;
	setAttr ".inca[0].incai" 1;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan" 1;
createNode transform -n "LargeInstanceParticle";
createNode particle -n "LargeInstanceParticleShape" -p "LargeInstanceParticle";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -h true -sn "myScalePP0" -ln "myScalePP0" -dt "doubleArray";
	addAttr -ci true -sn "myScalePP" -ln "myScalePP" -dt "doubleArray";
	addAttr -ci true -h true -sn "myRotPP0" -ln "myRotPP0" -dt "vectorArray";
	addAttr -ci true -sn "myRotPP" -ln "myRotPP" -dt "vectorArray";
	addAttr -ci true -h true -sn "myIndexPP0" -ln "myIndexPP0" -dt "doubleArray";
	addAttr -ci true -sn "myIndexPP" -ln "myIndexPP" -dt "doubleArray";
	addAttr -ci true -sn "goalPP" -ln "goalPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "goalPP0" -ln "goalPP0" -dt "doubleArray";
	addAttr -ci true -h true -sn "goalPPCache" -ln "goalPPCache" -dt "doubleArray";
	addAttr -ci true -sn "goalU" -ln "goalU" -dt "doubleArray";
	addAttr -ci true -h true -sn "goalU0" -ln "goalU0" -dt "doubleArray";
	addAttr -ci true -sn "goalV" -ln "goalV" -dt "doubleArray";
	addAttr -ci true -h true -sn "goalV0" -ln "goalV0" -dt "doubleArray";
	addAttr -ci true -sn "userScalar1PP" -ln "userScalar1PP" -dt "doubleArray";
	addAttr -ci true -h true -sn "userScalar1PP0" -ln "userScalar1PP0" -dt "doubleArray";
	addAttr -ci true -sn "userScalar2PP" -ln "userScalar2PP" -dt "doubleArray";
	addAttr -ci true -h true -sn "userScalar2PP0" -ln "userScalar2PP0" -dt "doubleArray";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".usc" yes;
	setAttr ".scp" -type "string" "brickWallDestroy3_startup";
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr -s 3 ".npt";
	setAttr ".lfm" 3;
	setAttr -s 4 ".xi";
	setAttr -s 7 ".xo";
	setAttr ".irbx" -type "string" "vector $pos = .I[0];\nif($pos.y<0 || $pos.x<-3)\n.O[0]=0;\n\nif(.I[1] != 0)\n.O[1]+=rand(10,20);\nelse";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" ".O[0]=10;\n.O[2]=(int)rand(10);\n.O[1]=rand(360);\n\nif(.I[1]==0){\n.O[3]=rand(.2,1.3);\n.O[4]=1;\n.O[5]=.I[2];\n.O[6]=.I[3];\n}\nelse if(.I[1]==1){\n.O[4]=0;\n.O[3]=rand(.05,.5);}\nelse{\n.O[4]=0;\n.O[3]=rand(.05,.2);}";
	setAttr -s 3 ".sd";
	setAttr -s 3 ".sd";
	setAttr ".cts" 1;
	setAttr ".gw[0]"  1;
	setAttr ".chw" 300;
	setAttr ".idt[0].iam" -type "stringArray" 12 "age" "age" "position" "worldPosition" "id" "particleId" "scale" "myScalePP" "objectIndex" "myIndexPP" "rotation" "myRotPP"  ;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan" 1;
	setAttr ".myScalePP0" -type "doubleArray" 0 ;
	setAttr ".myRotPP0" -type "vectorArray" 0 ;
	setAttr ".myIndexPP0" -type "doubleArray" 0 ;
	setAttr ".goalPP0" -type "doubleArray" 0 ;
	setAttr ".goalU0" -type "doubleArray" 0 ;
	setAttr ".goalV0" -type "doubleArray" 0 ;
	setAttr ".userScalar1PP0" -type "doubleArray" 0 ;
	setAttr ".userScalar2PP0" -type "doubleArray" 0 ;
createNode transform -n "fluid1";
	setAttr ".t" -type "double3" 13.071270878479528 12.073088379906958 0 ;
createNode fluidShape -n "fluidShape1" -p "fluid1";
	setAttr -k off ".v";
	setAttr ".rt" 1;
	setAttr ".vf" 0;
	setAttr ".iss" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sqvx" yes;
	setAttr ".rw" 8;
	setAttr ".rh" 6;
	setAttr ".dw" 30;
	setAttr ".dh" 25;
	setAttr ".dd" 37;
	setAttr -s 2 ".ifc";
	setAttr -s 2 ".sd";
	setAttr -s 2 ".sd";
	setAttr -s 2 ".fce";
	setAttr -s 2 ".eml";
	setAttr ".bod" 5;
	setAttr ".hds" 2;
	setAttr ".bndx" 2;
	setAttr ".bndy" 2;
	setAttr ".dsc" 0.67716532945632935;
	setAttr ".dds" 0.5;
	setAttr ".dsb" 2.9527559280395508;
	setAttr ".vsw" 15;
	setAttr ".vdp" 0.0081967208534479141;
	setAttr ".ss" yes;
	setAttr ".rin" 3;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].clp" 0.6086956262588501;
	setAttr ".cl[0].clc" -type "float3" 0.69499999 0.65746999 0.65746999 ;
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 0.77391302585601807;
	setAttr ".cl[1].clc" -type "float3" 0.5 0.26050001 0.26050001 ;
	setAttr ".cl[1].cli" 1;
	setAttr ".coi" 5;
	setAttr -s 2 ".opa[0:1]"  0 0 1 1 1 1;
	setAttr ".t" -type "float3" 0.10000763 0.10000763 0.10000763 ;
	setAttr ".shp" 0.47727271914482117;
	setAttr -s 3 ".i";
	setAttr ".i[0].ip" 0;
	setAttr ".i[0].ic" -type "float3" 0 0 0 ;
	setAttr ".i[0].ii" 1;
	setAttr ".i[1].ip" 0.80000001192092896;
	setAttr ".i[1].ic" -type "float3" 0.89999998 0.2 0 ;
	setAttr ".i[1].ii" 1;
	setAttr ".i[2].ip" 1;
	setAttr ".i[2].ic" -type "float3" 1.5 1 0 ;
	setAttr ".i[2].ii" 1;
	setAttr ".env[0].envp" 0;
	setAttr ".env[0].envc" -type "float3" 0 0 0 ;
	setAttr ".env[0].envi" 1;
	setAttr ".dos" 2;
createNode transform -n "FIELDS_________________";
createNode nucleus -n "nucleus1";
	setAttr -s 2 ".niao";
	setAttr -s 2 ".nias";
	setAttr -s 2 ".noao";
createNode pointCloudField -n "pointCloudField1";
	setAttr ".mag" 150;
	setAttr ".umd" yes;
	setAttr -s 2 ".fc[0:1]"  0 1 1 1 0 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
	setAttr ".pc" 1;
	setAttr ".rds" 4.4666666667287549;
createNode pointCloudField -n "pointCloudField2";
	setAttr ".mag" 300;
	setAttr ".umd" yes;
	setAttr -s 2 ".fc[0:1]"  0 1 1 1 0 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
	setAttr ".pc" 1;
	setAttr ".rds" 1.5666666656111681;
createNode gravityField -n "gravityField1";
	setAttr ".mag" 50;
	setAttr -s 2 ".ind";
	setAttr -s 2 ".of";
	setAttr -s 2 ".ppda";
	setAttr ".fc[0]"  0 1 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
	setAttr ".dy" -1;
createNode transform -n "spotLight1";
	setAttr ".t" -type "double3" 54.542735192974334 57.843924463843635 41.967331982165724 ;
	setAttr ".r" -type "double3" -35.40000000000002 53.20000000000001 2.654782457106123e-15 ;
createNode spotLight -n "spotLightShape1" -p "spotLight1";
	setAttr -k off ".v";
	setAttr ".in" 1.8115942478179932;
	setAttr ".col" 92.041097975684764;
	setAttr ".dms" yes;
createNode transform -n "cluster2Handle";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.700831683372483 11.932733122851561 0 ;
	setAttr ".s" -type "double3" 0.14660753538310831 1.0194248146808167 1.8543048737587975 ;
createNode clusterHandle -n "cluster2HandleShape" -p "cluster2Handle";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "cluster1Handle";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 11.720183833689394 -0.17849234205443487 ;
	setAttr ".r" -type "double3" 0 0 -90.000000000000028 ;
	setAttr ".s" -type "double3" 1 1 1.7957966029336885 ;
createNode clusterHandle -n "cluster1HandleShape" -p "cluster1Handle";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "polySurface10";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape10" -p "polySurface10";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 29 ".uvst[0].uvsp[0:28]" -type "float2" 0.625 0 0.73441088
		 0 0.76259649 0.11373419 0.76913816 0.15588933 0.70670772 0.25 0.625 0.25 0.53373986
		 0.56033993 0.72795123 0.49114504 0.70296067 0.59916157 0.59925437 0.79074055 0.52876019
		 0.57780755 0.54335237 0.25 0.50475967 0.19089261 0.58963197 0 0.55528378 0.31158406
		 0.625 0.33170772 0.61420608 0.25266173 0.57419145 0.40330109 0.57583094 0.76459551
		 0.57852232 0.59895051 0.625 0.89058906 0.58963197 1 0.58963197 0.89226669 0.625 1
		 0.60266864 0.50389904 0.59530967 0.4616251 0.61416715 0.47991264 0.59319186 0.45950726
		 0.56970119 0.3988108;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -0.43586546 0 -0.35559195 
		-0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 
		0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 
		-0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 
		0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 -0.43586546 0 -0.35559195 
		-0.43586546 0 -0.35559195;
	setAttr -s 16 ".vt[0:15]"  0.83975506 -0.83975506 0.83975506 0.83975506 0.83975506 0.83975506
		 0.83975506 -0.83975506 0.10472804 0.83975506 -0.07568416 -0.084623888 0.83975506 0.83975506 0.2908392
		 0.839755 0.20751578 -0.12857118 0.23996586 0.12503174 -0.010159157 0.37139878 0.83975506 0.42603076
		 0.29124275 0.83975506 0.83975506 0.60215116 -0.83975506 0.11599832 0.48282191 -0.57136273 0.055145532
		 0.20796721 0.046833731 0.0076100733 0.15646863 0.16266307 0.038906045 0.60215122 -0.83975506 0.83975506
		 0.03197585 0.44266915 0.83975506 0.084816292 0.32382166 0.16649683;
	setAttr -s 24 ".ed[0:23]"  0 1 0 1 4 0 2 0 0 2 9 0 2 3 0 3 5 0 3 10 0
		 6 11 0 4 7 0 5 4 0 6 12 0 5 6 0 8 1 0 8 7 0 7 15 0 13 0 0 14 8 0 10 9 0 10 11 0 9 13 0
		 12 11 0 14 13 0 15 12 0 15 14 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 6 -3 4 5 9 -2 -1
		mu 0 6 0 1 2 3 4 5
		f 5 -12 -6 6 18 -8
		mu 0 5 6 7 8 9 10
		f 5 -17 21 15 0 -13
		mu 0 5 11 12 13 0 5
		f 4 -14 12 1 8
		mu 0 4 14 11 5 15
		f 4 13 14 23 16
		mu 0 4 11 16 17 12
		f 4 -18 -7 -5 3
		mu 0 4 18 19 8 20
		f 4 -20 -4 2 -16
		mu 0 4 21 22 20 23
		f 3 -21 -11 7
		mu 0 3 24 25 26
		f 6 -23 -15 -9 -10 11 10
		mu 0 6 27 28 16 4 7 26
		f 7 19 -22 -24 22 20 -19 17
		mu 0 7 22 13 12 28 25 10 19;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface9";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape9" -p "polySurface9";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 30 ".uvst[0].uvsp[0:29]" -type "float2" 0.48577461 0.60297108
		 0.55220824 0.60254437 0.56233287 0.76787889 0.42585874 1 0.4982734 0.88056457 0.52256358
		 0.87585104 0.54196048 0.87894338 0.58762282 0.89209759 0.58762282 1 0.48255101 0.73167872
		 0.48128194 0.61754781 0.49148381 0.64705509 0.50580716 0.59302258 0.4962033 0.41468155
		 0.53300309 0.42759356 0.566751 0.45763499 0.57421446 0.49511921 0.51728797 0.62125093
		 0.51978838 0.41437882 0.53370565 0.39185041 0.56225669 0.45314062 0.4134683 0.12168916
		 0.49249578 0.41838911 0.42585874 0 0.50308216 0.19014683 0.54504061 0.40318543 0.52233213
		 0.54700148 0.58929229 0.78469294 0.51829088 0.84011233 0.58762282 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -0.00366503 0.201048 -0.41056049 
		-0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 
		-0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 
		0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 
		-0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 
		-0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049 -0.00366503 
		0.201048 -0.41056049 -0.00366503 0.201048 -0.41056049;
	setAttr -s 16 ".vt[0:15]"  0.28189203 -0.83975506 0.026491936 0.1515829 -0.83975506 0.0057171192
		 0.015373513 -0.40129319 -0.018634051 -0.49808419 -0.83975506 0.83975506 -0.011599481 -0.839755 0.037382968
		 -0.027009664 -0.40943265 -0.0094667971 -0.016091201 0.23242219 0.090746619 -0.58132362 -0.022242367 0.83975506
		 -0.15365502 0.13154793 0.10856312 0.58865368 -0.83975506 0.11486264 0.47155154 -0.57637197 0.055145573
		 0.19669715 0.041823886 0.0076101981 0.14519887 0.15765257 0.038905989 0.58865368 -0.83975506 0.83975506
		 0.073546372 0.31881166 0.16649714 0.020705789 0.43765905 0.83975506;
	setAttr -s 24 ".ed[0:23]"  1 0 0 1 4 0 2 5 0 1 2 0 3 13 0 3 7 0 4 3 0
		 5 4 0 8 5 0 7 15 0 8 6 0 8 7 0 9 0 0 10 0 0 11 2 0 12 6 0 14 6 0 10 9 0 10 11 0 9 13 0
		 12 11 0 14 12 0 15 13 0 15 14 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 3 -14 17 12
		mu 0 3 0 1 2
		f 6 -7 -2 0 -13 19 -5
		mu 0 6 3 4 5 6 7 8
		f 4 -8 -3 -4 1
		mu 0 4 9 10 11 5
		f 6 -9 10 -16 20 14 2
		mu 0 6 12 13 14 15 16 17
		f 3 -17 21 15
		mu 0 3 18 19 20
		f 5 -12 8 7 6 5
		mu 0 5 21 22 10 4 23
		f 5 -11 11 9 23 16
		mu 0 5 14 22 21 24 25
		f 5 -19 13 -1 3 -15
		mu 0 5 26 27 6 28 11
		f 4 -23 -10 -6 4
		mu 0 4 29 24 21 23
		f 7 -18 18 -21 -22 -24 22 -20
		mu 0 7 2 27 16 20 25 24 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface8";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape8" -p "polySurface8";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0.375 0.25 0.53678888
		 0.25 0.54917395 0.31392536 0.43393463 0.40327346 0.375 0.41091812 0.21408185 0.25
		 0.24475297 0.15257439 0.375 0.12517974 0.45076504 0.4417845 0.43514526 0.40206283
		 0.55275172 0.31034753 0.49946132 0.44019392 0.48385721 0.46773186 0.51204908 0.50306857
		 0.44255754 0.39110604 0.30157939 0.2094008 0.41015401 0.12517974 0.50020778 0.19397351
		 0.466616 0.41516447 0.48380333 0.46533918 0.52898145 0.51285613 0.49735433 0.45178813
		 0.49882236 0.44083285;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  0.25470126 -0.42048126 -0.29922774 
		0.25470126 -0.42048126 -0.29922774 0.25470126 -0.42048126 -0.29922774 0.25470126 
		-0.42048126 -0.29922774 0.25470126 -0.42048126 -0.29922774 0.25470126 -0.42048126 
		-0.29922774 0.25470126 -0.42048126 -0.29922774 0.25470126 -0.42048126 -0.29922774 
		0.25470126 -0.42048126 -0.29922774 0.25470126 -0.42048126 -0.29922774 0.25470126 
		-0.42048126 -0.29922774 0.25470126 -0.42048126 -0.29922774 0.25470126 -0.42048126 
		-0.29922774 0.25470126 -0.42048126 -0.29922774;
	setAttr -s 14 ".vt[0:13]"  -0.83975506 0.83975506 0.83975506 -0.83975506 0.83975506 -0.24129955
		 -0.44382989 0.83975506 -0.18994232 -0.12095678 0.30138266 0.021426961 -0.83975506 0.0012074709 0.83975506
		 -0.83975506 0.1852459 -0.03524987 -0.32031816 0.17006937 0.036906388 -0.03494332 0.25709933 0.084910616
		 -0.60358894 0.0012074721 0.83975506 -0.17250666 0.15622541 0.10272706 0.24714912 0.83975506 0.83975506
		 0.33035254 0.83975506 0.41030183 0.054694638 0.34348914 0.16066146 0.0013962388 0.4633669 0.83975506;
	setAttr -s 21 ".ed[0:20]"  0 10 0 0 1 0 2 1 0 3 6 0 2 3 0 4 0 0 5 1 0
		 5 4 0 6 9 0 6 5 0 7 3 0 8 4 0 8 13 0 9 7 0 9 8 0 11 2 0 12 7 0 10 11 0 11 12 0 13 10 0
		 13 12 0;
	setAttr -s 9 -ch 42 ".fc[0:8]" -type "polyFaces" 
		f 5 0 17 15 2 -2
		mu 0 5 0 1 2 3 4
		f 4 -7 7 5 1
		mu 0 4 5 6 7 0
		f 5 -5 -16 18 16 10
		mu 0 5 8 9 10 11 12
		f 5 -3 4 3 9 6
		mu 0 5 4 9 13 14 15
		f 5 -12 12 19 -1 -6
		mu 0 5 7 16 17 1 0
		f 5 -8 -10 8 14 11
		mu 0 5 7 15 18 19 16
		f 4 -14 -9 -4 -11
		mu 0 4 20 21 18 13
		f 5 -21 -13 -15 13 -17
		mu 0 5 22 17 16 21 12
		f 4 -18 -20 20 -19
		mu 0 4 2 1 17 11;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface7";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape7" -p "polySurface7";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:7]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.42199203
		 0 0.40966713 0.12104551 0.375 0.12104551 0.375 0.81178987 0.49545711 0.8788321 0.42199203
		 1 0.375 1 0.1867899 0 0.20334375 0.061281402 0.46422413 0.58429313 0.46395847 0.74297184
		 0.24800165 0.14237529 0.40550134 0.35008615 0.43716669 0.38646054 0.24392021 0.14861533
		 0.2010546 0.095428236 0.18313673 0.041074373 0.26090801 0.16560313 0.39278266 0.33736745
		 0.40966713 0.19724691 0.42805928 0.37735307;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  0.43513721 0.34055319 -0.19528249 
		0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 
		-0.19528249 0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 -0.19528249 0.43513721 
		0.34055319 -0.19528249 0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 -0.19528249 
		0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 -0.19528249 0.43513721 0.34055319 
		-0.19528249;
	setAttr -s 12 ".vt[0:11]"  -0.83975506 -0.83975506 0.83975506 -0.839755 -0.839755 -0.42464793
		 -0.839755 -0.56381571 -0.44919008 -0.83975506 -0.19866431 -0.32881713 -0.83975506 -0.026566386 0.83975506
		 -0.83975506 0.15864868 -0.040844202 -0.32031965 0.14347219 0.031311817 -0.52406073 -0.83975506 0.83975506
		 -0.030519351 -0.839755 0.025744304 -0.045860782 -0.41135246 -0.020896459 -0.60685986 -0.02656639 0.83975506
		 -0.17250609 0.12962805 0.097133413;
	setAttr -s 18 ".ed[0:17]"  0 7 0 0 4 0 1 0 0 2 3 0 2 1 0 3 5 0 6 3 0
		 5 4 0 6 11 0 6 5 0 8 1 0 9 2 0 10 4 0 7 10 0 8 7 0 9 8 0 11 9 0 10 11 0;
	setAttr -s 8 -ch 36 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 12 -2
		mu 0 4 0 1 2 3
		f 4 -11 14 -1 -3
		mu 0 4 4 5 6 7
		f 4 -5 -12 15 10
		mu 0 4 8 9 10 11
		f 5 -7 8 16 11 3
		mu 0 5 12 13 14 10 9
		f 6 -8 -6 -4 4 2 1
		mu 0 6 3 15 16 17 8 0
		f 3 -10 6 5
		mu 0 3 18 19 16
		f 5 -13 17 -9 9 7
		mu 0 5 3 20 21 19 15
		f 5 -14 -15 -16 -17 -18
		mu 0 5 2 6 11 10 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface6";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape6" -p "polySurface6";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 15 ".uvst[0].uvsp[0:14]" -type "float2" 0.375 0.5 0.43921477
		 0.5 0.48780739 0.57796347 0.375 0.65226626 0.125 0.097733766 0.19738448 0.097733766
		 0.24054721 0.15128951 0.20947161 0.25 0.125 0.25 0.43369055 0.40791541 0.48253107
		 0.37305406 0.375 0.41552836 0.45285541 0.3782253 0.375 0.4917081 0.35569105 0.40669346;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 10 ".pt[0:9]" -type "float3"  0.46083283 -0.32828969 0.41530159 
		0.46083283 -0.32828969 0.41530159 0.46083283 -0.32828969 0.41530159 0.46083283 -0.32828969 
		0.41530159 0.46083283 -0.32828969 0.41530159 0.46083283 -0.32828969 0.41530159 0.46083283 
		-0.32828969 0.41530159 0.46083283 -0.32828969 0.41530159 0.46083283 -0.32828969 0.41530159 
		0.46083283 -0.32828969 0.41530159;
	setAttr -s 10 ".vt[0:9]"  -0.83975506 0.83975506 -0.83975506 -0.83975506 -0.18317568 -0.83975506
		 -0.40835744 0.839755 -0.839755 -0.08191058 0.31599358 -0.83975506 -0.83975506 -0.18317565 -0.35347319
		 -0.11735678 0.29264635 -0.0063276142 -0.83975506 0.83975506 -0.27227128 -0.4454695 0.83975506 -0.22112682
		 -0.83975506 0.17661397 -0.063504249 -0.31671935 0.16133223 0.0091518983;
	setAttr -s 15 ".ed[0:14]"  0 2 0 0 1 0 3 1 0 2 3 0 3 5 0 4 1 0 4 8 0
		 4 9 0 6 0 0 7 2 0 8 6 0 7 5 0 7 6 0 9 5 0 8 9 0;
	setAttr -s 7 -ch 30 ".fc[0:6]" -type "polyFaces" 
		f 4 0 3 2 -2
		mu 0 4 0 1 2 3
		f 5 -6 6 10 8 1
		mu 0 5 4 5 6 7 8
		f 4 -10 11 -5 -4
		mu 0 4 1 9 10 2
		f 4 -13 9 -1 -9
		mu 0 4 11 9 1 0
		f 5 -14 -8 5 -3 4
		mu 0 5 10 12 13 3 2
		f 3 -15 -7 7
		mu 0 3 12 14 13
		f 5 -11 14 13 -12 12
		mu 0 5 7 14 12 10 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface5";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape5" -p "polySurface5";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 15 ".uvst[0].uvsp[0:14]" -type "float2" 0.375 0.75 0.52141339
		 0.75 0.52141339 0.87081891 0.49728227 0.87550163 0.375 0.8074435 0.125 0 0.18244348
		 0 0.17884779 0.040428422 0.125 0.04042843 0.375 0.70957154 0.50131822 0.68531281
		 0.466501 0.59914911 0.48088962 0.62671739 0.46285316 0.5823583 0.48088962 0.3396197;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 10 ".pt[0:9]" -type "float3"  0.34794939 0.62246954 0.41819283 
		0.34794939 0.62246954 0.41819283 0.34794939 0.62246954 0.41819283 0.34794939 0.62246954 
		0.41819283 0.34794939 0.62246954 0.41819283 0.34794939 0.62246954 0.41819283 0.34794939 
		0.62246954 0.41819283 0.34794939 0.62246954 0.41819283 0.34794939 0.62246954 0.41819283 
		0.34794939 0.62246954 0.41819283;
	setAttr -s 10 ".vt[0:9]"  -0.83975506 -0.83975506 -0.83975506 -0.83975506 -0.56815529 -0.83975506
		 0.14385632 -0.83975506 -0.83975506 0.0088557452 -0.405184 -0.83975506 -0.83975506 -0.83975506 -0.45384732
		 -0.83975506 -0.56815529 -0.47800341 0.14385632 -0.83975506 -0.028089151 0.0088557517 -0.40518406 -0.052224237
		 -0.018257558 -0.83975506 0.0033693761 -0.03352851 -0.4133237 -0.043056753;
	setAttr -s 15 ".ed[0:14]"  0 2 0 0 4 0 1 0 0 3 1 0 2 6 0 3 2 0 5 1 0
		 4 5 0 7 3 0 8 4 0 9 5 0 6 8 0 9 7 0 6 7 0 8 9 0;
	setAttr -s 7 -ch 30 ".fc[0:6]" -type "polyFaces" 
		f 5 0 4 11 9 -2
		mu 0 5 0 1 2 3 4
		f 4 1 7 6 2
		mu 0 4 5 6 7 8
		f 4 -4 5 -1 -3
		mu 0 4 9 10 1 0
		f 5 -11 12 8 3 -7
		mu 0 5 7 11 12 13 8
		f 4 -10 14 10 -8
		mu 0 4 4 3 11 7
		f 4 -14 -5 -6 -9
		mu 0 4 14 2 1 13
		f 4 -12 13 -13 -15
		mu 0 4 3 2 12 11;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface4";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape4" -p "polySurface4";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0.5362848 0.59837312
		 0.875 0.16315626 0.77426153 0.15929723 0.53432059 0.38156396 0.44276273 0.5 0.4371919
		 0.40713888 0.55209863 0.31804872 0.625 0.33909178 0.625 0.5 0.49036252 0.57637054
		 0.625 0.58684373 0.60572505 0.37661213 0.54312515 0.27228189 0.54062271 0.28114519
		 0.54760742 0.28254315 0.71409172 0.25 0.875 0.25 0.61906564 0.23482192 0.59613848
		 0.24000569 0.55592507 0.25464803 0.69756961 0.31909707 0.54163212 0.33438724 0.56063342
		 0.25998774;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  -0.20890391 -0.49319428 0.22857691 
		-0.20890391 -0.49319428 0.22857691 -0.20890391 -0.49319428 0.22857691 -0.20890391 
		-0.49319428 0.22857691 -0.20890391 -0.49319428 0.22857691 -0.20890391 -0.49319428 
		0.22857691 -0.20890391 -0.49319428 0.22857691 -0.20890391 -0.49319428 0.22857691 
		-0.20890391 -0.49319428 0.22857691 -0.20890391 -0.49319428 0.22857691 -0.20890391 
		-0.49319428 0.22857691 -0.20890391 -0.49319428 0.22857691 -0.20890391 -0.49319428 
		0.22857691 -0.20890391 -0.49319428 0.22857691;
	setAttr -s 14 ".vt[0:13]"  0.83975506 0.83975506 -0.83975506 0.83975506 0.25633526 -0.83975506
		 0.24376255 0.17888051 -0.83975506 -0.38452232 0.83975506 -0.83975506 -0.064744875 0.32669443 -0.839755
		 -0.10023477 0.30331841 -0.0053010657 -0.42194724 0.83975506 -0.21591045 -0.014219776 0.25903428 0.058183745
		 0.83975506 0.23041023 -0.16298985 0.23056714 0.14663349 -0.042722352 0.35000053 0.83975506 0.38260123
		 0.839755 0.839755 0.24123302 0.14706869 0.1842654 0.006343618 0.075416841 0.3454228 0.13393348;
	setAttr -s 21 ".ed[0:20]"  0 1 0 2 4 0 1 2 0 1 8 0 2 9 0 3 0 0 3 4 0
		 4 5 0 6 3 0 6 10 0 5 6 0 5 7 0 7 12 0 11 0 0 13 7 0 8 9 0 10 11 0 8 11 0 12 9 0 13 10 0
		 13 12 0;
	setAttr -s 9 -ch 42 ".fc[0:8]" -type "polyFaces" 
		f 4 -3 3 15 -5
		mu 0 4 0 1 2 3
		f 5 -9 9 16 13 -6
		mu 0 5 4 5 6 7 8
		f 5 -7 5 0 2 1
		mu 0 5 9 4 8 10 0
		f 4 6 7 10 8
		mu 0 4 4 11 12 5
		f 5 -15 19 -10 -11 11
		mu 0 5 13 14 6 5 12
		f 4 -18 -4 -1 -14
		mu 0 4 15 2 1 16
		f 6 -19 -13 -12 -8 -2 4
		mu 0 6 17 18 19 12 11 20
		f 3 -21 14 12
		mu 0 3 21 22 19
		f 6 -16 17 -17 -20 20 18
		mu 0 6 3 2 7 6 22 18;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface3";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape3" -p "polySurface3";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.52335137 0.75 0.50308555
		 0.68476367 0.5368008 0.60237062 0.625 0.59090835 0.625 0.63581896 0.54277939 0.75
		 0.52335137 0.38745201 0.50308561 0.39426413 0.53007615 0.37859887 0.53483921 0.38205156
		 0.54277939 0.87394667 0.52335137 0.87084937 0.77431202 0.15523456 0.76794124 0.11418104
		 0.875 0.11418104 0.875 0.15909165 0.60997564 0.61327344 0.625 0.41313627;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  -0.43024194 0.305363 0.40909988 
		-0.43024194 0.305363 0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 0.305363 
		0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 
		0.305363 0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 0.305363 0.40909988 
		-0.43024194 0.305363 0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 0.305363 
		0.40909988 -0.43024194 0.305363 0.40909988 -0.43024194 0.305363 0.40909988;
	setAttr -s 14 ".vt[0:13]"  0.28739357 -0.83975506 -0.83975506 0.83975506 -0.072682202 -0.83975506
		 0.83975506 0.22902907 -0.83975506 0.24722922 0.15202472 -0.83975506 0.15687549 -0.83975506 -0.83975506
		 0.020728819 -0.40149516 -0.83975506 0.28739357 -0.83975506 -0.0070762485 0.15687549 -0.83975506 -0.027884394
		 0.020728866 -0.40149522 -0.05222439 0.83975506 -0.072682187 -0.12053023 0.83975506 0.203117 -0.16332909
		 0.23405087 0.11981932 -0.043749217 0.47690707 -0.5765748 0.021555308 0.20205235 0.041621588 -0.025980119;
	setAttr -s 21 ".ed[0:20]"  1 0 0 0 6 0 1 9 0 2 1 0 3 5 0 2 10 0 3 2 0
		 3 11 0 4 0 0 5 4 0 4 7 0 5 8 0 6 12 0 7 8 0 7 6 0 13 8 0 9 10 0 12 9 0 13 11 0 10 11 0
		 13 12 0;
	setAttr -s 9 -ch 42 ".fc[0:8]" -type "polyFaces" 
		f 6 -10 -5 6 3 0 -9
		mu 0 6 0 1 2 3 4 5
		f 4 10 13 -12 9
		mu 0 4 0 6 7 1
		f 5 -16 18 -8 4 11
		mu 0 5 7 8 9 2 1
		f 4 -15 -11 8 1
		mu 0 4 10 11 0 5
		f 4 -17 -3 -4 5
		mu 0 4 12 13 14 15
		f 5 -18 -13 -2 -1 2
		mu 0 5 13 16 10 5 14
		f 4 -20 -6 -7 7
		mu 0 4 9 17 3 2
		f 5 -21 15 -14 14 12
		mu 0 5 16 8 7 11 10
		f 5 19 -19 20 17 16
		mu 0 5 17 9 8 16 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface2";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape2" -p "polySurface2";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 25 ".uvst[0].uvsp[0:24]" -type "float2" 0.53476828 0.6015389
		 0.53280663 0.59368181 0.52804351 0.61510724 0.50105309 0.73919892 0.50105304 0.68393195
		 0.125 0.041860271 0.17899086 0.04186026 0.19666708 0.095481075 0.125 0.095481068
		 0.375 0.65451896 0.48884216 0.57953453 0.375 0.70813978 0.41697851 0.49330398 0.44736332
		 0.47660166 0.469713 0.50675035 0.51131159 0.56397009 0.40628225 0.39888841 0.4835659
		 0.52004182 0.50105309 0.39421242 0.49474406 0.39152819 0.375 0.56424779 0.49474406
		 0.73257804 0.441802 0.45435679 0.49636936 0.53873581 0.52037764 0.57484865;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.30309027 0.12654871 0.38239098 
		0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 
		0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 
		0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 
		0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 
		0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 0.12654871 0.38239098 0.30309027 
		0.12654871 0.38239098 0.30309027 0.12654871 0.38239098;
	setAttr -s 16 ".vt[0:15]"  0.23357454 0.15761226 -0.83975506 -0.83975506 -0.55853605 -0.83975506
		 0.0070744008 -0.39590698 -0.83975506 -0.83975506 -0.19830936 -0.83975506 -0.07495901 0.3054387 -0.83975506
		 -0.83975506 -0.55853611 -0.47704232 -0.83975506 -0.1983093 -0.35829273 -0.11040518 0.28209147 -0.0063277185
		 -0.30976772 0.15077735 0.0091518536 0.007074418 -0.39590707 -0.052224178 -0.035309866 -0.40404677 -0.043056734
		 -0.024390884 0.23780772 0.05715654 -0.161955 0.13693325 0.074973091 0.22039615 0.12540686 -0.043749504
		 0.18839712 0.047207884 -0.02598005 0.13689785 0.16303872 0.0053163283;
	setAttr -s 24 ".ed[0:23]"  0 2 0 0 4 0 2 1 0 3 1 0 4 3 0 5 1 0 6 3 0
		 5 6 0 5 10 0 6 8 0 7 4 0 7 11 0 8 7 0 9 2 0 12 8 0 11 15 0 10 9 0 12 10 0 11 12 0
		 13 0 0 14 9 0 13 14 0 15 13 0 14 15 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 5 -20 21 20 13 -1
		mu 0 5 0 1 2 3 4
		f 4 -6 7 6 3
		mu 0 4 5 6 7 8
		f 5 -5 -2 0 2 -4
		mu 0 5 9 10 0 4 11
		f 6 -11 11 15 22 19 1
		mu 0 6 12 13 14 15 1 0
		f 5 -7 9 12 10 4
		mu 0 5 8 7 16 17 10
		f 5 -17 -9 5 -3 -14
		mu 0 5 18 19 20 11 4
		f 5 -18 14 -10 -8 8
		mu 0 5 21 22 16 7 20
		f 4 -19 -12 -13 -15
		mu 0 4 22 23 17 16
		f 6 -21 23 -16 18 17 16
		mu 0 6 3 2 24 23 22 19
		f 3 -22 -23 -24
		mu 0 3 2 1 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface1";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 15 ".uvst[0].uvsp[0:14]" -type "float2" 0.54375613 0.75 0.625
		 0.63717544 0.625 0.75 0.54375613 0.37800878 0.57163227 0.36886498 0.625 0.41210186
		 0.76768941 0.1128246 0.73972923 0 0.875 0 0.875 0.1128246 0.625 0.88527071 0.58884388
		 0.88698578 0.54375613 0.87399709 0.64578271 0.38895684 0.57163227 0.69467163;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.56685519 0.4607749 0.37961709 
		-0.56685519 0.4607749 0.37961709 -0.56685519 0.4607749 0.37961709 -0.56685519 0.4607749 
		0.37961709 -0.56685519 0.4607749 0.37961709 -0.56685519 0.4607749 0.37961709 -0.56685519 
		0.4607749 0.37961709 -0.56685519 0.4607749 0.37961709;
	setAttr -s 8 ".vt[0:7]"  0.83975506 -0.83975506 -0.83975506 0.83975506 -0.081794798 -0.83975506
		 0.29395539 -0.83975506 -0.83975506 0.29395542 -0.83975506 -0.006737709 0.83975506 -0.83975506 0.068999477
		 0.83975506 -0.081794746 -0.11883812 0.48122811 -0.57968658 0.021555323 0.59685665 -0.83975506 0.080520853;
	setAttr -s 12 ".ed[0:11]"  0 4 0 1 0 0 2 0 0 1 5 0 1 2 0 2 3 0 3 7 0
		 6 3 0 4 5 0 6 5 0 7 4 0 6 7 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 3 -5 1 -3
		mu 0 3 0 1 2
		f 5 -8 9 -4 4 5
		mu 0 5 3 4 5 1 0
		f 4 -9 -1 -2 3
		mu 0 4 6 7 8 9
		f 5 -11 -7 -6 2 0
		mu 0 5 10 11 12 0 2
		f 3 -12 7 6
		mu 0 3 13 14 12
		f 4 -10 11 10 8
		mu 0 4 5 14 11 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode instancer -n "instancer1";
	setAttr -s 10 ".inh";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 7 ".lnk";
	setAttr -s 7 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyPlane -n "polyPlane1";
	setAttr ".w" 23.470500322202906;
	setAttr ".h" 23.470500322202906;
	setAttr ".sw" 150;
	setAttr ".sh" 180;
	setAttr ".cuv" 2;
createNode cluster -n "cluster1";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode tweak -n "tweak1";
createNode objectSet -n "cluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode hyperGraphInfo -n "nodeEditorPanel1Info";
createNode hyperView -n "hyperView1";
	setAttr ".vl" -type "double2" 2221.428571428572 -42531.413354815617 ;
	setAttr ".vh" -type "double2" 4715.4761904761917 -41290.01521661297 ;
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout1";
	setAttr ".ihi" 0;
	setAttr -s 33 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 3325.853515625;
	setAttr ".hyp[2].y" -41836.45703125;
	setAttr ".hyp[2].nvs" 2944;
	setAttr ".hyp[3].x" 3703.021484375;
	setAttr ".hyp[3].y" -41642.3359375;
	setAttr ".hyp[3].nvs" 1888;
	setAttr ".hyp[4].x" 2894.128173828125;
	setAttr ".hyp[4].y" -41485.64453125;
	setAttr ".hyp[4].nvs" 2864;
	setAttr ".hyp[5].x" 1805.3056640625;
	setAttr ".hyp[5].y" -41568.2578125;
	setAttr ".hyp[5].nvs" 2832;
	setAttr ".hyp[6].x" 2652.270751953125;
	setAttr ".hyp[6].y" -41483.38671875;
	setAttr ".hyp[6].nvs" 2144;
	setAttr ".hyp[7].x" 1544.945068359375;
	setAttr ".hyp[7].y" -41568.4375;
	setAttr ".hyp[7].nvs" 2528;
	setAttr ".hyp[8].x" 3322.711669921875;
	setAttr ".hyp[8].y" -41740.71484375;
	setAttr ".hyp[8].nvs" 2976;
	setAttr ".hyp[9].x" 1804.966064453125;
	setAttr ".hyp[9].y" -41692.25390625;
	setAttr ".hyp[9].nvs" 2912;
	setAttr ".hyp[10].x" 1545.0302734375;
	setAttr ".hyp[10].y" -41692.1796875;
	setAttr ".hyp[10].nvs" 2544;
	setAttr ".hyp[11].x" 2381.18603515625;
	setAttr ".hyp[11].y" -41543.3359375;
	setAttr ".hyp[11].nvs" 2640;
	setAttr ".hyp[12].x" 2387.3056640625;
	setAttr ".hyp[12].y" -41688.703125;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 3366.4677734375;
	setAttr ".hyp[13].y" -41418.9296875;
	setAttr ".hyp[13].nvs" 2944;
	setAttr ".hyp[14].x" 2388.274658203125;
	setAttr ".hyp[14].y" -41385.125;
	setAttr ".hyp[14].nvs" 2640;
	setAttr ".hyp[15].x" 3352.0224609375;
	setAttr ".hyp[15].y" -41538.30859375;
	setAttr ".hyp[15].nvs" 2912;
	setAttr ".hyp[16].x" 3306.5751953125;
	setAttr ".hyp[16].y" -41938.26953125;
	setAttr ".hyp[16].nvs" 3152;
	setAttr ".hyp[17].x" 3337.155517578125;
	setAttr ".hyp[17].y" -41270.7265625;
	setAttr ".hyp[17].nvs" 3152;
	setAttr ".hyp[18].x" 3623.512939453125;
	setAttr ".hyp[18].y" -41938.53515625;
	setAttr ".hyp[18].nvs" 2480;
	setAttr ".hyp[19].x" 3911.458984375;
	setAttr ".hyp[19].y" -41517.671875;
	setAttr ".hyp[19].nvs" 3168;
	setAttr ".hyp[20].x" 3040.967041015625;
	setAttr ".hyp[20].y" -41332.40234375;
	setAttr ".hyp[20].nvs" 2272;
	setAttr ".hyp[21].x" 2107.463134765625;
	setAttr ".hyp[21].y" -41249.359375;
	setAttr ".hyp[21].nvs" 2448;
	setAttr ".hyp[22].x" 1811.0950927734375;
	setAttr ".hyp[22].y" -41373.921875;
	setAttr ".hyp[22].nvs" 2833;
	setAttr ".hyp[23].x" 2391.91650390625;
	setAttr ".hyp[23].y" -41247.390625;
	setAttr ".hyp[23].nvs" 2640;
	setAttr ".hyp[24].x" 3338.936767578125;
	setAttr ".hyp[24].y" -41166.6953125;
	setAttr ".hyp[24].nvs" 3152;
	setAttr ".hyp[25].x" 3307.127685546875;
	setAttr ".hyp[25].y" -42053.92578125;
	setAttr ".hyp[25].nvs" 3152;
	setAttr ".hyp[26].x" 3110.943603515625;
	setAttr ".hyp[26].y" -41581.35546875;
	setAttr ".hyp[26].nvs" 3409;
	setAttr ".hyp[27].x" 3790.943603515625;
	setAttr ".hyp[27].y" -41738.05078125;
	setAttr ".hyp[27].nvs" 3329;
	setAttr ".hyp[28].x" 3475.953369140625;
	setAttr ".hyp[28].y" -42149.640625;
	setAttr ".hyp[28].nvs" 2657;
	setAttr ".hyp[29].x" 3475.953369140625;
	setAttr ".hyp[29].y" -42309.640625;
	setAttr ".hyp[29].nvs" 2833;
	setAttr ".hyp[30].x" 3145.953369140625;
	setAttr ".hyp[30].y" -42430.69921875;
	setAttr ".hyp[30].nvs" 4129;
	setAttr ".hyp[31].x" 3077.3818359375;
	setAttr ".hyp[31].y" -41722.78515625;
	setAttr ".hyp[31].nvs" 1649;
	setAttr ".hyp[32].x" 3247.3818359375;
	setAttr ".hyp[32].y" -42253.55859375;
	setAttr ".hyp[32].nvs" 2305;
	setAttr ".anf" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n"
		+ "            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n"
		+ "            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n"
		+ "            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n"
		+ "            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n"
		+ "                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n"
		+ "                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"all\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n"
		+ "                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n"
		+ "                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n"
		+ "                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -shadows 0\n            $editorName;\n"
		+ "modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 0\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n"
		+ "                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n"
		+ "                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n"
		+ "                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n"
		+ "                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n"
		+ "                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n"
		+ "                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n"
		+ "                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n"
		+ "                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showShapes 1\n                -showSGShapes 1\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;\n\t\t\tif (`objExists nodeEditorPanel1Info`) nodeEditor -e -restoreInfo nodeEditorPanel1Info $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showShapes 1\n                -showSGShapes 1\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;\n\t\t\tif (`objExists nodeEditorPanel1Info`) nodeEditor -e -restoreInfo nodeEditorPanel1Info $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n"
		+ "\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n"
		+ "                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n"
		+ "                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n"
		+ "\t\t\t\t-defaultImage \"vacantCell.xpm\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 300 -ast 1 -aet 300 ";
	setAttr ".st" 6;
createNode group -n "group1";
	setAttr ".ct" 2;
	setAttr ".raf" -type "string" "1";
	setAttr -s 2 ".bos";
createNode timeToUnitConversion -n "timeToUnitConversion1";
	setAttr ".cf" 0.004;
createNode deleteComponent -n "deleteComponent1";
createNode lambert -n "lambert2";
createNode shadingEngine -n "lambert2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode file -n "file1";
	setAttr ".ftn" -type "string" "/Users/admin/Downloads/old_brick.jpg";
	setAttr ".ft" 1;
createNode place2dTexture -n "place2dTexture1";
	setAttr ".rf" 270;
	setAttr ".re" -type "float2" 2 2 ;
createNode polyCube -n "polyCube1";
	setAttr ".w" 22.436937142248919;
	setAttr ".h" 22.436937142248919;
	setAttr ".d" 22.436937142248919;
	setAttr ".sh" 40;
	setAttr ".sd" 70;
	setAttr ".cuv" 4;
createNode cluster -n "cluster2";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode tweak -n "tweak2";
createNode objectSet -n "cluster2Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster2GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster2GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode shadingEngine -n "nParticleBallsSE";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode particleSamplerInfo -n "particleSamplerInfo1";
createNode blinn -n "npBallsBlinn";
createNode particleCloud -n "npBallsVolume";
createNode hyperGraphInfo -n "nodeView1";
createNode hyperView -n "hyperView2";
	setAttr ".vl" -type "double2" 597.83527567181204 -42812.053142184435 ;
	setAttr ".vh" -type "double2" 2960.1814295544768 -41968.947054105891 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView1";
createNode hyperLayout -n "hyperLayout2";
	setAttr ".ihi" 0;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[0].x" 1408.5714111328125;
	setAttr ".hyp[0].y" -42375.71484375;
	setAttr ".hyp[0].nvs" 1649;
	setAttr ".hyp[1].x" 1957.142822265625;
	setAttr ".hyp[1].y" -42267.14453125;
	setAttr ".hyp[1].nvs" 2161;
	setAttr ".hyp[2].x" 1645.7142333984375;
	setAttr ".hyp[2].y" -42394.28515625;
	setAttr ".hyp[2].nvs" 2481;
	setAttr ".hyp[3].x" 1060;
	setAttr ".hyp[3].y" -42164.28515625;
	setAttr ".hyp[3].nvs" 2913;
	setAttr ".anf" yes;
createNode attributeTransfer -n "attributeTransfer1";
	setAttr ".wgt" yes;
	setAttr -s 2 ".k[0:1]"  0 1 2 1 0 1;
createNode peak -n "peak1";
	setAttr ".d" -2;
createNode tweak -n "tweak3";
createNode objectSet -n "peak1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "peak1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "peak1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode arrayDataContainer -n "arrayDataContainer1";
	setAttr ".st" 1;
	setAttr ".et" 300;
createNode timeToUnitConversion -n "timeToUnitConversion2";
	setAttr ".cf" 0.004;
createNode lambert -n "lambert3";
createNode shadingEngine -n "lambert3SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode pointAttributeToArray -n "pointAttributeToArray1";
	setAttr ".pp" yes;
	setAttr ".pn" yes;
createNode arrayDataContainer -n "arrayDataContainer2";
	setAttr ".s" 0.21686746965510295;
	setAttr ".st" 1;
	setAttr ".et" 300;
createNode timeToUnitConversion -n "timeToUnitConversion3";
	setAttr ".cf" 0.004;
createNode shadingEngine -n "fluidShape1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
createNode animCurveTA -n "emitter1_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 30.000000000000011 29 14.477512185929925
		 50 0 73 -14.477512185929927 100 -30.000000000000011;
createNode animCurveTA -n "emitter1_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 29 -3.9671308008375998 73 -3.9671308008376016
		 100 0;
createNode animCurveTA -n "emitter1_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 29 -15.504089833653088 73 15.504089833653088
		 100 0;
createNode attributeTransfer -n "attributeTransfer2";
	setAttr ".wgt" yes;
	setAttr -s 2 ".k[0:1]"  0 1 2 1 0 1;
	setAttr -s 2 ".bos";
	setAttr ".bos[2].be" 1;
	setAttr ".bos[2].bt" 3;
	setAttr ".bos[2].bc" yes;
	setAttr ".bos[2].btso" 1;
	setAttr ".bos[2].btcs" -type "double3" 1 1 1 ;
	setAttr ".bos[2].bso" 1;
	setAttr ".bos[2].bbcs" -type "double3" 1 1 1 ;
	setAttr ".bos[2].bppp" -type "pointArray" 0 ;
	setAttr ".bos[2].bnpp" -type "vectorArray" 0 ;
	setAttr ".bos[2].brpp" -type "doubleArray" 0 ;
	setAttr ".bos[2].bwpp" -type "doubleArray" 0 ;
	setAttr ".bos[2].brgbapp" -type "doubleArray" 0 ;
createNode arrayDataContainer -n "arrayDataContainer3";
	setAttr ".s" 0.22891566199979871;
	setAttr ".st" 1;
	setAttr ".et" 300;
createNode timeToUnitConversion -n "timeToUnitConversion4";
	setAttr ".cf" 0.004;
createNode hyperGraphInfo -n "nodeView2";
createNode hyperView -n "hyperView3";
	setAttr ".vl" -type "double2" 1424.6500479881861 -42218.926420463991 ;
	setAttr ".vh" -type "double2" 4061.5804714004471 -41272.87209610796 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView2";
createNode hyperLayout -n "hyperLayout3";
	setAttr ".ihi" 0;
	setAttr -s 17 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 2641.2060546875;
	setAttr ".hyp[2].y" -41479.17578125;
	setAttr ".hyp[2].nvs" 1648;
	setAttr ".hyp[3].x" 3325.853515625;
	setAttr ".hyp[3].y" -41836.45703125;
	setAttr ".hyp[3].nvs" 2944;
	setAttr ".hyp[4].x" 3703.021484375;
	setAttr ".hyp[4].y" -41642.3359375;
	setAttr ".hyp[4].nvs" 1888;
	setAttr ".hyp[5].x" 3045.830322265625;
	setAttr ".hyp[5].y" -41474.7421875;
	setAttr ".hyp[5].nvs" 2864;
	setAttr ".hyp[6].x" 1805.3056640625;
	setAttr ".hyp[6].y" -41568.2578125;
	setAttr ".hyp[6].nvs" 2832;
	setAttr ".hyp[7].x" 2822.39404296875;
	setAttr ".hyp[7].y" -41481.3359375;
	setAttr ".hyp[7].nvs" 2144;
	setAttr ".hyp[8].x" 1544.945068359375;
	setAttr ".hyp[8].y" -41568.4375;
	setAttr ".hyp[8].nvs" 2528;
	setAttr ".hyp[9].x" 3322.711669921875;
	setAttr ".hyp[9].y" -41740.71484375;
	setAttr ".hyp[9].nvs" 2976;
	setAttr ".hyp[10].x" 1804.966064453125;
	setAttr ".hyp[10].y" -41692.25390625;
	setAttr ".hyp[10].nvs" 2912;
	setAttr ".hyp[11].x" 1545.0302734375;
	setAttr ".hyp[11].y" -41692.1796875;
	setAttr ".hyp[11].nvs" 2544;
	setAttr ".hyp[12].x" 2381.18603515625;
	setAttr ".hyp[12].y" -41543.3359375;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 2378.02294921875;
	setAttr ".hyp[13].y" -41688.703125;
	setAttr ".hyp[13].nvs" 2640;
	setAttr ".hyp[14].x" 3346.007080078125;
	setAttr ".hyp[14].y" -41438.02734375;
	setAttr ".hyp[14].nvs" 2944;
	setAttr ".hyp[15].x" 2388.76318359375;
	setAttr ".hyp[15].y" -41422;
	setAttr ".hyp[15].nvs" 2640;
	setAttr ".hyp[16].x" 3347.930419921875;
	setAttr ".hyp[16].y" -41532.8515625;
	setAttr ".hyp[16].nvs" 2912;
	setAttr ".anf" yes;
createNode animCurveTU -n "emitterSmall_rate";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  99 5 100 0;
createNode animCurveTU -n "emitterLarge_rate";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  100 0 101 1 200 20 201 0;
createNode animCurveTA -n "emitterLarge_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  100 -4.4344376079368395 200 23.821551971891189;
createNode animCurveTA -n "emitterLarge_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  100 29.958931854973891 200 -15.317786373395428;
createNode animCurveTA -n "emitterLarge_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  100 -16.728608206724683 200 -31.26167560853747;
createNode hyperGraphInfo -n "nodeView3";
createNode hyperView -n "hyperView4";
	setAttr ".vl" -type "double2" 1585.6030243481844 -42081.85109947186 ;
	setAttr ".vh" -type "double2" 3903.1518824466161 -41412.471804506255 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView3";
createNode hyperLayout -n "hyperLayout4";
	setAttr ".ihi" 0;
	setAttr -s 19 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 2641.2060546875;
	setAttr ".hyp[2].y" -41479.17578125;
	setAttr ".hyp[2].nvs" 1648;
	setAttr ".hyp[3].x" 3325.853515625;
	setAttr ".hyp[3].y" -41836.45703125;
	setAttr ".hyp[3].nvs" 2944;
	setAttr ".hyp[4].x" 3703.021484375;
	setAttr ".hyp[4].y" -41642.3359375;
	setAttr ".hyp[4].nvs" 1888;
	setAttr ".hyp[5].x" 3049.875;
	setAttr ".hyp[5].y" -41476.765625;
	setAttr ".hyp[5].nvs" 2864;
	setAttr ".hyp[6].x" 1805.3056640625;
	setAttr ".hyp[6].y" -41568.2578125;
	setAttr ".hyp[6].nvs" 2832;
	setAttr ".hyp[7].x" 2822.39404296875;
	setAttr ".hyp[7].y" -41481.3359375;
	setAttr ".hyp[7].nvs" 2144;
	setAttr ".hyp[8].x" 1544.945068359375;
	setAttr ".hyp[8].y" -41568.4375;
	setAttr ".hyp[8].nvs" 2528;
	setAttr ".hyp[9].x" 3322.711669921875;
	setAttr ".hyp[9].y" -41740.71484375;
	setAttr ".hyp[9].nvs" 2976;
	setAttr ".hyp[10].x" 1804.966064453125;
	setAttr ".hyp[10].y" -41692.25390625;
	setAttr ".hyp[10].nvs" 2912;
	setAttr ".hyp[11].x" 1545.0302734375;
	setAttr ".hyp[11].y" -41692.1796875;
	setAttr ".hyp[11].nvs" 2544;
	setAttr ".hyp[12].x" 2381.18603515625;
	setAttr ".hyp[12].y" -41543.3359375;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 2378.02294921875;
	setAttr ".hyp[13].y" -41688.703125;
	setAttr ".hyp[13].nvs" 2640;
	setAttr ".hyp[14].x" 3346.007080078125;
	setAttr ".hyp[14].y" -41438.02734375;
	setAttr ".hyp[14].nvs" 2944;
	setAttr ".hyp[15].x" 2388.76318359375;
	setAttr ".hyp[15].y" -41422;
	setAttr ".hyp[15].nvs" 2640;
	setAttr ".hyp[16].x" 3347.930419921875;
	setAttr ".hyp[16].y" -41532.8515625;
	setAttr ".hyp[16].nvs" 2912;
	setAttr ".hyp[17].x" 3306.5751953125;
	setAttr ".hyp[17].y" -41938.26953125;
	setAttr ".hyp[17].nvs" 3152;
	setAttr ".hyp[18].nvs" 2384;
	setAttr ".anf" yes;
createNode animCurveTA -n "emitterLarge_rotateX1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  100 -4.4344376079368395 155 -3.3803765915411499
		 200 -4.4296516659814857;
createNode animCurveTA -n "emitterLarge_rotateY1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  100 29.958931854973891 155 1.8263128152212997
		 200 -29.851572683626937;
createNode animCurveTA -n "emitterLarge_rotateZ1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  100 -16.728608206724683 155 14.94362725790941
		 200 -12.302628579006488;
createNode displayLayer -n "layer1";
	setAttr ".do" 1;
createNode lambert -n "lambert4";
	setAttr ".c" -type "float3" 0.64099997 0.31010512 0.21858101 ;
createNode shadingEngine -n "lambert4SG";
	setAttr ".ihi" 0;
	setAttr -s 10 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 10 ".gn";
createNode materialInfo -n "materialInfo5";
createNode groupId -n "groupId18";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeView4";
createNode hyperView -n "hyperView5";
	setAttr ".vl" -type "double2" 1393.0780786319883 -42209.81203141428 ;
	setAttr ".vh" -type "double2" 4395.5938306994158 -41167.053506699769 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView4";
createNode hyperLayout -n "hyperLayout5";
	setAttr ".ihi" 0;
	setAttr -s 21 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 2641.2060546875;
	setAttr ".hyp[2].y" -41479.17578125;
	setAttr ".hyp[2].nvs" 1648;
	setAttr ".hyp[3].x" 3325.853515625;
	setAttr ".hyp[3].y" -41836.45703125;
	setAttr ".hyp[3].nvs" 2944;
	setAttr ".hyp[4].x" 3703.021484375;
	setAttr ".hyp[4].y" -41642.3359375;
	setAttr ".hyp[4].nvs" 1888;
	setAttr ".hyp[5].x" 3049.875;
	setAttr ".hyp[5].y" -41476.765625;
	setAttr ".hyp[5].nvs" 2864;
	setAttr ".hyp[6].x" 1805.3056640625;
	setAttr ".hyp[6].y" -41568.2578125;
	setAttr ".hyp[6].nvs" 2832;
	setAttr ".hyp[7].x" 2822.39404296875;
	setAttr ".hyp[7].y" -41481.3359375;
	setAttr ".hyp[7].nvs" 2144;
	setAttr ".hyp[8].x" 1544.945068359375;
	setAttr ".hyp[8].y" -41568.4375;
	setAttr ".hyp[8].nvs" 2528;
	setAttr ".hyp[9].x" 3322.711669921875;
	setAttr ".hyp[9].y" -41740.71484375;
	setAttr ".hyp[9].nvs" 2976;
	setAttr ".hyp[10].x" 1804.966064453125;
	setAttr ".hyp[10].y" -41692.25390625;
	setAttr ".hyp[10].nvs" 2912;
	setAttr ".hyp[11].x" 1545.0302734375;
	setAttr ".hyp[11].y" -41692.1796875;
	setAttr ".hyp[11].nvs" 2544;
	setAttr ".hyp[12].x" 2381.18603515625;
	setAttr ".hyp[12].y" -41543.3359375;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 2378.02294921875;
	setAttr ".hyp[13].y" -41688.703125;
	setAttr ".hyp[13].nvs" 2640;
	setAttr ".hyp[14].x" 3346.007080078125;
	setAttr ".hyp[14].y" -41438.02734375;
	setAttr ".hyp[14].nvs" 2944;
	setAttr ".hyp[15].x" 2386.91064453125;
	setAttr ".hyp[15].y" -41384.94921875;
	setAttr ".hyp[15].nvs" 2640;
	setAttr ".hyp[16].x" 3347.930419921875;
	setAttr ".hyp[16].y" -41532.8515625;
	setAttr ".hyp[16].nvs" 2912;
	setAttr ".hyp[17].x" 3306.5751953125;
	setAttr ".hyp[17].y" -41938.26953125;
	setAttr ".hyp[17].nvs" 3152;
	setAttr ".hyp[18].x" 3345.620361328125;
	setAttr ".hyp[18].y" -41333.65625;
	setAttr ".hyp[18].nvs" 3152;
	setAttr ".hyp[19].x" 3623.512939453125;
	setAttr ".hyp[19].y" -41938.53515625;
	setAttr ".hyp[19].nvs" 2480;
	setAttr ".hyp[20].x" 3654.134765625;
	setAttr ".hyp[20].y" -41334.734375;
	setAttr ".hyp[20].nvs" 3168;
	setAttr ".anf" yes;
createNode textureToArray -n "textureToArray1";
	setAttr ".odt" 1;
createNode attributeTransfer -n "attributeTransfer3";
	setAttr ".wgt" yes;
	setAttr -s 2 ".k[0:1]"  0 1 2 1 0 1;
createNode arrayDataContainer -n "arrayDataContainer4";
	setAttr ".s" 0.24096385620713945;
	setAttr ".st" 1;
	setAttr ".et" 300;
createNode timeToUnitConversion -n "timeToUnitConversion5";
	setAttr ".cf" 0.004;
createNode hyperGraphInfo -n "nodeView5";
createNode hyperView -n "hyperView6";
	setAttr ".vl" -type "double2" 1317.1483746336344 -41888.316374782298 ;
	setAttr ".vh" -type "double2" 4078.4308504535793 -41211.247984114416 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView5";
createNode hyperLayout -n "hyperLayout6";
	setAttr ".ihi" 0;
	setAttr -s 25 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 2635.749755859375;
	setAttr ".hyp[2].y" -41468.26171875;
	setAttr ".hyp[2].nvs" 1648;
	setAttr ".hyp[3].x" 3325.853515625;
	setAttr ".hyp[3].y" -41836.45703125;
	setAttr ".hyp[3].nvs" 2944;
	setAttr ".hyp[4].x" 3703.021484375;
	setAttr ".hyp[4].y" -41642.3359375;
	setAttr ".hyp[4].nvs" 1888;
	setAttr ".hyp[5].x" 3033.50634765625;
	setAttr ".hyp[5].y" -41479.4921875;
	setAttr ".hyp[5].nvs" 2864;
	setAttr ".hyp[6].x" 1805.3056640625;
	setAttr ".hyp[6].y" -41568.2578125;
	setAttr ".hyp[6].nvs" 2832;
	setAttr ".hyp[7].x" 2822.39404296875;
	setAttr ".hyp[7].y" -41481.3359375;
	setAttr ".hyp[7].nvs" 2144;
	setAttr ".hyp[8].x" 1544.945068359375;
	setAttr ".hyp[8].y" -41568.4375;
	setAttr ".hyp[8].nvs" 2528;
	setAttr ".hyp[9].x" 3322.711669921875;
	setAttr ".hyp[9].y" -41740.71484375;
	setAttr ".hyp[9].nvs" 2976;
	setAttr ".hyp[10].x" 1804.966064453125;
	setAttr ".hyp[10].y" -41692.25390625;
	setAttr ".hyp[10].nvs" 2912;
	setAttr ".hyp[11].x" 1545.0302734375;
	setAttr ".hyp[11].y" -41692.1796875;
	setAttr ".hyp[11].nvs" 2544;
	setAttr ".hyp[12].x" 2381.18603515625;
	setAttr ".hyp[12].y" -41543.3359375;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 2378.02294921875;
	setAttr ".hyp[13].y" -41688.703125;
	setAttr ".hyp[13].nvs" 2640;
	setAttr ".hyp[14].x" 3366.4677734375;
	setAttr ".hyp[14].y" -41418.9296875;
	setAttr ".hyp[14].nvs" 2944;
	setAttr ".hyp[15].x" 2388.274658203125;
	setAttr ".hyp[15].y" -41385.125;
	setAttr ".hyp[15].nvs" 2640;
	setAttr ".hyp[16].x" 3352.0224609375;
	setAttr ".hyp[16].y" -41538.30859375;
	setAttr ".hyp[16].nvs" 2912;
	setAttr ".hyp[17].x" 3306.5751953125;
	setAttr ".hyp[17].y" -41938.26953125;
	setAttr ".hyp[17].nvs" 3152;
	setAttr ".hyp[18].x" 3340.5908203125;
	setAttr ".hyp[18].y" -41242.8203125;
	setAttr ".hyp[18].nvs" 3152;
	setAttr ".hyp[19].x" 3623.512939453125;
	setAttr ".hyp[19].y" -41938.53515625;
	setAttr ".hyp[19].nvs" 2480;
	setAttr ".hyp[20].x" 3673.410888671875;
	setAttr ".hyp[20].y" -41334.734375;
	setAttr ".hyp[20].nvs" 3168;
	setAttr ".hyp[21].x" 3071.979736328125;
	setAttr ".hyp[21].y" -41251.76953125;
	setAttr ".hyp[21].nvs" 2272;
	setAttr ".hyp[22].x" 2107.463134765625;
	setAttr ".hyp[22].y" -41249.359375;
	setAttr ".hyp[22].nvs" 2448;
	setAttr ".hyp[23].x" 1811.0950927734375;
	setAttr ".hyp[23].y" -41373.921875;
	setAttr ".hyp[23].nvs" 2833;
	setAttr ".hyp[24].x" 2391.91650390625;
	setAttr ".hyp[24].y" -41247.390625;
	setAttr ".hyp[24].nvs" 2640;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "nodeView6";
createNode hyperView -n "hyperView7";
	setAttr ".vl" -type "double2" 1262.2289780111798 -41965.184277537395 ;
	setAttr ".vh" -type "double2" 4498.8874630453874 -41171.553357559555 ;
	setAttr ".dag" no;
	setAttr ".d" -type "string" "nodeView6";
createNode hyperLayout -n "hyperLayout7";
	setAttr ".ihi" 0;
	setAttr -s 25 ".hyp";
	setAttr ".hyp[0].x" 2106.628173828125;
	setAttr ".hyp[0].y" -41690.1875;
	setAttr ".hyp[0].nvs" 2448;
	setAttr ".hyp[1].x" 2095.55419921875;
	setAttr ".hyp[1].y" -41568.77734375;
	setAttr ".hyp[1].nvs" 2448;
	setAttr ".hyp[2].x" 2635.749755859375;
	setAttr ".hyp[2].y" -41468.26171875;
	setAttr ".hyp[2].nvs" 1648;
	setAttr ".hyp[3].x" 3325.853515625;
	setAttr ".hyp[3].y" -41836.45703125;
	setAttr ".hyp[3].nvs" 2944;
	setAttr ".hyp[4].x" 3703.021484375;
	setAttr ".hyp[4].y" -41642.3359375;
	setAttr ".hyp[4].nvs" 1888;
	setAttr ".hyp[5].x" 3033.50634765625;
	setAttr ".hyp[5].y" -41479.4921875;
	setAttr ".hyp[5].nvs" 2864;
	setAttr ".hyp[6].x" 1805.3056640625;
	setAttr ".hyp[6].y" -41568.2578125;
	setAttr ".hyp[6].nvs" 2832;
	setAttr ".hyp[7].x" 2822.39404296875;
	setAttr ".hyp[7].y" -41481.3359375;
	setAttr ".hyp[7].nvs" 2144;
	setAttr ".hyp[8].x" 1544.945068359375;
	setAttr ".hyp[8].y" -41568.4375;
	setAttr ".hyp[8].nvs" 2528;
	setAttr ".hyp[9].x" 3322.711669921875;
	setAttr ".hyp[9].y" -41740.71484375;
	setAttr ".hyp[9].nvs" 2976;
	setAttr ".hyp[10].x" 1804.966064453125;
	setAttr ".hyp[10].y" -41692.25390625;
	setAttr ".hyp[10].nvs" 2912;
	setAttr ".hyp[11].x" 1545.0302734375;
	setAttr ".hyp[11].y" -41692.1796875;
	setAttr ".hyp[11].nvs" 2544;
	setAttr ".hyp[12].x" 2381.18603515625;
	setAttr ".hyp[12].y" -41543.3359375;
	setAttr ".hyp[12].nvs" 2640;
	setAttr ".hyp[13].x" 2378.02294921875;
	setAttr ".hyp[13].y" -41688.703125;
	setAttr ".hyp[13].nvs" 2640;
	setAttr ".hyp[14].x" 3366.4677734375;
	setAttr ".hyp[14].y" -41418.9296875;
	setAttr ".hyp[14].nvs" 2944;
	setAttr ".hyp[15].x" 2388.274658203125;
	setAttr ".hyp[15].y" -41385.125;
	setAttr ".hyp[15].nvs" 2640;
	setAttr ".hyp[16].x" 3352.0224609375;
	setAttr ".hyp[16].y" -41538.30859375;
	setAttr ".hyp[16].nvs" 2912;
	setAttr ".hyp[17].x" 3306.5751953125;
	setAttr ".hyp[17].y" -41938.26953125;
	setAttr ".hyp[17].nvs" 3152;
	setAttr ".hyp[18].x" 3330.952880859375;
	setAttr ".hyp[18].y" -41250.05078125;
	setAttr ".hyp[18].nvs" 3152;
	setAttr ".hyp[19].x" 3623.512939453125;
	setAttr ".hyp[19].y" -41938.53515625;
	setAttr ".hyp[19].nvs" 2480;
	setAttr ".hyp[20].x" 3673.410888671875;
	setAttr ".hyp[20].y" -41334.734375;
	setAttr ".hyp[20].nvs" 3168;
	setAttr ".hyp[21].x" 3071.979736328125;
	setAttr ".hyp[21].y" -41251.76953125;
	setAttr ".hyp[21].nvs" 2272;
	setAttr ".hyp[22].x" 2107.463134765625;
	setAttr ".hyp[22].y" -41249.359375;
	setAttr ".hyp[22].nvs" 2448;
	setAttr ".hyp[23].x" 1811.0950927734375;
	setAttr ".hyp[23].y" -41373.921875;
	setAttr ".hyp[23].nvs" 2833;
	setAttr ".hyp[24].x" 2391.91650390625;
	setAttr ".hyp[24].y" -41247.390625;
	setAttr ".hyp[24].nvs" 2640;
	setAttr ".anf" yes;
createNode file -n "file2";
	setAttr ".ftn" -type "string" "/Users/admin/Desktop/amazingtexturesV3020.jpg";
	setAttr ".ft" 0;
createNode place2dTexture -n "place2dTexture6";
	setAttr ".re" -type "float2" 5 5 ;
createNode grid -n "grid1";
	setAttr ".fc" -type "float3" 0.43700001 0.18936667 0 ;
	setAttr ".lc" -type "float3" 0.671 0.671 0.671 ;
createNode place2dTexture -n "place2dTexture7";
	setAttr ".rf" 90;
	setAttr ".s" yes;
	setAttr ".re" -type "float2" 45 45 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 7 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :defaultTextureList1;
	setAttr -s 3 ".tx";
select -ne :lightList1;
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultLightSet;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "layer1.di" "frontWall.do";
connectAttr "cluster1GroupId.id" "frontWallShape.iog.og[0].gid";
connectAttr "cluster1Set.mwc" "frontWallShape.iog.og[0].gco";
connectAttr "groupId2.id" "frontWallShape.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "frontWallShape.iog.og[1].gco";
connectAttr "deleteComponent1.og" "frontWallShape.i";
connectAttr "polyPlane1.out" "frontWallShape1Orig.i";
connectAttr "layer1.di" "backWall.do";
connectAttr "cluster2GroupId.id" "backWallShape.iog.og[0].gid";
connectAttr "cluster2Set.mwc" "backWallShape.iog.og[0].gco";
connectAttr "groupId4.id" "backWallShape.iog.og[1].gid";
connectAttr "tweakSet2.mwc" "backWallShape.iog.og[1].gco";
connectAttr "peak1GroupId.id" "backWallShape.iog.og[2].gid";
connectAttr "peak1Set.mwc" "backWallShape.iog.og[2].gco";
connectAttr "groupId6.id" "backWallShape.iog.og[3].gid";
connectAttr "tweakSet3.mwc" "backWallShape.iog.og[3].gco";
connectAttr "peak1.og[0]" "backWallShape.i";
connectAttr "tweak3.vl[0].vt[0]" "backWallShape.twl";
connectAttr "polyCube1.out" "backWallShape1Orig.i";
connectAttr "boundingObject1.m" "boundingObjectShape1r.ipm";
connectAttr "smallGunShotShape.wps" "boundingObjectShape1r.ipp";
connectAttr "boundingObject2.m" "boundingObjectShape2.ipm";
connectAttr "largeGunShotShape.wps" "boundingObjectShape2.ipp";
connectAttr "boundingObject4.m" "boundingObjectShape4.ipm";
connectAttr "largeGunShotShape.wps" "boundingObjectShape4.ipp";
connectAttr "pointAttributeToArray1.oppp" "pointCloudParticleEmitter1.ippp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudParticleEmitter1.ivpp";
connectAttr "dustParticlesShape.ifl" "pointCloudParticleEmitter1.full[1]";
connectAttr "dustParticlesShape.tss" "pointCloudParticleEmitter1.dt[1]";
connectAttr "dustParticlesShape.inh" "pointCloudParticleEmitter1.inh[1]";
connectAttr "dustParticlesShape.stt" "pointCloudParticleEmitter1.stt[1]";
connectAttr "dustParticlesShape.sd[0]" "pointCloudParticleEmitter1.sd[1]";
connectAttr ":time1.o" "pointCloudParticleEmitter1.ct";
connectAttr "arrayDataContainer2.oa" "pointCloudParticleEmitter1.irtpp";
connectAttr "layer1.di" "pointCloudParticleEmitter1.do";
connectAttr "LargeInstanceParticleShape.ifl" "pointCloudParticleEmitter2.full[0]"
		;
connectAttr "LargeInstanceParticleShape.tss" "pointCloudParticleEmitter2.dt[0]";
connectAttr "LargeInstanceParticleShape.inh" "pointCloudParticleEmitter2.inh[0]"
		;
connectAttr "LargeInstanceParticleShape.stt" "pointCloudParticleEmitter2.stt[0]"
		;
connectAttr "LargeInstanceParticleShape.sd[0]" "pointCloudParticleEmitter2.sd[0]"
		;
connectAttr ":time1.o" "pointCloudParticleEmitter2.ct";
connectAttr "pointAttributeToArray1.oppp" "pointCloudParticleEmitter2.ippp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudParticleEmitter2.ivpp";
connectAttr "arrayDataContainer4.oa" "pointCloudParticleEmitter2.irtpp";
connectAttr "textureToArray1.oucpp" "pointCloudParticleEmitter2.ius1pp";
connectAttr "textureToArray1.ovcpp" "pointCloudParticleEmitter2.ius2pp";
connectAttr "LargeInstanceParticleShape.ifl" "pointCloudParticleEmitter4.full[0]"
		;
connectAttr "LargeInstanceParticleShape.tss" "pointCloudParticleEmitter4.dt[0]";
connectAttr "LargeInstanceParticleShape.inh" "pointCloudParticleEmitter4.inh[0]"
		;
connectAttr "LargeInstanceParticleShape.stt" "pointCloudParticleEmitter4.stt[0]"
		;
connectAttr "LargeInstanceParticleShape.sd[2]" "pointCloudParticleEmitter4.sd[0]"
		;
connectAttr ":time1.o" "pointCloudParticleEmitter4.ct";
connectAttr "arrayDataContainer2.oa" "pointCloudParticleEmitter4.irtpp";
connectAttr "pointAttributeToArray1.oppp" "pointCloudParticleEmitter4.ippp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudParticleEmitter4.ivpp";
connectAttr "arrayDataContainer4.oa" "pointCloudParticleEmitter3.irtpp";
connectAttr "pointAttributeToArray1.oppp" "pointCloudParticleEmitter3.ippp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudParticleEmitter3.ivpp";
connectAttr "LargeInstanceParticleShape.ifl" "pointCloudParticleEmitter3.full[0]"
		;
connectAttr "LargeInstanceParticleShape.tss" "pointCloudParticleEmitter3.dt[0]";
connectAttr "LargeInstanceParticleShape.inh" "pointCloudParticleEmitter3.inh[0]"
		;
connectAttr "LargeInstanceParticleShape.stt" "pointCloudParticleEmitter3.stt[0]"
		;
connectAttr "LargeInstanceParticleShape.sd[1]" "pointCloudParticleEmitter3.sd[0]"
		;
connectAttr ":time1.o" "pointCloudParticleEmitter3.ct";
connectAttr "fluidShape1.ifl" "pointCloudFluidEmitter1.full[0]";
connectAttr "fluidShape1.ots" "pointCloudFluidEmitter1.dt[0]";
connectAttr "fluidShape1.inh" "pointCloudFluidEmitter1.inh[0]";
connectAttr "fluidShape1.sti" "pointCloudFluidEmitter1.stt[0]";
connectAttr "fluidShape1.sd[0]" "pointCloudFluidEmitter1.sd[0]";
connectAttr ":time1.o" "pointCloudFluidEmitter1.ct";
connectAttr "arrayDataContainer2.oa" "pointCloudFluidEmitter1.irtpp";
connectAttr "pointAttributeToArray1.oppp" "pointCloudFluidEmitter1.ippp";
connectAttr "layer1.di" "pointCloudFluidEmitter1.do";
connectAttr "fluidShape1.ifl" "pointCloudFluidEmitter2.full[0]";
connectAttr "fluidShape1.ots" "pointCloudFluidEmitter2.dt[0]";
connectAttr "fluidShape1.inh" "pointCloudFluidEmitter2.inh[0]";
connectAttr "fluidShape1.sti" "pointCloudFluidEmitter2.stt[0]";
connectAttr "fluidShape1.sd[1]" "pointCloudFluidEmitter2.sd[0]";
connectAttr ":time1.o" "pointCloudFluidEmitter2.ct";
connectAttr "pointAttributeToArray1.oppp" "pointCloudFluidEmitter2.ippp";
connectAttr "arrayDataContainer3.oa" "pointCloudFluidEmitter2.irtpp";
connectAttr "layer1.di" "pointCloudFluidEmitter2.do";
connectAttr ":time1.o" "emitterLarge.ct";
connectAttr "largeGunShotShape.ifl" "emitterLarge.full[1]";
connectAttr "largeGunShotShape.tss" "emitterLarge.dt[1]";
connectAttr "largeGunShotShape.inh" "emitterLarge.inh[1]";
connectAttr "largeGunShotShape.stt" "emitterLarge.stt[1]";
connectAttr "largeGunShotShape.sd[0]" "emitterLarge.sd[1]";
connectAttr "emitterLarge_rate.o" "emitterLarge.rat";
connectAttr "emitter1_rotateX.o" "emitterSmall.rx";
connectAttr "emitter1_rotateZ.o" "emitterSmall.rz";
connectAttr "emitter1_rotateY.o" "emitterSmall.ry";
connectAttr ":time1.o" "emitterSmall.ct";
connectAttr "smallGunShotShape.ifl" "emitterSmall.full[0]";
connectAttr "smallGunShotShape.tss" "emitterSmall.dt[0]";
connectAttr "smallGunShotShape.inh" "emitterSmall.inh[0]";
connectAttr "smallGunShotShape.stt" "emitterSmall.stt[0]";
connectAttr "smallGunShotShape.sd[0]" "emitterSmall.sd[0]";
connectAttr "emitterSmall_rate.o" "emitterSmall.rat";
connectAttr ":time1.o" "smallGunShotShape.cti";
connectAttr "nucleus1.noao[0]" "smallGunShotShape.nxst";
connectAttr "nucleus1.stf" "smallGunShotShape.stf";
connectAttr "emitterSmall.ot[0]" "smallGunShotShape.npt[0]";
connectAttr "smallGunShotShape.inor" "smallGunShotShape.opacityPP";
connectAttr "smallGunShotShape.inrr" "smallGunShotShape.radiusPP";
connectAttr "smallGunShotShape.pos" "smallGunShotShape.xi[0]";
connectAttr "smallGunShotShape.xo[0]" "smallGunShotShape.lifespanPP";
connectAttr ":time1.o" "dustParticlesShape.cti";
connectAttr "pointCloudParticleEmitter1.ot[1]" "dustParticlesShape.npt[0]";
connectAttr "gravityField1.of[0]" "dustParticlesShape.ifc[0]";
connectAttr "dustParticlesShape.pos" "dustParticlesShape.xi[0]";
connectAttr "dustParticlesShape.xo[0]" "dustParticlesShape.lifespanPP";
connectAttr ":time1.o" "largeGunShotShape.cti";
connectAttr "nucleus1.noao[2]" "largeGunShotShape.nxst";
connectAttr "nucleus1.stf" "largeGunShotShape.stf";
connectAttr "largeGunShotShape.pos" "largeGunShotShape.xi[0]";
connectAttr "emitterLarge.ot[1]" "largeGunShotShape.npt[0]";
connectAttr "largeGunShotShape.xo[0]" "largeGunShotShape.lifespanPP";
connectAttr ":time1.o" "LargeInstanceParticleShape.cti";
connectAttr "pointCloudParticleEmitter2.ot[0]" "LargeInstanceParticleShape.npt[0]"
		;
connectAttr "pointCloudParticleEmitter3.ot[0]" "LargeInstanceParticleShape.npt[1]"
		;
connectAttr "pointCloudParticleEmitter4.ot[0]" "LargeInstanceParticleShape.npt[2]"
		;
connectAttr "backWallShape.w" "LargeInstanceParticleShape.ggeo[0]";
connectAttr "LargeInstanceParticleShape.pos" "LargeInstanceParticleShape.xi[0]";
connectAttr "LargeInstanceParticleShape.eid" "LargeInstanceParticleShape.xi[1]";
connectAttr "LargeInstanceParticleShape.userScalar1PP" "LargeInstanceParticleShape.xi[2]"
		;
connectAttr "LargeInstanceParticleShape.userScalar2PP" "LargeInstanceParticleShape.xi[3]"
		;
connectAttr "gravityField1.of[1]" "LargeInstanceParticleShape.ifc[0]";
connectAttr "LargeInstanceParticleShape.xo[0]" "LargeInstanceParticleShape.lifespanPP"
		;
connectAttr "LargeInstanceParticleShape.xo[1]" "LargeInstanceParticleShape.myRotPP"
		;
connectAttr "LargeInstanceParticleShape.xo[2]" "LargeInstanceParticleShape.myIndexPP"
		;
connectAttr "LargeInstanceParticleShape.xo[3]" "LargeInstanceParticleShape.myScalePP"
		;
connectAttr "LargeInstanceParticleShape.xo[4]" "LargeInstanceParticleShape.goalPP"
		;
connectAttr "LargeInstanceParticleShape.xo[5]" "LargeInstanceParticleShape.goalU"
		;
connectAttr "LargeInstanceParticleShape.xo[6]" "LargeInstanceParticleShape.goalV"
		;
connectAttr "layer1.di" "fluid1.do";
connectAttr ":time1.o" "fluidShape1.cti";
connectAttr "pointCloudFluidEmitter1.ef" "fluidShape1.eml[0].emfr";
connectAttr "pointCloudFluidEmitter2.ef" "fluidShape1.eml[2].emfr";
connectAttr "pointCloudFluidEmitter1.efc" "fluidShape1.fce[0]";
connectAttr "pointCloudFluidEmitter2.efc" "fluidShape1.fce[1]";
connectAttr "pointCloudField1.of[0]" "fluidShape1.ifc[0]";
connectAttr "pointCloudField2.of[0]" "fluidShape1.ifc[1]";
connectAttr ":time1.o" "nucleus1.cti";
connectAttr "smallGunShotShape.cust" "nucleus1.niao[0]";
connectAttr "largeGunShotShape.cust" "nucleus1.niao[2]";
connectAttr "smallGunShotShape.stst" "nucleus1.nias[0]";
connectAttr "largeGunShotShape.stst" "nucleus1.nias[2]";
connectAttr "layer1.di" "nucleus1.do";
connectAttr "fluidShape1.fd" "pointCloudField1.ind[0]";
connectAttr "arrayDataContainer2.oa" "pointCloudField1.impp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudField1.idpp";
connectAttr "pointAttributeToArray1.oppp" "pointCloudField1.ippp";
connectAttr "layer1.di" "pointCloudField1.do";
connectAttr "fluidShape1.fd" "pointCloudField2.ind[0]";
connectAttr "pointAttributeToArray1.oppp" "pointCloudField2.ippp";
connectAttr "pointAttributeToArray1.onpp" "pointCloudField2.idpp";
connectAttr "arrayDataContainer3.oa" "pointCloudField2.impp";
connectAttr "layer1.di" "pointCloudField2.do";
connectAttr "dustParticlesShape.fd" "gravityField1.ind[0]";
connectAttr "LargeInstanceParticleShape.fd" "gravityField1.ind[1]";
connectAttr "dustParticlesShape.ppfd[0]" "gravityField1.ppda[0]";
connectAttr "LargeInstanceParticleShape.ppfd[0]" "gravityField1.ppda[1]";
connectAttr "layer1.di" "gravityField1.do";
connectAttr "groupId18.id" "polySurfaceShape10.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape10.iog.og[0].gco";
connectAttr "groupId19.id" "polySurfaceShape9.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape9.iog.og[0].gco";
connectAttr "groupId20.id" "polySurfaceShape8.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape8.iog.og[0].gco";
connectAttr "groupId21.id" "polySurfaceShape7.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape7.iog.og[0].gco";
connectAttr "groupId22.id" "polySurfaceShape6.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape6.iog.og[0].gco";
connectAttr "groupId23.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "groupId24.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "groupId25.id" "polySurfaceShape3.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape3.iog.og[0].gco";
connectAttr "groupId26.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupId27.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "polySurfaceShape1.iog.og[0].gco";
connectAttr "LargeInstanceParticleShape.idt[0].ipd" "instancer1.inp";
connectAttr "polySurface10.m" "instancer1.inh[0]";
connectAttr "polySurface9.m" "instancer1.inh[1]";
connectAttr "polySurface8.m" "instancer1.inh[2]";
connectAttr "polySurface7.m" "instancer1.inh[3]";
connectAttr "polySurface6.m" "instancer1.inh[4]";
connectAttr "polySurface5.m" "instancer1.inh[5]";
connectAttr "polySurface4.m" "instancer1.inh[6]";
connectAttr "polySurface3.m" "instancer1.inh[7]";
connectAttr "polySurface2.m" "instancer1.inh[8]";
connectAttr "polySurface1.m" "instancer1.inh[9]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "nParticleBallsSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "fluidShape1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "nParticleBallsSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "fluidShape1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "cluster1GroupParts.og" "cluster1.ip[0].ig";
connectAttr "cluster1GroupId.id" "cluster1.ip[0].gi";
connectAttr "cluster1Handle.wm" "cluster1.ma";
connectAttr "cluster1HandleShape.x" "cluster1.x";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "cluster1GroupId.msg" "cluster1Set.gn" -na;
connectAttr "frontWallShape.iog.og[0]" "cluster1Set.dsm" -na;
connectAttr "cluster1.msg" "cluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "cluster1GroupParts.ig";
connectAttr "cluster1GroupId.id" "cluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "frontWallShape.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "frontWallShape1Orig.w" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "hyperView1.msg" "nodeEditorPanel1Info.b[0]";
connectAttr "hyperLayout1.msg" "hyperView1.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout1.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout1.hyp[1].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout1.hyp[2].dn";
connectAttr "fluidShape1.msg" "hyperLayout1.hyp[3].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout1.hyp[4].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout1.hyp[5].dn";
connectAttr "backWallShape.msg" "hyperLayout1.hyp[6].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout1.hyp[7].dn";
connectAttr "pointCloudField1.msg" "hyperLayout1.hyp[8].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout1.hyp[9].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout1.hyp[10].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout1.hyp[11].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout1.hyp[12].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout1.hyp[13].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout1.hyp[14].dn";
connectAttr "pointCloudField2.msg" "hyperLayout1.hyp[15].dn";
connectAttr "pointCloudParticleEmitter1.msg" "hyperLayout1.hyp[16].dn";
connectAttr "pointCloudParticleEmitter2.msg" "hyperLayout1.hyp[17].dn";
connectAttr "dustParticlesShape.msg" "hyperLayout1.hyp[18].dn";
connectAttr "LargeInstanceParticleShape.msg" "hyperLayout1.hyp[19].dn";
connectAttr "textureToArray1.msg" "hyperLayout1.hyp[20].dn";
connectAttr "attributeTransfer3.msg" "hyperLayout1.hyp[21].dn";
connectAttr "boundingObjectShape4.msg" "hyperLayout1.hyp[22].dn";
connectAttr "arrayDataContainer4.msg" "hyperLayout1.hyp[23].dn";
connectAttr "pointCloudParticleEmitter3.msg" "hyperLayout1.hyp[24].dn";
connectAttr "pointCloudParticleEmitter4.msg" "hyperLayout1.hyp[25].dn";
connectAttr "PARTICLES____________________.msg" "hyperLayout1.hyp[26].dn";
connectAttr "EMITTERS____________________.msg" "hyperLayout1.hyp[27].dn";
connectAttr "FIELDS_________________.msg" "hyperLayout1.hyp[29].dn";
connectAttr "SOUP_AUX_NODES____________________1.msg" "hyperLayout1.hyp[30].dn";
connectAttr "grid1.msg" "hyperLayout1.hyp[31].dn";
connectAttr "place2dTexture7.msg" "hyperLayout1.hyp[32].dn";
connectAttr "timeToUnitConversion1.o" "group1.ctm";
connectAttr "boundingObjectShape2.od" "group1.bos[0]";
connectAttr "boundingObjectShape2.opm" "group1.bos[0].bpm";
connectAttr "boundingObjectShape1r.od" "group1.bos[1]";
connectAttr "boundingObjectShape1r.opm" "group1.bos[1].bpm";
connectAttr "cluster1.og[0]" "group1.ig";
connectAttr ":time1.o" "timeToUnitConversion1.i";
connectAttr "group1.og" "deleteComponent1.ig";
connectAttr "group1.oc" "deleteComponent1.dc";
connectAttr "grid1.oc" "lambert2.c";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "frontWallShape.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "grid1.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "cluster2GroupParts.og" "cluster2.ip[0].ig";
connectAttr "cluster2GroupId.id" "cluster2.ip[0].gi";
connectAttr "cluster2Handle.wm" "cluster2.ma";
connectAttr "cluster2HandleShape.x" "cluster2.x";
connectAttr "groupParts4.og" "tweak2.ip[0].ig";
connectAttr "groupId4.id" "tweak2.ip[0].gi";
connectAttr "cluster2GroupId.msg" "cluster2Set.gn" -na;
connectAttr "backWallShape.iog.og[0]" "cluster2Set.dsm" -na;
connectAttr "cluster2.msg" "cluster2Set.ub[0]";
connectAttr "tweak2.og[0]" "cluster2GroupParts.ig";
connectAttr "cluster2GroupId.id" "cluster2GroupParts.gi";
connectAttr "groupId4.msg" "tweakSet2.gn" -na;
connectAttr "backWallShape.iog.og[1]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "backWallShape1Orig.w" "groupParts4.ig";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "npBallsBlinn.oc" "nParticleBallsSE.ss";
connectAttr "npBallsVolume.oi" "nParticleBallsSE.vs";
connectAttr "smallGunShotShape.iog" "nParticleBallsSE.dsm" -na;
connectAttr "largeGunShotShape.iog" "nParticleBallsSE.dsm" -na;
connectAttr "nParticleBallsSE.msg" "materialInfo2.sg";
connectAttr "npBallsBlinn.msg" "materialInfo2.m";
connectAttr "particleSamplerInfo1.msg" "materialInfo2.t" -na;
connectAttr "particleSamplerInfo1.oc" "npBallsBlinn.c";
connectAttr "particleSamplerInfo1.ot" "npBallsBlinn.it";
connectAttr "particleSamplerInfo1.oi" "npBallsBlinn.ic";
connectAttr "particleSamplerInfo1.ot" "npBallsVolume.t";
connectAttr "particleSamplerInfo1.oc" "npBallsVolume.c";
connectAttr "particleSamplerInfo1.oi" "npBallsVolume.i";
connectAttr "hyperView2.msg" "nodeView1.b[0]";
connectAttr "hyperLayout2.msg" "hyperView2.hl";
connectAttr "group1.msg" "hyperLayout2.hyp[0].dn";
connectAttr "frontWallShape.msg" "hyperLayout2.hyp[1].dn";
connectAttr "deleteComponent1.msg" "hyperLayout2.hyp[2].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout2.hyp[3].dn";
connectAttr "cluster2.og[0]" "attributeTransfer1.ig";
connectAttr "boundingObjectShape1r.od" "attributeTransfer1.bos[0]";
connectAttr "boundingObjectShape1r.opm" "attributeTransfer1.bos[0].bpm";
connectAttr "peak1GroupParts.og" "peak1.ip[0].ig";
connectAttr "peak1GroupId.id" "peak1.ip[0].gi";
connectAttr "arrayDataContainer1.oa" "peak1.iwpp";
connectAttr "groupParts6.og" "tweak3.ip[0].ig";
connectAttr "groupId6.id" "tweak3.ip[0].gi";
connectAttr "peak1GroupId.msg" "peak1Set.gn" -na;
connectAttr "backWallShape.iog.og[2]" "peak1Set.dsm" -na;
connectAttr "peak1.msg" "peak1Set.ub[0]";
connectAttr "tweak3.og[0]" "peak1GroupParts.ig";
connectAttr "peak1GroupId.id" "peak1GroupParts.gi";
connectAttr "groupId6.msg" "tweakSet3.gn" -na;
connectAttr "backWallShape.iog.og[3]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "attributeTransfer1.og" "groupParts6.ig";
connectAttr "groupId6.id" "groupParts6.gi";
connectAttr "timeToUnitConversion2.o" "arrayDataContainer1.it";
connectAttr "attributeTransfer2.owpp" "arrayDataContainer1.ia";
connectAttr ":time1.o" "timeToUnitConversion2.i";
connectAttr "file2.oc" "lambert3.c";
connectAttr "lambert3.oc" "lambert3SG.ss";
connectAttr "backWallShape.iog" "lambert3SG.dsm" -na;
connectAttr "lambert3SG.msg" "materialInfo3.sg";
connectAttr "lambert3.msg" "materialInfo3.m";
connectAttr "file2.msg" "materialInfo3.t" -na;
connectAttr "backWallShape.o" "pointAttributeToArray1.ig";
connectAttr "timeToUnitConversion3.o" "arrayDataContainer2.it";
connectAttr "attributeTransfer1.owpp" "arrayDataContainer2.ia";
connectAttr ":time1.o" "timeToUnitConversion3.i";
connectAttr "fluidShape1.ocl" "fluidShape1SG.vs";
connectAttr "fluidShape1.iog" "fluidShape1SG.dsm" -na;
connectAttr "fluidShape1SG.msg" "materialInfo4.sg";
connectAttr "boundingObjectShape2.od" "attributeTransfer2.bos[1]";
connectAttr "boundingObjectShape2.opm" "attributeTransfer2.bos[1].bpm";
connectAttr "cluster2.og[0]" "attributeTransfer2.ig";
connectAttr "timeToUnitConversion4.o" "arrayDataContainer3.it";
connectAttr "attributeTransfer2.owpp" "arrayDataContainer3.ia";
connectAttr ":time1.o" "timeToUnitConversion4.i";
connectAttr "hyperView3.msg" "nodeView2.b[0]";
connectAttr "hyperLayout3.msg" "hyperView3.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout3.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout3.hyp[1].dn";
connectAttr "peak1.msg" "hyperLayout3.hyp[2].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout3.hyp[3].dn";
connectAttr "fluidShape1.msg" "hyperLayout3.hyp[4].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout3.hyp[5].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout3.hyp[6].dn";
connectAttr "backWallShape.msg" "hyperLayout3.hyp[7].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout3.hyp[8].dn";
connectAttr "pointCloudField1.msg" "hyperLayout3.hyp[9].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout3.hyp[10].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout3.hyp[11].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout3.hyp[12].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout3.hyp[13].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout3.hyp[14].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout3.hyp[15].dn";
connectAttr "pointCloudField2.msg" "hyperLayout3.hyp[16].dn";
connectAttr "hyperView4.msg" "nodeView3.b[0]";
connectAttr "hyperLayout4.msg" "hyperView4.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout4.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout4.hyp[1].dn";
connectAttr "peak1.msg" "hyperLayout4.hyp[2].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout4.hyp[3].dn";
connectAttr "fluidShape1.msg" "hyperLayout4.hyp[4].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout4.hyp[5].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout4.hyp[6].dn";
connectAttr "backWallShape.msg" "hyperLayout4.hyp[7].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout4.hyp[8].dn";
connectAttr "pointCloudField1.msg" "hyperLayout4.hyp[9].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout4.hyp[10].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout4.hyp[11].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout4.hyp[12].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout4.hyp[13].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout4.hyp[14].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout4.hyp[15].dn";
connectAttr "pointCloudField2.msg" "hyperLayout4.hyp[16].dn";
connectAttr "pointCloudParticleEmitter1.msg" "hyperLayout4.hyp[17].dn";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "lambert4.oc" "lambert4SG.ss";
connectAttr "polySurfaceShape10.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape9.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape8.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape7.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape3.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "polySurfaceShape1.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "groupId18.msg" "lambert4SG.gn" -na;
connectAttr "groupId19.msg" "lambert4SG.gn" -na;
connectAttr "groupId20.msg" "lambert4SG.gn" -na;
connectAttr "groupId21.msg" "lambert4SG.gn" -na;
connectAttr "groupId22.msg" "lambert4SG.gn" -na;
connectAttr "groupId23.msg" "lambert4SG.gn" -na;
connectAttr "groupId24.msg" "lambert4SG.gn" -na;
connectAttr "groupId25.msg" "lambert4SG.gn" -na;
connectAttr "groupId26.msg" "lambert4SG.gn" -na;
connectAttr "groupId27.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo5.sg";
connectAttr "lambert4.msg" "materialInfo5.m";
connectAttr "hyperView5.msg" "nodeView4.b[0]";
connectAttr "hyperLayout5.msg" "hyperView5.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout5.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout5.hyp[1].dn";
connectAttr "peak1.msg" "hyperLayout5.hyp[2].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout5.hyp[3].dn";
connectAttr "fluidShape1.msg" "hyperLayout5.hyp[4].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout5.hyp[5].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout5.hyp[6].dn";
connectAttr "backWallShape.msg" "hyperLayout5.hyp[7].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout5.hyp[8].dn";
connectAttr "pointCloudField1.msg" "hyperLayout5.hyp[9].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout5.hyp[10].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout5.hyp[11].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout5.hyp[12].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout5.hyp[13].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout5.hyp[14].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout5.hyp[15].dn";
connectAttr "pointCloudField2.msg" "hyperLayout5.hyp[16].dn";
connectAttr "pointCloudParticleEmitter1.msg" "hyperLayout5.hyp[17].dn";
connectAttr "pointCloudParticleEmitter2.msg" "hyperLayout5.hyp[18].dn";
connectAttr "dustParticlesShape.msg" "hyperLayout5.hyp[19].dn";
connectAttr "LargeInstanceParticleShape.msg" "hyperLayout5.hyp[20].dn";
connectAttr "backWallShape.o" "textureToArray1.ig";
connectAttr "boundingObjectShape4.od" "attributeTransfer3.bos[1]";
connectAttr "boundingObjectShape4.opm" "attributeTransfer3.bos[1].bpm";
connectAttr "cluster2.og[0]" "attributeTransfer3.ig";
connectAttr "timeToUnitConversion5.o" "arrayDataContainer4.it";
connectAttr "attributeTransfer3.owpp" "arrayDataContainer4.ia";
connectAttr ":time1.o" "timeToUnitConversion5.i";
connectAttr "hyperView6.msg" "nodeView5.b[0]";
connectAttr "hyperLayout6.msg" "hyperView6.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout6.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout6.hyp[1].dn";
connectAttr "peak1.msg" "hyperLayout6.hyp[2].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout6.hyp[3].dn";
connectAttr "fluidShape1.msg" "hyperLayout6.hyp[4].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout6.hyp[5].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout6.hyp[6].dn";
connectAttr "backWallShape.msg" "hyperLayout6.hyp[7].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout6.hyp[8].dn";
connectAttr "pointCloudField1.msg" "hyperLayout6.hyp[9].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout6.hyp[10].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout6.hyp[11].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout6.hyp[12].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout6.hyp[13].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout6.hyp[14].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout6.hyp[15].dn";
connectAttr "pointCloudField2.msg" "hyperLayout6.hyp[16].dn";
connectAttr "pointCloudParticleEmitter1.msg" "hyperLayout6.hyp[17].dn";
connectAttr "pointCloudParticleEmitter2.msg" "hyperLayout6.hyp[18].dn";
connectAttr "dustParticlesShape.msg" "hyperLayout6.hyp[19].dn";
connectAttr "LargeInstanceParticleShape.msg" "hyperLayout6.hyp[20].dn";
connectAttr "textureToArray1.msg" "hyperLayout6.hyp[21].dn";
connectAttr "attributeTransfer3.msg" "hyperLayout6.hyp[22].dn";
connectAttr "boundingObjectShape4.msg" "hyperLayout6.hyp[23].dn";
connectAttr "arrayDataContainer4.msg" "hyperLayout6.hyp[24].dn";
connectAttr "hyperView7.msg" "nodeView6.b[0]";
connectAttr "hyperLayout7.msg" "hyperView7.hl";
connectAttr "attributeTransfer1.msg" "hyperLayout7.hyp[0].dn";
connectAttr "attributeTransfer2.msg" "hyperLayout7.hyp[1].dn";
connectAttr "peak1.msg" "hyperLayout7.hyp[2].dn";
connectAttr "pointCloudFluidEmitter1.msg" "hyperLayout7.hyp[3].dn";
connectAttr "fluidShape1.msg" "hyperLayout7.hyp[4].dn";
connectAttr "pointAttributeToArray1.msg" "hyperLayout7.hyp[5].dn";
connectAttr "boundingObjectShape2.msg" "hyperLayout7.hyp[6].dn";
connectAttr "backWallShape.msg" "hyperLayout7.hyp[7].dn";
connectAttr "largeGunShotShape.msg" "hyperLayout7.hyp[8].dn";
connectAttr "pointCloudField1.msg" "hyperLayout7.hyp[9].dn";
connectAttr "boundingObjectShape1r.msg" "hyperLayout7.hyp[10].dn";
connectAttr "smallGunShotShape.msg" "hyperLayout7.hyp[11].dn";
connectAttr "arrayDataContainer1.msg" "hyperLayout7.hyp[12].dn";
connectAttr "arrayDataContainer2.msg" "hyperLayout7.hyp[13].dn";
connectAttr "pointCloudFluidEmitter2.msg" "hyperLayout7.hyp[14].dn";
connectAttr "arrayDataContainer3.msg" "hyperLayout7.hyp[15].dn";
connectAttr "pointCloudField2.msg" "hyperLayout7.hyp[16].dn";
connectAttr "pointCloudParticleEmitter1.msg" "hyperLayout7.hyp[17].dn";
connectAttr "pointCloudParticleEmitter2.msg" "hyperLayout7.hyp[18].dn";
connectAttr "dustParticlesShape.msg" "hyperLayout7.hyp[19].dn";
connectAttr "LargeInstanceParticleShape.msg" "hyperLayout7.hyp[20].dn";
connectAttr "textureToArray1.msg" "hyperLayout7.hyp[21].dn";
connectAttr "attributeTransfer3.msg" "hyperLayout7.hyp[22].dn";
connectAttr "boundingObjectShape4.msg" "hyperLayout7.hyp[23].dn";
connectAttr "arrayDataContainer4.msg" "hyperLayout7.hyp[24].dn";
connectAttr "place2dTexture6.c" "file2.c";
connectAttr "place2dTexture6.tf" "file2.tf";
connectAttr "place2dTexture6.rf" "file2.rf";
connectAttr "place2dTexture6.mu" "file2.mu";
connectAttr "place2dTexture6.mv" "file2.mv";
connectAttr "place2dTexture6.s" "file2.s";
connectAttr "place2dTexture6.wu" "file2.wu";
connectAttr "place2dTexture6.wv" "file2.wv";
connectAttr "place2dTexture6.re" "file2.re";
connectAttr "place2dTexture6.of" "file2.of";
connectAttr "place2dTexture6.r" "file2.ro";
connectAttr "place2dTexture6.n" "file2.n";
connectAttr "place2dTexture6.vt1" "file2.vt1";
connectAttr "place2dTexture6.vt2" "file2.vt2";
connectAttr "place2dTexture6.vt3" "file2.vt3";
connectAttr "place2dTexture6.vc1" "file2.vc1";
connectAttr "place2dTexture6.o" "file2.uv";
connectAttr "place2dTexture6.ofs" "file2.fs";
connectAttr "place2dTexture7.o" "grid1.uv";
connectAttr "place2dTexture7.ofs" "grid1.fs";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "nParticleBallsSE.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "fluidShape1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "dustParticlesShape.iog" ":initialParticleSE.dsm" -na;
connectAttr "LargeInstanceParticleShape.iog" ":initialParticleSE.dsm" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "npBallsBlinn.msg" ":defaultShaderList1.s" -na;
connectAttr "npBallsVolume.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert3.msg" ":defaultShaderList1.s" -na;
connectAttr "fluidShape1.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert4.msg" ":defaultShaderList1.s" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "grid1.msg" ":defaultTextureList1.tx" -na;
connectAttr "spotLightShape1.ltd" ":lightList1.l" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture6.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture7.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "spotLight1.iog" ":defaultLightSet.dsm" -na;
// End of brickWallDestroy3.ma
