//Maya ASCII 2011 scene
//Name: arrayDataContainer_1.ma
//Last modified: Tue, Feb 02, 2010 09:47:34 PM
//Codeset: UTF-8
requires maya "2011";
requires "stereoCamera" "10.0";
requires "SOuP" "1.01";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2011";
fileInfo "version" "2011 Hotfix 1 x64";
fileInfo "cutIdentifier" "201004280207-773155";
fileInfo "osv" "Linux 2.6.30.5-43.fc11.x86_64 #1 SMP Thu Aug 27 21:39:52 EDT 2009 x86_64";
createNode transform -s -n "persp";
	setAttr ".t" -type "double3" 41.379688784916581 9.3530617712518609 3.2471495148580791 ;
	setAttr ".r" -type "double3" -6.3383527303475251 90.199999999639985 -1.0177774980683254e-13 ;
	setAttr ".rp" -type "double3" 0 0 3.5527136788005009e-15 ;
	setAttr ".rpt" -type "double3" 3.1181718080088461e-15 -2.6506084188613355e-16 -3.4569977946737768e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 51.740138921245908;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -4.2713135306574292 8.8677704437684302 -9.2225040379435832 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".col" -type "float3" 0.54901963 0.4951942 0.4951942 ;
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode pointEmitter -n "emitter1";
	setAttr ".r" -type "double3" -187.94181672108914 0 0 ;
	setAttr ".s" -type "double3" 0.82293325724290922 0.82293325724290922 0.82293325724290922 ;
	setAttr ".smd" 7;
	setAttr ".emt" 4;
	setAttr ".rat" 10000;
	setAttr ".sro" no;
	setAttr -l on ".urpp";
	setAttr ".d" -type "double3" 0 0 1 ;
	setAttr ".srnd" 10;
	setAttr ".afc" 0;
	setAttr ".alx" 5;
createNode transform -n "pCylinder1" -p "emitter1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.5665982813123598e-15 -1.2103292817537981 -0.16884751252508776 ;
	setAttr ".r" -type "double3" 5 0 0 ;
	setAttr ".s" -type "double3" 1.6404732560243511 1.4063677237840961 1.6404732560243516 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".qsp" 0;
createNode transform -n "nParticle1";
	setAttr ".v" no;
createNode nParticle -n "nParticleShape1" -p "nParticle1";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -sn "rgbPP" -ln "rgbPP" -dt "vectorArray";
	addAttr -ci true -h true -sn "rgbPP0" -ln "rgbPP0" -dt "vectorArray";
	addAttr -ci true -sn "opacityPP" -ln "opacityPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "opacityPP0" -ln "opacityPP0" -dt "doubleArray";
	addAttr -ci true -sn "radiusPP" -ln "radiusPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "radiusPP0" -ln "radiusPP0" -dt "doubleArray";
	addAttr -is true -ci true -sn "betterIllumination" -ln "betterIllumination" -min 
		0 -max 1 -at "bool";
	addAttr -is true -ci true -sn "surfaceShading" -ln "surfaceShading" -min 0 -max 
		1 -at "float";
	addAttr -is true -ci true -sn "flatShaded" -ln "flatShaded" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "event" -ln "event" -dt "doubleArray";
	addAttr -ci true -h true -sn "event0" -ln "event0" -dt "doubleArray";
	addAttr -ci true -sn "eventCountCache" -ln "eventCountCache" -at "long";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".nid" 107082;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr ".irbx" -type "string" "";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "";
	setAttr ".dw" 1.6;
	setAttr ".inh" 1;
	setAttr ".cts" 1;
	setAttr ".chw" 257;
	setAttr ".evn[0]" -type "string" "event0";
	setAttr ".evv[0]"  0;
	setAttr ".ecp[0]"  0;
	setAttr ".eve[0]"  4;
	setAttr ".evs[0]"  0;
	setAttr ".evd[0]"  1;
	setAttr ".evr[0]"  0;
	setAttr ".esp[0]"  0.5;
	setAttr ".epr[0]" -type "string" "";
	setAttr ".prt" 7;
	setAttr ".boce" 0.05000000074505806;
	setAttr ".fron" 0.0099999997764825821;
	setAttr ".adng" 0.0010000000474974513;
	setAttr ".cofl" 1;
	setAttr ".pmss" 12;
	setAttr ".scld" no;
	setAttr -s 2 ".fsc[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".pfdo[0:1]"  0 1 1 1 0 1;
	setAttr ".drg" 0;
	setAttr ".esph" yes;
	setAttr ".incp" 2;
	setAttr ".visc" 0.0010000000474974513;
	setAttr ".vssc[0]"  0 1 1;
	setAttr ".suft" 0.5;
	setAttr ".stns[0]"  0 1 1;
	setAttr ".thr" 0.02;
	setAttr ".ra" 0.125;
	setAttr ".rdc[0]"  0 1 1;
	setAttr ".rci" 1;
	setAttr ".mssc[0]"  0 1 1;
	setAttr ".pfsc[0]"  0 1 1;
	setAttr ".frsc[0]"  0 1 1;
	setAttr ".stsc[0]"  0 1 1;
	setAttr ".clsc[0]"  0 1 1;
	setAttr ".bosc[0]"  0 1 1;
	setAttr ".opc[0]"  0 1 1;
	setAttr ".oci" 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].clp" 0;
	setAttr ".cl[0].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 0 0.5 1 ;
	setAttr ".cl[1].cli" 1;
	setAttr ".coi" 3;
	setAttr ".inca[0].incap" 0;
	setAttr ".inca[0].incac" -type "float3" 0 0 0 ;
	setAttr ".inca[0].incai" 1;
	setAttr ".mstr" 0.75;
	setAttr ".vpvx" no;
	setAttr ".mts" 0.1;
	setAttr ".mtrr" 300;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan" 1;
	setAttr ".rgbPP0" -type "vectorArray" 0 ;
	setAttr ".opacityPP0" -type "doubleArray" 0 ;
	setAttr ".radiusPP0" -type "doubleArray" 0 ;
	setAttr ".event0" -type "doubleArray" 0 ;
createNode transform -n "polySurface1";
	setAttr ".t" -type "double3" 0 0 19.83965174937638 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcol" yes;
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".mvcs" -type "string" "velocityPV";
createNode transform -n "polySurface2";
	setAttr ".it" no;
createNode mesh -n "polySurfaceShape2" -p "polySurface2";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcol" yes;
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 3 ".clst";
	setAttr ".clst[0].clsn" -type "string" "velocityPV";
	setAttr ".clst[1].clsn" -type "string" "colorPV";
	setAttr ".clst[2].clsn" -type "string" "colorSet1";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".mvcs" -type "string" "velocityPV";
createNode transform -n "pCube1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.6434498185288628 4.4762055545940278 -6.5285044998014481 ;
	setAttr ".s" -type "double3" 1 1.283522951587023 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".qsp" 0;
createNode transform -n "nRigid1";
	setAttr ".v" no;
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr -l on ".s";
createNode nRigid -n "nRigidShape1" -p "nRigid1";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".nid" 720;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr -k off ".dve";
	setAttr -k off ".lfm";
	setAttr -k off ".lfr";
	setAttr -k off ".ead";
	setAttr ".irbx" -type "string" "";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "";
	setAttr -k off ".dw";
	setAttr -k off ".fiw";
	setAttr -k off ".con";
	setAttr -k off ".eiw";
	setAttr -k off ".mxc";
	setAttr -k off ".lod";
	setAttr -k off ".inh";
	setAttr ".cts" 1;
	setAttr -k off ".stf";
	setAttr -k off ".igs";
	setAttr -k off ".ecfh";
	setAttr -k off ".tgs";
	setAttr -k off ".gsm";
	setAttr -k off ".chd";
	setAttr ".chw" 257;
	setAttr -k off ".trd";
	setAttr -k off ".prt";
	setAttr ".thss" 0.088746733963489532;
	setAttr ".boce" 0.05000000074505806;
	setAttr ".fron" 0.0099999997764825821;
	setAttr ".actv" no;
	setAttr ".scld" no;
	setAttr ".por" 0.35498693585395813;
	setAttr ".tpc" yes;
	setAttr -s 2 ".fsc[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".pfdo[0:1]"  0 1 1 1 0 1;
	setAttr -k on ".lifespan" 1;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
createNode transform -n "nurbsCircle1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.2518032072955325 13.564109089530584 -6.7852484453306205 ;
createNode nurbsCurve -n "nurbsCircleShape1" -p "nurbsCircle1";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "positionMarker1" -p "nurbsCircleShape1";
createNode positionMarker -n "positionMarkerShape1" -p "positionMarker1";
	setAttr -k off ".v";
	setAttr ".uwo" yes;
	setAttr ".t" 1;
createNode transform -n "positionMarker2" -p "nurbsCircleShape1";
createNode positionMarker -n "positionMarkerShape2" -p "positionMarker2";
	setAttr -k off ".v";
	setAttr ".uwo" yes;
	setAttr ".lp" -type "double3" 1 0 0 ;
	setAttr ".t" 100;
createNode transform -n "mentalrayIbl1";
	setAttr ".v" no;
	setAttr ".s" -type "double3" 281.23135753187648 281.23135753187648 281.23135753187648 ;
createNode mentalrayIblShape -n "mentalrayIblShape1" -p "mentalrayIbl1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".cg" -type "float3" 0.2965591 0.2965591 0.2965591 ;
	setAttr ".tx" -type "string" "/home/generic/_a_download/urban_sky_preview.jpg";
	setAttr ".vien" yes;
	setAttr ".vifg" yes;
createNode transform -n "spotLight1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.051890730277096 7.2791731681943341 -5.7657686133305539 ;
	setAttr ".r" -type "double3" -25.496701290792807 81.702430580179339 -2.3141048281688662e-13 ;
createNode spotLight -n "spotLightShape1" -p "spotLight1";
	setAttr -k off ".v";
	setAttr ".col" 6.4808757155932124;
	setAttr ".ca" 160.04521442038563;
createNode transform -n "nRigid2";
	setAttr ".v" no;
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr -l on ".s";
createNode nRigid -n "nRigidShape2" -p "nRigid2";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".nid" 533;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr -k off ".dve";
	setAttr -k off ".lfm";
	setAttr -k off ".lfr";
	setAttr -k off ".ead";
	setAttr ".irbx" -type "string" "";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "";
	setAttr -k off ".dw";
	setAttr -k off ".fiw";
	setAttr -k off ".con";
	setAttr -k off ".eiw";
	setAttr -k off ".mxc";
	setAttr -k off ".lod";
	setAttr -k off ".inh";
	setAttr ".cts" 1;
	setAttr -k off ".stf";
	setAttr -k off ".igs";
	setAttr -k off ".ecfh";
	setAttr -k off ".tgs";
	setAttr -k off ".gsm";
	setAttr -k off ".chd";
	setAttr ".chw" 257;
	setAttr -k off ".trd";
	setAttr -k off ".prt";
	setAttr ".thss" 0.021499974653124809;
	setAttr ".fron" 0;
	setAttr ".actv" no;
	setAttr ".scld" no;
	setAttr ".por" 0.085999898612499237;
	setAttr ".tpc" yes;
	setAttr -s 2 ".fsc[0:1]"  0 1 1 1 0 1;
	setAttr -s 2 ".pfdo[0:1]"  0 1 1 1 0 1;
	setAttr -k on ".lifespan" 1;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
createNode transform -n "sunDirection";
	setAttr ".v" no;
	setAttr ".r" -type "double3" -75 0 0 ;
createNode directionalLight -n "sunShape" -p "sunDirection";
	setAttr -k off ".v";
	setAttr ".milt" yes;
createNode transform -n "pCube4";
	setAttr ".t" -type "double3" -2.6434498185288628 4.4762055545940278 -6.529 ;
	setAttr ".s" -type "double3" 1 1.283522951587023 1 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 115 ".uvst[0].uvsp[0:114]" -type "float2" 0.625 0.25 0.875 0.011799872 
		0.875 0.25 0.375 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.375 0.73820013 0.625 0.011799887 
		0.62578809 0.0047822841 0.6239208 0.75078815 0.62392074 0.74521768 0.37607926 0.74521768 
		0.625 0.73820013 0.62392074 0.99921191 0.62149143 0.75256199 0.87421191 0.0047822632 
		0.37860578 0.74907643 0.37848425 0.74903786 0.62149137 0.74907643 0.38129035 0.75456333 
		0.38124937 0.99540681 0.375 0.011799842 0.37612015 0.0047822678 0.37421191 0.0047822525 
		0.37408277 0.0048478013 0.125 0.011799887 0.12578811 0.0047822851 0.37607926 0.99918044 
		0.37607932 0.75078809 0.61875063 0.99543667 0.62149143 0.99743807 0.37854937 0.75256193 
		0.61875069 0.75456333 0.37856352 0.00092358346 0.62149149 0.00092357397 0.62392074 
		0.0047822706 0.37608245 0.99888653 0.37608945 0.99920446 0.37858799 0.00094878935 
		0.37626994 0.0047822534 0.37617737 0.0047778008 0.37850857 0.99743801 0.37848368 
		0.75254381 0.37851 0.99743694 0.37854421 0.99741203 0.38137409 0.99543774 0.38124937 
		0.99543667 0.38124937 0.99543667 0.38124937 0.99543667 0.37852919 0.75260425 0.37853089 
		0.75261265 0.37850955 0.75257522 0.3812494 0.75456333 0.37852645 0.75257504 0.3812494 
		0.75456333 0.38137844 0.75456333 0.38126981 0.99543267 0.38125485 0.99542183 0.38129026 
		0.99543667 0.37854818 0.75259089 0.37850857 0.75256401 0.38125488 0.75457829 0.38126984 
		0.75456733 0.38124943 0.75459325 0.37607926 0.0047822534 0.37607926 0.0047822534 
		0.37614253 0.0047840374 0.37606746 0.0048589581 0.37850857 0.99743801 0.37852424 
		0.99742657 0.37850857 0.99703062 0.37849018 0.99745142 0.37850857 0.99723256 0.37847796 
		0.99746037 0.3785004 0.99723822 0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 
		0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 0.37850857 0.99743801 0.37852719 
		0.99742442 0.37852919 0.75260425 0.37852919 0.75260425 0.37853089 0.75261265 0.38125488 
		0.75457829 0.38129035 0.75456333 0.38124943 0.75459325 0.37606746 0.0048589581 0.37606746 
		0.0048589581 0.37607926 0.0047822534 0.37606746 0.0048589581 0.37614253 0.0047840374 
		0.37612015 0.0047822678 0.37854421 0.99741203 0.37851 0.99743694 0.37850857 0.99743801 
		0.37850857 0.99743801 0.37849018 0.99745142 0.37849018 0.99745142 0.37847796 0.99746037 
		0.3785004 0.99723822 0.37850857 0.99723256 0.37850857 0.99743801 0.37851787 0.99743122 
		0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 0.99740404 0.37852719 0.99742442 
		0.37852719 0.99742442 0.37855509 0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 
		0.37850857 0.99743801;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".vt[0:44]"  -6.6892776 3.542733 9.1608858 6.6892786 3.5427353 9.1608686 
		-6.6892776 3.5427334 -9.1608639 6.6892776 3.5427358 -9.1608829 -6.6892776 -3.2083054 
		9.1608858 6.3548489 -3.5427332 8.826438 6.5015211 -3.5165575 8.9731102 6.6315231 
		-3.4071946 9.1031122 6.6892786 -3.2083027 9.1608686 -6.6315212 -3.407197 -9.1031055 
		-6.6892776 -3.2083051 -9.1608639 6.6315222 -3.4071941 -9.1031246 6.5015202 -3.5165572 
		-8.9731236 6.3548479 -3.542733 -8.8264523 6.6892776 -3.2083025 -9.1608829 -6.4985771 
		-3.5165601 8.9731274 -6.5002527 -3.5161319 8.9741449 -6.5022068 -3.5154939 8.9738674 
		-6.5031939 -3.515151 8.9720459 -6.5019722 -3.5160143 8.9707384 -6.5007076 -3.5165966 
		8.9693213 -6.4993649 -3.5169446 8.9679012 -6.4988775 -3.5171137 8.9695797 -6.4981871 
		-3.5171833 8.9714231 -6.3537531 -3.5427358 8.8261614 -6.3545542 -3.5427358 8.8253603 
		-6.354847 -3.5427358 8.8242655 -6.3526583 -3.5427358 8.8264542 -6.4993305 -3.5165598 
		-8.9731045 -6.5014668 -3.5165827 -8.9721279 -6.5003347 -3.5168421 -8.969389 -6.4993649 
		-3.5169444 -8.9678783 -6.5028496 -3.5154409 -8.9744339 -6.3545542 -3.5427356 -8.8253384 
		-6.3537531 -3.5427356 -8.8261395 -6.3526583 -3.5427356 -8.8264332 -6.354847 -3.5427356 
		-8.8242435 -6.6293325 -3.4071975 9.1031294 -6.6321096 -3.4051728 9.1037178 -6.6315212 
		-3.4071975 9.1009407 -6.6312284 -3.4071975 9.1020355 -6.6304274 -3.4071975 9.1028366 
		-6.4996691 -3.516784 8.972455 -6.5011177 -3.5163059 8.9723673 -6.5001993 -3.5167634 
		8.9709578;
	setAttr -s 86 ".ed[0:85]"  0 1 0 2 3 0 0 2 0 1 3 0 2 10 0 3 14 0 4 0 0 8 1 0 13 
		5 1 5 27 1 4 8 1 8 14 1 10 4 1 14 10 1 9 32 0 10 9 0 8 7 0 7 11 0 11 14 0 7 6 0 6 
		12 1 12 11 0 6 5 0 13 12 0 9 11 0 15 6 1 26 36 1 27 26 1 28 12 1 32 28 1 35 13 1 
		35 36 1 37 7 0 38 4 0 39 9 0 38 37 1 38 39 1 32 18 1 18 39 1 26 21 1 21 31 1 31 36 
		1 15 27 1 35 28 1 37 15 1 18 17 1 17 40 1 40 39 1 17 16 1 16 41 0 41 40 1 16 15 1 
		37 41 1 21 20 1 20 30 1 20 19 1 19 29 1 19 18 1 32 29 0 15 23 1 23 24 1 24 27 1 23 
		22 1 22 25 1 25 24 1 22 21 1 26 25 1 31 30 0 30 33 1 33 36 1 30 29 0 29 34 1 34 33 
		1 29 28 0 35 34 1 40 38 1 41 38 1 16 42 1 42 23 1 17 43 1 43 42 1 19 43 1 20 44 1 
		44 43 1 22 44 1 42 44 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 10 7 -1 -7 
		mu 0 4 22 8 0 3 
		f 4 1 5 13 -5 
		mu 0 4 5 6 13 7 
		f 4 11 -6 -4 -8 
		mu 0 4 8 1 2 0 
		f 4 12 6 2 4 
		mu 0 4 26 22 3 4 
		f 4 37 38 34 14 
		mu 0 4 43 74 28 29 
		f 5 -34 35 32 -17 -11 
		mu 0 5 22 68 23 36 8 
		f 4 44 25 -20 -33 
		mu 0 4 40 34 35 36 
		f 4 42 -10 -23 -26 
		mu 0 4 44 59 30 31 
		f 4 16 17 18 -12 
		mu 0 4 8 9 16 1 
		f 4 19 20 21 -18 
		mu 0 4 14 31 15 10 
		f 4 22 -9 23 -21 
		mu 0 4 31 30 33 15 
		f 4 43 28 -24 -31 
		mu 0 4 55 32 15 33 
		f 4 -16 -14 -19 -25 
		mu 0 4 12 7 13 11 
		f 5 -30 -15 24 -22 -29 
		mu 0 5 17 18 12 11 19 
		f 6 -32 30 8 9 27 26 
		mu 0 6 64 20 33 30 59 21 
		f 5 -37 33 -13 15 -35 
		mu 0 5 24 25 22 26 27 
		f 4 39 40 41 -27 
		mu 0 4 49 77 50 64 
		f 4 45 46 47 -39 
		mu 0 4 74 72 37 28 
		f 4 48 49 50 -47 
		mu 0 4 72 69 38 37 
		f 4 51 -45 52 -50 
		mu 0 4 39 34 40 41 
		f 4 53 54 -68 -41 
		mu 0 4 77 76 51 50 
		f 4 55 56 -71 -55 
		mu 0 4 76 42 83 51 
		f 4 57 -38 58 -57 
		mu 0 4 75 74 43 84 
		f 4 59 60 61 -43 
		mu 0 4 44 45 46 59 
		f 4 62 63 64 -61 
		mu 0 4 79 78 47 48 
		f 4 65 -40 66 -64 
		mu 0 4 78 77 49 47 
		f 4 67 68 69 -42 
		mu 0 4 50 51 62 64 
		f 4 70 71 72 -69 
		mu 0 4 51 52 53 62 
		f 4 73 -44 74 -72 
		mu 0 4 54 32 55 56 
		f 4 -65 -67 -28 -62 
		mu 0 4 57 58 21 59 
		f 3 -74 -59 29 
		mu 0 3 60 52 85 
		f 4 -73 -75 31 -70 
		mu 0 4 61 86 63 87 
		f 3 36 -48 75 
		mu 0 3 88 89 65 
		f 3 -76 -51 76 
		mu 0 3 66 90 91 
		f 3 -77 -53 -36 
		mu 0 3 67 92 93 
		f 4 -60 -52 77 78 
		mu 0 4 94 95 96 97 
		f 4 -78 -49 79 80 
		mu 0 4 70 71 98 99 
		f 4 -46 -58 81 -80 
		mu 0 4 73 100 101 102 
		f 4 -82 -56 82 83 
		mu 0 4 103 81 104 105 
		f 4 -54 -66 84 -83 
		mu 0 4 82 106 107 108 
		f 4 -85 -63 -79 85 
		mu 0 4 109 110 111 112 
		f 3 -81 -84 -86 
		mu 0 3 80 113 114 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".qsp" 0;
createNode transform -n "pCube5";
	setAttr ".t" -type "double3" -2.6434498185288628 4.4762055545940278 -6.529 ;
	setAttr ".s" -type "double3" 1 1.283522951587023 1 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 115 ".uvst[0].uvsp[0:114]" -type "float2" 0.625 0.25 0.875 0.011799872 
		0.875 0.25 0.375 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.375 0.73820013 0.625 0.011799887 
		0.62578809 0.0047822841 0.6239208 0.75078815 0.62392074 0.74521768 0.37607926 0.74521768 
		0.625 0.73820013 0.62392074 0.99921191 0.62149143 0.75256199 0.87421191 0.0047822632 
		0.37860578 0.74907643 0.37848425 0.74903786 0.62149137 0.74907643 0.38129035 0.75456333 
		0.38124937 0.99540681 0.375 0.011799842 0.37612015 0.0047822678 0.37421191 0.0047822525 
		0.37408277 0.0048478013 0.125 0.011799887 0.12578811 0.0047822851 0.37607926 0.99918044 
		0.37607932 0.75078809 0.61875063 0.99543667 0.62149143 0.99743807 0.37854937 0.75256193 
		0.61875069 0.75456333 0.37856352 0.00092358346 0.62149149 0.00092357397 0.62392074 
		0.0047822706 0.37608245 0.99888653 0.37608945 0.99920446 0.37858799 0.00094878935 
		0.37626994 0.0047822534 0.37617737 0.0047778008 0.37850857 0.99743801 0.37848368 
		0.75254381 0.37851 0.99743694 0.37854421 0.99741203 0.38137409 0.99543774 0.38124937 
		0.99543667 0.38124937 0.99543667 0.38124937 0.99543667 0.37852919 0.75260425 0.37853089 
		0.75261265 0.37850955 0.75257522 0.3812494 0.75456333 0.37852645 0.75257504 0.3812494 
		0.75456333 0.38137844 0.75456333 0.38126981 0.99543267 0.38125485 0.99542183 0.38129026 
		0.99543667 0.37854818 0.75259089 0.37850857 0.75256401 0.38125488 0.75457829 0.38126984 
		0.75456733 0.38124943 0.75459325 0.37607926 0.0047822534 0.37607926 0.0047822534 
		0.37614253 0.0047840374 0.37606746 0.0048589581 0.37850857 0.99743801 0.37852424 
		0.99742657 0.37850857 0.99703062 0.37849018 0.99745142 0.37850857 0.99723256 0.37847796 
		0.99746037 0.3785004 0.99723822 0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 
		0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 0.37850857 0.99743801 0.37852719 
		0.99742442 0.37852919 0.75260425 0.37852919 0.75260425 0.37853089 0.75261265 0.38125488 
		0.75457829 0.38129035 0.75456333 0.38124943 0.75459325 0.37606746 0.0048589581 0.37606746 
		0.0048589581 0.37607926 0.0047822534 0.37606746 0.0048589581 0.37614253 0.0047840374 
		0.37612015 0.0047822678 0.37854421 0.99741203 0.37851 0.99743694 0.37850857 0.99743801 
		0.37850857 0.99743801 0.37849018 0.99745142 0.37849018 0.99745142 0.37847796 0.99746037 
		0.3785004 0.99723822 0.37850857 0.99723256 0.37850857 0.99743801 0.37851787 0.99743122 
		0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 0.99740404 0.37852719 0.99742442 
		0.37852719 0.99742442 0.37855509 0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 
		0.37850857 0.99743801;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".vt[0:44]"  -6.6892776 3.542733 9.1608858 6.6892786 3.5427353 9.1608686 
		-6.6892776 3.5427334 -9.1608639 6.6892776 3.5427358 -9.1608829 -6.6892776 -3.2083054 
		9.1608858 6.3548489 -3.5427332 8.826438 6.5015211 -3.5165575 8.9731102 6.6315231 
		-3.4071946 9.1031122 6.6892786 -3.2083027 9.1608686 -6.6315212 -3.407197 -9.1031055 
		-6.6892776 -3.2083051 -9.1608639 6.6315222 -3.4071941 -9.1031246 6.5015202 -3.5165572 
		-8.9731236 6.3548479 -3.542733 -8.8264523 6.6892776 -3.2083025 -9.1608829 -6.4985771 
		-3.5165601 8.9731274 -6.5002527 -3.5161319 8.9741449 -6.5022068 -3.5154939 8.9738674 
		-6.5031939 -3.515151 8.9720459 -6.5019722 -3.5160143 8.9707384 -6.5007076 -3.5165966 
		8.9693213 -6.4993649 -3.5169446 8.9679012 -6.4988775 -3.5171137 8.9695797 -6.4981871 
		-3.5171833 8.9714231 -6.3537531 -3.5427358 8.8261614 -6.3545542 -3.5427358 8.8253603 
		-6.354847 -3.5427358 8.8242655 -6.3526583 -3.5427358 8.8264542 -6.4993305 -3.5165598 
		-8.9731045 -6.5014668 -3.5165827 -8.9721279 -6.5003347 -3.5168421 -8.969389 -6.4993649 
		-3.5169444 -8.9678783 -6.5028496 -3.5154409 -8.9744339 -6.3545542 -3.5427356 -8.8253384 
		-6.3537531 -3.5427356 -8.8261395 -6.3526583 -3.5427356 -8.8264332 -6.354847 -3.5427356 
		-8.8242435 -6.6293325 -3.4071975 9.1031294 -6.6321096 -3.4051728 9.1037178 -6.6315212 
		-3.4071975 9.1009407 -6.6312284 -3.4071975 9.1020355 -6.6304274 -3.4071975 9.1028366 
		-6.4996691 -3.516784 8.972455 -6.5011177 -3.5163059 8.9723673 -6.5001993 -3.5167634 
		8.9709578;
	setAttr -s 86 ".ed[0:85]"  0 1 0 2 3 0 0 2 0 1 3 0 2 10 0 3 14 0 4 0 0 8 1 0 13 
		5 1 5 27 1 4 8 1 8 14 1 10 4 1 14 10 1 9 32 0 10 9 0 8 7 0 7 11 0 11 14 0 7 6 0 6 
		12 1 12 11 0 6 5 0 13 12 0 9 11 0 15 6 1 26 36 1 27 26 1 28 12 1 32 28 1 35 13 1 
		35 36 1 37 7 0 38 4 0 39 9 0 38 37 1 38 39 1 32 18 1 18 39 1 26 21 1 21 31 1 31 36 
		1 15 27 1 35 28 1 37 15 1 18 17 1 17 40 1 40 39 1 17 16 1 16 41 0 41 40 1 16 15 1 
		37 41 1 21 20 1 20 30 1 20 19 1 19 29 1 19 18 1 32 29 0 15 23 1 23 24 1 24 27 1 23 
		22 1 22 25 1 25 24 1 22 21 1 26 25 1 31 30 0 30 33 1 33 36 1 30 29 0 29 34 1 34 33 
		1 29 28 0 35 34 1 40 38 1 41 38 1 16 42 1 42 23 1 17 43 1 43 42 1 19 43 1 20 44 1 
		44 43 1 22 44 1 42 44 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 10 7 -1 -7 
		mu 0 4 22 8 0 3 
		f 4 1 5 13 -5 
		mu 0 4 5 6 13 7 
		f 4 11 -6 -4 -8 
		mu 0 4 8 1 2 0 
		f 4 12 6 2 4 
		mu 0 4 26 22 3 4 
		f 4 37 38 34 14 
		mu 0 4 43 74 28 29 
		f 5 -34 35 32 -17 -11 
		mu 0 5 22 68 23 36 8 
		f 4 44 25 -20 -33 
		mu 0 4 40 34 35 36 
		f 4 42 -10 -23 -26 
		mu 0 4 44 59 30 31 
		f 4 16 17 18 -12 
		mu 0 4 8 9 16 1 
		f 4 19 20 21 -18 
		mu 0 4 14 31 15 10 
		f 4 22 -9 23 -21 
		mu 0 4 31 30 33 15 
		f 4 43 28 -24 -31 
		mu 0 4 55 32 15 33 
		f 4 -16 -14 -19 -25 
		mu 0 4 12 7 13 11 
		f 5 -30 -15 24 -22 -29 
		mu 0 5 17 18 12 11 19 
		f 6 -32 30 8 9 27 26 
		mu 0 6 64 20 33 30 59 21 
		f 5 -37 33 -13 15 -35 
		mu 0 5 24 25 22 26 27 
		f 4 39 40 41 -27 
		mu 0 4 49 77 50 64 
		f 4 45 46 47 -39 
		mu 0 4 74 72 37 28 
		f 4 48 49 50 -47 
		mu 0 4 72 69 38 37 
		f 4 51 -45 52 -50 
		mu 0 4 39 34 40 41 
		f 4 53 54 -68 -41 
		mu 0 4 77 76 51 50 
		f 4 55 56 -71 -55 
		mu 0 4 76 42 83 51 
		f 4 57 -38 58 -57 
		mu 0 4 75 74 43 84 
		f 4 59 60 61 -43 
		mu 0 4 44 45 46 59 
		f 4 62 63 64 -61 
		mu 0 4 79 78 47 48 
		f 4 65 -40 66 -64 
		mu 0 4 78 77 49 47 
		f 4 67 68 69 -42 
		mu 0 4 50 51 62 64 
		f 4 70 71 72 -69 
		mu 0 4 51 52 53 62 
		f 4 73 -44 74 -72 
		mu 0 4 54 32 55 56 
		f 4 -65 -67 -28 -62 
		mu 0 4 57 58 21 59 
		f 3 -74 -59 29 
		mu 0 3 60 52 85 
		f 4 -73 -75 31 -70 
		mu 0 4 61 86 63 87 
		f 3 36 -48 75 
		mu 0 3 88 89 65 
		f 3 -76 -51 76 
		mu 0 3 66 90 91 
		f 3 -77 -53 -36 
		mu 0 3 67 92 93 
		f 4 -60 -52 77 78 
		mu 0 4 94 95 96 97 
		f 4 -78 -49 79 80 
		mu 0 4 70 71 98 99 
		f 4 -46 -58 81 -80 
		mu 0 4 73 100 101 102 
		f 4 -82 -56 82 83 
		mu 0 4 103 81 104 105 
		f 4 -54 -66 84 -83 
		mu 0 4 82 106 107 108 
		f 4 -85 -63 -79 85 
		mu 0 4 109 110 111 112 
		f 3 -81 -84 -86 
		mu 0 3 80 113 114 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".qsp" 0;
createNode transform -n "pCube6";
	setAttr ".t" -type "double3" -2.6434498185288628 4.4762055545940278 13.356056877584551 ;
	setAttr ".s" -type "double3" 1 1.283522951587023 1 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 115 ".uvst[0].uvsp[0:114]" -type "float2" 0.625 0.25 0.875 0.011799872 
		0.875 0.25 0.375 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.375 0.73820013 0.625 0.011799887 
		0.62578809 0.0047822841 0.6239208 0.75078815 0.62392074 0.74521768 0.37607926 0.74521768 
		0.625 0.73820013 0.62392074 0.99921191 0.62149143 0.75256199 0.87421191 0.0047822632 
		0.37860578 0.74907643 0.37848425 0.74903786 0.62149137 0.74907643 0.38129035 0.75456333 
		0.38124937 0.99540681 0.375 0.011799842 0.37612015 0.0047822678 0.37421191 0.0047822525 
		0.37408277 0.0048478013 0.125 0.011799887 0.12578811 0.0047822851 0.37607926 0.99918044 
		0.37607932 0.75078809 0.61875063 0.99543667 0.62149143 0.99743807 0.37854937 0.75256193 
		0.61875069 0.75456333 0.37856352 0.00092358346 0.62149149 0.00092357397 0.62392074 
		0.0047822706 0.37608245 0.99888653 0.37608945 0.99920446 0.37858799 0.00094878935 
		0.37626994 0.0047822534 0.37617737 0.0047778008 0.37850857 0.99743801 0.37848368 
		0.75254381 0.37851 0.99743694 0.37854421 0.99741203 0.38137409 0.99543774 0.38124937 
		0.99543667 0.38124937 0.99543667 0.38124937 0.99543667 0.37852919 0.75260425 0.37853089 
		0.75261265 0.37850955 0.75257522 0.3812494 0.75456333 0.37852645 0.75257504 0.3812494 
		0.75456333 0.38137844 0.75456333 0.38126981 0.99543267 0.38125485 0.99542183 0.38129026 
		0.99543667 0.37854818 0.75259089 0.37850857 0.75256401 0.38125488 0.75457829 0.38126984 
		0.75456733 0.38124943 0.75459325 0.37607926 0.0047822534 0.37607926 0.0047822534 
		0.37614253 0.0047840374 0.37606746 0.0048589581 0.37850857 0.99743801 0.37852424 
		0.99742657 0.37850857 0.99703062 0.37849018 0.99745142 0.37850857 0.99723256 0.37847796 
		0.99746037 0.3785004 0.99723822 0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 
		0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 0.37850857 0.99743801 0.37852719 
		0.99742442 0.37852919 0.75260425 0.37852919 0.75260425 0.37853089 0.75261265 0.38125488 
		0.75457829 0.38129035 0.75456333 0.38124943 0.75459325 0.37606746 0.0048589581 0.37606746 
		0.0048589581 0.37607926 0.0047822534 0.37606746 0.0048589581 0.37614253 0.0047840374 
		0.37612015 0.0047822678 0.37854421 0.99741203 0.37851 0.99743694 0.37850857 0.99743801 
		0.37850857 0.99743801 0.37849018 0.99745142 0.37849018 0.99745142 0.37847796 0.99746037 
		0.3785004 0.99723822 0.37850857 0.99723256 0.37850857 0.99743801 0.37851787 0.99743122 
		0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 0.99740404 0.37852719 0.99742442 
		0.37852719 0.99742442 0.37855509 0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 
		0.37850857 0.99743801;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".vt[0:44]"  -6.6892776 3.542733 9.1608858 6.6892786 3.5427353 9.1608686 
		-6.6892776 3.5427334 -9.1608639 6.6892776 3.5427358 -9.1608829 -6.6892776 -3.2083054 
		9.1608858 6.3548489 -3.5427332 8.826438 6.5015211 -3.5165575 8.9731102 6.6315231 
		-3.4071946 9.1031122 6.6892786 -3.2083027 9.1608686 -6.6315212 -3.407197 -9.1031055 
		-6.6892776 -3.2083051 -9.1608639 6.6315222 -3.4071941 -9.1031246 6.5015202 -3.5165572 
		-8.9731236 6.3548479 -3.542733 -8.8264523 6.6892776 -3.2083025 -9.1608829 -6.4985771 
		-3.5165601 8.9731274 -6.5002527 -3.5161319 8.9741449 -6.5022068 -3.5154939 8.9738674 
		-6.5031939 -3.515151 8.9720459 -6.5019722 -3.5160143 8.9707384 -6.5007076 -3.5165966 
		8.9693213 -6.4993649 -3.5169446 8.9679012 -6.4988775 -3.5171137 8.9695797 -6.4981871 
		-3.5171833 8.9714231 -6.3537531 -3.5427358 8.8261614 -6.3545542 -3.5427358 8.8253603 
		-6.354847 -3.5427358 8.8242655 -6.3526583 -3.5427358 8.8264542 -6.4993305 -3.5165598 
		-8.9731045 -6.5014668 -3.5165827 -8.9721279 -6.5003347 -3.5168421 -8.969389 -6.4993649 
		-3.5169444 -8.9678783 -6.5028496 -3.5154409 -8.9744339 -6.3545542 -3.5427356 -8.8253384 
		-6.3537531 -3.5427356 -8.8261395 -6.3526583 -3.5427356 -8.8264332 -6.354847 -3.5427356 
		-8.8242435 -6.6293325 -3.4071975 9.1031294 -6.6321096 -3.4051728 9.1037178 -6.6315212 
		-3.4071975 9.1009407 -6.6312284 -3.4071975 9.1020355 -6.6304274 -3.4071975 9.1028366 
		-6.4996691 -3.516784 8.972455 -6.5011177 -3.5163059 8.9723673 -6.5001993 -3.5167634 
		8.9709578;
	setAttr -s 86 ".ed[0:85]"  0 1 0 2 3 0 0 2 0 1 3 0 2 10 0 3 14 0 4 0 0 8 1 0 13 
		5 1 5 27 1 4 8 1 8 14 1 10 4 1 14 10 1 9 32 0 10 9 0 8 7 0 7 11 0 11 14 0 7 6 0 6 
		12 1 12 11 0 6 5 0 13 12 0 9 11 0 15 6 1 26 36 1 27 26 1 28 12 1 32 28 1 35 13 1 
		35 36 1 37 7 0 38 4 0 39 9 0 38 37 1 38 39 1 32 18 1 18 39 1 26 21 1 21 31 1 31 36 
		1 15 27 1 35 28 1 37 15 1 18 17 1 17 40 1 40 39 1 17 16 1 16 41 0 41 40 1 16 15 1 
		37 41 1 21 20 1 20 30 1 20 19 1 19 29 1 19 18 1 32 29 0 15 23 1 23 24 1 24 27 1 23 
		22 1 22 25 1 25 24 1 22 21 1 26 25 1 31 30 0 30 33 1 33 36 1 30 29 0 29 34 1 34 33 
		1 29 28 0 35 34 1 40 38 1 41 38 1 16 42 1 42 23 1 17 43 1 43 42 1 19 43 1 20 44 1 
		44 43 1 22 44 1 42 44 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 10 7 -1 -7 
		mu 0 4 22 8 0 3 
		f 4 1 5 13 -5 
		mu 0 4 5 6 13 7 
		f 4 11 -6 -4 -8 
		mu 0 4 8 1 2 0 
		f 4 12 6 2 4 
		mu 0 4 26 22 3 4 
		f 4 37 38 34 14 
		mu 0 4 43 74 28 29 
		f 5 -34 35 32 -17 -11 
		mu 0 5 22 68 23 36 8 
		f 4 44 25 -20 -33 
		mu 0 4 40 34 35 36 
		f 4 42 -10 -23 -26 
		mu 0 4 44 59 30 31 
		f 4 16 17 18 -12 
		mu 0 4 8 9 16 1 
		f 4 19 20 21 -18 
		mu 0 4 14 31 15 10 
		f 4 22 -9 23 -21 
		mu 0 4 31 30 33 15 
		f 4 43 28 -24 -31 
		mu 0 4 55 32 15 33 
		f 4 -16 -14 -19 -25 
		mu 0 4 12 7 13 11 
		f 5 -30 -15 24 -22 -29 
		mu 0 5 17 18 12 11 19 
		f 6 -32 30 8 9 27 26 
		mu 0 6 64 20 33 30 59 21 
		f 5 -37 33 -13 15 -35 
		mu 0 5 24 25 22 26 27 
		f 4 39 40 41 -27 
		mu 0 4 49 77 50 64 
		f 4 45 46 47 -39 
		mu 0 4 74 72 37 28 
		f 4 48 49 50 -47 
		mu 0 4 72 69 38 37 
		f 4 51 -45 52 -50 
		mu 0 4 39 34 40 41 
		f 4 53 54 -68 -41 
		mu 0 4 77 76 51 50 
		f 4 55 56 -71 -55 
		mu 0 4 76 42 83 51 
		f 4 57 -38 58 -57 
		mu 0 4 75 74 43 84 
		f 4 59 60 61 -43 
		mu 0 4 44 45 46 59 
		f 4 62 63 64 -61 
		mu 0 4 79 78 47 48 
		f 4 65 -40 66 -64 
		mu 0 4 78 77 49 47 
		f 4 67 68 69 -42 
		mu 0 4 50 51 62 64 
		f 4 70 71 72 -69 
		mu 0 4 51 52 53 62 
		f 4 73 -44 74 -72 
		mu 0 4 54 32 55 56 
		f 4 -65 -67 -28 -62 
		mu 0 4 57 58 21 59 
		f 3 -74 -59 29 
		mu 0 3 60 52 85 
		f 4 -73 -75 31 -70 
		mu 0 4 61 86 63 87 
		f 3 36 -48 75 
		mu 0 3 88 89 65 
		f 3 -76 -51 76 
		mu 0 3 66 90 91 
		f 3 -77 -53 -36 
		mu 0 3 67 92 93 
		f 4 -60 -52 77 78 
		mu 0 4 94 95 96 97 
		f 4 -78 -49 79 80 
		mu 0 4 70 71 98 99 
		f 4 -46 -58 81 -80 
		mu 0 4 73 100 101 102 
		f 4 -82 -56 82 83 
		mu 0 4 103 81 104 105 
		f 4 -54 -66 84 -83 
		mu 0 4 82 106 107 108 
		f 4 -85 -63 -79 85 
		mu 0 4 109 110 111 112 
		f 3 -81 -84 -86 
		mu 0 3 80 113 114 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".qsp" 0;
createNode transform -n "pCube7";
	setAttr ".t" -type "double3" -2.6434498185288628 4.4762055545940278 13.356056877584551 ;
	setAttr ".s" -type "double3" 1 1.283522951587023 1 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 115 ".uvst[0].uvsp[0:114]" -type "float2" 0.625 0.25 0.875 0.011799872 
		0.875 0.25 0.375 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.375 0.73820013 0.625 0.011799887 
		0.62578809 0.0047822841 0.6239208 0.75078815 0.62392074 0.74521768 0.37607926 0.74521768 
		0.625 0.73820013 0.62392074 0.99921191 0.62149143 0.75256199 0.87421191 0.0047822632 
		0.37860578 0.74907643 0.37848425 0.74903786 0.62149137 0.74907643 0.38129035 0.75456333 
		0.38124937 0.99540681 0.375 0.011799842 0.37612015 0.0047822678 0.37421191 0.0047822525 
		0.37408277 0.0048478013 0.125 0.011799887 0.12578811 0.0047822851 0.37607926 0.99918044 
		0.37607932 0.75078809 0.61875063 0.99543667 0.62149143 0.99743807 0.37854937 0.75256193 
		0.61875069 0.75456333 0.37856352 0.00092358346 0.62149149 0.00092357397 0.62392074 
		0.0047822706 0.37608245 0.99888653 0.37608945 0.99920446 0.37858799 0.00094878935 
		0.37626994 0.0047822534 0.37617737 0.0047778008 0.37850857 0.99743801 0.37848368 
		0.75254381 0.37851 0.99743694 0.37854421 0.99741203 0.38137409 0.99543774 0.38124937 
		0.99543667 0.38124937 0.99543667 0.38124937 0.99543667 0.37852919 0.75260425 0.37853089 
		0.75261265 0.37850955 0.75257522 0.3812494 0.75456333 0.37852645 0.75257504 0.3812494 
		0.75456333 0.38137844 0.75456333 0.38126981 0.99543267 0.38125485 0.99542183 0.38129026 
		0.99543667 0.37854818 0.75259089 0.37850857 0.75256401 0.38125488 0.75457829 0.38126984 
		0.75456733 0.38124943 0.75459325 0.37607926 0.0047822534 0.37607926 0.0047822534 
		0.37614253 0.0047840374 0.37606746 0.0048589581 0.37850857 0.99743801 0.37852424 
		0.99742657 0.37850857 0.99703062 0.37849018 0.99745142 0.37850857 0.99723256 0.37847796 
		0.99746037 0.3785004 0.99723822 0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 
		0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 0.37850857 0.99743801 0.37852719 
		0.99742442 0.37852919 0.75260425 0.37852919 0.75260425 0.37853089 0.75261265 0.38125488 
		0.75457829 0.38129035 0.75456333 0.38124943 0.75459325 0.37606746 0.0048589581 0.37606746 
		0.0048589581 0.37607926 0.0047822534 0.37606746 0.0048589581 0.37614253 0.0047840374 
		0.37612015 0.0047822678 0.37854421 0.99741203 0.37851 0.99743694 0.37850857 0.99743801 
		0.37850857 0.99743801 0.37849018 0.99745142 0.37849018 0.99745142 0.37847796 0.99746037 
		0.3785004 0.99723822 0.37850857 0.99723256 0.37850857 0.99743801 0.37851787 0.99743122 
		0.37851787 0.99743122 0.37854576 0.99741083 0.37855509 0.99740404 0.37852719 0.99742442 
		0.37852719 0.99742442 0.37855509 0.99740404 0.37856436 0.99739724 0.37853646 0.99741763 
		0.37850857 0.99743801;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".vt[0:44]"  -6.6892776 3.542733 9.1608858 6.6892786 3.5427353 9.1608686 
		-6.6892776 3.5427334 -9.1608639 6.6892776 3.5427358 -9.1608829 -6.6892776 -3.2083054 
		9.1608858 6.3548489 -3.5427332 8.826438 6.5015211 -3.5165575 8.9731102 6.6315231 
		-3.4071946 9.1031122 6.6892786 -3.2083027 9.1608686 -6.6315212 -3.407197 -9.1031055 
		-6.6892776 -3.2083051 -9.1608639 6.6315222 -3.4071941 -9.1031246 6.5015202 -3.5165572 
		-8.9731236 6.3548479 -3.542733 -8.8264523 6.6892776 -3.2083025 -9.1608829 -6.4985771 
		-3.5165601 8.9731274 -6.5002527 -3.5161319 8.9741449 -6.5022068 -3.5154939 8.9738674 
		-6.5031939 -3.515151 8.9720459 -6.5019722 -3.5160143 8.9707384 -6.5007076 -3.5165966 
		8.9693213 -6.4993649 -3.5169446 8.9679012 -6.4988775 -3.5171137 8.9695797 -6.4981871 
		-3.5171833 8.9714231 -6.3537531 -3.5427358 8.8261614 -6.3545542 -3.5427358 8.8253603 
		-6.354847 -3.5427358 8.8242655 -6.3526583 -3.5427358 8.8264542 -6.4993305 -3.5165598 
		-8.9731045 -6.5014668 -3.5165827 -8.9721279 -6.5003347 -3.5168421 -8.969389 -6.4993649 
		-3.5169444 -8.9678783 -6.5028496 -3.5154409 -8.9744339 -6.3545542 -3.5427356 -8.8253384 
		-6.3537531 -3.5427356 -8.8261395 -6.3526583 -3.5427356 -8.8264332 -6.354847 -3.5427356 
		-8.8242435 -6.6293325 -3.4071975 9.1031294 -6.6321096 -3.4051728 9.1037178 -6.6315212 
		-3.4071975 9.1009407 -6.6312284 -3.4071975 9.1020355 -6.6304274 -3.4071975 9.1028366 
		-6.4996691 -3.516784 8.972455 -6.5011177 -3.5163059 8.9723673 -6.5001993 -3.5167634 
		8.9709578;
	setAttr -s 86 ".ed[0:85]"  0 1 0 2 3 0 0 2 0 1 3 0 2 10 0 3 14 0 4 0 0 8 1 0 13 
		5 1 5 27 1 4 8 1 8 14 1 10 4 1 14 10 1 9 32 0 10 9 0 8 7 0 7 11 0 11 14 0 7 6 0 6 
		12 1 12 11 0 6 5 0 13 12 0 9 11 0 15 6 1 26 36 1 27 26 1 28 12 1 32 28 1 35 13 1 
		35 36 1 37 7 0 38 4 0 39 9 0 38 37 1 38 39 1 32 18 1 18 39 1 26 21 1 21 31 1 31 36 
		1 15 27 1 35 28 1 37 15 1 18 17 1 17 40 1 40 39 1 17 16 1 16 41 0 41 40 1 16 15 1 
		37 41 1 21 20 1 20 30 1 20 19 1 19 29 1 19 18 1 32 29 0 15 23 1 23 24 1 24 27 1 23 
		22 1 22 25 1 25 24 1 22 21 1 26 25 1 31 30 0 30 33 1 33 36 1 30 29 0 29 34 1 34 33 
		1 29 28 0 35 34 1 40 38 1 41 38 1 16 42 1 42 23 1 17 43 1 43 42 1 19 43 1 20 44 1 
		44 43 1 22 44 1 42 44 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 10 7 -1 -7 
		mu 0 4 22 8 0 3 
		f 4 1 5 13 -5 
		mu 0 4 5 6 13 7 
		f 4 11 -6 -4 -8 
		mu 0 4 8 1 2 0 
		f 4 12 6 2 4 
		mu 0 4 26 22 3 4 
		f 4 37 38 34 14 
		mu 0 4 43 74 28 29 
		f 5 -34 35 32 -17 -11 
		mu 0 5 22 68 23 36 8 
		f 4 44 25 -20 -33 
		mu 0 4 40 34 35 36 
		f 4 42 -10 -23 -26 
		mu 0 4 44 59 30 31 
		f 4 16 17 18 -12 
		mu 0 4 8 9 16 1 
		f 4 19 20 21 -18 
		mu 0 4 14 31 15 10 
		f 4 22 -9 23 -21 
		mu 0 4 31 30 33 15 
		f 4 43 28 -24 -31 
		mu 0 4 55 32 15 33 
		f 4 -16 -14 -19 -25 
		mu 0 4 12 7 13 11 
		f 5 -30 -15 24 -22 -29 
		mu 0 5 17 18 12 11 19 
		f 6 -32 30 8 9 27 26 
		mu 0 6 64 20 33 30 59 21 
		f 5 -37 33 -13 15 -35 
		mu 0 5 24 25 22 26 27 
		f 4 39 40 41 -27 
		mu 0 4 49 77 50 64 
		f 4 45 46 47 -39 
		mu 0 4 74 72 37 28 
		f 4 48 49 50 -47 
		mu 0 4 72 69 38 37 
		f 4 51 -45 52 -50 
		mu 0 4 39 34 40 41 
		f 4 53 54 -68 -41 
		mu 0 4 77 76 51 50 
		f 4 55 56 -71 -55 
		mu 0 4 76 42 83 51 
		f 4 57 -38 58 -57 
		mu 0 4 75 74 43 84 
		f 4 59 60 61 -43 
		mu 0 4 44 45 46 59 
		f 4 62 63 64 -61 
		mu 0 4 79 78 47 48 
		f 4 65 -40 66 -64 
		mu 0 4 78 77 49 47 
		f 4 67 68 69 -42 
		mu 0 4 50 51 62 64 
		f 4 70 71 72 -69 
		mu 0 4 51 52 53 62 
		f 4 73 -44 74 -72 
		mu 0 4 54 32 55 56 
		f 4 -65 -67 -28 -62 
		mu 0 4 57 58 21 59 
		f 3 -74 -59 29 
		mu 0 3 60 52 85 
		f 4 -73 -75 31 -70 
		mu 0 4 61 86 63 87 
		f 3 36 -48 75 
		mu 0 3 88 89 65 
		f 3 -76 -51 76 
		mu 0 3 66 90 91 
		f 3 -77 -53 -36 
		mu 0 3 67 92 93 
		f 4 -60 -52 77 78 
		mu 0 4 94 95 96 97 
		f 4 -78 -49 79 80 
		mu 0 4 70 71 98 99 
		f 4 -46 -58 81 -80 
		mu 0 4 73 100 101 102 
		f 4 -82 -56 82 83 
		mu 0 4 103 81 104 105 
		f 4 -54 -66 84 -83 
		mu 0 4 82 106 107 108 
		f 4 -85 -63 -79 85 
		mu 0 4 109 110 111 112 
		f 3 -81 -84 -86 
		mu 0 3 80 113 114 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".qsp" 0;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 8 ".lnk";
	setAttr -s 8 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode nucleus -n "nucleus1";
	setAttr -s 2 ".nipo";
	setAttr -s 2 ".nips";
	setAttr ".nupl" yes;
	setAttr ".npbc" 0.10000000149011612;
	setAttr ".npfr" 0;
	setAttr ".sstp" 12;
	setAttr ".spsc" 1.5;
createNode shadingEngine -n "nParticleCloudSE";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode particleSamplerInfo -n "particleSamplerInfo1";
createNode blinn -n "npCloudBlinn";
createNode particleCloud -n "npCloudVolume";
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n"
		+ "            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n"
		+ "                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n"
		+ "                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n"
		+ "            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n"
		+ "                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 0\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n"
		+ "                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 0\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n"
		+ "                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n"
		+ "                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -downstream 1\n                -ignoreAssets 1\n"
		+ "                -upstream 1\n                -rootsFromSelection 1\n                -traversalDepthLimit -1\n                -island 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -downstream 1\n                -ignoreAssets 1\n                -upstream 1\n                -rootsFromSelection 1\n                -traversalDepthLimit -1\n                -island 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 257 -ast 1 -aet 400 ";
	setAttr ".st" 6;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 17 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".hier" yes;
	setAttr ".fdag" yes;
	setAttr ".xtf" yes;
	setAttr ".xvc" yes;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".ca" yes;
	setAttr ".fgr" 10;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
	setAttr ".scan" 0;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode peak -n "peak1";
	setAttr -av ".d" -0.075;
createNode tweak -n "tweak1";
createNode objectSet -n "peak1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "peak1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "peak1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode tweak -n "tweak2";
createNode objectSet -n "tweak2Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "tweak2GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "tweak2GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode mia_physicalsun -n "mia_physicalsun1";
	setAttr ".S12" yes;
createNode lambert -n "lambert2";
	setAttr ".c" -type "float3" 0.51999998 0.76800001 1 ;
createNode shadingEngine -n "lambert2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode makeNurbCircle -n "makeNurbCircle1";
	setAttr ".nr" -type "double3" 0 1 0 ;
	setAttr ".r" 3.4606654028570971;
createNode blinn -n "blinn1";
	setAttr ".it" -type "float3" 0.92857254 0.92857254 0.92857254 ;
	setAttr ".rfl" 0;
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode mia_material -n "mia_material1";
	setAttr ".S00" 0.10000000149011612;
	setAttr ".S01" -type "float3" 0 0.5 1 ;
	setAttr ".S02" 0.5;
	setAttr ".S03" 1;
	setAttr ".S10" 1;
	setAttr ".S13" 1.3300000429153442;
	setAttr ".S17" -type "float3" 0.69999999 0.5 0.2 ;
	setAttr ".S22" yes;
	setAttr ".S36" -type "float3" 0.2 0.2 0.2 ;
	setAttr ".S37" 4;
	setAttr ".S42" -type "float3" 0.2 0.2 0.2 ;
	setAttr ".S43" 6;
	setAttr ".S50" 4;
	setAttr ".S64" yes;
createNode shadingEngine -n "mia_material1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
createNode shadingEngine -n "mia_material1SG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
createNode animCurveTA -n "emitter1_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 400 1200;
createNode addDoubleLinear -n "addDoubleLinear3";
createNode motionPath -n "motionPath1";
	setAttr -s 4 ".pmt[0:3]"  1 100 1 100;
	setAttr -s 2 ".pmt";
	setAttr ".f" yes;
	setAttr ".fa" 0;
	setAttr ".ua" 1;
	setAttr ".fm" yes;
createNode animCurveTL -n "motionPath1_uValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 100 1;
	setAttr ".pst" 3;
createNode addDoubleLinear -n "addDoubleLinear2";
createNode addDoubleLinear -n "addDoubleLinear1";
createNode cacheFile -n "nParticleShape1Cache1";
	setAttr ".cn" -type "string" "nParticleShape1";
	setAttr ".cp" -type "string" "/home/generic/maya/projects/default//data/peak_liquids_IT(3)/";
	setAttr ".os" 1;
	setAttr ".oe" 257;
	setAttr ".ss" 1;
	setAttr ".se" 257;
	setAttr ".sf" 1;
createNode phong -n "phong1";
	setAttr ".c" -type "float3" 0 0.5 1 ;
	setAttr ".sc" -type "float3" 0.25 0.25 0.25 ;
	setAttr ".cp" 30;
createNode shadingEngine -n "phong1SG";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo6";
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S01" 0.20000000298023224;
	setAttr ".S02" 0.75;
	setAttr ".S03" 3;
createNode mia_physicalsun -n "mia_physicalsun2";
createNode mia_physicalsky -n "mia_physicalsky1";
	addAttr -ci true -h true -sn "miSkyExposure" -ln "miSkyExposure" -at "message";
	setAttr ".S06" -5;
	setAttr ".S07" 1;
	setAttr ".S18" yes;
createNode deleteComponent -n "deleteComponent2";
	setAttr ".dc" -type "componentList" 1 "f[20:39]";
createNode polyCylinder -n "polyCylinder1";
	setAttr ".r" 1.3192789103897424;
	setAttr ".h" 3.5471081940554141;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyBevel -n "polyBevel2";
	setAttr ".ics" -type "componentList" 1 "e[16:19]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6434498185288628 3.5427342729214479 -19.315705394199867 1
		;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".o" 0.05;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyBevel -n "polyBevel1";
	setAttr ".ics" -type "componentList" 3 "e[0]" "e[3]" "e[10:11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6434498185288628 3.5427342729214479 -23.700556630972002 1
		;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".o" 0.05;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode deleteComponent -n "deleteComponent1";
	setAttr ".dc" -type "componentList" 1 "f[1]";
createNode polyCube -n "polyCube1";
	setAttr ".w" 13.378559082135101;
	setAttr ".h" 7.0854685458428959;
	setAttr ".d" 18.321750441350748;
	setAttr ".cuv" 4;
createNode polySmoothFace -n "polySmoothFace1";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".dv" 0;
	setAttr ".kb" no;
	setAttr ".ksb" no;
	setAttr ".ps" 0.10000000149011612;
	setAttr ".ro" 1;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode polyAverageVertex -n "polyAverageVertex1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "vtx[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".i" 4;
select -ne :time1;
	setAttr ".ihi" 0;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 8 ".st";
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :lightList1;
	setAttr -s 3 ".l";
select -ne :lambert1;
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.40372321 0.40372321 0.40372321 ;
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 4 ".u";
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
	setAttr ".imfkey" -type "string" "iff";
	setAttr ".an" yes;
	setAttr ".ef" 257;
	setAttr ".ep" 4;
	setAttr ".pff" yes;
	setAttr ".ifp" -type "string" "waterTank";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
	setAttr -s 3 ".dsm";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 10 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Lights" "Cameras" "Locators" "Manipulators" "Grid" "HUD"  ;
	setAttr ".otfva" -type "Int32Array" 10 1 1 1 1 1 1 1 1 1 1 ;
	setAttr ".aasc" 16;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".ef" 100;
	setAttr ".fn" -type "string" "peak_liquids";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "mia_exposure_simple1.msg" ":perspShape.mils";
connectAttr "mia_physicalsky1.msg" ":perspShape.mies";
connectAttr ":time1.o" "emitter1.ct";
connectAttr "nParticleShape1.ifl" "emitter1.full[0]";
connectAttr "nParticleShape1.tss" "emitter1.dt[0]";
connectAttr "nParticleShape1.inh" "emitter1.inh[0]";
connectAttr "nParticleShape1.stt" "emitter1.stt[0]";
connectAttr "nParticleShape1.sd[0]" "emitter1.sd[0]";
connectAttr "addDoubleLinear1.o" "emitter1.tx";
connectAttr "addDoubleLinear2.o" "emitter1.ty";
connectAttr "addDoubleLinear3.o" "emitter1.tz";
connectAttr "motionPath1.msg" "emitter1.sml";
connectAttr "emitter1_rotateY.o" "emitter1.ry";
connectAttr "motionPath1.rz" "emitter1.rz";
connectAttr "motionPath1.ro" "emitter1.ro";
connectAttr "deleteComponent2.og" "pCylinderShape1.i";
connectAttr ":time1.o" "nParticleShape1.cti";
connectAttr "nucleus1.noao[0]" "nParticleShape1.nxst";
connectAttr "nucleus1.stf" "nParticleShape1.stf";
connectAttr "emitter1.ot[0]" "nParticleShape1.npt[0]";
connectAttr "nParticleShape1.incr" "nParticleShape1.rgbPP";
connectAttr "nParticleShape1.inor" "nParticleShape1.opacityPP";
connectAttr "nParticleShape1.inrr" "nParticleShape1.radiusPP";
connectAttr "nParticleShape1.msg" "nParticleShape1.etg[0]";
connectAttr "nParticleShape1Cache1.ocd[0]" "nParticleShape1.poss";
connectAttr "nParticleShape1Cache1.ir" "nParticleShape1.pfc";
connectAttr "nParticleShape1Cache1.ocad" "nParticleShape1.chad";
connectAttr "nParticleShape1.oms" "polySurfaceShape1.i";
connectAttr "polyAverageVertex1.out" "polySurfaceShape2.i";
connectAttr "peak1GroupId.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr "peak1Set.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupId2.id" "polySurfaceShape2.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "polySurfaceShape2.iog.og[1].gco";
connectAttr "tweak2GroupId.id" "polySurfaceShape2.iog.og[2].gid";
connectAttr "tweak2Set.mwc" "polySurfaceShape2.iog.og[2].gco";
connectAttr "polyBevel2.out" "pCubeShape1.i";
connectAttr "nucleus1.stf" "nRigidShape1.stf";
connectAttr ":time1.o" "nRigidShape1.cti";
connectAttr "pCubeShape1.w" "nRigidShape1.imsh";
connectAttr "makeNurbCircle1.oc" "nurbsCircleShape1.cr";
connectAttr "nucleus1.stf" "nRigidShape2.stf";
connectAttr ":time1.o" "nRigidShape2.cti";
connectAttr "pCylinderShape1.w" "nRigidShape2.imsh";
connectAttr "mia_physicalsun2.msg" "sunShape.mils";
connectAttr "mia_physicalsun2.msg" "sunShape.mipe";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "nParticleCloudSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "mia_material1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "mia_material1SG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "nParticleCloudSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "mia_material1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "mia_material1SG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":time1.o" "nucleus1.cti";
connectAttr "nParticleShape1.cust" "nucleus1.niao[0]";
connectAttr "nParticleShape1.stst" "nucleus1.nias[0]";
connectAttr "nRigidShape1.cust" "nucleus1.nipo[0]";
connectAttr "nRigidShape2.cust" "nucleus1.nipo[1]";
connectAttr "nRigidShape1.stst" "nucleus1.nips[0]";
connectAttr "nRigidShape2.stst" "nucleus1.nips[1]";
connectAttr "npCloudBlinn.oc" "nParticleCloudSE.ss";
connectAttr "npCloudVolume.oi" "nParticleCloudSE.vs";
connectAttr "nParticleShape1.iog" "nParticleCloudSE.dsm" -na;
connectAttr "nParticleCloudSE.msg" "materialInfo1.sg";
connectAttr "npCloudBlinn.msg" "materialInfo1.m";
connectAttr "particleSamplerInfo1.msg" "materialInfo1.t" -na;
connectAttr "particleSamplerInfo1.oc" "npCloudBlinn.c";
connectAttr "particleSamplerInfo1.ot" "npCloudBlinn.it";
connectAttr "particleSamplerInfo1.oi" "npCloudBlinn.ic";
connectAttr "particleSamplerInfo1.ot" "npCloudVolume.t";
connectAttr "particleSamplerInfo1.oc" "npCloudVolume.c";
connectAttr "particleSamplerInfo1.oi" "npCloudVolume.i";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miContourPreset.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":Draft.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":DraftMotionBlur.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":DraftRapidMotion.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":Preview.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":PreviewMotionblur.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":PreviewRapidMotion.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":PreviewCaustics.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":PreviewGlobalIllum.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":PreviewFinalGather.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":Production.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":ProductionMotionblur.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":ProductionRapidMotion.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":ProductionFineTrace.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":ProductionRapidFur.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":ProductionRapidHair.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "mentalrayIblShape1.msg" ":mentalrayGlobals.ibl";
connectAttr "mia_physicalsky1.msg" ":mentalrayGlobals.sunAndSkyShader";
connectAttr "peak1GroupParts.og" "peak1.ip[0].ig";
connectAttr "peak1GroupId.id" "peak1.ip[0].gi";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "peak1GroupId.msg" "peak1Set.gn" -na;
connectAttr "polySurfaceShape2.iog.og[0]" "peak1Set.dsm" -na;
connectAttr "peak1.msg" "peak1Set.ub[0]";
connectAttr "tweak1.og[0]" "peak1GroupParts.ig";
connectAttr "peak1GroupId.id" "peak1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "polySurfaceShape2.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "polySurfaceShape1.o" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "tweak2GroupParts.og" "tweak2.ip[0].ig";
connectAttr "tweak2GroupId.id" "tweak2.ip[0].gi";
connectAttr "tweak2GroupId.msg" "tweak2Set.gn" -na;
connectAttr "polySurfaceShape2.iog.og[2]" "tweak2Set.dsm" -na;
connectAttr "tweak2.msg" "tweak2Set.ub[0]";
connectAttr "peak1.og[0]" "tweak2GroupParts.ig";
connectAttr "tweak2GroupId.id" "tweak2GroupParts.gi";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "lambert2SG.msg" "materialInfo2.sg";
connectAttr "lambert2.msg" "materialInfo2.m";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "pCubeShape4.iog" "blinn1SG.dsm" -na;
connectAttr "pCubeShape5.iog" "blinn1SG.dsm" -na;
connectAttr "pCubeShape6.iog" "blinn1SG.dsm" -na;
connectAttr "pCubeShape7.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo3.sg";
connectAttr "blinn1.msg" "materialInfo3.m";
connectAttr "mia_material1.S67" "mia_material1SG.mips";
connectAttr "mia_material1.S67" "mia_material1SG.miss";
connectAttr "mia_material1.S67" "mia_material1SG.mims";
connectAttr "mia_material1SG.msg" "materialInfo4.sg";
connectAttr "mia_material1.msg" "materialInfo4.m";
connectAttr "mia_material1.S67" "mia_material1SG1.mims";
connectAttr "mia_material1.S67" "mia_material1SG1.mips";
connectAttr "mia_material1.S67" "mia_material1SG1.miss";
connectAttr "mia_material1SG1.msg" "materialInfo5.sg";
connectAttr "mia_material1.msg" "materialInfo5.m";
connectAttr "emitter1.tmrz" "addDoubleLinear3.i1";
connectAttr "motionPath1.zc" "addDoubleLinear3.i2";
connectAttr "motionPath1_uValue.o" "motionPath1.u";
connectAttr "nurbsCircleShape1.ws" "motionPath1.gp";
connectAttr "positionMarkerShape1.t" "motionPath1.pmt[2]";
connectAttr "positionMarkerShape2.t" "motionPath1.pmt[3]";
connectAttr "emitter1.tmry" "addDoubleLinear2.i1";
connectAttr "motionPath1.yc" "addDoubleLinear2.i2";
connectAttr "emitter1.tmrx" "addDoubleLinear1.i1";
connectAttr "motionPath1.xc" "addDoubleLinear1.i2";
connectAttr ":time1.o" "nParticleShape1Cache1.tim";
connectAttr "phong1.oc" "phong1SG.ss";
connectAttr "polySurfaceShape2.iog" "phong1SG.dsm" -na;
connectAttr "polySurfaceShape1.iog" "phong1SG.dsm" -na;
connectAttr "phong1SG.msg" "materialInfo6.sg";
connectAttr "phong1.msg" "materialInfo6.m";
connectAttr "mia_physicalsky1.S00" "mia_physicalsun2.S00";
connectAttr "mia_physicalsky1.S01" "mia_physicalsun2.S01";
connectAttr "mia_physicalsky1.S02" "mia_physicalsun2.S02";
connectAttr "mia_physicalsky1.S03" "mia_physicalsun2.S03";
connectAttr "mia_physicalsky1.S04" "mia_physicalsun2.S04";
connectAttr "mia_physicalsky1.S05" "mia_physicalsun2.S05";
connectAttr "mia_physicalsky1.S06" "mia_physicalsun2.S06";
connectAttr "mia_physicalsky1.S18" "mia_physicalsun2.S12";
connectAttr "sunDirection.msg" "mia_physicalsky1.S11";
connectAttr "mia_exposure_simple1.msg" "mia_physicalsky1.miSkyExposure";
connectAttr "polyCylinder1.out" "deleteComponent2.ig";
connectAttr "polyBevel1.out" "polyBevel2.ip";
connectAttr "pCubeShape1.wm" "polyBevel2.mp";
connectAttr "deleteComponent1.og" "polyBevel1.ip";
connectAttr "pCubeShape1.wm" "polyBevel1.mp";
connectAttr "polyCube1.out" "deleteComponent1.ig";
connectAttr "tweak2.og[0]" "polySmoothFace1.ip";
connectAttr "polySmoothFace1.out" "polyAverageVertex1.ip";
connectAttr "polySurfaceShape2.wm" "polyAverageVertex1.mp";
connectAttr "nParticleCloudSE.pa" ":renderPartition.st" -na;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "mia_material1SG.pa" ":renderPartition.st" -na;
connectAttr "mia_material1SG1.pa" ":renderPartition.st" -na;
connectAttr "phong1SG.pa" ":renderPartition.st" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "npCloudBlinn.msg" ":defaultShaderList1.s" -na;
connectAttr "npCloudVolume.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "mia_material1.msg" ":defaultShaderList1.s" -na;
connectAttr "phong1.msg" ":defaultShaderList1.s" -na;
connectAttr "mentalrayIblShape1.ltd" ":lightList1.l" -na;
connectAttr "spotLightShape1.ltd" ":lightList1.l" -na;
connectAttr "sunShape.ltd" ":lightList1.l" -na;
connectAttr "mia_physicalsun1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "mia_physicalsun2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "mia_physicalsky1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "mentalrayIbl1.iog" ":defaultLightSet.dsm" -na;
connectAttr "spotLight1.iog" ":defaultLightSet.dsm" -na;
connectAttr "sunDirection.iog" ":defaultLightSet.dsm" -na;
// End of peak_watersim.ma
