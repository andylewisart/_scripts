'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
#import maya.mel as mel
import pymel.core as pm

import pyside_util
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'Inhance Reference Tool'
WINDOW_VERSION = 1.0
WINDOW_NAME = 'Reference_tool_window'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'inhanceReferenceEditor_01.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
   def __init__( self, parent = pyside_util.get_maya_window(), *args ):
      super( Reference_Tool, self ).__init__( parent )
      self.setupUi( self )
      
      projDir = re.sub(r"/Maya/","",cmds.workspace( q=True,rd=True))
      print projDir


      self.newAsset_episodeLnEdit.setText(projDir)
      self.episode_lnEdit.setText(projDir)
      self.getReferences('ref_asset_list', projDir)
      self.getReferences('existAsset_list', projDir)
      self.existingReferences('ref_asset_list')


      #reference tab UI
      self.setWindowTitle( '{0} {1}'.format( WINDOW_TITLE, str( WINDOW_VERSION ) ) )
      self.ok_btn.clicked.connect( self.referenceReferences )
      self.select_project_btn.clicked.connect( lambda: self.getEpisode("episode_lnEdit","ref_asset_list"))
      self.episode_lnEdit.returnPressed.connect(lambda: self.getReferences("ref_asset_list", self.episode_lnEdit.text()))
      self.cancel_btn.clicked.connect( self.close )


      #publish tab UI
      self.newAsset_episodeButton.clicked.connect( lambda: self.getEpisode("newAsset_episodeLnEdit","existAsset_list"))
      self.newAsset_episodeLnEdit.returnPressed.connect(lambda: self.getReferences("existAsset_list", self.newAsset_episodeLnEdit.text()))
      self.existAsset_publish_button.clicked.connect( self.versionUp)
      self.newAsset_publish_button.clicked.connect( self.createAssetDir)

      self.exit_btn.clicked.connect( self.close )

      self.show()
    

   def getEpisode(self, ui_lnEdit, ui_list):
      '''Select maya project dir for current episode'''
      #Open file browser

      selection = pm.fileDialog2(fm = 3)[0]

      selection = selection.replace("\\", "/")

      cmdLnEditString = "self.%s.setText(selection)"%ui_lnEdit
      #print cmdLnEditString
      exec cmdLnEditString

      #my ghetto way of pathing this to other functions

      getReferences(ui_list,selection)


   def getReferences(self, ui_list, selection):
      clear = ("self.%s.clear()")%ui_list
      exec clear
      referenceFiles = []
      path = selection+"/Maya/assets/_publish"
      assets = os.listdir(path)


      for i, d in enumerate(assets):

        types = os.listdir(path+"/"+d)
        
        # types.append(path + "/" +str(d)+"/model/")
        # types.append(path + "/" +str(d)+"/rig/")
        #select the files 
        for s, t in enumerate(types):
          #print t + " bree"
          if t == "model":
            files = path+"/"+d +"/"+t
            files = os.listdir(files)
            for j, k in enumerate(files):
              print k
              if os.path.isfile(path+"/"+d+"/model/"+k):
                referenceFiles.append(path+"/"+d+"/model/"+k)
          elif t == "rig":
            files = path+"/"+d +"/"+t
            files = os.listdir(files)
            for j, k in enumerate(files):
              if os.path.isfile(path+"/"+d+"/rig/"+k):
                referenceFiles.append(path+"/"+d+"/rig/"+k)

      for i, d in enumerate(referenceFiles):
        if re.findall("_v\d\d\.", d) !=[]:
          pass
        else:
          cmdListString ="self.%s.addItem(d)"%ui_list
        #print cmdListString
          exec cmdListString
      
   def referenceReferences(self):
      for i in range (self.scene_ref_list.count()):
         makeRef =  self.scene_ref_list.item(i).text()
         namespace =re.findall(r"/(\w+)\.", makeRef)
         print namespace
         cmds.file( makeRef, reference=True, namespace = namespace[0])

   def versionUp(self):
      assetSelect = self.existAsset_list.currentItem().text()
      assetPath = os.path.dirname(assetSelect)
      assetFile = os.path.splitext(os.path.basename(assetSelect))[0]
      allAssets = os.listdir(assetPath)
      print assetPath, assetFile, allAssets

      if pm.ls('MASTER_geo', sl = True, type = 'transform' ) or pm.ls('MASTER_rig', sl = True, type = 'transform' ):
        versions = []
        for i, d in enumerate(allAssets):
          test = re.findall(r"_v\d\d\.", d)
          if test != []:
            versions.append(test)

          print versions

        if versions == []:
          cmds.file(assetPath+"/"+assetFile+"_v01", es = True, type = 'mayaBinary', f = True)
          cmds.file(assetPath+"/"+assetFile, es = True, type = 'mayaBinary', f = True)

        elif versions[0]:
          version = 1
          while True:
              v = assetPath+"/"+assetFile+"_v"+ str('%02d' % version)+".mb"
              if os.path.isfile(v) == True:
                  print v + "CHECK"
                  version +=1
              else:
                  break
          cmds.file(v, es = True, type = 'mayaBinary', f = True)
          cmds.file(assetPath+"/"+assetFile, es = True, type = 'mayaBinary', f = True)

        else:
          pm.warning("Error finding version of publish")


      else:
        pm.warning( "Please select transform node named either MASTER_geo or MASTER_rig" )


   def createAssetDir(self):
      name = self.newAsset_name_lnEdit.text()
      path = self.newAsset_episodeLnEdit.text()+"/Maya/assets/_publish/"+name
      if name=='Name' or name =='':
        pm.warning("Please name asset") 
        return 0

      if self.model_radioBtn.isChecked():
        if os.path.isdir(path):
          pm.warning("Asset already exists! Use existing asset panel to publish.")
        else:
          print path
          print 'model checked'
          os.mkdir(path)
          os.mkdir(path+"/model")
          cmds.file(path+"/model"+"/"+name+"_v01", es = True, type = 'mayaBinary', f = True)
          cmds.file(path+"/model"+"/"+name, es = True, type = 'mayaBinary', f = True)
      elif self.rig_radioBtn.isChecked():
        print 'rig checked'
        if os.path.exists(path):
          if os.path.exists(path+"/rig"):
            pm.warning("Rig asset already exists! Use existing asset panel to publish.")
          else:
            os.mkdir(path+"/rig")
            cmds.file(path+"/rig"+"/"+name+"_v01", es = True, type = 'mayaBinary', f = True)
            cmds.file(path+"/rig"+"/"+name, es = True, type = 'mayaBinary', f = True)
        else:
          pm.warning("Asset directory not found. Is the model published yet? Is the name spelled consistently?")
      else:
        pm.warning("Radio button problem???")

   def existingReferences(self,ui_list):
      references = cmds.file( q=True, l=True )
      for i, d in enumerate(references):
        print d + "QUERY"



def testing():
  print "TACO"
            


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'inhanceReferenceEditor_01.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
