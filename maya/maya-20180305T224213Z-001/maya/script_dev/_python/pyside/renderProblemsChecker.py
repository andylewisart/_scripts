'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re
import time

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm

import pyside_util
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'Render Problems Checker'
WINDOW_VERSION = 0.02
WINDOW_NAME = 'renderProblemsChecker'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'renderProblemsChecker.0.02.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
  def __init__( self, parent = pyside_util.get_maya_window(), *args ):
    super( Reference_Tool, self ).__init__( parent )
    self.setupUi( self )

    self.checkScene_btn.clicked.connect(self.checkScene)
    self.materials_tableWidget.cellClicked.connect(self.selectThing)
    self.materials_tableWidget.itemChanged.connect(self.attrChanged)

    self.show()


  def selectThing(self):

    thing = self.materials_tableWidget.currentItem().text()
    try:
      pm.select(thing)
    except: 
      pass

  def checkScene(self):
    matSel = pm.ls(type = 'VRayFastSSS2')

    for i in pm.ls(type='VRayMtl'):
      matSel.append(i)
    for i in pm.ls(type='VRayLightMtl'):
      matSel.append(i)
    for i in pm.ls(type='VRayFlakesMtl'):
      matSel.append(i)
    print matSel

    
    for i, d in enumerate(matSel):
      selAttrs = pm.listAttr(d)
      for j, k in enumerate(selAttrs):
        if re.findall(r'ubdiv', k)!=[]:
        #try:
          if pm.getAttr(d+"."+k)>32:
            #count+=1
            pm.warning("Subdivs too high!")

            materialCol = QtGui.QTableWidgetItem()
            materialCol.setText("Material")
            self.materials_tableWidget.setHorizontalHeaderItem(0,materialCol)

            attrCol = QtGui.QTableWidgetItem()
            attrCol.setText("Attribute")
            self.materials_tableWidget.setHorizontalHeaderItem(0,attrCol)

            subdivCol = QtGui.QTableWidgetItem()
            subdivCol.setText("Value")
            self.materials_tableWidget.setHorizontalHeaderItem(1,subdivCol)

            lastrow= self.materials_tableWidget.rowCount()
            self.materials_tableWidget.insertRow(lastrow)
            item = QtGui.QTableWidgetItem()
            item.setText("{0}".format(d))
            self.materials_tableWidget.setItem(lastrow, 0, item)
            #toggle editable flag
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)

            subdivs = QtGui.QTableWidgetItem()
            subdivs.setText("{0}".format(pm.getAttr(d+"."+k)))
            self.materials_tableWidget.setItem(lastrow, 1, subdivs)

  def attrChanged(self):
    print self.materials_tableWidget.currentItem().text()
    col = self.materials_tableWidget.currentColumn()
    row = self.materials_tableWidget.currentRow()
    print (self.materials_tableWidget.item(row, col-1)).text()
    #pm.setAttr((self.materials_tableWidget.item(row, col-1))

            

        # except:
        #   print "EXCEPT"
        #   continue



  def testing(self):
    print "check"



  #self.statusBar().showMessage(sender.text() + ' was pressed')
  #btn = QtGui.QPushButton('Button', self)
          


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'renderProblemsChecker.0.02.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
