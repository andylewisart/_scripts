'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re
import time

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm

import pyside_util
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'Blend Shape Control Generator'
WINDOW_VERSION = 0.01
WINDOW_NAME = 'blendShapeControlGenerator'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'blendShapeControlGenerator.0.01.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
  def __init__( self, parent = pyside_util.get_maya_window(), *args ):
    super( Reference_Tool, self ).__init__( parent )
    self.setupUi( self )

    self.model_btn.clicked.connect( self.loadModel )

    self.show()

  def loadModel(self):

    sel = pm.ls(sl=1, type = 'mesh')[0]
    self.model_lineEdit.setText(str(sel))

    inputs = pm.listHistory(sel)

    for i in inputs:
        if pm.nodeType(i) == 'blendShape':
            self.bShp_listWidget.addItem(str(i))



  def testing(self):
    print "check"


          


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'blendShapeControlGenerator.0.01.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
