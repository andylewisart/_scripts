'''
========================================================================
INSTRUCTIONS::
========================================================================

Unzip files into your script directory.
(Keep pyside_example.py, pyside_util.py, pyside_example_ui.py, and pyside_example.ui together )

Open Maya 2014

# To run .ui file
import pyside_example
pyside_example.show_ui()

# To run compiled .ui file
import pyside_example
pyside_example.show_compiled()

========================================================================
UI TO PYSIDE::
========================================================================

Install PySide

Open command line

Run:
PYTHONLOCATION\python PYTHONLOCATION\pyside-uic.exe SCRIPTLOCATION\pyside_example.ui -o SCRIPTLOCATION\pyside_example.py

========================================================================
---->  Import Modules  <----
========================================================================
'''
import os
import sys
import re
import time

import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm

import pyside_util
#import pyside_example_ui

'''
========================================================================
---->  Global Variables  <----
========================================================================
'''
TOOLS_PATH = os.path.dirname( __file__ )

WINDOW_TITLE = 'Set Driven Key Master'
WINDOW_VERSION = 0.01
WINDOW_NAME = 'set_driven_key_master'

UI_FILE_PATH = os.path.join( TOOLS_PATH, 'SDK_interface.0.01.ui' )
UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

'''
========================================================================
---->  Create/Connect UI Functionality  <----
========================================================================
'''
class Reference_Tool( BASE_CLASS, UI_OBJECT ):
  def __init__( self, parent = pyside_util.get_maya_window(), *args ):
    super( Reference_Tool, self ).__init__( parent )
    self.setupUi( self )

    self.selectDriver_btn.clicked.connect( self.selectDriver )
    self.selectDriven_btn.clicked.connect( self.selectDriven )


    self.show()


  def selectDriver(self):
    sel = cmds.ls(sl=1)
    if len(sel) > 1:
      cmds.warning("Please select only one driver at a time")
    else:
      self.drivingObj_textEdit.setText(sel[0])
      attrList = cmds.listAttr(sel, k=1)
      for i, d in enumerate(attrList):
        self.drivingAttr_listWidget.addItem(d)

  def selectDriven(self):
    self.drivenObjAttr_listWidget.clear()
    self.drivenObjects_listWidget.clear()

    sel = cmds.ls(sl=1)

    attrList = []
    for i, c in enumerate(sel):
      self.drivenObjects_listWidget.addItem(c)
      attrList.append(cmds.listAttr(c, k=1))

    itr=0
    commonList = []
    for e in attrList:
        if itr != 0:    
          commonList = set(attrList[itr]) & set(attrList[itr-1])
        else:
          commonList = attrList[itr]
        itr+=1

    for f in commonList:
      self.drivenObjAttr_listWidget.addItem(f)

  def testing(self):
    print self.objID_lnEdit.text()



  #self.statusBar().showMessage(sender.text() + ' was pressed')
  #btn = QtGui.QPushButton('Button', self)
          


'''
========================================================================
---->  Show .ui File  <----
========================================================================
'''
def show_ui():
   UI_FILE_PATH = os.path.join( TOOLS_PATH, 'SDK_interface.0.01.ui' )
   UI_OBJECT, BASE_CLASS = pyside_util.get_pyside_class( UI_FILE_PATH )

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
  
'''
========================================================================
---->  Show PySide File  <----
========================================================================
''' 
def show_compiled():
   BASE_CLASS = QtGui.QMainWindow
   UI_OBJECT = pyside_example_ui.Ui_Reference_tool_window

   if cmds.window( WINDOW_NAME, exists = True, q = True ):
      cmds.deleteUI( WINDOW_NAME )
      
   Reference_Tool()
