// Copy mel file into your user scripts folder 
// (example: MyDocuments/maya/version/scripts)
// overwrite if necessary
// Gen T
// http://gennyx.blogspot.com
//-----------------------------------------
// These modifications were made:
//-----------------------------------------


//06/07/2014

Corrected error where changing the memory limit caused an error due to a typo in control name.


//03/22/2014

Added option to divide and render image into tiles in cases where system resources cannot
accommodate the image resolution (for print). Good suggestion David!
 
Added "Skip Existing Frames" checkbox from Render settings

UI changes:

-Job priority changed from integer entry to drop down menu

-Enclosed all controls under a parent layout so now the window is resizable and the submit job buttons are always accessable regardless of scrolling

-Categorized groups of controls with better labeling

/////////////////////////////////////////////////////////////////////////


// 12/20/2012
// Thanks to Francois Maree for the suggestion

Auto Threads - Automatically use available threads

By Frame - Render frames in increments of this value. Render settings must be set to render out a sequence of images

//:::::::::::::::::::::::::::::::::::::::::::::::::

// fixed
//12/22/2012
//error where the byFrame control could not be found.


/////////////////////////////////////////////////////////////////////////
// 11/30/2012

Render Thread � Specify the number of CPU threads

Verbosity � Level of detail in render progress messages

Auto Memory Limit � Dynamically calculate memory before each render

Memory Limit - Soft cap for memory used by Mental Ray

Time Out � Amount of time before job aborts and restarts

/////////////////////////////////////////////////////////////////////////




