﻿
var everyItem = app.project.items;
var selItems = app.project.selection;
var regex = /\/v\d\d\d(\/)/;  
var regex2 = /.+(?=v\d\d\d)/;
var regex3 = /v\d\d\d/; 
var regex4 = /.+(?=\d\d\d\d\.)/;

//create isSequence() function
if (!FootageItem.prototype.isSequence){  
  FootageItem.prototype.isSequence = function() //based on code posted by Paul Tuersley at www.aenhancers.com  
  {  
  var isIt = false;  
  if(!this.footageMissing && this.mainSource.file.exists)  
  {  
  var importFile = new File(this.mainSource.file);  
  var importOptions = new ImportOptions(importFile);  
  var tempImport = app.project.importFile(importOptions);  
  if (tempImport.mainSource.isStill && !this.mainSource.isStill) isIt = true;  
  tempImport.remove();  
  }  
  return isIt;  
  }  
}  

for(var o=0 ; o<=selItems.length-1; o++)
{

    if (selItems[o] instanceof FootageItem)
    {
        if (selItems[o].file!=undefined)
        {
            var thisName = selItems[o].name
            var curPath = selItems[o].file.fullName;
            var thisAlphaMode = selItems[o].mainSource.alphaMode;
            var splitPath = curPath.split(regex3.exec(curPath));
//~             $.writeln(splitPath[1])
            //split out path before version numbers
            var myfolder= Folder(splitPath[0]);
            var truncFileName = regex4.exec(splitPath[1])
            truncFileName = String(truncFileName).replace(/\//, "");
//~             $.writeln(truncFileName)
            var latestVersion = new File();
            
            retrievedFiles = myfolder.getFiles ("*")
            
            for(i in retrievedFiles)
            {         
                if(regex3.test(retrievedFiles[i].name))
                {
                    if(retrievedFiles[i].name > latestVersion.name)
                    {
                        latestVersion = retrievedFiles[i];
                     }
                }
            }
        str1 = truncFileName+".+"
        var re = new RegExp(str1, "g");
        latestFile = latestVersion.getFiles (re)[0]
//~         $.writeln("%%%%")
        $.writeln(latestFile);
        
        
        
            try
            {
                if (selItems[o].isSequence())
                {
                    selItems[o].replaceWithSequence(new File(latestFile),false);
                }
                else
                {
                    selItems[o].replace(new File(latestFile));
                }
                 selItems[o].name = thisName;
                 selItems[o].mainSource.alphaMode = thisAlphaMode;
             }
            catch(e)
            {
                $.writeln(e)
             }
             
        }
    }
}



