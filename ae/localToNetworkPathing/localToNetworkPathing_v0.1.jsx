﻿$.writeln(app.project.item[0])
allItems = app.project.items;

if (!FootageItem.prototype.isSequence){  
  FootageItem.prototype.isSequence = function() //based on code posted by Paul Tuersley at www.aenhancers.com  
  {  
  var isIt = false;  
  if(!this.footageMissing && this.mainSource.file.exists)  
  {  
  var importFile = new File(this.mainSource.file);  
  var importOptions = new ImportOptions(importFile);  
  var tempImport = app.project.importFile(importOptions);  
  if (tempImport.mainSource.isStill && !this.mainSource.isStill) isIt = true;  
  tempImport.remove();  
  }  
  return isIt;  
  }  
}  

  for (var i = 1; i <= app.project.numItems; i++)
  {
   if (app.project.item(i) instanceof FootageItem) 
   {
       try
       {
//~        $.writeln(app.project.item(i).reflect.properties)
       $.writeln(app.project.item(i).file.fullName)
        var thisLocation = app.project.item(i).file.fullName;
        var thisAlphaMode = app.project.item(i).mainSource.alphaMode;
        if (thisLocation.indexOf("/z/")>-1)
        {
            try
                {
                    if (app.project.item(i).isSequence())
                    {
                        app.project.item(i).replaceWithSequence(new File(thisLocation.replace("/z/", "//thanos/3D/")), false);
                     }
                     else
                     {
                         app.project.item(i).replace(new File(thisLocation.replace("/z/", "//thanos/3D/")));
                     }
                    app.project.item(i).mainSource.alphaMode = thisAlphaMode;

                 }
            catch(e)
                {
                    $.writeln(e)
                 }
         }
        }
    catch(f)
    {
        $.writeln(f)
        }
    }
  }
