﻿/* 
 Name:
	JMC_Folders
 Version:
	2.0 (29th January, 2014 ) by Justin McClure Creative
      Changelog:
	First Launch
 Author:
	Janson Linn, updated by Justin McClure
 Description:  
	This script allows users to generate the JMC file structure quickly and easily, so they can GYST! 
    
 Usage:
	In After Effects CS4 or later, run the script
	Hit Generate Folders!
*/

  
var win = new Window('palette', 'JMC Folder Structure');
var testLabel = win.add('statictext', undefined, 'Developed by Justin McClure Creative');
var btnSelect = win.add('button', undefined, 'Generate Folders!');


btnSelect.onClick = function(){
    
var compFolder0 = app.project.items.addFolder('Comps'); 
var compFolder1= app.project.items.addFolder('Precomps');//folder1
var compFolder3= app.project.items.addFolder('Master Comps');//folder2
compFolder1.parentFolder = compFolder0;
compFolder3.parentFolder = compFolder0;//makes folder 3 child of folder1

var compFolder0 = app.project.items.addFolder('Solids');

//initiates QT set
var compFolder = app.project.items.addFolder('Assets'); 
var compFolder2= app.project.items.addFolder('Video');//folder1
var compFolder3= app.project.items.addFolder('Images');//folder2
var compFolder4= app.project.items.addFolder('PSD');//folder3
var compFolder5= app.project.items.addFolder('Seq');
var compFolder6= app.project.items.addFolder('Audio');
var compFolder7= app.project.items.addFolder('Other');
var compFolder8= app.project.items.addFolder('AI');
compFolder2.parentFolder = compFolder;//makes folder 2 child of folder1
compFolder3.parentFolder = compFolder;//makes folder 3 child of folder1
compFolder4.parentFolder = compFolder;
compFolder5.parentFolder = compFolder;
compFolder6.parentFolder = compFolder;
compFolder7.parentFolder = compFolder;
compFolder8.parentFolder = compFolder;

//alerts user on completion
//alert('Folders were successfully generated. Enjoy.');
}
 

win.center();
win.show();