{
   //include JSON API
   function JSON2API(){
    "object"!=typeof JSON&&(JSON={}),function(){"use strict"
    function f(t){return 10>t?"0"+t:t}function quote(t){return escapable.lastIndex=0,escapable.test(t)?'"'+t.replace(escapable,function(t){var e=meta[t]
    return"string"==typeof e?e:"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+t+'"'}function str(t,e){var r,n,o,f,u,p=gap,i=e[t]
    switch(i&&"object"==typeof i&&"function"==typeof i.toJSON&&(i=i.toJSON(t)),"function"==typeof rep&&(i=rep.call(e,t,i)),typeof i){case"string":return quote(i)
    case"number":return isFinite(i)?i+"":"null"
    case"boolean":case"null":return i+""
    case"object":if(!i)return"null"
    if(gap+=indent,u=[],"[object Array]"===Object.prototype.toString.apply(i)){for(f=i.length,r=0;f>r;r+=1)u[r]=str(r,i)||"null"
    return o=0===u.length?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+p+"]":"["+u.join(",")+"]",gap=p,o}if(rep&&"object"==typeof rep)for(f=rep.length,r=0;f>r;r+=1)"string"==typeof rep[r]&&(n=rep[r],o=str(n,i),o&&u.push(quote(n)+(gap?": ":":")+o))
    else for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(o=str(n,i),o&&u.push(quote(n)+(gap?": ":":")+o))
    return o=0===u.length?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+p+"}":"{"+u.join(",")+"}",gap=p,o}}"function"!=typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()})
    var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","  ":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep
    "function"!=typeof JSON.stringify&&(JSON.stringify=function(t,e,r){var n
    if(gap="",indent="","number"==typeof r)for(n=0;r>n;n+=1)indent+=" "
    else"string"==typeof r&&(indent=r)
    if(rep=e,e&&"function"!=typeof e&&("object"!=typeof e||"number"!=typeof e.length))throw Error("JSON.stringify")
    return str("",{"":t})}),"function"!=typeof JSON.parse&&(JSON.parse=function(text,reviver){function walk(t,e){var r,n,o=t[e]
    if(o&&"object"==typeof o)for(r in o)Object.prototype.hasOwnProperty.call(o,r)&&(n=walk(o,r),void 0!==n?o[r]=n:delete o[r])
    return reviver.call(t,e,o)}var j
    if(text+="",cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(t){return"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})),/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),"function"==typeof reviver?walk({"":j},""):j
    throw new SyntaxError("JSON.parse")})}()
   }
   JSON2API();

   function SpritesheetTools(thisObj){
      
      //Scripts version number
      var sheetah_ver = "2.4.2";

      //File Pattern Items include variable names to be attached
      //to the filename of a generated spritesheet

      //Examples:
      //_#[30,6,5,15]
      //_#[48,48]

      var filePatterns = [];
      filePatterns.push(['sprites','columns','rows','fps']);
      filePatterns.push(['spriteWidth','spriteHeight']);

      //the current Active File Pattern Preset that will be applied
      var filePatternIndex = 1;


      var scope = thisObj;

      function writeTxtFile(data){

          // prompt to save file
          var theFile = new File("~/temp/"+app.project.activeItem.name+".json");
          theFile = theFile.saveDlg('Save as:');

          // if user didn't cancel...
          if (theFile != null) {
            
            // open file for "w"riting,
            theFile.open("w","TEXT","????");
            
            theFile.writeln(data);
            
            // close the text file
            theFile.close();
            
            // open text file in default app
            //theFile.execute();
          }

      }
      
      function exportFrame(spriteComp, ext){

          //render the Composition
          var ext = ext.toLowerCase();

          //Template
          var templateBasename = ext.toUpperCase()+" with Alpha";
          var renderTemplate = false;
          var outputTemplate = false;

          //shortcut to RenderQueue
          var queue = app.project.renderQueue;
          
          //deny rendering on all present RenderQueue Items
          for (var i = 0; i < queue.items.length; i++) {
            var item = queue.item(i+1);
            
            if (item.status === RQItemStatus.DONE) {

              item.remove();

            }else if (item.status === RQItemStatus.QUEUED || item === RQItemStatus.WILL_CONTINUE || item === RQItemStatus.NEEDS_OUTPUT) {
              
              item.render = false;

            }

          }

          //check if templates are present
          var tempComp = app.project.items.addComp('temp',4.0,4.0,1.0,1.0,1.0);
          var rqi_tempComp = queue.items.add(tempComp);

          //check if render template exists
          for(var i = 0; i<rqi_tempComp.templates.length; i++){
            if(rqi_tempComp.templates[i] === templateBasename+" Render"){
              renderTemplate = rqi_tempComp.templates[i];
              break;
            }
          }

          //check if output template exists
          for(var i = 0; i<rqi_tempComp.outputModules[1].templates.length; i++){
            if(rqi_tempComp.outputModules[1].templates[i] === templateBasename){
              outputTemplate = rqi_tempComp.outputModules[1].templates[i];
              break;
            }
          }

          rqi_tempComp.remove();
          tempComp.remove();

          //extract templates from template file if not found
          if (!renderTemplate || !outputTemplate) {
            
            //import template project to save transparent template
            var exportModuleFilename = templateBasename+".aep";
            var exportModuleFile = ((new File($.fileName)).path) + "/SheetahResources/templates/"+exportModuleFilename;
                exportModuleFile = new ImportOptions(File(exportModuleFile));

            app.project.importFile(exportModuleFile);
            
            //find the right template item
            for (var i = 0; i < queue.items.length; i++) {
              var item = queue.item(i+1);

              if(item.comp.name === ext+"_export"){

                //save render and output templates

                if(!renderTemplate){
                  renderTemplate = templateBasename+" Render";
                  item.saveAsTemplate(renderTemplate);
                }

                if(!outputTemplate){
                  outputTemplate = templateBasename;

                  var file_name = File.decode( "template."+ext ),
                    new_path = "~/template",
                    new_dir = new Folder( new_path ),
                    new_path = new_dir.fsName;

                  var new_data = {
                    "Output File Info": {

                      "Base Path":new_path,
                      "File Name":file_name

                    }
                  };

                  item.outputModules[1].setSettings( new_data );
                  item.outputModules[1].saveAsTemplate(outputTemplate);
                }

                //clear render queue from dummy item
                item.remove();

                //remove imported Template Project Folder
                for (var j = 0; j < app.project.items.length; j++) {
                  var folder = app.project.item(j+1);

                  if(folder instanceof FolderItem && folder.name === exportModuleFilename){
                    folder.remove();
                    break;
                  }
                }

                break;
              }
            }

          }
          var compRenderItem = queue.items.add(spriteComp);

          //apply render and output templates
          compRenderItem.applyTemplate(templateBasename+" Render");
          compRenderItem.outputModules[1].applyTemplate(templateBasename);
          compRenderItem.timeSpanDuration = spriteComp.duration;

          // save file dialog
          var renderFile = new File('~/temp/'+spriteComp.name+'.'+ext);
          renderFile = renderFile.saveDlg('Save Spritesheet',ext.toUpperCase()+':*.'+ext);
          //alert(renderFile.name);

          var file_name = File.decode(renderFile.name),
          new_path = renderFile.path,
          new_dir = new Folder( new_path ),
          new_path = new_dir.fsName;

          var new_data = {
            "Output File Info": {

            "Base Path":new_path,
            "File Name":file_name

            }
          };

          // if user didn't cancel...
          if (renderFile != null) {

            compRenderItem.outputModules[1].setSettings( new_data );

            //rename file to delete the frame tag "00000"
            compRenderItem.onStatusChanged = function() {
              if(compRenderItem.status === RQItemStatus.DONE){
                queue.stopRendering();

                //$.sleep(100);
                var renamedFile = new File(renderFile.path+'/'+renderFile.name+'00000');

                //check if file without frame tag is already present and delete to replace
                //the user already decided to overwrite the file in the first dialog so removing should be okay
                //if we do not remove the file we cannot rename
                var alreadyPresentFile = new File(renderFile.path+'/'+renderFile.name);
                alreadyPresentFile.remove();


                renamedFile.rename(renderFile.name);


              }
            }

            //start render
            queue.render();

          }
      }

      function onSaveSpritesheet(){

        var scriptName = "Save Spritesheet";
        var comp = app.project.activeItem;

        if ((comp == null) || !(comp instanceof CompItem)) {

          alert("Please select or open a composition first.", scriptName);
          return;

        }else{

          //check if animation layers in comp
          var animationLayers = [];

          for (var i = 0; i < comp.layers.length; i++) {
              var layer = comp.layer(i+1);
              
              if (layer.timeRemapEnabled && !!layer.property("Time Remap").expression) {
                
                animationLayers.push({
                  name: layer.name,
                  expression: layer.property("Time Remap").expression
                });

                layer.property("Time Remap").expression = "";
                layer.timeRemapEnabled = false;
              }
          };

          var sprites = parseInt(comp.duration/comp.frameDuration);
          var columns = parseInt(Math.sqrt(sprites));
          var rows = Math.ceil(sprites/columns);

          var tc = scope.columns.text;
          var tr = scope.rows.text;

          //Generate rows and cols distribution based on user settings
          if(tc === "auto"){
            if(tr !== "auto"){
              rows = parseInt(tr);
              rows = rows > sprites ? sprites : rows;
              columns = Math.ceil(sprites/rows);
            }
          }else if(tr === "auto"){
              columns = parseInt(tc);
              columns = columns > sprites ? sprites : columns;
              rows = Math.ceil(sprites/columns);
          }

          var spriteWidth = comp.width;
          var spriteHeight = comp.height;
          var fps = comp.frameRate;

          //Building the file name preset according to the active file pattern preset
          var spriteCompName = comp.name+'_#[';
          for (var i = 0; i < filePatterns[filePatternIndex].length; i++) {
            spriteCompName += eval(filePatterns[filePatternIndex][i]);
            if(i !== filePatterns[filePatternIndex].length-1)
              spriteCompName += ',';
          };
          spriteCompName +=']';

          var finalCompWidth = comp.width*columns,
          	  finalCompHeight = comp.height*rows;

          if(scope.powertwo.value){
          	  finalCompWidth = Math.pow(2,parseInt(Math.log(comp.width*columns-1)/Math.log(2))+1);
          	  finalCompHeight = Math.pow(2,parseInt(Math.log(comp.height*rows-1)/Math.log(2))+1);
          }

          //Check if dimensions are exceeded and give reasonable advice.
          if(finalCompWidth >= 30000 || finalCompHeight >= 30000){
            var error = "Spritesheet Comps ";

            if(finalCompWidth >= 30000 && finalCompHeight >= 30000){
              error+="width ("+finalCompWidth+"px) and height ("+finalCompHeight;
            }else if(finalCompWidth >= 30000){
              error+="width ("+finalCompWidth;
            }else if(finalCompHeight >= 30000){
              error+="height ("+finalCompWidth;
            }

            error+="px) exceeds AEs max. 30000px. err:["+sprites+","+comp.width+","+comp.height+"]";
            alert(error,scriptName);

            error = "Your comps ";
            if(comp.width > 1000){
              error+= "width ("+comp.width+"px)";
            }
            if(comp.width > 1000 && comp.height > 1000){
              error+=" and ";
            }
            if(comp.height > 1000){
              error+= "height ("+comp.height+"px)";
            }
            if((comp.width > 1000 || comp.height > 1000) && sprites > 200){
              error+=" and your ";
            }
            if(sprites > 200){
              error+="framcount ("+sprites+")";
            }

            error+=" is very large. Please check your composition.";
            alert(error,scriptName);

            return;
          }

          var spriteComp = app.project.items.addComp(
                      spriteCompName,                                   //name
                      finalCompWidth,                                   //width
                      finalCompHeight,                                  //height
                      1.0,                                              //pixelAspect
                      1.0,                                              //duration in seconds
                      1.0);                                             //framerate

          var rowi = 0;
          var coli = 0;

          for (var i = 0; i < sprites; i++) {

            var spriteLayer = spriteComp.layers.add(comp);

            spriteLayer.timeRemapEnabled = true;
            spriteLayer.property("Time Remap").expression = i/comp.frameRate;

            var posp = spriteLayer.position;
            var posv = posp.value;

            posv[0] = posv[0] - spriteComp.width/2 + comp.width/2;
            posv[1] = posv[1] - spriteComp.height/2 + comp.height/2;

            posv[0] = posv[0] + comp.width * coli;
            posv[1] = posv[1] + comp.height * rowi;

            posp.setValue(posv);

            coli++;
            if (coli > (columns - 1)) {
              rowi++;
              coli = 0;
            }

          };

          //render spritesheet

          var desiredFormat = scope.dropdown.selection;
          desiredFormat = desiredFormat.toString().replace(/\s/g, '');

          exportFrame(spriteComp, desiredFormat);

          //remove Composition
          spriteComp.remove();

          //restore animation layers
          for (var i = 0; i < comp.layers.length; i++) {
              var layer = comp.layer(i+1);

              for (var j = 0; j < animationLayers.length; j++) {
                if(layer.name === animationLayers[j].name){
                  layer.timeRemapEnabled = true;
                  layer.expression = animationLayers[j].expression;
                }
              }
          }
        
        }

      }


      function onSetup(){

         var scriptName = "Setup Animmation";
         var activeItem = app.project.activeItem;

         if ((activeItem == null) || !(activeItem instanceof CompItem)) {
           
           alert("Please select or open a composition first.", scriptName);
           return;
           
         } else {
           
           var selectedLayers = activeItem.selectedLayers;
           
           if (activeItem.selectedLayers.length == 0) {
               
               alert("Please select a layer in the active comp first.", scriptName);
               return;
               
           } else {

               var layer = selectedLayers[0];

               if(! layer.canSetTimeRemapEnabled){

                  alert("This layer cannot be time remapped. Please Use Comps or MovieClips.", scriptName);
                  return;

               }else{
                  layer.timeRemapEnabled = true;
               }

               app.beginUndoGroup("Sheetah");

               layer.Effects.addProperty("Slider Control");
               layer.property("Time Remap").expression = '(Math.min( parseInt(source.duration/source.frameDuration - 1), Math.max( 0,  parseInt(effect("Slider Control")("Slider")) ) ) | 0)/(1/thisComp.frameDuration)';

               //advance layer to end of composition
               layer.inPoint = 0;
               layer.outPoint = activeItem.duration;
               
               //move keyframes for good looks ;P
               var timeVal = layer.property("Time Remap").keyValue(2);
               layer.property("Time Remap").setValueAtTime(activeItem.duration,timeVal);
               layer.property("Time Remap").removeKey(2);

               //add expression to slider map ranges according to comp Values
               var slider = ((layer.property("Effects")).property("Slider Control")).property("Slider");
               slider.setValueAtTime(0,0);
               //slider.expression = "Math.min( parseInt(source.duration/source.frameDuration), Math.max( 0, value ) ) | 0";

               layer.effectsActive = true;

               app.endUndoGroup();
           }
         }
      }

      function onKeyframesToText(){

        var scriptName = "Export Animations";
        var activeItem = app.project.activeItem;

        if ((activeItem == null) || !(activeItem instanceof CompItem)) {

          alert("Please select or open a composition first.", scriptName);
          return;

        } else {

          var activeComp = activeItem;
          var animations = [];

          for (var i = 0; i < activeComp.layers.length; i++) {
          
            var layer = activeComp.layer(i+1);
            var layerEffects = undefined;
            var layerSliderControl = undefined;

            layerEffects = layer.property("Effects");
            layerSliderControl = (!! layerEffects) ? layerEffects.property("Slider Control") : undefined; 

            if (!! layerEffects && !! layerSliderControl && layer.timeRemapEnabled) {
              
              var anim = {};
              anim.name = layer.name;
              anim.frameRate = activeComp.frameRate;
              anim.frameDuration = activeComp.frameDuration;
              anim.duration = layer.outPoint;
              anim.length = parseInt(anim.duration/activeComp.frameDuration);
              anim.frames = [];

              var time = 0.0;
              for(var frame = 0; frame < anim.length; frame++){
                  anim.frames.push(layer.property("Time Remap").valueAtTime(time,false)*(1/activeComp.frameDuration));
                  time = time+activeComp.frameDuration;
              }

              for(var j=0; j<animations.length; j++){
                if(anim.name === animations[j].name){
                  layer.name += '_'+i;
                  anim.name = layer.name;
                }
              }
              
              animations.push(anim);

            }else{
              //primitive error handling
              var error = "The layer "+layer.name;

              if(!layer.timeRemapEnabled){
                error+= " has no Time Remapping";
              }else if(!(!!layerEffects)){
                error+= " has no Effects attached";
              }else if(!(!!layerSliderControl)){
                error+= " has no Slider Control attached";
              }
              
              error+=". Did you 'Setup' the layer ?";

              alert(error, scriptName);
            }

          }
          if(!(!!scope.checkbox.value)){

            var res="";
            for (var t = 0; t < animations.length; t++) {
              res+=animations[t].name+"\r\n"+animations[t].frames.toString()+"\r\n"+"\r\n";
            }
            
            scope.modal = new Window("window", "Animations");
         	scope.modal.margins = [5,5,5,5];
         	scope.modal.alignChildren = 'left';
         	scope.modalText = scope.modal.add("editText",[0,0,240,300],"",{ readonly:true, borderless:true, multiline:true, scrollable:true });
            scope.modalText.text = res;
            scope.modal.show();

          }else{
            writeTxtFile(JSON.stringify(animations));
          }

        }

      }

      // This function adds a new script-launching button to the palette
      function addScriptButton(palette, buttonLabel, onclickfunc, rect)
      {
         var newButton = palette.add("button", rect, buttonLabel);
         newButton.onClick = onclickfunc;

         return newButton;
      }

      function buildUI(thisObj){
         var panel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Sheetah", undefined, {resizeable:true});

         panel.margins       = 12;
         panel.alignChildren = 'left';
         
         var maxWidth = 180;
         
         var logoGroup = panel.add("group",[0,0,maxWidth,35]);
         logoGroup.orientation = 'stack';

         //Logo
         var logoContainer = logoGroup.add("group",undefined);
         logoContainer.orientation = 'column';
         logoContainer.spacing = 1;
         logoContainer.margin = 1;
         var logo = logoContainer.add("image", [0,0,maxWidth,31],((new File($.fileName)).path)+'/SheetahResources/img/logo.png');
         logo.spacing = 1;
         logo.margin = 1;
         //show version number
         //var versionText = logoContainer.add("staticText {justify:'center', bounds:[0,0,180,15]}");
         var versionText = logoContainer.add("iconbutton",[0,0,180,25],((new File($.fileName)).path)+'/SheetahResources/img/help.png',{style:'toolbutton'});
         versionText.spacing = 1;
         versionText.margin = 1;

         versionText.text = "v"+sheetah_ver;
         
         var loadUrl = function(){
            versionText.value = false;
            (new File(((new File($.fileName)).path)+'/SheetahResources/templates/aescripts.html')).execute();
         };
         logo.onClick = function(){loadUrl()};
         versionText.onClick = function(){loadUrl()};

         //Spritesheet Tools
         var upperGroup = logoGroup.add("panel", [0,0,170,35], "Dimensions (columns, rows)");
         var mr = 8;
         var grWidth = 120;
         var group2 = upperGroup.add("group", [0,0,grWidth,15]);
         group2.orientation = 'row';
         group2.alignment = [ScriptUI.Alignment.LEFT, ScriptUI.Alignment.CENTER];
         scope.columns = group2.add("editText",[0,0,39,15],'',{ borderless:true });
         group2.add("staticText {justify: 'center', text:'x', bounds:[0,0,45,15]}");
         scope.rows = group2.add("editText",[0,0,39,15],'',{ borderless:true});
         upperGroup.hide();

         var defaultColumnsValue;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "SpritesheetsColumnsValue")){
             defaultColumnsValue = app.settings.getSetting("GentlymadSpritesheetTools", "SpritesheetsColumnsValue");
         }
         defaultColumnsValue = (defaultColumnsValue !== undefined) ? defaultColumnsValue : "auto";
         scope.columns.text = defaultColumnsValue;

         scope.columns.onChange = function(){
          var txt = scope.columns.text;

          if(!isNaN(parseInt(txt))){
            var num = Math.abs(parseInt(txt));
            if(num<1){
              num = 1;
            }
            scope.columns.text = ""+num;
            scope.rows.text = "auto";

          }else{
            scope.columns.text = "auto";
          }

          app.settings.saveSetting("GentlymadSpritesheetTools", "SpritesheetsColumnsValue", scope.columns.text);
          app.settings.saveSetting("GentlymadSpritesheetTools", "SpritesheetsRowsValue", scope.rows.text);
         }
         
         var defaultRowsValue;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "SpritesheetsRowsValue")){
             defaultRowsValue = app.settings.getSetting("GentlymadSpritesheetTools", "SpritesheetsRowsValue");
         }
         defaultRowsValue = (defaultRowsValue !== undefined) ? defaultRowsValue : "auto";
         scope.rows.text = defaultRowsValue;

         scope.rows.onChange = function(){
          var txt = scope.rows.text;

          if(!isNaN(parseInt(txt))){
            var num = Math.abs(parseInt(txt));
            if(num<1){
              num = 1;
            }
            scope.rows.text = ""+num;
            scope.columns.text = "auto";

          }else{
            scope.rows.text = "auto";
          }

          app.settings.saveSetting("GentlymadSpritesheetTools", "SpritesheetsRowsValue", scope.rows.text);
          app.settings.saveSetting("GentlymadSpritesheetTools", "SpritesheetsColumnsValue", scope.columns.text);
         }

         // group
         var group = panel.add("group", [0,0,maxWidth,30]);
         addScriptButton(group, "Save Spritesheet", onSaveSpritesheet,[0,0,115,30]);
         
         // format dropdown and options
         scope.dropdown = group.add("dropdownlist", [0,0,55,30]);
         scope.dropdown.add("item"," png");
         //scope.dropdown.add("item"," psd");
         scope.dropdown.add("item"," tga");
         scope.dropdown.add("item"," tif");

         //default dropdown selection
         var defaultSelection;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "spritesheetFormatSelectionIndex")){
            defaultSelection = app.settings.getSetting("GentlymadSpritesheetTools", "spritesheetFormatSelectionIndex");
         }
         defaultSelection = (defaultSelection !== undefined) ? parseInt(defaultSelection) : 0;
         scope.dropdown.selection = defaultSelection;

         //save user selection in preferences
         scope.dropdown.onChange = function(){
            app.settings.saveSetting("GentlymadSpritesheetTools", "spritesheetFormatSelectionIndex", scope.dropdown.selection.index);
         }

         // --- end of spritesheet tools
         panel.add("image", [0,0,maxWidth,6],((new File($.fileName)).path)+'/SheetahResources/img/divider.png');

         // Animation Tools

         //setup composition for animating
         addScriptButton(panel, "Setup for Animation", onSetup, [0,0,maxWidth,30]);

         //get keyframes as test
         addScriptButton(panel, "Export Animations", onKeyframesToText, [0,0,maxWidth,30]);
         
         // --- end of spritesheet tools
         panel.add("image", [0,0,maxWidth,3],((new File($.fileName)).path)+'/SheetahResources/img/divider.png');

         //checkbox to save Keyframes as Text file
         var endGroup = panel.add("group", [0,0,maxWidth,24]);
         scope.checkbox = endGroup.add("checkbox", [0,0,(maxWidth/2)-43,24], ".json");
         var checkboxChecked;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "SaveAsCheckboxChecked")){
            checkboxChecked = app.settings.getSetting("GentlymadSpritesheetTools", "SaveAsCheckboxChecked");
         }
         checkboxChecked = (checkboxChecked !== undefined) ? checkboxChecked : false;
         checkboxChecked = (checkboxChecked === "true") ? true : false;
         scope.checkbox.value = checkboxChecked;

         scope.checkbox.onClick = function(){
            app.settings.saveSetting("GentlymadSpritesheetTools", "SaveAsCheckboxChecked", scope.checkbox.value.toString());
         }

         scope.adv = endGroup.add("checkbox", [0,0,(maxWidth/2)-32,24], "distrib");
         var advChecked;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "AdvCheckboxChecked")){
            advChecked = app.settings.getSetting("GentlymadSpritesheetTools", "AdvCheckboxChecked");
         }
         advChecked = (advChecked !== undefined) ? advChecked : false;
         advChecked = (advChecked === "true") ? true : false;
         scope.adv.value = advChecked;
        
         if(scope.adv.value){
            upperGroup.show();
            logoContainer.hide();
         }else{
            upperGroup.hide();
            logoContainer.show();
         }

         scope.adv.onClick = function(){
         	if(scope.adv.value){
	            upperGroup.show();
	            logoContainer.hide();
	        }else{
	            upperGroup.hide();
	            logoContainer.show();
	        }

            app.settings.saveSetting("GentlymadSpritesheetTools", "AdvCheckboxChecked", scope.adv.value.toString());
         }

         scope.powertwo = endGroup.add("checkbox", [0,0,(maxWidth/2)-35,24], "pow 2");
         var powertwoChecked;
         if(app.settings.haveSetting("GentlymadSpritesheetTools", "PowerTwoCheckboxChecked")){
            powertwoChecked = app.settings.getSetting("GentlymadSpritesheetTools", "PowerTwoCheckboxChecked");
         }
         powertwoChecked = (powertwoChecked !== undefined) ? powertwoChecked : false;
         powertwoChecked = (powertwoChecked === "true") ? true : false;
         scope.powertwo.value = powertwoChecked;

         scope.powertwo.onClick = function(){
            app.settings.saveSetting("GentlymadSpritesheetTools", "PowerTwoCheckboxChecked", scope.powertwo.value.toString());
         }

         // --- end of animation tools

         panel.layout.layout(true);
         return panel;
      }
   
      var SpritesheetToolsUI = buildUI(thisObj);

      if (SpritesheetToolsUI != null && SpritesheetToolsUI instanceof Window){
         SpritesheetToolsUI.center();
         SpritesheetToolsUI.show();
      }

   }

   SpritesheetTools(this);
}