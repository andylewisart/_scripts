// Ease and Wizz 2.0.4 : Curvaceous : inOutQuad : all keyframes
// Ian Haigh (http://ianhaigh.com/easeandwizz/)
// Last built: 2013-07-05T11:46:51+10:00


function easeandwizz_inOutQuad(t, b, c, d) {
	if ((t/=d/2) < 1) return c/2*t*t + b;
	return -c/2 * ((--t)*(t-2) - 1) + b;
}


function easeAndWizz() {
	try {
		var key1 = key(1);
		var key2 = key(numKeys);
	} catch(e) {
		return null;
	}
	
	t = time - key1.time;
	d = key2.time - key1.time;

	sX = key1.time;
	eX = key2.time - key1.time;


	if ((time < key1.time) || (time > key2.time)) {
		return null;
	} else {
		return valueAtTime(easeandwizz_inOutQuad(t, sX, eX, d));
	}
}

(easeAndWizz() || value);

