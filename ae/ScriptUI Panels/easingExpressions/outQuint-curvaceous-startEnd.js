// Ease and Wizz 2.0.4 : Curvaceous : outQuint : all keyframes
// Ian Haigh (http://ianhaigh.com/easeandwizz/)
// Last built: 2013-07-05T11:46:51+10:00


function easeandwizz_outQuint(t, b, c, d) {
	return c*((t=t/d-1)*t*t*t*t + 1) + b;
}


function easeAndWizz() {
	try {
		var key1 = key(1);
		var key2 = key(numKeys);
	} catch(e) {
		return null;
	}
	
	t = time - key1.time;
	d = key2.time - key1.time;

	sX = key1.time;
	eX = key2.time - key1.time;


	if ((time < key1.time) || (time > key2.time)) {
		return null;
	} else {
		return valueAtTime(easeandwizz_outQuint(t, sX, eX, d));
	}
}

(easeAndWizz() || value);

