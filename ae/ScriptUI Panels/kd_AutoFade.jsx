﻿	// kd_AutoFade v1.2
	//
	// Copyright (c) 2010 @takumi_kashima. All rights reserved.	
	// This script automatically adds fadein and fadeout expressions to selected layers.
	//
	// 20010/05/04 Added 'In and Out', 'In only', 'Out only' parameters.

function createPalette(thisObj) {
	palette= (thisObj instanceof Panel) ? thisObj : new Window("palette", "AutoFade", undefined, {resizeable: true});
	//palette = new Window("palette", "AutoFade", undefined, {resizeable: true});
	palette.alignChildren = 'left';
	with(palette){
		palette.H = add('group');
		palette.H.orientation = 'row';
		with(palette.H){
			palette.H.sText = add('statictext', undefined, 'time(s):');
			palette.H.ftime = add('edittext', undefined, '0.5');
			palette.H.ftime.preferredSize = [40,20];
			}
		palette.I = add('group');
		palette.I.orientation = 'row';
		with(palette.I){
			palette.I.inout = add('dropdownlist',[10,10,160,30],['In and Out', 'In only', 'Out only']);
			palette.I.inout.selection = 0;
			}
		palette.T = add('group');
		palette.T.orientation = 'row';
		with(palette.T){
			palette.T.on = add('checkbox', undefined, 'Opacity');
			palette.T.on.value=true;
			palette.T.from = add('edittext', undefined, '0');
			palette.T.from.preferredSize = [40,20];
			palette.T.sText = add('statictext', undefined, '-->');
			palette.T.to = add('edittext', undefined, '100');
			palette.T.to.preferredSize = [40,20];
			}
		palette.S = add('group');
		with(palette.S){
			palette.S.on = add('checkbox', undefined, 'Scale');
			palette.S.from = add('edittext', undefined, '0');
			palette.S.from.preferredSize = [40,20];
			palette.S.sText = add('statictext', undefined, '-->');
			palette.S.to = add('edittext', undefined, '100');
			palette.S.to.preferredSize = [40,20];
			}
		palette.R = add('group');
		palette.R.orientation = 'row';
		with(palette.R){
			palette.R.on = add('checkbox', undefined, 'Rotation');
			palette.R.from = add('edittext', undefined, '30');
			palette.R.from.preferredSize = [40,20];
			palette.R.sText = add('statictext', undefined, '-->');
			palette.R.to = add('edittext', undefined, '0');
			palette.R.to.preferredSize = [40,20];
			}
/*		palette.P = add('group');
		palette.P.orientation = 'row';
		with(palette.P){
			palette.P.on = add('checkbox', undefined, 'Position');
			palette.P.sText = add('statictext', undefined, 'x:');
			palette.P.x = add('edittext', undefined, '100');
			palette.P.x.preferredSize = [40,20];
			palette.P.sText1 = add('statictext', undefined, 'y:');
			palette.P.y = add('edittext', undefined, '100');
			palette.P.y.preferredSize = [40,20];
			}
*/
		palette.F = add('group');
		palette.F.orientation = 'row';
		palette.F.alignChildren = 'fill';
		with(palette.F){
			palette.F.easing = add('checkbox', undefined, 'Easing');
			palette.F.Btn = add('button', undefined, 'Apply');
			}
		palette.F.easing.value = true;
		palette.F.Btn.onClick = applyExpression;
	
		};
	return palette;
}

var palette = createPalette(this);
if (palette instanceof Window){
	palette.show();
}else{
	palette.layout.layout(true);
}



function applyExpression() {
	var ExpText = '';
	var curComp = app.project.activeItem;
	
	for (var layerId = 0; layerId < curComp.selectedLayers.length; layerId++){
		var layer = curComp.selectedLayers[layerId];
		// #Opacity
		if(palette.T.on.value){
			ExpText = makeExpression_t(palette.F.easing.value, palette.H.ftime.text, palette.T.from.text, palette.T.to.text, palette.I.inout.selection.text);
			layer('opacity').expression=ExpText;
		}
		// #Scale
		if(palette.S.on.value){
			ExpText = makeExpression_s(palette.F.easing.value, palette.H.ftime.text, palette.S.from.text, palette.S.to.text, palette.I.inout.selection.text);
			layer('scale').expression=ExpText;
		}
		// #Rotation
		if(palette.R.on.value){
			ExpText = makeExpression_r(palette.F.easing.value, palette.H.ftime.text, palette.R.from.text, palette.R.to.text, palette.I.inout.selection.text);
			layer('rotation').expression=ExpText;
		}
    }
	
}

function makeExpression_t(easing, ftime, from, to, ioc){
	exp='';
	func1='';
	func2='';
	if(easing){
		func1 = 'easeIn';
		func2 = 'easeOut';
	}else{
		func1 = 'linear';
		func2 = 'linear';	
	}
	
	if(ioc == 'In and Out'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	'+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else if(time >= thisLayer.outPoint-fTime){\r' +
				'	'+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+','+from+');\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}else if(ioc == 'In only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	'+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}else if(ioc == 'Out only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time >= thisLayer.outPoint-fTime){\r' +
				'	'+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+','+from+');\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}

	return exp;
}

function makeExpression_s(easing, ftime, from, to, ioc){
	exp='';
	func1='';
	func2='';
	if(easing){
		func1 = 'easeIn';
		func2 = 'easeOut';
	}else{
		func1 = 'linear';
		func2 = 'linear';	
	}
	
	if(ioc == 'In and Out'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	s='+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else if(time >= thisLayer.outPoint-fTime){\r' +
				'	s='+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+','+from+');\r' +
				'}else{\r' +
				'	s='+to+';\r'+
				'}'+
				'[s, s]';
	}else if(ioc == 'In only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	s='+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else{\r' +
				'	s='+to+';\r'+
				'}'+
				'[s, s]';
	}else if(ioc == 'Out only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time >= thisLayer.outPoint-fTime){\r' +
				'	s='+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+','+from+');\r' +
				'}else{\r' +
				'	s='+to+';\r'+
				'}'+
				'[s, s]';
	}

	return exp;
}

function makeExpression_r(easing, ftime, from, to, ioc){
	exp='';
	func1='';
	func2='';
	if(easing){
		func1 = 'easeIn';
		func2 = 'easeOut';
	}else{
		func1 = 'linear';
		func2 = 'linear';	
	}
	
	if(ioc == 'In and Out'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	'+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else if(time >= thisLayer.outPoint-fTime){\r' +
				'	'+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+',-('+from+'));\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}else if(ioc == 'In only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time <= thisLayer.inPoint+fTime){\r'+
				'	'+func1+'(time,thisLayer.inPoint,thisLayer.inPoint+fTime,'+from+','+to+');\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}else if(ioc == 'Out only'){
		exp = 'fTime = '+ftime+';\r'+
				'if(time >= thisLayer.outPoint-fTime){\r' +
				'	'+func2+'(time,thisLayer.outPoint-fTime,thisLayer.outPoint,'+to+',-('+from+'));\r' +
				'}else{\r' +
				'	'+to+';\r'+
				'}';
	}

	return exp;
}






